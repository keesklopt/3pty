package ICW;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws IOException
	{  
			//set the path that contains the folder with all instances
			String path = "C:/Users/FArnold/Desktop/VRP instances/Angerat";
			final File folder = new File(path);
			ArrayList<String> files = listFilesForFolder(folder);
			int maxRepetitions = 100;
			
			for (int instance=0; instance<files.size(); instance++)
			{
				//load the VRP instance
				String pathInstance = path + "/" + files.get(instance);
				VRPModel model = InstanceLoader.loadInstance(pathInstance);

				double bestSolution = 99999;
				double sum = 0;
				double countValid = 0;

				for (int repetition=0; repetition<maxRepetitions; repetition++)
				{
					VRPModel modelRep = model.copy();
					double result = modelRep.ICW(10000);
					if (result < 99999)
					{
						sum += result;
						countValid++;
					}
					if (result < bestSolution) 
						bestSolution = result;	
				}//repetition
				
				//System.out.println(bestSolution + "; " + sum / countValid + "; " + countValid);
				System.out.println("Best Solution: " + instance + ". Best Solution: " + bestSolution + "; " + "Average: " + sum / countValid + "; " + countValid);
			}//instance

	}//main
	
	public static ArrayList<String> listFilesForFolder(final File folder) 
	{
		ArrayList<String> files = new ArrayList<String>();
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            files.addAll(listFilesForFolder(fileEntry));
	        } else {
	        	files.add(fileEntry.getName());
	        }
	    }
	    return files;
	}
}
