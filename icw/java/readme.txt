Faculty of Applied Economics - University of Antwerp.
Operations Research Group ANT/OR.
Author: Florian Arnold.
Email: florian.arnold@uantwerpen.be

Supplementary material of the paper "A critical analysis of the improved Clarke
and Wright savings algorithm" by Arnold, Sörensen and Palhazi Cuervo.

Implementation of the improved Clarke and Wright savings algorithm described in 
ScienceAsia, 38(3):307–318, 2012, by Pichpibul and Kawtummachai.

Experiments: in order to perform the experiments described in the paper (run the
algorithm 100 times for each instance), execute the main file "Main.java". Make 
sure to specify the folder where the instances are located in the source code.
