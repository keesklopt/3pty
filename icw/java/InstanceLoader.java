package ICW;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class InstanceLoader {

	//******************************************************************************************
	//Creates a VRP model from an input file with the format of the "Augerat" instances
	//******************************************************************************************
	public static VRPModel loadInstance(String file) throws IOException
	{		
		@SuppressWarnings("resource")
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		String s, keyword;
		
		keyword = "DIMENSION : ";
		s=in.readLine();
		s = s.trim();
		
		//read in #vehicles
		int vehicles = Integer.parseInt(s.substring(s.length()-1));
		if (s.charAt(s.length()-2)=='1')
			vehicles = Integer.parseInt(s.substring(s.length()-2));
		while (s!=null && s.indexOf(keyword) == -1)
			s=in.readLine();
		s = s.trim();
		
		//read in #customers
		int vertices = Integer.parseInt(s.substring(12));
		int customers	= vertices - 1;
		
		//create VRP model
		VRPModel model = new VRPModel(customers);
		model.vehicles = vehicles;
		
		//read in capacaity Limit
		keyword = "CAPACITY : ";
		s=in.readLine();
		while (s!=null && s.indexOf(keyword) == -1)
			s=in.readLine();
		s = s.trim();
		model.capacityLimitVehicle = Integer.parseInt(s.substring(11));
		
		//read the location for each node
		int index = 0;
		s=in.readLine();
		keyword = "DEMAND_SECTION";
		while (!(s=in.readLine()).contains(keyword))
		{
			if (s.startsWith(" "))
				s = s.substring(1, s.length());
			String[] info = s.split(" ");
			model.nodes[index] = model.new Node(index);
			int pos = 0;
			while ( info[pos].equals("") || info[pos].equals(" ") )
				pos++;
			pos +=1;
			while ( info[pos].equals("") || info[pos].equals(" ") )
				pos++;
			model.nodes[index].x		= Double.parseDouble(info[pos]);//Integer.parseInt(info[1]);
			pos +=1;
			while ( info[pos].equals("") || info[pos].equals(" ") )
				pos++;
			model.nodes[index].y		= Double.parseDouble(info[pos]);//Integer.parseInt(info[2]);
			index++;
		}
		
		//read the demands
		index = 0;
		keyword = "DEPOT_SECTION";
		while (!(s=in.readLine()).contains(keyword))
		{
			String[] info = s.split(" ");//\t
			int pos = 0;
			while ( info[pos].equals("") || info[pos].equals(" "))
				pos++;
			pos +=1;
			while ( info[pos].equals("") || info[pos].equals(" ") )
				pos++;
			model.nodes[index].demand = Integer.parseInt(info[pos]);
			index++;
		}
		
		//put the depot from the first to the last position
		VRPModel.Node temp = model.nodes[0];
		for (int i=1; i<customers+1; i++)
		{
			model.nodes[i-1]= model.nodes[i];
			model.nodes[i-1].index--;
		}
		model.nodes[model.customers] = temp;
		model.nodes[model.customers].index= model.customers;
		
		//compute the distances between each pair of nodes
		for(int i=0; i<model.nodes.length; i++)
		{
			model.nodes[i].distances = new double[model.nodes.length];
			for(int j=0; j<model.nodes.length; j++)
				model.nodes[i].distances[j] = (int) (0.5 + Math.sqrt( Math.pow( model.nodes[i].x - model.nodes[j].x , 2) + Math.pow( model.nodes[i].y - model.nodes[j].y , 2) ) );
		}
		
		return model;	
	}
	
}
