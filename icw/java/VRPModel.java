package ICW;

import java.util.ArrayList;
import java.util.Collections;


public class VRPModel {

	//model parameters
	public int customers;	
	public int vertices;
	public int vehicles;
	public int capacityLimitVehicle;
	
	//variables
	public Node[] nodes;
	public ArrayList<Route> routes;
	
	//---------------------------------------------
	//Constructor
	//---------------------------------------------
	public VRPModel(int customers)
	{
		this.customers = customers;
		this.vertices  = customers + 1;
		nodes = new Node[vertices];
		routes = new ArrayList<Route>();
	}
	
	//---------------------------------------------
	//A function to copy a model
	//---------------------------------------------
	public VRPModel copy()
	{
		VRPModel model = new VRPModel(customers);
		model.vehicles = vehicles;
		model.capacityLimitVehicle = capacityLimitVehicle;
		for (int i=0; i<nodes.length; i++)
			if (nodes[i] != null)
				model.nodes[i]	=  nodes[i].copy();
		return model;
	}
	
	//---------------------------------------------
	//A Class to define a Node
	//---------------------------------------------
	public class Node
	{
		//variables
		public int index;
		public double x;
		public double y;
		public int demand;
		public int routeIndex;
		public double[] distances;
		
		//constructor
		public Node(int i)
		{
				index = i;
		}
		
		public Node copy()
		{
			Node n	= new Node(index);
			n.x				= x;
			n.y				= y;
			n.demand        = demand;
			n.routeIndex    = routeIndex;
			n.distances		= distances.clone();
			return n;
		}
		
	}//class Node

	//---------------------------------------------
	//A Class to define a Route
	//---------------------------------------------
	public class Route 
	{
		public ArrayList<Node> nodes;
		public int length; //number of customers on the route
		public int load;
		public double cost;
		
		public Route()
		{
			 nodes = new ArrayList<Node>();
			 this.cost	= 0;
			 this.load	= 0;
			 this.length	= 0;
		}
		
		public Route copy()
		{
			Route r 	= new Route();
			r.cost 		= this.cost;
			r.load 		= this.load;
			r.length 	= this.length;
			r.nodes 	= new ArrayList<Node>();
			for (int i=0; i<this.nodes.size(); i++)
				r.nodes.add(this.nodes.get(i));
			return r;
		}
	}//class Route
	
	
	//---------------------------------------------
	//A Class to define a Saving
	//---------------------------------------------
	public class Saving implements Comparable<Saving>
	{
		public double saving;
		public int from;
		public int to;
			
		public Saving(double v, int f, int t)
		{
			saving = v;
			from = f;
			to = t;
		}

		public Saving copy()
		{
			Saving s = new Saving(saving,from,to);
			return s;
		}
		
		public int compareTo(Saving o) 
		{
			if(o.saving<this.saving)
				return -1;
			else if(o.saving == this.saving)
				return 0;
			else
				return 1;
		}
	}
	
	private ArrayList<Saving> copySavingList(ArrayList<Saving> original)
	{
		ArrayList<Saving> copy = new ArrayList<Saving>();
		for (int i=0;i<original.size();i++)
			copy.add(original.get(i).copy() );
		return copy;
	}
	
	//---------------------------------------------
	//The improved Clark and Wright algorithm
	//---------------------------------------------
	public double ICW( int maxIterations)
	{
		
		//compute and order the savings for all pairs of customers
		ArrayList<Saving> bestSavingList = new ArrayList<Saving>();
		ArrayList<Integer> assignedCustomers = new ArrayList<Integer>();
		double			  saving;
		
		for (int i=0; i<customers; i++)
		{
			assignedCustomers.add(i);
			for (int j=i+1; j<customers; j++)
			{
				saving	 = nodes[i].distances[customers] + nodes[j].distances[customers] - nodes[i].distances[j];
				if (saving<0.01)
				saving = 0.01;
				Saving s = new Saving( saving, i, j );
				bestSavingList.add(s);
			}
		}
		Collections.sort( bestSavingList );

		//initialization
		ArrayList<Saving> previousSavingList = null;
		int countNotImproved = 0;
		
		//---------------------------------------------
		//compute the solution with the original Clark and Wright algorithm
		//---------------------------------------------
		double bestCosts = clarkWrightParallel( copySavingList( bestSavingList ));
		double previousCosts       = bestCosts;
		
		if (routes.size() > vehicles )
			{previousCosts = 999999; bestCosts = 999999;}

		//---------------------------------------------
		//In each iteration, shake the savings list and either accept or ignore it
		//---------------------------------------------
		for (int it=0; it<maxIterations; it++ )
		{
			previousSavingList = copySavingList( bestSavingList );
				
			//---------------------------------------------
			//shake the bestSavingList with the randomized tournament selection
			//---------------------------------------------
			ArrayList<Saving> shakenSavingsList = new ArrayList<Saving>();
			while (!previousSavingList.isEmpty())
			{
				//select a random tournament size
				int T 	= (int)(Math.random()*7.0) + 3;
				if (T > previousSavingList.size())
					T = previousSavingList.size();
				//select a random winner from the tournament
				int winner = -1;
				int sum = 0;
				for (int i=0; i<T; i++)
					sum += (int) previousSavingList.get(i).saving;
				double	ran = Math.random() * (double)sum;
				int cdr = 0;
				for(int i=0; i<T; i++)
				{
					cdr += previousSavingList.get(i).saving;
					if ((double)cdr >= ran) 	
					{
						winner = i;
						break;
					}
				}
				if (winner == -1)
					System.out.println("Error: no winner selected");
				shakenSavingsList.add( previousSavingList.get(winner).copy() );
				previousSavingList.remove(winner);
			}
			
			double newCosts = clarkWrightParallel( copySavingList( shakenSavingsList ));
					
			//Do we accept the new savings list?
			if ( newCosts  < previousCosts)
			{
				//is the solution valid?
				if ( routes.size() <= this.vehicles )
				{
					previousCosts = newCosts;
				}
				else
					previousCosts = 999999;
				bestSavingList   =  copySavingList( shakenSavingsList );
				countNotImproved = 0;
			}
			else
				countNotImproved++;
			
			//abort the search after 1000 iterations without improvements
			if (countNotImproved>1000)
				break;
			
			if (newCosts < bestCosts && this.vehicles >= routes.size())
				bestCosts = newCosts;
		}//for iteration
		
		return bestCosts;
	}//ICW
	
	
	//---------------------------------------------
	//The parallel Clark and Wright algorithm
	//---------------------------------------------
	private double clarkWrightParallel( ArrayList<Saving> savings )
	{
		int depot 	= customers;
		routes.clear();
		
		//---------------------------------------------
		//add edges between pairs of nodes with the highest savings
		//---------------------------------------------		
		int countVisits		= 0;
		boolean[] visited	= new boolean[customers];
		ArrayList<Integer> extensionPoints = new ArrayList<Integer>();
		ArrayList<Integer> interiorPoints  = new ArrayList<Integer>();
		int countRoutes = 0;
			
		while ( savings.size() > 0)
		{	
				//---------------------------------------------
				// get the edge with the next highest saving
				//---------------------------------------------
				int n1 = savings.get(0).from;
				int n2 = savings.get(0).to;
				
				//---------------------------------------------
				// case 1: one of the nodes is an interior Point
				//---------------------------------------------
				if ( interiorPoints.contains(n1) || interiorPoints.contains(n2) )
				{
					//do nothing
				}
				//---------------------------------------------
				// case 2: both nodes are neither extension nor interior points
				//---------------------------------------------
				else if ( !extensionPoints.contains(n1) && !extensionPoints.contains(n2) )
				{
					if ( nodes[ n1 ].demand + nodes[ n2 ].demand <= capacityLimitVehicle )
					{
						//start a new route with these two nodes
						Route r = new Route();
						r.nodes.add(nodes[depot]);
						r.nodes.add(nodes[ n1 ]);
						r.nodes.add(nodes[ n2 ]);
						r.nodes.add(nodes[depot]);
						r.length 			= 2;
						r.load				= nodes[ n1 ].demand + nodes[ n2 ].demand;
						r.cost				= nodes[ n1 ].distances[ n2 ] + nodes[ n1 ].distances[ depot ] + nodes[ n2 ].distances[ depot ];
						countVisits = countVisits + 2;
						extensionPoints.add( n1 );
						extensionPoints.add( n2 );
						visited[n1]=true;
						visited[n2]=true;
						nodes[ n1 ].routeIndex = countRoutes;
						nodes[ n2 ].routeIndex = countRoutes;
						
						routes.add( r.copy() );
						countRoutes++;
					}
				}

				//---------------------------------------------
				// case 3a: the first node is an extension point 
				//---------------------------------------------
				else if ( extensionPoints.contains(n1) && !extensionPoints.contains(n2))
				{
					
					Route r = routes.get( nodes[n1].routeIndex );
					double addCost = nodes[ n1 ].distances[ n2 ] + nodes[ n2 ].distances[ depot ] - nodes[ n1 ].distances[ depot ];
					if ( r.load + nodes[ n2 ].demand <= capacityLimitVehicle )
					{
						//extend the route
						if ( r.nodes.get(1).index == n1 )
							r.nodes.add(1, nodes[ n2 ]);
						else
							r.nodes.add(r.length+1, nodes[ n2 ]);
						r.length++;
						r.load	+= nodes[ n2 ].demand;
						r.cost += addCost;
						nodes[ n2 ].routeIndex = nodes[n1].routeIndex;
						countVisits++;
						extensionPoints.remove(Integer.valueOf(n1));
						extensionPoints.add( n2 );
						interiorPoints.add( n1 );
						visited[n2]=true;

					}
				}
				//---------------------------------------------
				// case 3b: the second node is an extension point 
				//---------------------------------------------
				else if ( !extensionPoints.contains(n1) && extensionPoints.contains(n2) )
				{
					Route r = routes.get( nodes[n2].routeIndex );
					double addCost = nodes[ n2 ].distances[ n1 ] + nodes[ n1 ].distances[ depot ] - nodes[ n2 ].distances[ depot ];
					if ( r.load + nodes[ n1 ].demand <= capacityLimitVehicle )
					{
						//extend the route
						if ( r.nodes.get(1).index == n2 )
							r.nodes.add(1, nodes[ n1 ]);
						else
							r.nodes.add(r.length+1, nodes[ n1 ]);
						r.length++;
						r.load	+= nodes[ n1 ].demand;
						r.cost += addCost;
						nodes[ n1 ].routeIndex = nodes[n2].routeIndex;
						countVisits++;
						extensionPoints.remove(Integer.valueOf(n2));
						extensionPoints.add( n1 );
						interiorPoints.add( n2 );
						visited[n1]=true;
					}
				}
				//---------------------------------------------
				// case 4: both nodes are extension points; merge the two routes
				//---------------------------------------------
				else if ( extensionPoints.contains(n1) && extensionPoints.contains(n2) )
				{
					if (nodes[n1].routeIndex != nodes[n2].routeIndex )
					{
						Route r1 = routes.get( nodes[n1].routeIndex );
						Route r2 = routes.get( nodes[n2].routeIndex );
						double addCost = r1.cost + r2.cost + nodes[ n2 ].distances[ n1 ] - nodes[ n1 ].distances[ depot ] - nodes[ n2 ].distances[ depot ];
						if ( r1.load + r2.load <= capacityLimitVehicle )
						{
							//merge the routes
							//bring into format depot -...- n1 - n2 -...- depot
							if ( r1.nodes.get(1).index == n1 )
								Collections.reverse(r1.nodes);
							if ( r2.nodes.get(1).index != n2 )
								Collections.reverse(r2.nodes);
							//remove two depots
							r1.nodes.remove( r1.nodes.size()-1 );
							r2.nodes.remove( 0 );
							//attach route 2 to route 1
							r1.nodes.addAll(r1.nodes.size(), r2.nodes);
							r1.length 	+= r2.length;
							r1.load		+= r2.load;
							Route temp	= r1.copy();
							
							//update the routes
							//adapt the routeIndices of all other routes
							int index1 = nodes[n1].routeIndex;
							int index2 = nodes[n2].routeIndex;
							for (int i=index1+1; i<routes.size(); i++)
								for (int j=1; j<routes.get(i).length+1; j++)
									routes.get(i).nodes.get(j).routeIndex--;
							for (int i=index2+1; i<routes.size(); i++)
								for (int j=1; j<routes.get(i).length+1; j++)
									routes.get(i).nodes.get(j).routeIndex--;
							//remove the two previous routes (in the right order)
							if ( index1 < index2 )
							{
								routes.remove(index2);
								routes.remove(index1);
							}
							else
							{
								routes.remove(index1);
								routes.remove(index2);
							}	
							//insert the merged route
							for (int i=1; i<temp.length+1; i++)
								temp.nodes.get(i).routeIndex = countRoutes-2;
							temp.cost = addCost;
							routes.add(temp.copy());
							countRoutes--;
	
							extensionPoints.remove(Integer.valueOf(n1));
							extensionPoints.remove(Integer.valueOf(n2));
							interiorPoints.add( n1 );
							interiorPoints.add( n2 );
						}
					}
				}
				savings.remove(0);
		}
		
		//catch the case, that there is only one customer left that does not fit in any route
		if ( countVisits < customers)
		{
			for (int i=0; i<customers; i++)
				if (!visited[i])
				{
					//create an own route for this customer
					Route r = new Route();
					r.nodes.add(nodes[depot]);
					r.nodes.add(nodes[ i ]);
					r.nodes.add(nodes[depot]);
					r.length 			= 1;
					r.load				= nodes[ i ].demand;
					nodes[ i ].routeIndex = countRoutes;
					countVisits++;
					routes.add(r.copy());
				}
		}
		
		//compute the costs of the solution
		double costs = 0;
		for (int r=0; r<routes.size(); r++)
		{
			for (int i=1; i<routes.get(r).length+2; i++)
				 costs += routes.get(r).nodes.get(i-1).distances[routes.get(r).nodes.get(i).index];
		}
		
		return costs;
		
	}//Clark and Wright
	
}//VRPModel

