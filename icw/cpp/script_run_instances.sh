#!/bin/bash
# Faculty of Applied Economics - University of Antwerp.
# Operations Research Group ANT/OR.
# 
# File name: script_run_instances.sh
# Description: Run the improved Clark and Wright algorithm to solve a set of instances.
#              The algorithm is executed [NUM_IT] times to solve each instance
#              specified in the file [INST_FILE]. The results of the executions are
#              stored in the folder [RESUL_FOLD].
#
# Author: Daniel Palhazi Cuervo.


################## Important variables ##################

# Command to be executed 
COMMAND="bin/ICW"

# Folder where the instances are located
INST_FOLD="instances"

# File that contains the list of instances to be executed
INST_FILE="list_instances.txt"

# Number of times the algorithm is going to be executed
NUM_IT=1

# Cost of the best solution found (out of the all the executions)
BEST_COST=10000000000.0 

# Sum of the costs of the solutions found
SUM_COST=0.0

# Cost of the solution just generated
COST=0.0

# Number of succesful executions found
NUM_SUS_IT=0

# Folder where the results will be saved
RESUL_FOLD="results"

# Regular expression to check if a variable contains a number
RE='^[0-9]+([.][0-9]+)?$'

################## Execution of the algorithm ##################

# Check if the temporary folder exists
if [ -d ${RESUL_FOLD} ]; then 
	rm -r ${RESUL_FOLD}
fi

# Create the temporary folder
mkdir ${RESUL_FOLD}

seq $NUM_IT | parallel -j 1 "${COMMAND} ${INST_FOLD}/{2} > ${RESUL_FOLD}/{2}_{1}.txt" :::: - $INST_FOLD/$INST_FILE


################## Collection of the results ##################

# For each instance in the file with the list of instances
cat $INST_FOLD/$INST_FILE | while read INSTANCE
do
	BEST_COST=10000000000.0
	SUM_COST=0.0
	COST=0.0
	NUM_SUS_IT=0
	
	# Collect the information from the files generated
	for it in `eval echo {1..$NUM_IT}`; do

		# Extract the cost of the solution generated
		COST=`cat ${RESUL_FOLD}/${INSTANCE}_${it}.txt | grep 'Total traveled distance:' | tail -1 | awk '{printf $5}'`

		# If a number was read from the file
		if [[ $COST =~ $RE ]] ; then
  			((NUM_SUS_IT++))

			# If the new solution is better, replace the best solution found so far
			if (( $(echo "$COST < $BEST_COST" |bc -l) )); then
				BEST_COST=$COST
			fi

			SUM_COST=`echo "$SUM_COST + $COST" | bc -l`	
		fi
	done
	
	# Display the information
	if [ "$NUM_SUS_IT" -eq 0 ]; then 
		printf "%-15s Num suc:%3d /%d    Best:        -       Avg:        -      \n" $INSTANCE $NUM_SUS_IT $NUM_IT
	else
		SUM_COST=`echo "$SUM_COST / $NUM_SUS_IT" | bc -l`
		printf "%-15s Num suc:%3d /%d    Best: %10.2f     Avg: %10.2f\n" $INSTANCE $NUM_SUS_IT $NUM_IT $BEST_COST $SUM_COST
	fi	
done
