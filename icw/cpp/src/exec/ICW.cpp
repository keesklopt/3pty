/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: ICW.cpp
 * Description: Main file to execute the Improved Clark and Wright.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#include <sys/time.h>
#include <cstdlib>

#include "Utils.hpp"
#include "VRP_instance.hpp"
#include "VRP_solution.hpp"
#include "Clark_wright_imp.hpp"


int main (int argc, char *argv[]) {
	/*Random seed initialization - DO NOT DELETE.*/
	Utils::ini_seed();

	VRP_instance* instance; /* Instance to be solved*/
	VRP_solution* solution; /* Solution to the instance*/

	/* Execution of the improved version of the Clark and Wright algorithm*/
	Clark_wright_imp* clark_wright_imp;

	/* Read the instance*/
	instance = VRP_instance::read_instance_file(argv[1]);

	/* Print the information of the instance*/
	instance->print_info();

	/* Execute the improved version of the Clark and Wright algorithm*/
	clark_wright_imp = new Clark_wright_imp(instance);

	solution = clark_wright_imp->get_solution();

	solution->printf_info();

	delete clark_wright_imp;
	delete solution;
	delete instance;
}

