/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: Entry_list.cpp
 * Description: Class that contains the information of an entry in the list for executing
 * the Clark and Wright heuristic.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#include <Entry_list.hpp>

Entry_list::Entry_list(int l_c, int r_c, double d_t) {
	cus_1 = l_c;
	cus_2 = r_c;
	diff_trav_dist = d_t;
}

Entry_list::~Entry_list() {
}

int Entry_list::get_first_cus() {
	return cus_1;
}

int Entry_list::get_second_cus() {
	return cus_2;
}

double Entry_list::get_diff_trav_dist() {
	return diff_trav_dist;
}

void Entry_list::print_info() {
	printf("First: %d, Second: %d, Differential distance: %.2lf\n", cus_1, cus_2, diff_trav_dist);
}

bool Entry_list::compare_pointers(const Entry_list* i, const Entry_list* j) {
	/* If the entries have the same differential travel distance*/
	if (Utils::are_equal(i->diff_trav_dist, j->diff_trav_dist)) {
		/* If the same customer involved is the same*/
		if (i->cus_1 == j->cus_1) {
			return (i->cus_2 < j->cus_2);
		} else {
			return (i->cus_1 < j->cus_1);
		}
	} else {
		return (i->diff_trav_dist < j->diff_trav_dist);
	}
}

