/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: Clark_wright_imp.cpp
 * Description: Implementation of the improved version of the Clark and Wright algorithm.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#include "Clark_wright_imp.hpp"

Clark_wright_imp::Clark_wright_imp(VRP_instance* i) {
	instance_info = i;
	solution = new VRP_solution(instance_info);

	/* Execute the regular Clark and Wright algorithm*/
	execute_clark_wright_imp();
	//execute_clark_wright();
}

Clark_wright_imp::~Clark_wright_imp() {
	delete solution;
}

/**************** Lists of possible concatenations ****************/

Entry_list* Clark_wright_imp::pop_at(std::list<Entry_list*>* list,  int index) {
	/* Check that index of the element is valid*/
	assert(index >= 0 && (unsigned int)index < list->size());

	Entry_list* element; /* Element to be popped*/
	std::list<Entry_list*>::iterator it; /* Iterator to traverse the list*/

	it = list->begin();
	for (int i = 0; i < index; i++) {
		it++;
	}

	element = (*it);
	list->erase(it);
	return element;
}

int Clark_wright_imp::roulette_wheel(std::list<Entry_list*>* list,  int num_comp) {
	/* Check that the number of competitors is a positive value*/
	assert(num_comp > 0);
	/* Check that there are enough elements in the list*/
	assert((unsigned int)num_comp <= list->size());

	std::list<Entry_list*>::iterator it; /* Auxiliary iterator to traverse the list*/
	double sum_diff_trav; /* Sum (of the absolute vale) of the competitors' differentials of the traveled distance*/
	double separator_comp[num_comp]; /*Roulette wheel separator for each competitor*/
	double random_number; /* Random number for the roulette wheel*/
	int index = -1; /* Index of the selected competitor*/

	/* Calculate the sum of the competitors' differentials of the traveled distance*/
	sum_diff_trav = 0.0;
	it = list->begin();
	//printf("Differentials: ");
	for (int i = 0; i < num_comp; i++) {
		sum_diff_trav += (double)abs((*it)->get_diff_trav_dist());
		it++;
	}
	//printf("\n");

	/* If all the competitors have a differential equal to 0*/
	if (sum_diff_trav == 0) {
		index = (int)(rand() % (num_comp));
		return index;
	}

	/* Calculate the separator for each competitor*/
	it = list->begin();
	separator_comp[0] = abs((*it)->get_diff_trav_dist()) / sum_diff_trav;
	it++;
	for (int i = 1; i < num_comp; i++) {
		separator_comp[i] = separator_comp[i - 1] + (abs((*it)->get_diff_trav_dist()) / sum_diff_trav);
		it++;
	}

	//printf("\n");

	/* Spin the wheel (Random number) - avoid getting exactly one */
	do {
		random_number = ((double) (rand())  / (RAND_MAX));
	} while (Utils::are_equal(random_number, 1.0));


	/* Look for the selected competitor*/
	if (random_number <= separator_comp[0]) {
		index = 0;
	} else {
		for (int i = 1; i < num_comp; i++) {
			if (random_number > separator_comp[i -1] && random_number <= separator_comp[i]) {
				index = i;
				break;
			}
		}
	}

	/* Check that the output is an index within the range*/
	assert(index >= 0 && index <= num_comp);

	return index;
}

int Clark_wright_imp::roulette_wheel_random_size(std::list<Entry_list*>* list) {
	/* Check that the list is not empty*/
	assert(list->size() > 0);

	unsigned int low_lim = 3; /* Lower limit on the number of competitors*/
	unsigned int up_lim = 9; /* Upper limit on the number of competitors*/
	int num_comp; /* Actual number of competitors*/

	/* If there are less elements in the list than the lower limit */
	if (list->size() <= low_lim) {
		num_comp = 1 + (int)(rand() % ((int)list->size()));
	} else {
		/* If there are less elements in the list than the upper limit*/
		if (list->size() <= up_lim) {
			num_comp = (int)low_lim + (int)(rand() % (int)(list->size() - low_lim + 1));
		} else {
			num_comp = (int)low_lim + (int)(rand() % (int)(up_lim - low_lim + 1));
		}
	}

	return roulette_wheel(list, num_comp);
}

std::list<Entry_list*>* Clark_wright_imp::copy_list_shuffled(std::list<Entry_list*>* original_list) {
	unsigned int size_list ; /* Size of the original list*/
	std::list<Entry_list*>* copy_original_list; /* Copy of the original list*/
	int element_pop; /* Element to pop from the copy of the original list*/
	std::list<Entry_list*>* new_list; /* New list*/

	copy_original_list = new std::list<Entry_list*>
							(original_list->begin(), original_list->end());

	new_list = new std::list<Entry_list*>();

	size_list = original_list->size();

	for (unsigned int i = 0; i < size_list; i++) {
		element_pop = roulette_wheel_random_size(copy_original_list);
		new_list->push_back(pop_at(copy_original_list, element_pop));
	}

	delete copy_original_list;

	return new_list;
}

/**************** Improved version of the Clark and Wright algorithm ****************/

void Clark_wright_imp::execute_clark_wright(std::list<Entry_list*>* list, VRP_solution* sol) {
	/* Empty the solution first*/
	sol->empty();


	/* Iterate over the list */
	for (std::list<Entry_list*>::iterator it = list->begin(); it != list->end(); ++it) {
		/* If an entry can be applied, apply it*/
		if (sol->entry_can_app(*it)) {
			sol->app_entry(*it);
		}
	}

	/* Iterate over all the customers and insert those that have not been assigned in an empty route*/
	for (int i = 1; i < instance_info->get_num_nodes(); i++) {
		if (!sol->is_cus_assigned(i)) {
			sol->insert_cus_empty_route(i);
		}
	}
}

void Clark_wright_imp::execute_clark_wright_imp() {

	int num_it = 10000; /* Number of iterations to execute*/
	int num_it_stop = 1000; /* Number of iterations after which, if there is no improvement, the algorithm stops*/
	int num_it_wi = 0; /* Number of iterations without improvement*/
	double dist_best_sol = std::numeric_limits<double>::max(); /* Traveled distance of the best solution*/

	VRP_solution* best_feas_sol = new VRP_solution(instance_info); /* Best feasible solution found so far*/
	VRP_solution* cur_sol = new VRP_solution(instance_info); /* Current solution being explored*/

	std::list<Entry_list*>* chosen_list; /* List chosen to be used as a base for future iterations*/
	std::list<Entry_list*>* cur_list; /* List to be used in the current iteration*/

	/* Calculate the sorted list used in the original Clark and Wright algorithm*/
	cur_list = instance_info->get_list_concatenations();
	cur_list->sort(Entry_list::compare_pointers);

	/* Set the list as the chosen list*/
	chosen_list = new std::list<Entry_list*>(cur_list->begin(), cur_list->end());


	for (int i = 0; i < num_it; i++) {
		/* Execute the Clark and Wright algorithm with the shuffled list*/
		execute_clark_wright(cur_list, cur_sol);

		/* If the solution found is better*/
		if (cur_sol->get_distance() < dist_best_sol) {

			/* Reset the number of iterations without improvement*/
			num_it_wi = 0;

			/* Update the chosen savings list*/
			delete chosen_list;
			chosen_list = new std::list<Entry_list*>(cur_list->begin(), cur_list->end());

			/* Update the cost of the best solution found*/
			dist_best_sol = cur_sol->get_distance();

			/* If the solution found complies with the constraint on the number of vehicles*/
			if(cur_sol->comp_num_veh_const()) {

				/* If the best feasible solution has been initialized already*/
				if (best_feas_sol->is_feasible()) {

					/* If the solution found is better than the best feasible solution*/
					if (cur_sol->get_distance() < best_feas_sol->get_distance()) {
						best_feas_sol->get_copy_from(cur_sol);

						/* Reset the number of iterations without improvement*/
						//num_it_wi = 0;
						printf("New best feasible solution found (at iteration %d) - Distance: %.2lf\n", i, best_feas_sol->get_distance());
					}

				/* If the best solution has not been initialized, do so*/
				} else {
					best_feas_sol->get_copy_from(cur_sol);
					/* Reset the number of iterations without improvement*/
					//num_it_wi = 0;

					printf("New best feasible solution found (at iteration %d) - Distance: %.2lf\n", i, best_feas_sol->get_distance());
				}
			/* If the solution found is not feasible*/
			} else {
				//printf(" Solution found not feasible \n");
				dist_best_sol = std::numeric_limits<double>::max();
			}

		}

		/* Increase the number of iterations*/
		num_it_wi++;

		if (num_it_wi == num_it_stop) {
			printf("Execution stopped (at iteration %d)\n", i);
			break;
		}

		/* Shuffle the list*/
		delete cur_list;
		cur_list = copy_list_shuffled(chosen_list);
	}


	solution->get_copy_from(best_feas_sol);
}

void Clark_wright_imp::execute_clark_wright() {

	/* List of entries*/
	std::list<Entry_list*>* list = instance_info->get_list_concatenations();

	/* Sort the list of entries in decreasing order*/
	list->sort(Entry_list::compare_pointers);

	execute_clark_wright(list, solution);

	/* Remove the entries in the list*/
	for (std::list<Entry_list*>::iterator it = list->begin(); it != list->end(); ++it) {
		delete *it;
	}

	delete list;
}

VRP_solution* Clark_wright_imp::get_solution() {
	return solution->get_copy();
}
