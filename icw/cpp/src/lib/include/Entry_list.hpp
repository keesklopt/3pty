/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: Entry_list.hpp
 * Description: Class that contains the information of an entry in the list for executing
 * the Clark and Wright heuristic.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#ifndef ENTRY_LIST_HPP_
#define ENTRY_LIST_HPP_

#include <stdio.h>
#include "Utils.hpp"

class Entry_list {
private:
	int cus_1; /* First customer involved*/
	int cus_2; /* Second customer (first customer in the right route)*/
	double diff_trav_dist; /* Differential of the traveled distance due to the processing of the entry */

public:
	/* Constructor*/
	Entry_list(int, /* Left customer*/
			int, /* Right customer*/
			double /* Differential of the traveled distance*/);

	/* Destructor*/
	~Entry_list();

	/* Get the first customer*/
	int get_first_cus();

	/* Get the second customer*/
	int get_second_cus();

	/* Get the differential of the traveled distance*/
	double get_diff_trav_dist();

	/* Print information about the object*/
	void print_info();

	/* Compare two pointers of this object considering their differential of the traveled distance*/
	static bool compare_pointers(const Entry_list*, const Entry_list*);

};



#endif /* ENTRY_LIST_HPP_ */
