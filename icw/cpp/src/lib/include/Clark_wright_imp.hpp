/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: Clark_wright_imp.hpp
 * Description: Implementation of the improved version of the Clark and Wright algorithm.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#ifndef CLARK_WRIGHT_IMP_HPP_
#define CLARK_WRIGHT_IMP_HPP_

#include <Entry_list.hpp>
#include <list>
#include <math.h>
#include <stdlib.h>

#include "VRP_instance.hpp"
#include "VRP_solution.hpp"

class Clark_wright_imp{
private:
	VRP_instance* instance_info; /* Information about the instance*/
	VRP_solution* solution; /* Solution produced by the algorithm*/


	/**************** Lists of possible concatenations ****************/

	/* Pops an element of a list*/
	Entry_list* pop_at(std::list<Entry_list*>*, /* List of entries*/
			int /* Index of the element*/);

	/* Select an index in the list by performing roulette wheel tournament*/
	int roulette_wheel(std::list<Entry_list*>*, /* List of entries*/
			int /* Number of competitors in the tournament*/);

	/* Select and index in the list by performing a roulette wheel tournament with a random size
	 * chosen between an upper and a lower limit */
	int roulette_wheel_random_size(std::list<Entry_list*>* /* List of entries*/);

	/* Copy the list but in a shuffled order (according to procedure described in the paper)*/
	std::list<Entry_list*>* copy_list_shuffled(std::list<Entry_list*>* /* List of entries*/);


	/**************** Improved version of the Clark and Wright algorithm ****************/

	/* Execute the Clark and Wright algorithm for a given list of entries*/
	void execute_clark_wright(std::list<Entry_list*>*, /* List of entries*/
			VRP_solution* /* Object where the final solution will be stored*/);

	/* Actual execution of the improved version of the Clark and Wright algorithm.
	 * This version is the one described in the paper*/
	void execute_clark_wright_imp();

	/* Execution of the traditional Clark and Wright algorithm*/
	void execute_clark_wright();

public:
	/* Constructor*/
	Clark_wright_imp(VRP_instance* /* Instance*/);

	/* Destructor*/
	~Clark_wright_imp();

	/* Get a copy of the solution produced by the algorithm*/
	VRP_solution* get_solution();


};

#endif /* CLARK_WRIGHT_IMP_HPP_ */
