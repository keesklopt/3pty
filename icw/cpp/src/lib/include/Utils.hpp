/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: Utils.hpp
 * Description: Implementation of utility functions that are helpful for all the other classes.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include "cmath"
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>

class Utils {
private:
	static const double eps = 0.00000001;

public:

	/* Checks if two doubles can be considered to be equal.*/
	static bool are_equal(double d1, /* First double*/
			double d2 /* Second double*/);

	/* Initialize the random seed. */
	static void ini_seed();
};



#endif /* UTILS_HPP_ */
