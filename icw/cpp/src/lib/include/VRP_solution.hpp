/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: VRP_solution.hpp
 * Description: Representation of a VRP solution.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#ifndef VRP_SOLUTION_HPP_
#define VRP_SOLUTION_HPP_


#include <Entry_list.hpp>
#include "VRP_instance.hpp"

class VRP_solution {
private:

	VRP_instance* instance_info; /* Information about the instance*/
	int *route_size; /* Size of the routes*/
	int *route_ini; /* Array with the first customer visited in each route*/
	int *route_end; /* Array with the last customers visited in each route*/
	int *next_cus; /* Next customer of the route*/
	int *prev_cus; /* Previous customer of the route*/
	int *route_cus; /* Index of the route each customer belongs to*/
	int *pos_route_cus; /* Position that each customer occupies in its route*/
	double *load; /* Amount of load that is recollected in each route*/
	double *distance; /* Distance traveled by each vehicle*/
	double total_distance; /* Total total_distance traveled*/
	int num_veh_used; /*Total number of vehicles used in the solution*/
	int max_num_veh; /* Maximum number of vehicles that can be used in the solution*/

public:

	/* Constructor */
	VRP_solution(VRP_instance* /*Information about the problem*/);

	/* Destructor.*/
	~VRP_solution();

	/* Get a copy of the object*/
	VRP_solution* get_copy();

	/* Get a copy from another object*/
	void get_copy_from(VRP_solution* /* Solution to be copied*/);

	/* Empty a solution*/
	void empty();

	/* Get the number of vehicles used by the solution*/
	int get_num_veh();

	/* Get the size of a route*/
	int get_route_size(int /*Route*/);

	/* Get the load carried in a route*/
	double get_route_load(int /*Route*/);

	/* Check if a customer has been assigned to a route*/
	bool is_cus_assigned(int /* Customer*/);

	/* Check if a route is empty*/
	bool is_route_empty(int /*Route*/);

	/* Get the route which a customer is assigned to*/
	int get_route_cus(int /*Customer*/);

	/* Check if the customer is the first one visited in its route*/
	bool is_cus_first_route(int /*Customer*/);

	/* Check if a customer is the last one visited in its route*/
	bool is_cus_last_route(int /*Customer*/);

	/* Check if a customer is either the first or the last one visited in its route*/
	bool is_cus_extr_route(int);

	/* Get the first customer of a route*/
	int get_first_cus_route(int /*Route*/);

	/* Get the last customer of a route*/
	int get_last_cus_route(int /*Route*/);

	/* Get the next customer in the route of a given customer*/
	int get_next_cus(int /*Customer*/);

	/* Get the previous customer in the route of a given customer*/
	int get_prev_cus(int /*Customer*/);

	/* Get the load transported by a vehicle*/
	double get_load(int /*Route*/);

	/* Get the distance traveled in a route*/
	double get_distance(int /*Route*/);

	/* Get the total distance traveled in the solution*/
	double get_distance();

	/**************** Insertion of customers ****************/

	/* Verification of the parameters in order to perform an insertion.*/
	void insert_cus_assert(int, /*Customer to be inserted*/
			int, /*Route where the customer is going to be inserted*/
			int, /*Customer that will be visited before*/
			int  /*Customer that will be visited after*/);

	/* Calculate the differential of the distance traveled by the vehicle if a customer
	 * is inserted between two customers.*/
	double insert_cus_diff_trav_dist(int, /*Customer to be inserted*/
			int, /*Route where the customer is going to be inserted*/
			int, /*Customer that will be visited before*/
			int  /*Customer that will be visited after*/);

	/* Calculate the differential of the load carried by the vehicle if a customer
	 *  is inserted between two customers.*/
	double insert_cus_diff_load(int, /*Customer to be inserted*/
			int, /*Route where the customer is going to be inserted*/
			int, /*Customer that will be visited before*/
			int  /*Customer that will be visited after*/);

	/* Check if the insertion of a customer complies with the capacity constraint.*/
	bool insert_cus_comp_cap_const(int, /*Customer to be inserted*/
			int, /*Route where the customer is going to be inserted*/
			int, /*Customer that will be visited before*/
			int  /*Customer that will be visited after*/);

	/* Insert a customer between two customers.*/
	void insert_cus(int, /*Customer to be inserted*/
			int, /*Route where the customer is going to be inserted*/
			int, /*Customer that will be visited before*/
			int  /*Customer that will be visited after*/);

	/**************** Insertion of customers in empty routes ****************/

	/* Verification of the parameters in order to perform an insertion of a
	 * customer in an empty route*/
	void insert_cus_empty_route_assert(int /*Customer to be inserted*/);

	/* Calculate the differential of the distance traveled if a customer
	 * is inserted in an empty route */
	double insert_cus_empty_route_diff_trav_dist(int /*Customer to be inserted*/);

	/* Insert a customer in an empty route */
	void insert_cus_empty_route(int /*Customer to be inserted*/);


	/**************** Concatenation of routes ****************/

	/* Verification of the parameters in order to perform a concatenation*/
	void concat_routes_assert(int, /* Index of the first route*/
			int /* Index of the second route*/);

	/* Calculate the differential of the distance traveled by the vehicles if two routes
	 * are concatenated.*/
	double concat_routes_diff_trav_dist(int, /* Index of the first route*/
			int /* Index of the second route*/);

	/* Calculate the differential of the load carried by a vehicle if two routes
	 * are concatenated.*/
	double concat_routes_diff_load(int, /* Index of the first route*/
			int /* Index of the second route*/);

	/* Check if the concatenation of two routes complies with the capacity constraint.*/
	bool concat_routes_comp_cap_const(int, /* Index of the first route*/
			int /* Index of the second route*/);

	/* Concatenate two routes.*/
	void concat_routes(int, /* Index of the first route*/
			int /* Index of the second route*/);


	/**************** Reversal of routes ****************/

	/* Verification of the parameters in order to reverse a route*/
	void reverse_route_assert(int /* Index of the route*/);

	/* Reverse a route*/
	void reverse_route(int /* Index of the route*/);



	/**************** Feasibility and validation ****************/

	/* Return true if the solution is feasible.*/
	bool is_feasible();

	/* Check if the solution complies with the constraint on the number of vehicles*/
	bool comp_num_veh_const();

	/* Return true if the data structure of the solution is consistent.*/
	bool is_data_structure_ok();

	/******************* Printing and displaying information *******************/

	/* Print the information of the solution.*/
	void printf_info();


	/******************* Evaluation of entries in the list for the Clark and Wright algorithm *******************/

	/* Check if an entry in a list complies with rule number one*/
	bool entry_comp_rule_1(Entry_list* /* Entry in the list*/);

	/* Check if an entry in a list complies with rule number two*/
	bool entry_comp_rule_2(Entry_list* /* Entry in the list*/);

	/* Check if an entry in a list complies with rule number three*/
	bool entry_comp_rule_3(Entry_list* /* Entry in the list*/);

	/* Check if an entry in a list can be applied to the solution*/
	bool entry_can_app(Entry_list* /* Entry in the list*/);

	/******************* Application of entries in the list for the Clark and Wright algorithm *******************/

	/* Apply an entry in a list that complies with rule number one*/
	void app_entry_rule_1(Entry_list* /* Entry in the list*/);

	/* Apply an entry in a list that complies with the rule number two*/
	void app_entry_rule_2(Entry_list* /* Entry in the list*/);

	/* Apply an entry in a list that complies with the rule number three*/
	void app_entry_rule_3(Entry_list* /* Entry in the list*/);

	/* Apply an entry in a list to the solution*/
	void app_entry(Entry_list* /* Information about the entry in the list*/);

	/******************* General assertions *******************/

	/* Verification that the index of a route is valid*/
	void assert_route_index(int /*Route index*/);

	/* Verification that the index of a route is valid, and that the route is empty*/
	void assert_route_empty(int /*Route index*/);

	/* Verification that the index of a route is valid, and that the route is not empty*/
	void assert_route_not_empty(int /*Route index*/);

	/* Verification that the index of a node is valid, that it refers to a customer,
	 * and that the customer assigned to a route*/
	void assert_assigned_customer(int /*Node index*/);

	/* Verification that the index of a node is valid, that it refers to a customer,
	 * and that the customer is not assigned to a route*/
	void assert_unassigned_customer(int /*Node index*/);

	/* Verification that a possible concatenation is valid*/
	void assert_concat_info(Entry_list* /* Information about the possible concatenation*/);


};


#endif /* VRP_SOLUTION_HPP_ */
