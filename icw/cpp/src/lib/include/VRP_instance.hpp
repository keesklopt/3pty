/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: VRP_instance.hpp
 * Description: Representation of an instance of the VRP.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#ifndef VRP_INSTANCE_H_
#define VRP_INSTANCE_H_

#include <Entry_list.hpp>
#include <stdio.h>
#include <cmath>
#include <iostream>
#include <limits>
#include "assert.h"
#include <list>

#include "Utils.hpp"

class VRP_instance{
private:
	int num_nodes; /*Number of nodes*/
	int max_num_veh; /*Maximum number of vehicles*/
	double veh_cap; /*Capacity of each vehicle*/
	double* x_coord; /* X coordinate of each node*/
	double* y_coord; /* Y coordinate of each node*/
	double* demand; /*Demand of each node*/
	double** distance; /*Distance between two nodes*/
	double distance_best_sol; /*Distance of the best solution known for the instance*/

	/* Constructor with the minimum information required to create an object*/
	VRP_instance(int, /*Number of nodes*/
			 int, /*Minimum number of vehicles*/
			 double  /*Vehicle capacity*/);
public:

	/* Constructor with no purpose, just a formality*/
	VRP_instance();

	/*Destructor.*/
	~VRP_instance();

	/* Get the number of nodes*/
	int get_num_nodes();

	/* Get the maximum number of vehicles*/
	int get_max_num_veh();

	/* Get the vehicle capacity*/
	double get_veh_cap();

	/* Get the distance of the best solution known*/
	double get_best_sol_distance();

	/* Get the demand of a node*/
	double get_demand_cus(int /*Node index*/);

	/* Get the coordinate X of a node*/
	double get_x_coord_cus(int /*Node index*/);

	/* Get the coordinate Y of a node*/
	double get_y_coord_cus(int /*Node index*/);

	/* Get the distance from one node to another*/
	double get_distance(int, /*Origin node*/
			int /*Destination node*/);

	/* Get the total demand of the instance */
	double get_total_demand();

	/******************* Printing and displaying information *******************/

	/* Print the information of the instance.*/
	void print_info();

	/* Print the demand of the customers*/
	void print_demand_cus();

	/* Print the location of the nodes*/
	void print_location_nodes();



	/******************* Reading information *******************/

	/* Read a file that contains the information of an instance*/
	static VRP_instance* read_instance_file(const char* /* Path of the instance file*/);

	/******************* Concatenation information *******************/

	/* Get a list of all possible concatenations that can be performed. It assumes that
	 * the distances between the nodes are symmetric. */
	std::list<Entry_list*>* get_list_concatenations();


	/******************* General assertions *******************/

	/* Verification that the index of a node is valid*/
	void assert_node_index(int /*Node index*/);

	/* Verification that the index of a node is valid, and it refers to a customer*/
	void assert_customer_index(int /*Node index*/);

	/* Verification that the cost of the best solution known is valid*/
	void assert_best_sol_cost(double /*Cost*/);

};

#endif /* VRP_INSTANCE_H_ */
