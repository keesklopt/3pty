/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: Utils.cpp
 * Description: Implementation of utility functions that are helpful for all the other classes.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#include "Utils.hpp"

bool Utils::are_equal(double d1, double d2) {
	return (std::abs(d1 - d2) < eps);
}

void Utils::ini_seed() {
	timeval t1;
	gettimeofday(&t1, NULL);
	srand(t1.tv_usec * t1.tv_sec);
}
