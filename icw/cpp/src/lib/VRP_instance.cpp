/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: VRP_info.hpp
 * Description: Representation of the problem information.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#include "VRP_instance.hpp"

using namespace std;

VRP_instance::VRP_instance(int n_n, int n_v, double v_c) {
	num_nodes = n_n;
	max_num_veh = n_v;
	veh_cap = v_c;
	distance_best_sol = 0;

	/* Creation and initialization of the demand vector*/
	demand = new double[num_nodes];
	for(int i = 0; i < num_nodes; i++)
		demand[i] = 0;

	/* Creation and initialization of the coordinates*/
	x_coord = new double[num_nodes];
	y_coord = new double[num_nodes];

	/* Creation and initialization of the distance matrix*/
	distance = new double*[num_nodes];

	for (int i = 0; i < num_nodes; i++)
		distance[i] = new double[num_nodes];

	for (int i = 0; i < num_nodes; i++)
		for (int j = 0; j < num_nodes; j++)
			distance[i][j] = 0;
}

VRP_instance::~VRP_instance() {
	/* Deletion of the demand vector*/
	delete[] demand;

	/* Deletion of the coordinates*/
	delete[] x_coord;
	delete[] y_coord;

	/* Deletion of the distance matrix*/
	for (int i = 0; i < num_nodes; i++)
		delete[] distance[i];

	delete[] distance;
}

int VRP_instance::get_num_nodes() {
	return num_nodes;
}

int VRP_instance::get_max_num_veh() {
	return max_num_veh;
}

double VRP_instance::get_veh_cap() {
	return veh_cap;
}

double VRP_instance::get_best_sol_distance() {
	return distance_best_sol;
}

double VRP_instance::get_demand_cus(int n_i) {
	/* Assert the node index*/
	assert_node_index(n_i);

	return demand[n_i];
}

double VRP_instance::get_x_coord_cus(int n_i) {
	/* Assert the node index*/
	assert_node_index(n_i);

	return x_coord[n_i];
}

double VRP_instance::get_y_coord_cus(int n_i) {
	/* Assert the node index*/
	assert_node_index(n_i);

	return y_coord[n_i];
}

double VRP_instance::get_distance(int o_n_i, int d_n_i) {
	/* Assert the node indices*/
	assert_node_index(o_n_i);
	assert_node_index(d_n_i);

	return distance[o_n_i][d_n_i];
}

double VRP_instance::get_total_demand() {
	double total = 0;
	for (int i = 0; i < num_nodes; i++) {
		total +=demand[i];
	}
	return total;
}

/******************* Printing and displaying information *******************/

void VRP_instance::print_info() {
	printf("Instance information:\n");
	printf("  - Number of depots: %d \n", 1);
	printf("  - Number of customers: %d \n", get_num_nodes() - 1);
	printf("  - Maximum number of vehicles: %d\n", get_max_num_veh());
	printf("  - Vehicles' capacity: %.2f\n", get_veh_cap());
	printf("  - Best solution known: %.2f\n", get_best_sol_distance());
	print_demand_cus();
	print_location_nodes();

}

void VRP_instance::print_demand_cus() {
	printf("  - Demands:\n");
	for (int k = 1; k < num_nodes; k += 9) {
		printf("     Customer::  ");
		for (int c = k; c < min(k + 9, num_nodes); c++)
			printf("%-9d", c);
		printf("\n       Demand::  ");
		for (int c = k; c < min(k + 9, num_nodes); c++)
			printf("%-9.2f", demand[c]);
		printf("\n     ----------\n");
	}
	printf("  - Total demand: %.2f\n", get_total_demand());
}

void VRP_instance::print_location_nodes() {
	printf("  - Location of the nodes: \n");
	for (int k = 0; k < num_nodes; k += 9) {
		printf("        Node::   ");
		for (int c = k; c < min(k + 9, num_nodes); c++)
			printf("%-9d", c);
		printf("\n     X-coord::   ");
		for (int c = k; c < min(k + 9, num_nodes); c++)
			printf("%-9.2f", x_coord[c]);
		printf("\n     Y-coord::   ");
		for (int c = k; c < min(k + 9, num_nodes); c++)
			printf("%-9.2f", y_coord[c]);
		printf("\n   ---------\n");
	}

}

/******************* Reading information *******************/

VRP_instance* VRP_instance::read_instance_file(const char* file_path) {
	FILE *file; /*File*/
	int num_nodes_file; /* Number of nodes in the file*/
	int min_num_veh_file; /* Minimum number of vehicles in the file*/
	double veh_cap_file; /* Vehicle capacity in the file*/
	double cost_bes_sol_file; /* Cost of the best solution in the file*/
	double coord_x_file; /* X coordinate in the file*/
	double coord_y_file; /* Y coordinate in the file*/
	double demand_node_file; /* Demand of a node in the file*/
	int id_nodel; /* ID of the node in the file*/
	VRP_instance* instance = NULL;

	file = fopen(file_path, "r");

	/* Check if the file exists or not.*/
	if (file == NULL) {
		printf(   "ERROR! Instance file not found \n");
		return NULL;
	}

	/* Skip the first line */
	fscanf(file, "%*[^\n]\n", NULL);

	/* Read the minimum number of vehicles and the cost of the best solution known*/
	fscanf(file, "COMMENT : (%*[^,], %*[^:]: %d, %*[^ \t] value: %lf)\n", &min_num_veh_file, &cost_bes_sol_file);

	/* Skip a line */
	fscanf(file, "%*[^\n]\n", NULL);

	/* Read the number of nodes in the instance*/
	fscanf(file, "DIMENSION : %d\n", &num_nodes_file);

	/* Skip a line */
	fscanf(file, "%*[^\n]\n", NULL);

	/* Read the capacity of the vehicles*/
	fscanf(file, "CAPACITY : %lf\n", &veh_cap_file);

	/* Skip a line */
	fscanf(file, "%*[^\n]\n", NULL);

	/* Create the object*/
	instance = new VRP_instance(num_nodes_file, min_num_veh_file, veh_cap_file);

	instance->distance_best_sol = cost_bes_sol_file;

	/* Read the coordinates of the nodes*/
	for (int i = 0; i < num_nodes_file; i++) {
		fscanf(file, "%d %lf %lf\n", &id_nodel, &coord_x_file, &coord_y_file);
		instance->x_coord[i] = coord_x_file;
		instance->y_coord[i] = coord_y_file;
	}

	/* Calculate the distance matrix*/
	for (int i = 0; i < num_nodes_file; i++) {
		instance->distance[i][i] = 0;
		for (int j = i + 1 ; j < num_nodes_file; j++) {
			/* Regular calculation*/
			//instance->distance[i][j] = instance->distance[j][i] =
			//		(sqrt(pow(instance->x_coord[i] - instance->x_coord[j], 2)
			//	     + pow(instance->y_coord[i] - instance->y_coord[j], 2)));
			/* Rounding calculations*/
			instance->distance[i][j] = instance->distance[j][i] =
					round(sqrt(pow(instance->x_coord[i] - instance->x_coord[j], 2)
				         + pow(instance->y_coord[i] - instance->y_coord[j], 2)));
		}
	}

	/* Skip a line */
	fscanf(file, "%*[^\n]\n", NULL);

	/* Read the demands of the nodes*/
	for (int i = 0; i < num_nodes_file; i++) {
		fscanf(file, "%d %lf\n", &id_nodel, &demand_node_file);
		instance->demand[i] = demand_node_file;
	}


	fclose(file);

	return instance;
}

/******************* Concatenation information *******************/

std::list<Entry_list*>* VRP_instance::get_list_concatenations() {
	double diff_trav_dist = 0; /* Differential traveled distance for the concatenation*/
	std::list<Entry_list*>* list = new std::list<Entry_list*>(); /* List */


	for (int i = 1; i < get_num_nodes(); i++) {
		for (int j = i+1; j < get_num_nodes(); j++) {
			diff_trav_dist = get_distance(i, j) - get_distance(i, 0) - get_distance(0, j);
			list->push_back(new Entry_list(i, j, diff_trav_dist));
		}
	}
	return list;
}

/******************* General assertions *******************/

void VRP_instance::assert_node_index(int n_i) {
	assert(n_i >= 0 && n_i < get_num_nodes());
}

void VRP_instance::assert_customer_index(int c) {
	assert_node_index(c);
	assert(c > 0);
}

void VRP_instance::assert_best_sol_cost(double c) {
	assert(c >= 0.0);
}

