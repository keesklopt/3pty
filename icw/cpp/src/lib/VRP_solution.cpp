/*
 * Faculty of Applied Economics - University of Antwerp.
 * Operations Research Group ANT/OR.
 *
 * File name: VRP_solution.hpp
 * Description: Representation of a VRP solution.
 *
 * Author: Daniel Palhazi Cuervo.
 */

#include "VRP_solution.hpp"


VRP_solution::VRP_solution(VRP_instance* i) {
	instance_info = i;
	total_distance = 0.0;
	num_veh_used = 0;
	max_num_veh = instance_info->get_num_nodes() - 1;

	route_size = new int[max_num_veh];
	for (int i = 0; i < max_num_veh; i++)
		route_size[i] = 0;

	route_ini = new int[max_num_veh];
	for (int i = 0; i < max_num_veh; i++)
		route_ini[i] = 0;

	route_end = new int[max_num_veh];
	for (int i = 0; i < max_num_veh; i++)
		route_end[i] = 0;

	next_cus = new int[instance_info->get_num_nodes()];
	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		next_cus[i] = 0;

	prev_cus = new int[instance_info->get_num_nodes()];
	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		prev_cus[i] = 0;

	route_cus = new int[instance_info->get_num_nodes()];
	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		route_cus[i] = -1;

	pos_route_cus = new int[instance_info->get_num_nodes()];
	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		pos_route_cus[i] = -1;

	load = new double[max_num_veh];
	for (int i = 0; i < max_num_veh; i++)
		load[i] = 0;

	distance = new double[max_num_veh];
	for (int i = 0; i < max_num_veh; i++)
		distance[i] = 0;
}

VRP_solution::~VRP_solution() {
	delete[] route_size;
	delete[] route_ini;
	delete[] route_end;
	delete[] next_cus;
	delete[] prev_cus;
	delete[] route_cus;
	delete[] pos_route_cus;
	delete[] load;
	delete[] distance;
}

VRP_solution* VRP_solution::get_copy() {
	VRP_solution* new_sol = new VRP_solution(instance_info);

	new_sol->total_distance = total_distance;
	new_sol->num_veh_used = num_veh_used;

	for (int i = 0; i < max_num_veh; i++)
		new_sol->route_size[i] = route_size[i];

	for (int i = 0; i < max_num_veh; i++)
		new_sol->route_ini[i] = route_ini[i];

	for (int i = 0; i < max_num_veh; i++)
		new_sol->route_end[i] = route_end[i];

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		new_sol->next_cus[i] = next_cus[i];

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		new_sol->prev_cus[i] = prev_cus[i];

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		new_sol->route_cus[i] = route_cus[i];

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		new_sol->pos_route_cus[i] = pos_route_cus[i];

	for (int i = 0; i < max_num_veh; i++)
		new_sol->load[i] = load[i];

	for (int i = 0; i < max_num_veh; i++)
		new_sol->distance[i] = distance[i];

	/* Verify that the data structure of the new solution is OK*/
	assert(new_sol->is_data_structure_ok());

	return new_sol;
}

void VRP_solution::get_copy_from(VRP_solution* original) {
	/* Check that both solutions correspond to the same instance */
	assert(instance_info == original->instance_info);

	total_distance = original->total_distance;
	num_veh_used = original->num_veh_used;

	for (int i = 0; i < max_num_veh; i++)
		route_size[i] = original->route_size[i];

	for (int i = 0; i < max_num_veh; i++)
		route_ini[i] = original->route_ini[i];

	for (int i = 0; i < max_num_veh; i++)
		route_end[i] = original->route_end[i];

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		next_cus[i] = original->next_cus[i];

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		prev_cus[i] = original->prev_cus[i];

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		route_cus[i] = original->route_cus[i];

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		pos_route_cus[i] = original->pos_route_cus[i];

	for (int i = 0; i < max_num_veh; i++)
		load[i] = original->load[i];

	for (int i = 0; i < max_num_veh; i++)
		distance[i] = original->distance[i];

	/* Verify that the data structure of the new solution is OK*/
	assert(is_data_structure_ok());
}

void VRP_solution::empty() {
	total_distance = 0.0;
	num_veh_used = 0;

	for (int i = 0; i < max_num_veh; i++)
		route_size[i] = 0;

	for (int i = 0; i < max_num_veh; i++)
		route_ini[i] = 0;

	for (int i = 0; i < max_num_veh; i++)
		route_end[i] = 0;

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		next_cus[i] = 0;

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		prev_cus[i] = 0;

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		route_cus[i] = -1;

	for (int i = 0; i < instance_info->get_num_nodes(); i++)
		pos_route_cus[i] = -1;

	for (int i = 0; i < max_num_veh; i++)
		load[i] = 0;

	for (int i = 0; i < max_num_veh; i++)
		distance[i] = 0;
}

int VRP_solution::get_num_veh() {
	return num_veh_used;
}

int VRP_solution::get_route_size(int r) {
	assert_route_index(r);
	return route_size[r];
}

double VRP_solution::get_route_load(int r) {
	assert_route_index(r);
	return load[r];
}

bool VRP_solution::is_cus_assigned(int c) {
	instance_info->assert_customer_index(c);

	return (route_cus[c] != -1 && pos_route_cus[c] != -1);

}

bool VRP_solution::is_route_empty(int r) {
	assert_route_index(r);
	return (get_route_size(r) == 0);
}

int VRP_solution::get_route_cus(int c) {
	instance_info->assert_customer_index(c);
	assert_assigned_customer(c);
	return route_cus[c];
}

bool VRP_solution::is_cus_first_route(int c) {
	instance_info->assert_customer_index(c);
	assert_assigned_customer(c);

	return get_first_cus_route(get_route_cus(c)) == c;
}

bool VRP_solution::is_cus_last_route(int c) {
	instance_info->assert_customer_index(c);
	assert_assigned_customer(c);

	return get_last_cus_route(get_route_cus(c)) == c;
}

bool VRP_solution::is_cus_extr_route(int c) {
	instance_info->assert_customer_index(c);
	assert_assigned_customer(c);

	return (is_cus_first_route(c) || is_cus_last_route(c));
}

int VRP_solution::get_first_cus_route(int r) {
	assert_route_index(r);
	return route_ini[r];
}

int VRP_solution::get_last_cus_route(int r) {
	assert_route_index(r);
	return route_end[r];
}

int VRP_solution::get_next_cus(int c) {
	instance_info->assert_customer_index(c);
	assert_assigned_customer(c);
	return next_cus[c];
}

int VRP_solution::get_prev_cus(int c) {
	instance_info->assert_customer_index(c);
	assert_assigned_customer(c);
	return prev_cus[c];
}

double VRP_solution::get_load(int r) {
	assert_route_index(r);
	return load[r];
}

double VRP_solution::get_distance(int r) {
	assert_route_index(r);
	return distance[r];
}

double VRP_solution::get_distance() {
	return total_distance;
}

/**************** Insertion of customers ****************/

void VRP_solution::insert_cus_assert(int c, int r, int l_c, int r_c) {
	instance_info->assert_node_index(l_c);
	instance_info->assert_node_index(r_c);
	instance_info->assert_customer_index(c);
}

double VRP_solution::insert_cus_diff_trav_dist(int c, int r, int l_c, int r_c) {
	insert_cus_assert(c, r, l_c, r_c);

	return (instance_info->get_distance(l_c, c) + instance_info->get_distance(c, r_c) - instance_info->get_distance(l_c, r_c));
}

double VRP_solution::insert_cus_diff_load(int c, int r, int l_c, int r_c) {
	insert_cus_assert(c, r, l_c, r_c);

	return(instance_info->get_demand_cus(c));
}

bool VRP_solution::insert_cus_comp_cap_const(int c, int r, int l_c, int r_c) {
	insert_cus_assert(c, r, l_c, r_c);

	return (load[r] + instance_info->get_demand_cus(c) < instance_info->get_veh_cap());
}

void VRP_solution::insert_cus(int c, int r, int l_c, int r_c) {
	insert_cus_assert(c, r, l_c, r_c);
	assert_unassigned_customer(c);

	int cus_rem; /* Customer remaining in the route (in order to increase the positions in the route of the other customers).*/

	load[r] += insert_cus_diff_load(c, r, l_c, r_c);
	distance[r] += insert_cus_diff_trav_dist(c, r, l_c, r_c);
	total_distance += insert_cus_diff_trav_dist(c, r, l_c, r_c);

	if (l_c == 0 && r_c == 0) {
		route_ini[r] = c;
		route_end[r] = c;
		next_cus[c] = 0;
		prev_cus[c] = 0;
		pos_route_cus[c] = 0;

		/* Increase the number of vehicles used*/
		num_veh_used++;

	} else if (l_c == 0 && r_c != 0) {
		route_ini[r] = c;
		next_cus[c] = r_c;
		prev_cus[c] = l_c;
		prev_cus[r_c] = c;
		pos_route_cus[c] = 0;

		/* Increase the position of the other customers to the right.*/
		cus_rem = r_c;
		while (cus_rem != 0) {
			pos_route_cus[cus_rem]++;
			cus_rem = next_cus[cus_rem];
		}

	} else if (l_c != 0 && r_c == 0) {
		route_end[r] = c;
		next_cus[c] = r_c;
		prev_cus[c] = l_c;
		next_cus[l_c] = c;
		pos_route_cus[c] = pos_route_cus[l_c] + 1;

	} else if (l_c != 0 && r_c != 0) {
		assert (route_cus[l_c] == route_cus[r_c]);
		next_cus[c] = r_c;
		prev_cus[c] = l_c;
		next_cus[l_c] = c;
		prev_cus[r_c] = c;

		pos_route_cus[c] = pos_route_cus[r_c];

		/* Increase the position of the other customers to the right.*/
		cus_rem = r_c;
		while (cus_rem != 0) {
			pos_route_cus[cus_rem]++;
			cus_rem = next_cus[cus_rem];
		}
	}

	route_size[r]++;
	route_cus[c] = r;

	/* Verify that the data structure is OK*/
	assert(is_data_structure_ok());
}

/**************** Insertion of customers in empty routes ****************/

void VRP_solution::insert_cus_empty_route_assert(int c) {
	instance_info->assert_customer_index(c);
	assert(get_num_veh() <= max_num_veh);
}

double VRP_solution::insert_cus_empty_route_diff_trav_dist(int c) {
	insert_cus_empty_route_assert(c);

	return (instance_info->get_distance(0, c) + instance_info->get_distance(c, 0));
}

void VRP_solution::insert_cus_empty_route(int c) {
	insert_cus_empty_route_assert(c);
	assert_unassigned_customer(c);

	int r = 0; /* Route where the customer is to be inserted*/

	/* Find an empty route*/
	while(!is_route_empty(r)) {
		r++;
	}

	/* Insert the customer*/
	insert_cus(c, r, 0, 0);
}

/**************** Concatenation of routes ****************/

void VRP_solution::concat_routes_assert(int r_l, int r_r) {
	assert_route_index(r_l);
	assert_route_not_empty(r_l);
	assert_route_index(r_r);
	assert_route_not_empty(r_r);
}

double VRP_solution::concat_routes_diff_trav_dist(int r_l, int r_r) {
	concat_routes_assert(r_l, r_r);
	return(instance_info->get_distance(get_last_cus_route(r_l), get_first_cus_route(r_r))
		  - instance_info->get_distance(get_last_cus_route(r_l), 0)
		  - instance_info->get_distance(0, get_first_cus_route(r_r)));
}

double VRP_solution::concat_routes_diff_load(int r_l, int r_r) {
	concat_routes_assert(r_l, r_r);
	return(get_load(r_r));
}

bool VRP_solution::concat_routes_comp_cap_const(int r_l, int r_r) {
	concat_routes_assert(r_l, r_r);

	return (load[r_l] + get_load(r_r) <= instance_info->get_veh_cap());
}

void VRP_solution::concat_routes(int r_l, int r_r) {
	concat_routes_assert(r_l, r_r);

	int cus_right_route; /* Customer in the right route.*/

	load[r_l] += concat_routes_diff_load(r_l, r_r);
	load[r_r] = 0.0;
	distance[r_l] += distance[r_r] + concat_routes_diff_trav_dist(r_l, r_r);
	distance[r_r] = 0.0;
	total_distance += concat_routes_diff_trav_dist(r_l, r_r);

	/* Make the connection from the last customer in the left route, to the first
	 * customer of the right route*/
	next_cus[route_end[r_l]] = route_ini[r_r];
	prev_cus[route_ini[r_r]] = route_end[r_l];

	/* Iterate over the customers in the right (now sub) route*/
	cus_right_route = route_ini[r_r];
	while (cus_right_route != 0) {
		pos_route_cus[cus_right_route] += route_size[r_l];
		route_cus[cus_right_route] = r_l;
		cus_right_route = next_cus[cus_right_route];
	}

	/* Modification of the left route */
	route_end[r_l] = route_end[r_r];
	route_size[r_l] += route_size[r_r];

	/* Clearing the right route*/
	route_ini[r_r] = 0;
	route_size[r_r] = 0;
	route_end[r_r] = 0;

	/* Decrease the number of vehicles used */
	num_veh_used--;
}

/**************** Reversal of routes ****************/

void VRP_solution::reverse_route_assert(int r) {
	assert_route_index(r);
}

void VRP_solution::reverse_route(int r) {
	reverse_route_assert(r);
	assert(get_route_size(r) >= 1);

	int aux_cus; /* Auxiliary customer for the reversing operations*/
	int exp_cus; /* Customer explored during while reversing customers in the route*/

	/* If the route one has one customer, return*/
	if (get_route_size(r) == 1) {
		return;
	}

	/* Modify the beginning and the end of the route*/
	aux_cus = get_first_cus_route(r);
	route_ini[r] = route_end[r];
	route_end[r] = aux_cus;

	exp_cus = route_ini[r];
	for (int i = 0; i < route_size[r]; i++) {
		/* Swap the next and the previous customers*/
		aux_cus = next_cus[exp_cus];
		next_cus[exp_cus] = prev_cus[exp_cus];
		prev_cus[exp_cus] = aux_cus;

		/* Change the position of the customer in the route*/
		pos_route_cus[exp_cus] = i;

		/* Move to the next customer*/
		exp_cus = next_cus[exp_cus];
	}
}



/**************** Feasibility and validation ****************/

bool VRP_solution::is_feasible() {
	/* Check for load excess.*/
	for (int i = 0; i < max_num_veh; i++)
		if (load[i] > instance_info->get_veh_cap())
			return false;

	/* Check for unassigned customers.*/
	for (int i = 1; i < instance_info->get_num_nodes(); i++)
		if (route_cus[i] == -1)
			return false;

	return true;
}

bool VRP_solution::comp_num_veh_const() {
	return (get_num_veh() == instance_info->get_max_num_veh());
}

bool VRP_solution::is_data_structure_ok(){
	double dist_calc = 0.0; /* Distance traveled calculated for verification*/
	double load_calc = 0.0; /* Load in a route calculated for verification*/

	int c_expl; /* Customer explored.*/
	int pos_route; /* Position that the customer should occupy in the route.*/

	/* Iterate over all the vehicles in the solution that can be used*/
	for (int r = 0; r < max_num_veh; r++) {

		/* If the vehicle's route is empty, verify its consistency */
		if (route_size[r] == 0) {
			if (route_ini[r] != 0 || route_end[r] != 0 || !Utils::are_equal(distance[r], 0.0) || !Utils::are_equal(load[r], 0.0)) {
				printf("Error in an empty route.\n");
				return false;
			}
		/* If the vehicle is used (its route is not empty)*/
		} else {
			dist_calc = 0.0;
			load_calc = 0.0;

			/* Verify the consistency of the first customer visited*/
			pos_route = 0;
			c_expl = route_ini[r];

			if(prev_cus[c_expl] != 0 || pos_route_cus[c_expl] != 0) {
				printf("Error in the customer %d. Previous customer: %d, Next customer: %d, Position in route: %d.\n",
						c_expl, prev_cus[c_expl], next_cus[c_expl], pos_route_cus[c_expl]);
				printf("Previous customer of the next customer: %d.\n", prev_cus[next_cus[c_expl]]);
				return false;
			}

			load_calc += instance_info->get_demand_cus(c_expl);

			dist_calc += instance_info->get_distance(0, c_expl);
			dist_calc += instance_info->get_distance(c_expl, next_cus[c_expl]);

			/* Iterate over all the customers visited in the route*/
			c_expl = next_cus[c_expl];
			pos_route++;

			while (c_expl != 0) {
				/* Verify the consistency of the inner customers */
				if (c_expl != route_end[r]) {
					if (next_cus[prev_cus[c_expl]] != c_expl
						|| prev_cus[next_cus[c_expl]] != c_expl
						|| pos_route_cus[c_expl] != pos_route) {

						printf("Error in the customer %d. Previous customer: %d, Next customer: %d, Position in route: %d.\n",
								c_expl, prev_cus[c_expl], next_cus[c_expl], pos_route_cus[c_expl]);
						printf("Next customer of the previous customer: %d, Previous customer of the next customer: %d.\n",
								next_cus[prev_cus[c_expl]], prev_cus[next_cus[c_expl]]);
						return false;
					}

				/* Verify the consistency of the last customer */
				} else if (c_expl == route_end[r]) {
					if (next_cus[prev_cus[c_expl]] != c_expl
						|| pos_route_cus[c_expl] != pos_route) {

						printf("Error in the final customer (%d). Previous customer: %d, Next customer: %d, Position in route: %d.\n",
								c_expl, prev_cus[c_expl], next_cus[c_expl], pos_route_cus[c_expl]);
						printf("Next customer of the previous customer: %d.\n",
								next_cus[prev_cus[c_expl]]);
						return false;
					}
				}
				load_calc += instance_info->get_demand_cus(c_expl);
				dist_calc += instance_info->get_distance(c_expl, next_cus[c_expl]);

				pos_route++;
				c_expl = next_cus[c_expl];
			}

			/* Verify the consistency of the route's load*/
			if (!Utils::are_equal(load_calc, load[r])) {
				printf("Error in the calculation of the load in route %d. \n", r);
				printf("The current load stored is %lf, but the actual load should be %lf \n)", load[r], load_calc);
				return false;
			}

			/* Verify the consistency of the route's distance*/
			if (!Utils::are_equal(dist_calc, distance[r])) {
				printf("Error in the calculation of the distance traveled by route %d. \n", r);
				printf("The current distance stored is %lf, but the actual distance should be %lf \n)", distance[r], dist_calc);
				return false;
			}
		}
	}

	/* Calculate the total distance traveled by the vehicles */
	dist_calc = 0.0;
	for (int r = 0; r < max_num_veh; r++) {
		dist_calc += distance[r];
	}

	/* Verify the consistency of the total distance traveled*/
	if (!Utils::are_equal(dist_calc, total_distance)) {
		printf("Error in the calculation of the distance traveled. \n");
		printf("The current load stored is %lf, but the actual load should be %lf \n)", total_distance, dist_calc);
		return false;
	}

	return true;
}


/******************* Printing and displaying information *******************/


void VRP_solution::printf_info() {
	int cus; /*Customer to be examined.*/

	printf("Solution information:\n");
	if (!is_feasible()) {
		printf("  - WARNING!!!!!! The solution is not feasible. \n");
		return;
	}

	printf("  - Number of vehicles used: %d \n", num_veh_used);
	printf("  - Total traveled distance: %.2f\n", total_distance);
	printf("  - Percentage with respect to the best solution known %.2f%%\n", total_distance/instance_info->get_best_sol_distance() * 100);
	for (int i = 0; i < max_num_veh; i++) {
		if (!is_route_empty(i)) {
			printf("     [%7.2f](%2d){%7.2f}: <%d", load[i], route_size[i], distance[i], 0);
			cus = route_ini[i];
			while (cus != 0) {
				printf(", %2d", cus);
				cus = next_cus[cus];
			}
			printf(", %2d>", 0);
			printf("\n");
		}
	}
}


/******************* Evaluation of entries in the list for the Clark and Wright algorithm *******************/

bool VRP_solution::entry_comp_rule_1(Entry_list* entry) {
	int cus_1 = entry->get_first_cus(); /* First customer*/
	int cus_2 = entry->get_second_cus(); /* Second customer*/

	return (!is_cus_assigned(cus_1) && !is_cus_assigned(cus_2)
			&&  instance_info->get_demand_cus(cus_1) + instance_info->get_demand_cus(cus_2)
			     <= instance_info->get_veh_cap());
}

bool VRP_solution::entry_comp_rule_2(Entry_list* entry) {

	int cus_1 = entry->get_first_cus(); /* First customer*/
	int cus_2 = entry->get_second_cus(); /* Second customer*/

	int route = -1; /* Route where the customer will be inserted*/


	/* If the first customer is assigned, the first customer is at a extreme of its route
	 * and the second customer is not assigned */
	if (is_cus_assigned(cus_1) && is_cus_extr_route(cus_1)
			&& !is_cus_assigned(cus_2)) {

		/* If the insertion complies with the capacity constraint*/
		route = get_route_cus(cus_1);
		if (get_route_load(route) + instance_info->get_demand_cus(cus_2) <= instance_info->get_veh_cap())
			return true;
	}

	/* If the first customer is not assigned assigned, the second customer is assigned
		 * and the second customer is at a extreme of its route */
	if (!is_cus_assigned(cus_1)
			&& is_cus_assigned(cus_2) && is_cus_extr_route(cus_2)) {

		/* If the insertion complies with the capacity constraint*/
		route = get_route_cus(cus_2);
		if (get_route_load(route) + instance_info->get_demand_cus(cus_1) <= instance_info->get_veh_cap())
			return true;
	}

	return false;
}

bool VRP_solution::entry_comp_rule_3(Entry_list* entry) {
	int cus_1 = entry->get_first_cus(); /* First customer*/
	int cus_2 = entry->get_second_cus(); /* Second customer*/

	int route_1 = -1; /*Route of the first customer*/
	int route_2 = -1; /* Route of the second customer*/

	/* If any of the customers is not assigned, return false*/
	if (!is_cus_assigned(cus_1) || !is_cus_assigned(cus_2)) {
		return false;
	}

	route_1 = get_route_cus(cus_1);
	route_2 = get_route_cus(cus_2);

	return(	/* The customers are in separate routes*/
			route_1 != route_2 &&
			/* Both customers are extreme points in their routes*/
			is_cus_extr_route(cus_1) && is_cus_extr_route(cus_2) &&
			/* The concatenation of both routes is feasible in terms of the capacity constraint*/
			concat_routes_comp_cap_const(route_1, route_2));
}

bool VRP_solution::entry_can_app(Entry_list* entry) {
	return entry_comp_rule_1(entry)
			|| entry_comp_rule_2(entry)
			|| entry_comp_rule_3(entry);
}

/******************* Application of entries in the list for the Clark and Wright algorithm *******************/

void VRP_solution::app_entry_rule_1(Entry_list* entry) {
	assert(entry_comp_rule_1(entry));

	int cus_1 = entry->get_first_cus(); /* First customer*/
	int cus_2 = entry->get_second_cus(); /* Second customer*/

	int route = -1; /* Route where the customer will be inserted*/

	//printf("Executing the first rule\n");
	insert_cus_empty_route(cus_1);
	route = get_route_cus(cus_1);
	//printf(" - Route where to insert the customer %d\n", route);
	//printf(" - Second customer to be inserted %d\n", entry->get_second_cus());
	insert_cus(cus_2, route, cus_1, 0);
}

void VRP_solution::app_entry_rule_2(Entry_list* entry) {
	assert(entry_comp_rule_2(entry));

	int cus_1 = entry->get_first_cus(); /* First customer*/
	int cus_2 = entry->get_second_cus(); /* Second customer*/

	int route = -1; /* Route where the customer will be inserted*/

	/* If the first customer is already assigned*/
	if (is_cus_assigned(cus_1)) {
		route = get_route_cus(cus_1);
		/* If the first customer is at the beginning of the route*/
		if (is_cus_first_route(cus_1)){
			insert_cus(cus_2, route, 0, cus_1);
		} else if (is_cus_last_route(cus_1)) { /* If the first customer is at the end of the route*/
			insert_cus(cus_2, route, cus_1, 0);
		}
	/* If the second customer is already assigned*/
	} else if (is_cus_assigned(cus_2)) {
		route = get_route_cus(cus_2);
		/* If the second customer is at the beginning of the route*/
		if (is_cus_first_route(cus_2)){
			insert_cus(cus_1, route, 0, cus_2);
		} else if (is_cus_last_route(cus_2)) { /* If the second customer is at the end of the route*/
			insert_cus(cus_1, route, cus_2, 0);
		}
	}
}

void VRP_solution::app_entry_rule_3(Entry_list* entry) {
	assert(entry_comp_rule_3(entry));

	int cus_1 = entry->get_first_cus(); /* First customer*/
	int cus_2 = entry->get_second_cus(); /* Second customer*/

	int route_1 = get_route_cus(cus_1); /* Route where the first customer is located*/
	int route_2 = get_route_cus(cus_2); /* Route where the second customer is located*/

	/* If the first customer is at the beginning of the route*/
	if(is_cus_first_route(cus_1)) {
		/* If the second customer is at the beginning of the route*/
		if (is_cus_first_route(cus_2)) {
			reverse_route(route_1);
			concat_routes(route_1, route_2);
		/* If the second customer is at the end of the route*/
		} else if(is_cus_last_route(cus_2)) {
			concat_routes(route_2, route_1);
		}
	} else if (is_cus_last_route(cus_1)) {
		/* If the second customer is at the beginning of the route*/
		if (is_cus_first_route(cus_2)) {
			concat_routes(route_1, route_2);
			/* If the second customer is at the end of the route*/
		} else if(is_cus_last_route(cus_2)) {
			reverse_route(route_2);
			concat_routes(route_1, route_2);
		}
	}
}

void VRP_solution::app_entry(Entry_list* entry) {
	/* Check that the entry can actually be applied*/
	assert(entry_can_app(entry));
	if(entry_comp_rule_1(entry))
		app_entry_rule_1(entry);
	else if(entry_comp_rule_2(entry))
		app_entry_rule_2(entry);
	else if(entry_comp_rule_3(entry))
		app_entry_rule_3(entry);
}

/******************* General assertions *******************/

 void VRP_solution::assert_route_index(int r) {
	assert(r >= 0 && r < max_num_veh);
}

void VRP_solution::assert_route_empty(int r) {
	assert_route_index(r);
	assert(is_route_empty(r));
}

void VRP_solution::assert_route_not_empty(int r) {
	assert_route_index(r);
	assert(!is_route_empty(r));
}

void VRP_solution::assert_assigned_customer(int c) {
	instance_info->assert_customer_index(c);
	assert(route_cus[c] != -1 && pos_route_cus[c] != -1);
}

void VRP_solution::assert_unassigned_customer(int c) {
	instance_info->assert_customer_index(c);
	assert(route_cus[c] == -1 && pos_route_cus[c] == -1);
}

void VRP_solution::assert_concat_info(Entry_list* concat) {
	instance_info->assert_customer_index(concat->get_first_cus());
	instance_info->assert_customer_index(concat->get_second_cus());
}
