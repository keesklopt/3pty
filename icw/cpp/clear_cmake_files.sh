#!/bin/bash
# Faculty of Applied Economics - University of Antwerp
# Operations Research Group ANT/OR
# 
# File name: clear_cmake_files.sh
# Description: deletes all the files produced by the cmake command
#
# Author: Daniel Palhazi Cuervo


# Function that checks if a folder exists. If it does, it removes it.
function delete_folder() {
	if [ -d "$1" ]; then
		rm -rdf "$1"
	fi
}


################### Folders ####################
# Folder where the output of the cmake command will be stored
BUILD_FOLD="build"

# Folder where the binaries of the executables will be stored
BIN_FOLD="bin"

# Format the folders if they already exist
delete_folder $BUILD_FOLD
delete_folder $BIN_FOLD


