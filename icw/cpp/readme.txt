Faculty of Applied Economics - University of Antwerp.
Operations Research Group ANT/OR.
Author: Daniel Palhazi Cuervo.
Email: daniel.palhazicuervo@uantwerpen.be

Supplementary material of the paper "A critical analysis of the improved Clarke
and Wright savings algorithm" by Arnold, Sörensen and Palhazi Cuervo.

Implementation of the improved Clarke and Wright savings algorithm described in 
ScienceAsia, 38(3):307–318, 2012, by Pichpibul and Kawtummachai.

Folders:
   - instances: folder with the VRP instances
   - src: folder with the source files

Compilation: in order to compile the code, run the script "cmake_release.sh". If 
necessary, the files produced by previous compilations can be deleted by running
the script "clear_cmake_files.sh"

Execution: in order to execute the algorithm to solve a single instance, execute
the program "bin/ICW" from the parent folder. The program receives the path of
the instance as an input parameter. Example: "./bin/ICW instances/A-n32-k5.vrp"

Experiments: in order to perform the experiments described in the paper (run the
algorithm 100 times for each instance), execute the script "script_run_instances.sh"
