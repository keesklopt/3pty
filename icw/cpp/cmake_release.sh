#!/bin/bash
# Faculty of Applied Economics - University of Antwerp
# Operations Research Group ANT/OR
# 
# File name: compile_debug.sh
# Description: executes the cmake command in order to compile a project
#              in Debug mode
#
# Author: Daniel Palhazi Cuervo


# Function that checks if a folder exists. If it does, remove its content.
# If it doesn't, create it
# Input: path of the folder
function format_folder() {
	if [ -d "$1" ]; then
		rm -rdf "$1"
	fi
	mkdir "$1"
}



################### Folders ####################
# Folder where the output of the cmake command will be stored
BUILD_FOLD="build"

# Folder where the binaries of the executables will be stored
BIN_FOLD="bin"

# Format the folders if they already exist
format_folder $BUILD_FOLD
format_folder $BIN_FOLD

# Move to the build folder and invoke the cmake command from there
cd $BUILD_FOLD
cmake -DCMAKE_BUILD_TYPE=Release ..
make
cd ..


