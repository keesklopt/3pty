#!/bin/sh

# Copyright (C) 2007  International Business Machines.
# All Rights Reserved.
# This file is distributed under the Common Public License.
# It is part of the BuildTools project in COIN-OR (www.coin-or.org)
#
## $Id: commit_new_release 1518 2010-04-25 16:33:17Z tkr $
#
# Author: Andreas Waechter     IBM      2007-06-21
# Modified by: Lou Hafer       SFU	2008-04-17

#set -x -v

set -e

# Remember what was done during release generation.

if test -r .new_release_data; then
  . .new_release_data
else
  echo ''
  echo 'Error: You need to run prepare_new_release first.'
  echo ''
  exit -1;
fi

# Commit the release to stable so we can do a repository-side copy to create
# the release.

echo ''
echo '===> Temporarily committing changed version of stable...'
echo ''

rev_num_before=`svn info . | grep -E '^Revision:' | sed -e 's|Revision: ||'`
echo "Revision number before commit: $rev_num_before"

cmd="svn ci -m \"temporarily committing release candidate to stable\""
echo $cmd
eval $cmd

# Update to confirm the commit. Avoid pulling in externals --- if we're doing
# circular dependencies, they may not exist. As it stands, the main purpose of
# this call is to allow us to easily obtain the current revision. It might be
# useful to strengthen this and check that the value is what we're expecting
# --- one greater than the revision before commit. `--ignore-externals' could
# be made provisional on the existence of circular dependency by passing a
# boolean through .new_release_data. This would strengthen the update, in that
# the update would confirm existence of the externals.

cmd='svn update --ignore-externals'
echo $cmd
eval $cmd

rev_num=`svn info . | grep -E '^Revision:' | sed -e 's|Revision: ||'`
echo "Current revision number is: $rev_num"

# Create the release with a repository-side copy.

echo ''
echo "===> Creating new release $new_ver from stable $stableBranch (rev $rev_num)..."
echo ''

cmd="svn copy -m \"creating releases/$new_ver from stable/$stableBranch (rev $rev_num)\" $stableURL $releaseURL"
echo $cmd
eval $cmd

# And restore the stable branch to it's original condition. Start by reverting
# to the original externals.

if test -r Externals; then
  echo ''
  echo '===> Restoring original externals...'
  echo ''

  mv Externals.bak Externals
  svn pset svn:externals -F Externals .
fi

# Revert the package id in configure.ac and propagate with run_autotools.  Note
# that this does not exclude configure.ac for externals in the normal place.
# But since changes to externals are not swept up by the commit, it doesn't
# matter. On the other hand, if this whole checkout is a temporary for the
# purpose of release generation, I'm not entirely convinced we need to bother
# to exclude configure.ac in the actual ThirdParty code base. Comments from
# ThirdParty maintainers welcome.

conf_ac_files=`find . -name 'configure.ac' | grep -v -E 'ThirdParty/.*/.*/configure.ac'`

echo ''
echo "===> Restoring version number (${stableBranch}stable) in configure.ac files"
for i in $conf_ac_files; do
  sed -e "s|AC_INIT\(.*\)\[[0-9\.]*\],\(.*\)|AC_INIT\1[${stableBranch}],\2|" $i > bla
  mv bla $i
  svn di $i
done

echo ''
echo '===> Running the autotools'
echo ''
curdir=`pwd`
cd $buildBase
BuildTools/run_autotools
cd "$curdir"

# Commit the restored stable branch.

echo ''
echo '===> Committing restored stable...'
echo ''

cmd="svn ci -m \"restoring stable/$stableBranch\""
echo $cmd
eval $cmd

echo ''
echo "Done, new release $releaseURL created"
echo ''
echo "You can now delete the directory $buildBase including subdirectories"

rm .new_release_data
