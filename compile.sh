#!/bin/bash
pushd concorde
make
popd

pushd GeographicLib-1.29
make
popd

pushd lk-0.5.0/src
scons
popd

sudo apt-get install libbz2-dev libluabind-dev libprotobuf-dev libosmpbf-dev libstxxl-dev libtbb-dev libxml2-dev libboost-all-dev cmake
pushd osrm-backend
mkdir build
pushd build
cmake ..
make
popd
popd

pushd glpk-4.45
./configure
make
sudo make install
popd

pushd Osi-0.107.3
./configure --enable-static=yes --enable-shared=no --with-glpk=yes --with-glpk-lib=-lglpk
make
make install
popd

pushd VRPH-1.0.0
make
popd
