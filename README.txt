Frozen 3pt libraries, some patched to just have a compilable set.
For instance VRPH and the latest glpk do not fit well together so i installed an older glpk version.

Most of these have pregenerated makefiles. to redo you might need to configure or use cmake !!

concorde : 
    cd concorde
    make

GeographicLib-1.29 : 
    cd GeographicLib-1.19
    make

lk-0.5.0 : 
    cd lk-0.5.0/src
    scons

osrm-backend : 
    apt-get install libbz2-dev libluabind-dev libprotobuf-dev libosmpbf-dev libstxxl-dev libtbb-dev libxml2-dev libboost-all-dev
    cd osrm-backend
    make

glpk-4.45
	cd glpk-4.45
	./configure
	make
	sudo make install

# without this , vrp will not compile the 
/Osi-0.107.3:
    cd Osi-0.107.3
    ./configure --enable-static=yes --enable-shared=no --with-glpk=yes --with-glpk-lib=-lglpk
	make
    make install

VRPH-1.0.0
    cd VRPH-1.0.0
    make
