/*5:*/


#if !defined(_TABUHASH_H_)
#define _TABUHASH_H_
extern const char*tabuhash_rcs_id;
/*8:*/


#include "dirty.h"

/*:8*//*19:*/


#include "pool.h"

/*:19*/


/*7:*/


typedef struct tabu_hash_elem_s{
int u,v;
struct tabu_hash_elem_s*next;
}tabu_hash_elem_t;

typedef struct{
dirty_set_t*dirty;
tabu_hash_elem_t**chain;
int chain_limit;
/*13:*/


tabu_hash_elem_t*thread;
int size;
int max_size;

/*:13*/


/*18:*/


pool_t*elem_pool;

/*:18*/


}tabu_hash_t;

/*:7*/


/*6:*/


tabu_hash_t*tabu_hash_bd_create(int vertex_bound,int max_size);
void tabu_hash_bd_destroy(tabu_hash_t*th);
int tabu_hash_bd_includes(tabu_hash_t*th,int u,int v);
void tabu_hash_bd_add(tabu_hash_t*th,int u,int v);
void tabu_hash_bd_make_empty(tabu_hash_t*th);
tabu_hash_t*tabu_hash_unbd_create(int vertex_bound,int max_size);
void tabu_hash_unbd_destroy(tabu_hash_t*th);
//int(*tabu_hash_unbd_includes)(tabu_hash_t*th,int u,int v);
void tabu_hash_unbd_add(tabu_hash_t*th,int u,int v);
void tabu_hash_unbd_make_empty(tabu_hash_t*th);

/*:6*/


#endif

/*:5*/
