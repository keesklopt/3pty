/*3:*/



const char*prog_name= "clusternoise";
const char*clusternoise_rcs_id= "$Id: clusternoise.w,v 1.1 1998/12/05 19:27:56 neto Exp neto $";
#include <config.h>
#include "lkconfig.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "length.h"
#include "read.h"
#include "decluster.h"
#include "kdtree.h"
#include "dsort.h"

static double dsj_random(const int i,const int j);
static length_t noisy_cost(int u,int v);

int noround= 0;
int verbose= 0;
long seed= -1998;
void(*sort)(void*base,size_t nmemb,size_t size,
int(*compar)(const void*,const void*))= dsort;


/*5:*/


#if SIZEOF_INT==4
typedef int int32;
#elif SIZEOF_SHORT==4
typedef short int int32;
#elif SIZEOF_LONG==4
typedef long int32;
#else
#error "I need a 32 bit integer for consistent results with DSJ_RANDOM"
#endif

/*:5*/


/*6:*/


static double dsj_random_factor= 1.0/2147483648.0;
static int32 dsj_random_param= 99163;

/*:6*/


/*4:*/



static double
dsj_random(const int ii,const int jj)
{
const int32 i= ii,j= jj;
const int32 salt1= 0x12345672*(i+1)+1;
const int32 salt2= 0x12345672*(j+1)+1;
int32 x,y,z;

x= salt1&salt2;
y= salt1|salt2;
z= dsj_random_param;

x*= z;
y*= x;
z*= y;

z^= dsj_random_param;

x*= z;
y*= x;
z*= y;

x= ((salt1+salt2)^z)&0x7fffffff;
return(double)(x*dsj_random_factor);
}


/*:4*//*7:*/


static length_t
noisy_cost(int u,int v)
{
return(length_t)
((LENGTH_TYPE_IS_INTEGRAL?0.5:0)
+(decluster_d(u,v)*(1.0+dsj_random(u,v))));
}

/*:7*/



int main(int argc,char**argv)
{
tsp_instance_t*tsp;
decluster_tree_t*mst;
if(argc>=2){seed= atoi(argv[1]);}

tsp= read_tsp_file(stdin,NULL,0);
if(E2_supports(tsp)){
E2_create(tsp);
}
mst= decluster_setup(tsp->n);
decluster_mst(tsp,mst);
decluster_preprocess(mst);
/*8:*/


dsj_random_factor= 1.0/2147483648.0;
dsj_random_param= 1+104*seed;
{FILE*out= stdout;int row,col,n= tsp->n;
fprintf(out,"NAME: cn.%ld.%s\n",seed,tsp->name);
fprintf(out,"TYPE: TSP\n");
fprintf(out,"COMMENT: %s | clusternoise %ld\n",
(tsp->comment?tsp->comment:""),seed);
fprintf(out,"DIMENSION: %d\n",tsp->n);
fprintf(out,"EDGE_WEIGHT_TYPE: EXPLICIT\n");
fprintf(out,"EDGE_WEIGHT_FORMAT: UPPER_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for(row= 0;row<n;row++){
for(col= row+1;col<n;col++){
fprintf(out," %ld",(long)noisy_cost(row,col));
}
fprintf(out,"\n");
}
fprintf(out,"EOF\n");
}


/*:8*/


return 0;
}


/*:3*/
