/*2:*/



const char*prog_name= "tsp2matrix";
const char*tsp2matrix_rcs_id= "$Id: tsp2matrix.w,v 1.1 1998/12/05 20:10:14 neto Exp neto $";
#include <config.h>
#include "lkconfig.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "length.h"
#include "read.h"

int noround= 0;

int main(int argc,char**argv)
{
tsp_instance_t*tsp;
if(!(LENGTH_TYPE_IS_INTEGRAL)){
fprintf(stderr,"%s: Warning: Length type is not integral. Truncating lengths.\n",argv[0]);
}
tsp= read_tsp_file(stdin,NULL,0);
write_tsp_file_clip_matrix(tsp,stdout,0,1);
return 0;
}
/*:2*/
