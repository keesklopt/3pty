#define gb_next_rand() (*gb_fptr>=0?*gb_fptr--:gb_flip_cycle() )  \

#define mod_diff(x,y) (((x) -(y) ) &0x7fffffff)  \

#define two_to_the_31 ((unsigned long) 0x80000000)  \

/*3:*/


/*4:*/


static long A[56]= {-1};

/*:4*/


/*5:*/


long*gb_fptr= A;

/*:5*/


/*7:*/



long gb_flip_cycle(void);
long gb_flip_cycle(void)

{register long*ii,*jj;
for(ii= &A[1],jj= &A[32];jj<=&A[55];ii++,jj++)
*ii= mod_diff(*ii,*jj);
for(jj= &A[1];ii<=&A[55];ii++,jj++)
*ii= mod_diff(*ii,*jj);
gb_fptr= &A[54];
return A[55];
}

/*:7*//*8:*/



void gb_init_rand(long seed);
void gb_init_rand(long seed)

{register long i;
register long prev= seed,next= 1;
seed= prev= mod_diff(prev,0);
A[55]= prev;
for(i= 21;i;i= (i+21)%55){
A[i]= next;
/*9:*/


next= mod_diff(prev,next);
if(seed&1)seed= 0x40000000+(seed>>1);
else seed>>= 1;
next= mod_diff(next,seed);

/*:9*/

;
prev= A[i];
}
/*10:*/


(void)gb_flip_cycle();
(void)gb_flip_cycle();
(void)gb_flip_cycle();
(void)gb_flip_cycle();
(void)gb_flip_cycle();

/*:10*/

;
}

/*:8*//*12:*/



long gb_unif_rand(long m);
long gb_unif_rand(long m)

{register unsigned long t= two_to_the_31-(two_to_the_31%m);
register long r;
do{
r= gb_next_rand();
}while(t<=(unsigned long)r);
return r%m;
}

/*:12*//*15:*/


#include <stdlib.h> 
#include "gb_flip.h"

gb_prng_t*gb_prng_new(long seed);
gb_prng_t*
gb_prng_new(long seed)
{
gb_prng_t*prng= (gb_prng_t *)malloc(sizeof(gb_prng_t));
if(prng){
register long i;
register long prev= seed,next= 1;
prng->A[0]= -1;
seed= prev= mod_diff(prev,0);
prng->A[55]= prev;
for(i= 21;i;i= (i+21)%55){
prng->A[i]= next;
/*9:*/


next= mod_diff(prev,next);
if(seed&1)seed= 0x40000000+(seed>>1);
else seed>>= 1;
next= mod_diff(next,seed);

/*:9*/

;
prev= prng->A[i];
}
gb_prng_cycle(prng);
gb_prng_cycle(prng);
gb_prng_cycle(prng);
gb_prng_cycle(prng);
gb_prng_cycle(prng);
}
return prng;
}


/*:15*//*16:*/


long gb_prng_cycle(gb_prng_t*prng);
long gb_prng_cycle(gb_prng_t*prng)
{register long*ii,*jj;
for(ii= &prng->A[1],jj= &prng->A[32];jj<=&prng->A[55];ii++,jj++)
*ii= mod_diff(*ii,*jj);
for(jj= &prng->A[1];ii<=&prng->A[55];ii++,jj++)
*ii= mod_diff(*ii,*jj);
prng->fptr= &prng->A[54];
return prng->A[55];
}

/*:16*//*17:*/


long gb_prng_unif_rand(gb_prng_t*prng,long m);
long gb_prng_unif_rand(gb_prng_t*prng,long m)
{register unsigned long t= two_to_the_31-(two_to_the_31%m);
register long r;
do{
r= gb_prng_next_rand(prng);
}while(t<=(unsigned long)r);
return r%m;
}

/*:17*//*18:*/


void
gb_prng_free(gb_prng_t*prng)
{
if(prng){free(prng);}
}

/*:18*/



/*:3*/
