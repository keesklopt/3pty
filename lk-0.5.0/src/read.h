/*2:*/


extern const char*read_rcs_id;
/*9:*/


/*16:*/


typedef enum
{NO_EDGE_TYPE,EUC_2D,CEIL_2D,GEO,ATT,DSJ_RANDOM,EXPLICIT,RANDOM_EDGES}edge_weight_type_t;

/*:16*//*22:*/


typedef enum{NO_EDGE_FORMAT,LOWER_DIAG_ROW,FULL_MATRIX,UPPER_ROW}
edge_weight_format_t;

/*:22*//*31:*/


typedef struct{double x[2];}coord_2d;

/*:31*/


typedef struct{
char*name;
char*comment;
int n;
int input_n;
double scale;
double dsj_random_factor;
double dsj_random_param;
/*18:*/


edge_weight_type_t edge_weight_type;

/*:18*//*24:*/


edge_weight_format_t edge_weight_format;

/*:24*//*28:*/


length_t**edge_weights;

/*:28*//*32:*/


coord_2d*coord;
double xmin,xmax,ymin,ymax;

/*:32*//*37:*/


long seed;

/*:37*//*41:*/


short**short_edge_weights;

/*:41*/


}tsp_instance_t;

/*:9*/


/*44:*/


extern length_t(*cost)(const int,const int);
extern length_t(*pseudo_cost)(const int,const int);


/*:44*/


/*5:*/


tsp_instance_t*
switch_to(tsp_instance_t*new_problem);

/*:5*//*8:*/


tsp_instance_t*read_tsp_file(FILE*in,FILE*out,const int remove_odd_vertex);


/*:8*//*51:*/


double cost_from_euc2d_raw(const int i,const int j);


/*:51*//*67:*/


void write_tsp_file(tsp_instance_t*tsp,FILE*out);
void write_tsp_file_clip(tsp_instance_t*tsp,FILE*out,int force_even_num);
void write_tsp_file_clip_matrix(
tsp_instance_t*tsp,FILE*out,int force_even_num,int force_matrix);

/*:67*/




/*:2*/
