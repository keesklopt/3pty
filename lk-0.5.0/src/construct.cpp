/*1:*/


#include <config.h>
#include "lkconfig.h"
/*25:*/


#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#define FIXINCLUDES_NEED_NRAND48 
#include "fixincludes.h"
#undef FIXINCLUDES_NEED_NRAND48

/*:25*/


/*3:*/


#include "construct.h"

/*:3*//*7:*/


#include "error.h"

/*:7*//*12:*/


#include "prng.h"
#include "read.h"


/*:12*//*24:*/


#include "memory.h"
#include "pq.h"
#include "pool.h"

/*:24*//*28:*/


#include "lk.h"
#include "kdtree.h"

/*:28*//*42:*/


#include "dsort.h"

#define init_max_per_city_nn_cache 30

#define max(X,Y) ((X) <(Y) ?(Y) :(X) ) 

#define min(X,Y) ((X) >(Y) ?(Y) :(X) ) 



/*:42*/


/*19:*/


typedef struct{
length_t len;
int this_end;
int other_end;
}pq_edge_t;

/*:19*//*45:*/


typedef struct{
int count;
int city[2];
}adj_entry_t;

/*:45*/


/*20:*/


static void pq_edge_print(FILE*out,void*edgep);
static void
pq_edge_print(FILE*out,void*edgep)
{
int num_chosen= min(11,12);

pq_edge_t*edge= (pq_edge_t *) edgep;
if(edge==NULL){fprintf(out,"(null)");}
else{
fprintf(out,"{%p,%d,%d,"length_t_spec"}",
edge,edge->this_end,edge->other_end,length_t_pcast(edge->len));
}
}

/*:20*//*26:*/


static int
cmp_pq_edge(const void*a,const void*b){
length_t len_diff= ((const pq_edge_t*)a)->len-((const pq_edge_t*)b)->len;
return len_diff<0?-1:(len_diff>0?1:
(int)(((const pq_edge_t*)a)-((const pq_edge_t*)b)));
}

/*:26*/


/*4:*/


length_t
construct(const int n,int*tour,const int heuristic,const long heur_param,
const long random_seed)
{
switch(heuristic){
/*8:*/


case CONSTRUCT_CANONICAL:{
int i;length_t len;
for(i= 0;i<n;i++)tour[i]= i;

len= cost(tour[0],tour[n-1]);
for(i= 1;i<n;i++)len+= cost(tour[i-1],tour[i]);
return len;
break;
}

/*:8*//*10:*/


case CONSTRUCT_RANDOM:{
int i;length_t len;
prng_t*random_stream;

for(i= 0;i<n;i++){
tour[i]= i;
}

random_stream= prng_new(PRNG_DEFAULT,heur_param);
for(i= 0;i<n;i++){
const int next= prng_unif_int(random_stream,n-i);
const int t= tour[next];
tour[next]= tour[n-1-i];
tour[n-1-i]= t;
}
prng_free(random_stream);

len= cost(tour[0],tour[n-1]);
for(i= 1;i<n;i++)len+= cost(tour[i-1],tour[i]);

return len;
break;
}


/*:10*//*16:*/


case CONSTRUCT_GREEDY:
{
length_t len= 0;
const int E2_case= E2_supports(tsp_instance);
#define DO_TOUR
/*21:*/


pool_t*nn_link_pool= pool_create(sizeof(pq_edge_t),n);
pq_t*pq_edge= pq_create_size(cmp_pq_edge,n);

/*:21*//*29:*/


int*farthest_in_queue= NULL;

/*:29*//*32:*/


int*unsaturated= NULL,num_unsaturated= 0,*inv_unsaturated= NULL;

/*:32*//*37:*/


int sqrt_n= (int)sqrt((double)n);

/*:37*//*43:*/


pq_edge_t*nn_work= NULL;


/*:43*//*46:*/


#if defined(DO_TOUR)
adj_entry_t*adj= new_arr_of(adj_entry_t,n);
#endif

/*:46*//*49:*/


#if defined(DO_TOUR)
int*tail= new_arr_of(int,n);
#endif

/*:49*//*55:*/


#if defined(DO_RANDOM)
prng_t*random_stream= NULL;
#endif

/*:55*/


/*22:*/


errorif(pq_edge==NULL,"Couldn't create the priority queue!");
pq_set_print_func(pq_edge,pq_edge_print);

/*:22*//*27:*/


if(E2_case){
int i;
for(i= 0;i<n;i++){
pq_edge_t*e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->this_end= i;
e->other_end= E2_nn(i);
e->len= cost(i,e->other_end);
pq_insert(pq_edge,e);
/*48:*/


#if defined(DO_TOUR)
adj[i].count= 0;
#endif

/*:48*//*51:*/


#if defined(DO_TOUR)
tail[i]= i;
#endif

/*:51*//*68:*/


#if defined(DO_MATCHING)
mate[i]= -1;
#endif


/*:68*/


}
}else{
/*30:*/


farthest_in_queue= new_arr_of(int,n);

/*:30*//*33:*/


unsaturated= new_arr_of(int,n);
inv_unsaturated= new_arr_of(int,n);
num_unsaturated= n;
{int i;
for(i= 0;i<n;i++)inv_unsaturated[i]= unsaturated[i]= i;
}

/*:33*//*36:*/


{
int i;
nn_work= new_arr_of(pq_edge_t,n);
for(i= 0;i<n;i++){
const int x= i,not_me= -1;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


/*48:*/


#if defined(DO_TOUR)
adj[i].count= 0;
#endif

/*:48*//*51:*/


#if defined(DO_TOUR)
tail[i]= i;
#endif

/*:51*//*68:*/


#if defined(DO_MATCHING)
mate[i]= -1;
#endif


/*:68*/


}
}

/*:36*/


}

/*:27*//*56:*/


#if defined(DO_RANDOM)
random_stream= prng_new(PRNG_DEFAULT,random_seed^(6502*6510));
#endif

/*:56*/


/*52:*/


{int i,x,y,tx,ty;
errorif(n<3,"Only %d cities.  Can't build a tour.",n);
tx= ty= -1;
for(i= 0;i<n-1;i++){
pq_edge_t*e;
/*53:*/


#if defined(DO_RANDOM)
{
pq_edge_t*candidate[2];
/*58:*/


/*75:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Priority queue before Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:75*/


while(1){
e= pq_delete_min(pq_edge);
if(e==NULL)break;
/*76:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Extract ");
pq_edge_print(stdout,e);

printf("\nPriority queue just after extract:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:76*/


x= e->this_end;y= e->other_end;
if(adj[x].count==2){
continue;
}
if(adj[y].count<2&&y!=tail[x])break;
/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/


}
/*77:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=2000){
printf("Priority queue AFTER Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
if(verbose>=1000)printf(" valid");
#endif
#endif

/*:77*/



/*:58*/

candidate[0]= e;
/*58:*/


/*75:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Priority queue before Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:75*/


while(1){
e= pq_delete_min(pq_edge);
if(e==NULL)break;
/*76:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Extract ");
pq_edge_print(stdout,e);

printf("\nPriority queue just after extract:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:76*/


x= e->this_end;y= e->other_end;
if(adj[x].count==2){
continue;
}
if(adj[y].count<2&&y!=tail[x])break;
/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/


}
/*77:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=2000){
printf("Priority queue AFTER Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
if(verbose>=1000)printf(" valid");
#endif
#endif

/*:77*/



/*:58*/


if(e&&e->this_end==candidate[0]->other_end
&&e->other_end==candidate[0]->this_end){
pq_edge_t*push_back= e;
/*58:*/


/*75:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Priority queue before Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:75*/


while(1){
e= pq_delete_min(pq_edge);
if(e==NULL)break;
/*76:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Extract ");
pq_edge_print(stdout,e);

printf("\nPriority queue just after extract:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:76*/


x= e->this_end;y= e->other_end;
if(adj[x].count==2){
continue;
}
if(adj[y].count<2&&y!=tail[x])break;
/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/


}
/*77:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=2000){
printf("Priority queue AFTER Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
if(verbose>=1000)printf(" valid");
#endif
#endif

/*:77*/



/*:58*/


pq_insert(pq_edge,push_back);
}
candidate[1]= e;
e= candidate[0];
if(candidate[1]){
const int chosen= (prng_unif_int(random_stream,3)==0);

e= candidate[chosen];
pq_insert(pq_edge,candidate[1-chosen]);
}
}
#endif

/*:53*//*54:*/


#if !defined(DO_RANDOM)
/*58:*/


/*75:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Priority queue before Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:75*/


while(1){
e=(pq_edge_t *) pq_delete_min(pq_edge);
if(e==NULL)break;
/*76:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Extract ");
pq_edge_print(stdout,e);

printf("\nPriority queue just after extract:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:76*/


x= e->this_end;y= e->other_end;
if(adj[x].count==2){
continue;
}
if(adj[y].count<2&&y!=tail[x])break;
/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/


}
/*77:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=2000){
printf("Priority queue AFTER Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
if(verbose>=1000)printf(" valid");
#endif
#endif

/*:77*/



/*:58*/


#endif

/*:54*//*59:*/


errorif(e==NULL,"Exhausted the priority queue of links.");
/*74:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=1000)printf(" accept\n");
#endif
#endif


/*:74*/


len+= e->len;
x= e->this_end;
y= e->other_end;


/*:59*/


adj[x].city[adj[x].count++]= y;
adj[y].city[adj[y].count++]= x;
if(adj[y].count==2){if(E2_case)E2_hide(y);else{const int c= y;
/*35:*/


{const int inv= inv_unsaturated[c];
if(inv<num_unsaturated&&unsaturated[inv]==c){
const int move_city= unsaturated[--num_unsaturated];
unsaturated[inv]= move_city;
inv_unsaturated[move_city]= inv;
/*79:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"             Marking %d as saturated (inv= %d)\n",c,inv);
#endif
#endif


/*:79*/


}
}

/*:35*/

}}
if(adj[x].count==2){if(E2_case)E2_hide(x);else{const int c= x;
/*35:*/


{const int inv= inv_unsaturated[c];
if(inv<num_unsaturated&&unsaturated[inv]==c){
const int move_city= unsaturated[--num_unsaturated];
unsaturated[inv]= move_city;
inv_unsaturated[move_city]= inv;
/*79:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"             Marking %d as saturated (inv= %d)\n",c,inv);
#endif
#endif


/*:79*/


}
}

/*:35*/

}}
else{/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/

}
tx= tail[x];
ty= tail[y];
tail[tx]= ty;
tail[ty]= tx;
}
adj[tx].city[adj[tx].count++]= ty;
adj[ty].city[adj[ty].count++]= tx;
if(E2_case)E2_unhide_all();
len+= cost(tx,ty);
}

/*:52*//*61:*/


{int i= 0,prev= -1,here= 0;
do{
tour[i++]= here;
if(adj[here].city[0]!=prev){prev= here;here= adj[here].city[0];}
else{prev= here;here= adj[here].city[1];}
}while(here!=0);
errorif(i!=n,"Not a tour.");
}


/*:61*/


/*23:*/


pool_destroy(nn_link_pool);
pq_destroy(pq_edge);

/*:23*//*31:*/


if(!E2_case){free_mem(farthest_in_queue);mem_deduct(sizeof(int)*n);}


/*:31*//*34:*/


if(!E2_case){
free_mem(unsaturated);mem_deduct(sizeof(int)*n);
free_mem(inv_unsaturated);mem_deduct(sizeof(int)*n);
}

/*:34*//*44:*/


if(!E2_case){free_mem(nn_work);mem_deduct(sizeof(pq_edge_t)*n);}


/*:44*//*47:*/


#if defined(DO_TOUR)
free_mem(adj);
#endif

/*:47*//*50:*/


#if defined(DO_TOUR)
free_mem(tail);mem_deduct(sizeof(int)*n);
#endif

/*:50*//*57:*/


#if defined(DO_RANDOM)
prng_free(random_stream);
#endif

/*:57*/


#undef DO_TOUR
return len;
break;
}

/*:16*//*17:*/


case CONSTRUCT_GREEDY_RANDOM:
{
length_t len= 0;
const int E2_case= E2_supports(tsp_instance);
#define DO_RANDOM
#define DO_TOUR
/*21:*/


pool_t*nn_link_pool= pool_create(sizeof(pq_edge_t),n);
pq_t*pq_edge= pq_create_size(cmp_pq_edge,n);

/*:21*//*29:*/


int*farthest_in_queue= NULL;

/*:29*//*32:*/


int*unsaturated= NULL,num_unsaturated= 0,*inv_unsaturated= NULL;

/*:32*//*37:*/


int sqrt_n= (int)sqrt((double)n);

/*:37*//*43:*/


pq_edge_t*nn_work= NULL;


/*:43*//*46:*/


#if defined(DO_TOUR)
adj_entry_t*adj= new_arr_of(adj_entry_t,n);
#endif

/*:46*//*49:*/


#if defined(DO_TOUR)
int*tail= new_arr_of(int,n);
#endif

/*:49*//*55:*/


#if defined(DO_RANDOM)
prng_t*random_stream= NULL;
#endif

/*:55*/


/*22:*/


errorif(pq_edge==NULL,"Couldn't create the priority queue!");
pq_set_print_func(pq_edge,pq_edge_print);

/*:22*//*27:*/


if(E2_case){
int i;
for(i= 0;i<n;i++){
pq_edge_t*e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->this_end= i;
e->other_end= E2_nn(i);
e->len= cost(i,e->other_end);
pq_insert(pq_edge,e);
/*48:*/


#if defined(DO_TOUR)
adj[i].count= 0;
#endif

/*:48*//*51:*/


#if defined(DO_TOUR)
tail[i]= i;
#endif

/*:51*//*68:*/


#if defined(DO_MATCHING)
mate[i]= -1;
#endif


/*:68*/


}
}else{
/*30:*/


farthest_in_queue= new_arr_of(int,n);

/*:30*//*33:*/


unsaturated= new_arr_of(int,n);
inv_unsaturated= new_arr_of(int,n);
num_unsaturated= n;
{int i;
for(i= 0;i<n;i++)inv_unsaturated[i]= unsaturated[i]= i;
}

/*:33*//*36:*/


{
int i;
nn_work= new_arr_of(pq_edge_t,n);
for(i= 0;i<n;i++){
const int x= i,not_me= -1;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e=(pq_edge_t *) pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


/*48:*/


#if defined(DO_TOUR)
adj[i].count= 0;
#endif

/*:48*//*51:*/


#if defined(DO_TOUR)
tail[i]= i;
#endif

/*:51*//*68:*/


#if defined(DO_MATCHING)
mate[i]= -1;
#endif


/*:68*/


}
}

/*:36*/


}

/*:27*//*56:*/


#if defined(DO_RANDOM)
random_stream= prng_new(PRNG_DEFAULT,random_seed^(6502*6510));
#endif

/*:56*/


/*52:*/


{int i,x,y,tx,ty;
errorif(n<3,"Only %d cities.  Can't build a tour.",n);
tx= ty= -1;
for(i= 0;i<n-1;i++){
pq_edge_t*e;
/*53:*/


#if defined(DO_RANDOM)
{
pq_edge_t*candidate[2];
/*58:*/


/*75:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Priority queue before Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:75*/


while(1){
e= (pq_edge_t *)pq_delete_min(pq_edge);
if(e==NULL)break;
/*76:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Extract ");
pq_edge_print(stdout,e);

printf("\nPriority queue just after extract:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:76*/


x= e->this_end;y= e->other_end;
if(adj[x].count==2){
continue;
}
if(adj[y].count<2&&y!=tail[x])break;
/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e=(pq_edge_t *) pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/


}
/*77:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=2000){
printf("Priority queue AFTER Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
if(verbose>=1000)printf(" valid");
#endif
#endif

/*:77*/



/*:58*/

candidate[0]= e;
/*58:*/


/*75:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Priority queue before Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:75*/


while(1){
e= (pq_edge_t *)pq_delete_min(pq_edge);
if(e==NULL)break;
/*76:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Extract ");
pq_edge_print(stdout,e);

printf("\nPriority queue just after extract:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:76*/


x= e->this_end;y= e->other_end;
if(adj[x].count==2){
continue;
}
if(adj[y].count<2&&y!=tail[x])break;
/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/


}
/*77:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=2000){
printf("Priority queue AFTER Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
if(verbose>=1000)printf(" valid");
#endif
#endif

/*:77*/



/*:58*/


if(e&&e->this_end==candidate[0]->other_end
&&e->other_end==candidate[0]->this_end){
pq_edge_t*push_back= e;
/*58:*/


/*75:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Priority queue before Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:75*/


while(1){
e= (pq_edge_t *)pq_delete_min(pq_edge);
if(e==NULL)break;
/*76:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Extract ");
pq_edge_print(stdout,e);

printf("\nPriority queue just after extract:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:76*/


x= e->this_end;y= e->other_end;
if(adj[x].count==2){
continue;
}
if(adj[y].count<2&&y!=tail[x])break;
/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e=(pq_edge_t *) pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/


}
/*77:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=2000){
printf("Priority queue AFTER Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
if(verbose>=1000)printf(" valid");
#endif
#endif

/*:77*/



/*:58*/


pq_insert(pq_edge,push_back);
}
candidate[1]= e;
e= candidate[0];
if(candidate[1]){
const int chosen= (prng_unif_int(random_stream,3)==0);

e= candidate[chosen];
pq_insert(pq_edge,candidate[1-chosen]);
}
}
#endif

/*:53*//*54:*/


#if !defined(DO_RANDOM)
/*58:*/


/*75:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Priority queue before Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:75*/


while(1){
e= pq_delete_min(pq_edge);
if(e==NULL)break;
/*76:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 2000
if(verbose>=2000){
printf("Extract ");
pq_edge_print(stdout,e);

printf("\nPriority queue just after extract:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
#endif
#endif

/*:76*/


x= e->this_end;y= e->other_end;
if(adj[x].count==2){
continue;
}
if(adj[y].count<2&&y!=tail[x])break;
/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/


}
/*77:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=2000){
printf("Priority queue AFTER Get one short valid edge or NULL:\n");
pq_print(pq_edge,stdout);
printf("\n");
}
if(verbose>=1000)printf(" valid");
#endif
#endif

/*:77*/



/*:58*/


#endif

/*:54*//*59:*/


errorif(e==NULL,"Exhausted the priority queue of links.");
/*74:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 1000
if(verbose>=1000)printf(" accept\n");
#endif
#endif


/*:74*/


len+= e->len;
x= e->this_end;
y= e->other_end;


/*:59*/


adj[x].city[adj[x].count++]= y;
adj[y].city[adj[y].count++]= x;
if(adj[y].count==2){if(E2_case)E2_hide(y);else{const int c= y;
/*35:*/


{const int inv= inv_unsaturated[c];
if(inv<num_unsaturated&&unsaturated[inv]==c){
const int move_city= unsaturated[--num_unsaturated];
unsaturated[inv]= move_city;
inv_unsaturated[move_city]= inv;
/*79:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"             Marking %d as saturated (inv= %d)\n",c,inv);
#endif
#endif


/*:79*/


}
}

/*:35*/

}}
if(adj[x].count==2){if(E2_case)E2_hide(x);else{const int c= x;
/*35:*/


{const int inv= inv_unsaturated[c];
if(inv<num_unsaturated&&unsaturated[inv]==c){
const int move_city= unsaturated[--num_unsaturated];
unsaturated[inv]= move_city;
inv_unsaturated[move_city]= inv;
/*79:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"             Marking %d as saturated (inv= %d)\n",c,inv);
#endif
#endif


/*:79*/


}
}

/*:35*/

}}
else{/*60:*/


if(E2_case){
E2_hide(tail[x]);
e->other_end= E2_nn(x);
e->len= cost(x,e->other_end);
E2_unhide(tail[x]);
pq_insert(pq_edge,e);
}else{
pool_free(nn_link_pool,e);
if(farthest_in_queue[x]==y){
const int not_me= tail[x];
/*82:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"Need to refresh because I got x=%d %d y=%d %d "
length_t_native_spec"\n",
x,e->this_end,y,e->other_end,length_t_native_pcast(e->len));
#endif
#endif

/*:82*/


/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:60*/

}
tx= tail[x];
ty= tail[y];
tail[tx]= ty;
tail[ty]= tx;
}
adj[tx].city[adj[tx].count++]= ty;
adj[ty].city[adj[ty].count++]= tx;
if(E2_case)E2_unhide_all();
len+= cost(tx,ty);
}

/*:52*//*61:*/


{int i= 0,prev= -1,here= 0;
do{
tour[i++]= here;
if(adj[here].city[0]!=prev){prev= here;here= adj[here].city[0];}
else{prev= here;here= adj[here].city[1];}
}while(here!=0);
errorif(i!=n,"Not a tour.");
}


/*:61*/


/*23:*/


pool_destroy(nn_link_pool);
pq_destroy(pq_edge);

/*:23*//*31:*/


if(!E2_case){free_mem(farthest_in_queue);mem_deduct(sizeof(int)*n);}


/*:31*//*34:*/


if(!E2_case){
free_mem(unsaturated);mem_deduct(sizeof(int)*n);
free_mem(inv_unsaturated);mem_deduct(sizeof(int)*n);
}

/*:34*//*44:*/


if(!E2_case){free_mem(nn_work);mem_deduct(sizeof(pq_edge_t)*n);}


/*:44*//*47:*/


#if defined(DO_TOUR)
free_mem(adj);
#endif

/*:47*//*50:*/


#if defined(DO_TOUR)
free_mem(tail);mem_deduct(sizeof(int)*n);
#endif

/*:50*//*57:*/


#if defined(DO_RANDOM)
prng_free(random_stream);
#endif

/*:57*/


#undef DO_TOUR
#undef DO_RANDOM
return len;
break;
}

/*:17*/


default:errorif(1,"Unknown heuristic: %d",heuristic);
}
return(length_t)0;
}

/*:4*//*62:*/


length_t
construct_matching(int n,int*mate,int alg,long alg_param,const long
random_seed)
{
int i;
length_t weight= 0;
errorif(n%2,
"Perfect matchings need an even number of vertices; given %d",n);
errorif(mate==NULL,
"Tried to construct a matching before space is allocated");
switch(alg){
/*64:*/


case CONSTRUCT_CANONICAL:
for(i= 0;i<n;i+= 2){
mate[i]= i+1;
mate[i+1]= i;
weight+= cost(i,i+1);
}
break;

/*:64*//*65:*/


case CONSTRUCT_RANDOM:
{
int*unmated= new_arr_of(int,n),num_unmated= n,u,v,ui,vi;
prng_t*random_stream= prng_new(PRNG_DEFAULT,alg_param);
for(i= 0;i<n;i++)unmated[i]= i;
while(num_unmated>0){
ui= prng_unif_int(random_stream,num_unmated);
u= unmated[ui];unmated[ui]= unmated[--num_unmated];
vi= prng_unif_int(random_stream,num_unmated);
v= unmated[vi];unmated[vi]= unmated[--num_unmated];
mate[u]= v;
mate[v]= u;
weight+= cost(u,v);
}
prng_free(random_stream);
free_mem(unmated);mem_deduct(sizeof(int)*n);
}
break;

/*:65*//*66:*/


case CONSTRUCT_GREEDY:
{
const int E2_case= E2_supports(tsp_instance);
#define DO_MATCHING
/*21:*/


pool_t*nn_link_pool= pool_create(sizeof(pq_edge_t),n);
pq_t*pq_edge= pq_create_size(cmp_pq_edge,n);

/*:21*//*29:*/


int*farthest_in_queue= NULL;

/*:29*//*32:*/


int*unsaturated= NULL,num_unsaturated= 0,*inv_unsaturated= NULL;

/*:32*//*37:*/


int sqrt_n= (int)sqrt((double)n);

/*:37*//*43:*/


pq_edge_t*nn_work= NULL;


/*:43*//*46:*/


#if defined(DO_TOUR)
adj_entry_t*adj= new_arr_of(adj_entry_t,n);
#endif

/*:46*//*49:*/


#if defined(DO_TOUR)
int*tail= new_arr_of(int,n);
#endif

/*:49*//*55:*/


#if defined(DO_RANDOM)
prng_t*random_stream= NULL;
#endif

/*:55*/


/*22:*/


errorif(pq_edge==NULL,"Couldn't create the priority queue!");
pq_set_print_func(pq_edge,pq_edge_print);

/*:22*//*27:*/


if(E2_case){
int i;
for(i= 0;i<n;i++){
pq_edge_t*e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->this_end= i;
e->other_end= E2_nn(i);
e->len= cost(i,e->other_end);
pq_insert(pq_edge,e);
/*48:*/


#if defined(DO_TOUR)
adj[i].count= 0;
#endif

/*:48*//*51:*/


#if defined(DO_TOUR)
tail[i]= i;
#endif

/*:51*//*68:*/


#if defined(DO_MATCHING)
mate[i]= -1;
#endif


/*:68*/


}
}else{
/*30:*/


farthest_in_queue= new_arr_of(int,n);

/*:30*//*33:*/


unsaturated= new_arr_of(int,n);
inv_unsaturated= new_arr_of(int,n);
num_unsaturated= n;
{int i;
for(i= 0;i<n;i++)inv_unsaturated[i]= unsaturated[i]= i;
}

/*:33*//*36:*/


{
int i;
nn_work= new_arr_of(pq_edge_t,n);
for(i= 0;i<n;i++){
const int x= i,not_me= -1;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


/*48:*/


#if defined(DO_TOUR)
adj[i].count= 0;
#endif

/*:48*//*51:*/


#if defined(DO_TOUR)
tail[i]= i;
#endif

/*:51*//*68:*/


#if defined(DO_MATCHING)
mate[i]= -1;
#endif


/*:68*/


}
}

/*:36*/


}

/*:27*//*56:*/


#if defined(DO_RANDOM)
random_stream= prng_new(PRNG_DEFAULT,random_seed^(6502*6510));
#endif

/*:56*/


/*69:*/


{
int num_remaining= n/2;
while(num_remaining>0){
int u,v;
pq_edge_t*next_edge;
/*70:*/


#if defined(DO_RANDOM)
{
pq_edge_t*candidate[2];
/*72:*/


while(1){
next_edge= pq_delete_min(pq_edge);
if(next_edge==NULL)break;
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0)continue;
if(mate[v]>=0){
/*73:*/


if(E2_case){
next_edge->other_end= E2_nn(u);
next_edge->len= cost(u,next_edge->other_end);
pq_insert(pq_edge,next_edge);
}else{
pool_free(nn_link_pool,next_edge);
if(farthest_in_queue[u]==v){
const int x= u,not_me= x;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:73*/

;
}else break;
}


/*:72*/


candidate[0]= next_edge;
/*72:*/


while(1){
next_edge= pq_delete_min(pq_edge);
if(next_edge==NULL)break;
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0)continue;
if(mate[v]>=0){
/*73:*/


if(E2_case){
next_edge->other_end= E2_nn(u);
next_edge->len= cost(u,next_edge->other_end);
pq_insert(pq_edge,next_edge);
}else{
pool_free(nn_link_pool,next_edge);
if(farthest_in_queue[u]==v){
const int x= u,not_me= x;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:73*/

;
}else break;
}


/*:72*/


if(next_edge&&next_edge->this_end==candidate[0]->other_end
&&next_edge->other_end==candidate[0]->this_end){
pq_edge_t*push_back= next_edge;
/*72:*/


while(1){
next_edge= pq_delete_min(pq_edge);
if(next_edge==NULL)break;
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0)continue;
if(mate[v]>=0){
/*73:*/


if(E2_case){
next_edge->other_end= E2_nn(u);
next_edge->len= cost(u,next_edge->other_end);
pq_insert(pq_edge,next_edge);
}else{
pool_free(nn_link_pool,next_edge);
if(farthest_in_queue[u]==v){
const int x= u,not_me= x;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:73*/

;
}else break;
}


/*:72*/


pq_insert(pq_edge,push_back);
}
candidate[1]= next_edge;
next_edge= candidate[0];
if(candidate[1]){
const int chosen= prng_unif_int(random_stream,3)==0;

next_edge= candidate[chosen];
pq_insert(pq_edge,candidate[1-chosen]);
}
}
#endif

/*:70*//*71:*/


#if !defined(DO_RANDOM)
/*72:*/


while(1){
next_edge= (pq_edge_t *)pq_delete_min(pq_edge);
if(next_edge==NULL)break;
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0)continue;
if(mate[v]>=0){
/*73:*/


if(E2_case){
next_edge->other_end= E2_nn(u);
next_edge->len= cost(u,next_edge->other_end);
pq_insert(pq_edge,next_edge);
}else{
pool_free(nn_link_pool,next_edge);
if(farthest_in_queue[u]==v){
const int x= u,not_me= x;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:73*/

;
}else break;
}


/*:72*/


#endif


/*:71*/


errorif(next_edge==NULL,
"Priority queue exhausted while we expect 2*%d more",num_remaining);
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0||mate[v]>=0){
errorif(mate[u]>=0,"Mate[%d] = %d is not -1",u,mate[u]);
errorif(mate[v]>=0,"Mate[%d] = %d is not -1",v,mate[v]);
}
mate[u]= v;
mate[v]= u;
if(E2_case)E2_hide(u);else{const int c= u;/*35:*/


{const int inv= inv_unsaturated[c];
if(inv<num_unsaturated&&unsaturated[inv]==c){
const int move_city= unsaturated[--num_unsaturated];
unsaturated[inv]= move_city;
inv_unsaturated[move_city]= inv;
/*79:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"             Marking %d as saturated (inv= %d)\n",c,inv);
#endif
#endif


/*:79*/


}
}

/*:35*/

}
if(E2_case)E2_hide(v);else{const int c= v;/*35:*/


{const int inv= inv_unsaturated[c];
if(inv<num_unsaturated&&unsaturated[inv]==c){
const int move_city= unsaturated[--num_unsaturated];
unsaturated[inv]= move_city;
inv_unsaturated[move_city]= inv;
/*79:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"             Marking %d as saturated (inv= %d)\n",c,inv);
#endif
#endif


/*:79*/


}
}

/*:35*/

}
weight+= cost(u,v);
num_remaining--;
}
}
if(E2_case)E2_unhide_all();


/*:69*/


/*23:*/


pool_destroy(nn_link_pool);
pq_destroy(pq_edge);

/*:23*//*31:*/


if(!E2_case){free_mem(farthest_in_queue);mem_deduct(sizeof(int)*n);}


/*:31*//*34:*/


if(!E2_case){
free_mem(unsaturated);mem_deduct(sizeof(int)*n);
free_mem(inv_unsaturated);mem_deduct(sizeof(int)*n);
}

/*:34*//*44:*/


if(!E2_case){free_mem(nn_work);mem_deduct(sizeof(pq_edge_t)*n);}


/*:44*//*47:*/


#if defined(DO_TOUR)
free_mem(adj);
#endif

/*:47*//*50:*/


#if defined(DO_TOUR)
free_mem(tail);mem_deduct(sizeof(int)*n);
#endif

/*:50*//*57:*/


#if defined(DO_RANDOM)
prng_free(random_stream);
#endif

/*:57*/


#undef DO_MATCHING
}
break;

/*:66*//*67:*/


case CONSTRUCT_GREEDY_RANDOM:
{
const int E2_case= E2_supports(tsp_instance);
#define DO_RANDOM
#define DO_MATCHING
/*21:*/


pool_t*nn_link_pool= pool_create(sizeof(pq_edge_t),n);
pq_t*pq_edge= pq_create_size(cmp_pq_edge,n);

/*:21*//*29:*/


int*farthest_in_queue= NULL;

/*:29*//*32:*/


int*unsaturated= NULL,num_unsaturated= 0,*inv_unsaturated= NULL;

/*:32*//*37:*/


int sqrt_n= (int)sqrt((double)n);

/*:37*//*43:*/


pq_edge_t*nn_work= NULL;


/*:43*//*46:*/


#if defined(DO_TOUR)
adj_entry_t*adj= new_arr_of(adj_entry_t,n);
#endif

/*:46*//*49:*/


#if defined(DO_TOUR)
int*tail= new_arr_of(int,n);
#endif

/*:49*//*55:*/


#if defined(DO_RANDOM)
prng_t*random_stream= NULL;
#endif

/*:55*/


/*22:*/


errorif(pq_edge==NULL,"Couldn't create the priority queue!");
pq_set_print_func(pq_edge,pq_edge_print);

/*:22*//*27:*/


if(E2_case){
int i;
for(i= 0;i<n;i++){
pq_edge_t*e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->this_end= i;
e->other_end= E2_nn(i);
e->len= cost(i,e->other_end);
pq_insert(pq_edge,e);
/*48:*/


#if defined(DO_TOUR)
adj[i].count= 0;
#endif

/*:48*//*51:*/


#if defined(DO_TOUR)
tail[i]= i;
#endif

/*:51*//*68:*/


#if defined(DO_MATCHING)
mate[i]= -1;
#endif


/*:68*/


}
}else{
/*30:*/


farthest_in_queue= new_arr_of(int,n);

/*:30*//*33:*/


unsaturated= new_arr_of(int,n);
inv_unsaturated= new_arr_of(int,n);
num_unsaturated= n;
{int i;
for(i= 0;i<n;i++)inv_unsaturated[i]= unsaturated[i]= i;
}

/*:33*//*36:*/


{
int i;
nn_work= new_arr_of(pq_edge_t,n);
for(i= 0;i<n;i++){
const int x= i,not_me= -1;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


/*48:*/


#if defined(DO_TOUR)
adj[i].count= 0;
#endif

/*:48*//*51:*/


#if defined(DO_TOUR)
tail[i]= i;
#endif

/*:51*//*68:*/


#if defined(DO_MATCHING)
mate[i]= -1;
#endif


/*:68*/


}
}

/*:36*/


}

/*:27*//*56:*/


#if defined(DO_RANDOM)
random_stream= prng_new(PRNG_DEFAULT,random_seed^(6502*6510));
#endif

/*:56*/


/*69:*/


{
int num_remaining= n/2;
while(num_remaining>0){
int u,v;
pq_edge_t*next_edge;
/*70:*/


#if defined(DO_RANDOM)
{
pq_edge_t*candidate[2];
/*72:*/


while(1){
next_edge= (pq_edge_t *)pq_delete_min(pq_edge);
if(next_edge==NULL)break;
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0)continue;
if(mate[v]>=0){
/*73:*/


if(E2_case){
next_edge->other_end= E2_nn(u);
next_edge->len= cost(u,next_edge->other_end);
pq_insert(pq_edge,next_edge);
}else{
pool_free(nn_link_pool,next_edge);
if(farthest_in_queue[u]==v){
const int x= u,not_me= x;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:73*/

;
}else break;
}


/*:72*/


candidate[0]= next_edge;
/*72:*/


while(1){
next_edge= (pq_edge_t *)pq_delete_min(pq_edge);
if(next_edge==NULL)break;
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0)continue;
if(mate[v]>=0){
/*73:*/


if(E2_case){
next_edge->other_end= E2_nn(u);
next_edge->len= cost(u,next_edge->other_end);
pq_insert(pq_edge,next_edge);
}else{
pool_free(nn_link_pool,next_edge);
if(farthest_in_queue[u]==v){
const int x= u,not_me= x;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:73*/

;
}else break;
}


/*:72*/


if(next_edge&&next_edge->this_end==candidate[0]->other_end
&&next_edge->other_end==candidate[0]->this_end){
pq_edge_t*push_back= next_edge;
/*72:*/


while(1){
next_edge= (pq_edge_t *)pq_delete_min(pq_edge);
if(next_edge==NULL)break;
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0)continue;
if(mate[v]>=0){
/*73:*/


if(E2_case){
next_edge->other_end= E2_nn(u);
next_edge->len= cost(u,next_edge->other_end);
pq_insert(pq_edge,next_edge);
}else{
pool_free(nn_link_pool,next_edge);
if(farthest_in_queue[u]==v){
const int x= u,not_me= x;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= (pq_edge_t *)pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:73*/

;
}else break;
}


/*:72*/


pq_insert(pq_edge,push_back);
}
candidate[1]= next_edge;
next_edge= candidate[0];
if(candidate[1]){
const int chosen= prng_unif_int(random_stream,3)==0;

next_edge= candidate[chosen];
pq_insert(pq_edge,candidate[1-chosen]);
}
}
#endif

/*:70*//*71:*/


#if !defined(DO_RANDOM)
/*72:*/


while(1){
next_edge= pq_delete_min(pq_edge);
if(next_edge==NULL)break;
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0)continue;
if(mate[v]>=0){
/*73:*/


if(E2_case){
next_edge->other_end= E2_nn(u);
next_edge->len= cost(u,next_edge->other_end);
pq_insert(pq_edge,next_edge);
}else{
pool_free(nn_link_pool,next_edge);
if(farthest_in_queue[u]==v){
const int x= u,not_me= x;
/*38:*/


if(num_unsaturated>sqrt_n){
/*41:*/


{
int i,num_chosen,farthest_city;
size_t w;
length_t farthest_len;
/*81:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: Select fresh neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif

/*:81*/


for(i= w= 0;i<num_unsaturated;i++){
const int c= unsaturated[i];
if(c==x||c==not_me)continue;
nn_work[w].this_end= x;
nn_work[w].other_end= c;
nn_work[w].len= cost(x,c);
w++;
}
num_chosen= min(w,init_max_per_city_nn_cache);
errorif(num_chosen==0,"Bug!");
(void)select_range(nn_work,w,sizeof(pq_edge_t),cmp_pq_edge,
0,num_chosen,0);
farthest_len= nn_work[num_chosen-1].len;
farthest_city= nn_work[num_chosen-1].other_end;
for(i= 0;i<num_chosen;i++){
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
*e= nn_work[i];
pq_insert(pq_edge,e);
if(farthest_len<e->len){
farthest_city= e->other_end;
farthest_len= e->len;
}
}
farthest_in_queue[x]= farthest_city;
}

/*:41*/

;
}else{
/*39:*/


{int i;
/*78:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"construct: All neighbours for %d, excepting %d\n",
x,not_me);fflush(stderr);
#endif
#endif


/*:78*/


for(i= 0;i<num_unsaturated;i++){
const int y= unsaturated[i];
length_t len;
if(y==x||y==not_me)continue;
len= cost(x,y);
/*40:*/


{
pq_edge_t*e;
e= pool_alloc(nn_link_pool);
e->len= len;
e->this_end= x;
e->other_end= y;
pq_insert(pq_edge,e);
/*80:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 750
fprintf(stderr,"construct: alloc new link (%d,%d) "
length_t_native_spec"\n",
e->this_end,e->other_end,length_t_native_pcast(e->len));fflush(stderr);
#endif
#endif

/*:80*/


}


/*:40*/


}
}


/*:39*/


}

/*:38*/


}
}

/*:73*/

;
}else break;
}


/*:72*/


#endif


/*:71*/


errorif(next_edge==NULL,
"Priority queue exhausted while we expect 2*%d more",num_remaining);
u= next_edge->this_end;
v= next_edge->other_end;
if(mate[u]>=0||mate[v]>=0){
errorif(mate[u]>=0,"Mate[%d] = %d is not -1",u,mate[u]);
errorif(mate[v]>=0,"Mate[%d] = %d is not -1",v,mate[v]);
}
mate[u]= v;
mate[v]= u;
if(E2_case)E2_hide(u);else{const int c= u;/*35:*/


{const int inv= inv_unsaturated[c];
if(inv<num_unsaturated&&unsaturated[inv]==c){
const int move_city= unsaturated[--num_unsaturated];
unsaturated[inv]= move_city;
inv_unsaturated[move_city]= inv;
/*79:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"             Marking %d as saturated (inv= %d)\n",c,inv);
#endif
#endif


/*:79*/


}
}

/*:35*/

}
if(E2_case)E2_hide(v);else{const int c= v;/*35:*/


{const int inv= inv_unsaturated[c];
if(inv<num_unsaturated&&unsaturated[inv]==c){
const int move_city= unsaturated[--num_unsaturated];
unsaturated[inv]= move_city;
inv_unsaturated[move_city]= inv;
/*79:*/


#if defined(CONSTRUCT_MAX_VERBOSE)
#if CONSTRUCT_MAX_VERBOSE >= 500
fprintf(stderr,"             Marking %d as saturated (inv= %d)\n",c,inv);
#endif
#endif


/*:79*/


}
}

/*:35*/

}
weight+= cost(u,v);
num_remaining--;
}
}
if(E2_case)E2_unhide_all();


/*:69*/


/*23:*/


pool_destroy(nn_link_pool);
pq_destroy(pq_edge);

/*:23*//*31:*/


if(!E2_case){free_mem(farthest_in_queue);mem_deduct(sizeof(int)*n);}


/*:31*//*34:*/


if(!E2_case){
free_mem(unsaturated);mem_deduct(sizeof(int)*n);
free_mem(inv_unsaturated);mem_deduct(sizeof(int)*n);
}

/*:34*//*44:*/


if(!E2_case){free_mem(nn_work);mem_deduct(sizeof(pq_edge_t)*n);}


/*:44*//*47:*/


#if defined(DO_TOUR)
free_mem(adj);
#endif

/*:47*//*50:*/


#if defined(DO_TOUR)
free_mem(tail);mem_deduct(sizeof(int)*n);
#endif

/*:50*//*57:*/


#if defined(DO_RANDOM)
prng_free(random_stream);
#endif

/*:57*/


#undef DO_MATCHING
#undef DO_RANDOM
}
break;

/*:67*/


default:
errorif(1,"Unrecognized matching construction algorithm %d",alg);
}
return weight;
}

/*:62*/


const char*construct_rcs_id= "$Id: construct.w,v 1.141 1998/10/10 19:26:57 neto Exp neto $";

/*:1*/
