/*6:*/



#ifndef gb_next_rand
#define gb_next_rand() (*gb_fptr>=0?*gb_fptr--:gb_flip_cycle())
#endif

 extern long*gb_fptr;

extern long gb_flip_cycle(void);


/*:6*//*11:*/



extern void gb_init_rand(long seed);


/*:11*//*13:*/



extern long gb_unif_rand(long m);

/*:13*//*14:*/


#define gb_prng_next_rand(PRNG) (*(PRNG)->fptr>=0? *(PRNG)->fptr--: gb_prng_cycle((PRNG)))

typedef struct{
long*fptr;
long A[56];
}gb_prng_t;

gb_prng_t*gb_prng_new(long seed);
long gb_prng_cycle(gb_prng_t*prng);
long gb_prng_unif_rand(gb_prng_t*prng,long m);
void gb_prng_free(gb_prng_t*prng);

/*:14*/
