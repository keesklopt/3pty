/*4:*/


extern const char*resource_rcs_id;
/*6:*/


void resource_setup(const int);

/*:6*//*11:*/


void resource_cleanup(void);

/*:11*//*13:*/


int resource_mark(const char*);


/*:13*//*16:*/


void resource_report(FILE*,int,int);


/*:16*//*18:*/


double resource_user_tick_from(int marknum);
double resource_user_tick(void);


/*:18*//*20:*/


void resource_abnormal_exit_output(void);

/*:20*/



/*:4*/
