/*8:*/


#if !defined(_PRNG_H_)
#define _PRNG_H_
extern const char*prng_rcs_id;
#if !defined(HAVE_NRAND48)
#define HAVE_NRAND48 0
#endif
/*14:*/

#include "error.h"

/*:14*//*28:*/


#include "gb_flip.h"

/*:28*//*38:*/


#if HAVE_NRAND48
#include <stdlib.h>
#define FIXINCLUDES_NEED_NRAND48
#include "fixincludes.h"
#undef FIXINCLUDES_NEED_NRAND48
#endif

/*:38*/


/*11:*/


typedef enum{/*27:*/


PRNG_GB_FLIP,

/*:27*//*33:*/


PRNG_NRAND48,

/*:33*//*42:*/


PRNG_NUM_KINDS,
PRNG_DEFAULT= PRNG_GB_FLIP

/*:42*/

}prng_kind_t;
typedef struct{
prng_kind_t kind;
int seed;
/*20:*/


int have_saved_normal;
double saved_normal;

/*:20*/


union{
/*29:*/


gb_prng_t*gb_flipper;

/*:29*//*34:*/


unsigned short nrand48_state[3];

/*:34*/


}gen;
}prng_t;

/*:11*/


/*9:*/


prng_t*prng_new(prng_kind_t kind,int seed);
double prng_normal(prng_t*g,double mean,double stddev);
void prng_free(prng_t*g);
int prng_kind_from_name(char*name);
const char*prng_name_from_kind(int kind);
int prng_kind(prng_t*g);

/*:9*/


#include "prngconfig.h"

#ifndef _WIN32
#if PRNG_DESIRE_INLINING && COMPILER_SUPPORTS_INLINE
#define inline_proto inline extern
#define inline_def inline extern
#endif
#include <math.h> 
/*10:*/

#define inline_proto inline extern
#define inline_def inline extern
inline_proto int prng_unif_int(prng_t*g,long m);
inline_proto double prng_unif_double_01(prng_t*g);
inline_proto double prng_unif_double_range(prng_t*g,double a,double b);

/*:10*/


/*13:*/


inline_def int
prng_unif_int(prng_t*g,long m)
{
switch(g->kind){
/*32:*/


case PRNG_GB_FLIP:
return gb_prng_unif_rand(g->gen.gb_flipper,m);


/*:32*//*37:*/


case PRNG_NRAND48:
#if HAVE_NRAND48
{unsigned long t= 0x80000000UL-(0x80000000UL%m);
long r;
do{
r= nrand48(g->gen.nrand48_state);
}while(t<=(unsigned long)r);
return r%m;
}
#else
errorif(1,"Sorry, nrand48 is not available on this system.");
return 0;
#endif
break;

/*:37*/


default:errorif(1,"No such kind of random number generator: %d",g->kind);
return 0;
}
}

/*:13*//*17:*/


inline_def double
prng_unif_double_01(prng_t*g)
{
#if FLT_RADIX==2
double i= prng_unif_int(g,1<<30);
double j= prng_unif_int(g,1<<(DBL_MANT_DIG-30));
return ldexp(ldexp(i,-30)+j,-(DBL_MANT_DIG-30));
#else 
double x= 0.0,full= 0.0;
while(full<1.0){
x= ldexp(x+prng_unif_int(g,2),-1);
full= ldexp(1.0+full,-1);
}
return x;
#endif
}

/*:17*//*19:*/


inline_def double
prng_unif_double_range(prng_t*g,double a,double b)
{
const double t= prng_unif_double_01(g);
return a+t*(b-a);
}


/*:19*/


#undef inline_proto
#undef inline_def
#else
#define inline_proto
/*10:*/


inline_proto int prng_unif_int(prng_t*g,long m);
inline_proto double prng_unif_double_01(prng_t*g);
inline_proto double prng_unif_double_range(prng_t*g,double a,double b);

/*:10*/


#undef inline_proto
#endif

#endif 

/*:8*/
