/*6:*/




const char*prng_rcs_id= "$Id: prng.w,v 1.12 1998/08/27 19:13:28 neto Exp neto $";
#include <config.h>
/*16:*/


#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

/*:16*//*18:*/


#include <float.h>
#include <math.h>



/*:18*//*25:*/


#ifdef __linux__
#include <errno.h>
#endif
#include <string.h>
//#include <io.h>



/*:25*/


/*15:*/


#include "memory.h"

/*:15*/


#include "prng.h"

#define inline_def  
/*13:*/


#ifndef __linux__
inline_def int
prng_unif_int(prng_t*g,long m)
{
switch(g->kind){
/*32:*/


case PRNG_GB_FLIP:
return gb_prng_unif_rand(g->gen.gb_flipper,m);


/*:32*//*37:*/


case PRNG_NRAND48:
#if HAVE_NRAND48
{unsigned long t= 0x80000000UL-(0x80000000UL%m);
long r;
do{
r= nrand48(g->gen.nrand48_state);
}while(t<=(unsigned long)r);
return r%m;
}
#else
errorif(1,"Sorry, nrand48 is not available on this system.");
return 0;
#endif
break;

/*:37*/


default:errorif(1,"No such kind of random number generator: %d",g->kind);
return 0;
}
}


/*:13*//*17:*/


inline_def double
prng_unif_double_01(prng_t*g)
{
#if FLT_RADIX==2
double i= prng_unif_int(g,1<<30);
double j= prng_unif_int(g,1<<(DBL_MANT_DIG-30));
return ldexp(ldexp(i,-30)+j,-(DBL_MANT_DIG-30));
#else 
double x= 0.0,full= 0.0;
while(full<1.0){
x= ldexp(x+prng_unif_int(g,2),-1);
full= ldexp(1.0+full,-1);
}
return x;
#endif
}

/*:17*//*19:*/


inline_def double
prng_unif_double_range(prng_t*g,double a,double b)
{
const double t= prng_unif_double_01(g);
return a+t*(b-a);
}

/*:19*/

#endif


#undef inline_def
/*12:*/


prng_t*
prng_new(prng_kind_t kind,int seed)
{
prng_t*g= new_of(prng_t);
errorif(g==NULL,"Couldn't allocate a new generic random number generator");
g->kind= kind;
switch(kind){
/*30:*/


case PRNG_GB_FLIP:
g->gen.gb_flipper= gb_prng_new(seed);
errorif(g->gen.gb_flipper==NULL,"Couldn't allocate a new GB_FLIP generator");
break;


/*:30*//*35:*/


case PRNG_NRAND48:
#if HAVE_NRAND48
g->gen.nrand48_state[0]= 0x330E;
g->gen.nrand48_state[1]= seed&0xffff;
g->gen.nrand48_state[2]= (seed>>16)&0xffff;
#else
errorif(1,"Sorry, nrand48 is not available on this system.");
#endif
break;


/*:35*/


default:errorif(1,"No such kind of random number generator: %d",kind);
}
/*21:*/


g->have_saved_normal= 0;

/*:21*/


return g;
}

void
prng_free(prng_t*g)
{
if(g){
switch(g->kind){
/*31:*/


case PRNG_GB_FLIP:
gb_prng_free(g->gen.gb_flipper);
break;

/*:31*//*36:*/


case PRNG_NRAND48:
break;

/*:36*/


default:break;
}
}
}

/*:12*//*22:*/


double
prng_normal(prng_t*g,double mean,double stddev)
{
if(g->have_saved_normal){
g->have_saved_normal= 0;
return mean+stddev*g->saved_normal;
}else{
double v_1,v_2,s,n_1,n_2;
do{
v_1= prng_unif_double_01(g);
v_2= prng_unif_double_01(g);
/*23:*/


v_1= 2*v_1-1;if(v_1>=0)v_1= 1-v_1;
v_2= 2*v_2-1;if(v_2>=0)v_2= 1-v_2;

/*:23*/


s= v_1*v_1+v_2*v_2;
}while(s>=1);
/*24:*/


{double Rprime;
#ifdef __linux__
errno= 0;
#endif
Rprime= sqrt(-2*log(s)/s);
#ifdef __linux__
switch(errno){
case ERANGE:
case EDOM:
n_1= v_1<0?-DBL_MAX:DBL_MAX;
n_2= v_2<0?-DBL_MAX:DBL_MAX;
break;
default:
#endif
n_1= v_1*Rprime;
n_2= v_2*Rprime;
#ifdef __linux__
}
errno= 0;
#endif
}


/*:24*/


g->saved_normal= n_2;
g->have_saved_normal= 1;
return mean+n_1*stddev;
}
}

/*:22*//*40:*/


int
prng_kind_from_name(char*name){
int i;
/*39:*/


const static char*prng_name_map[PRNG_NUM_KINDS]= {
"gb_flip",
"nrand48"
};

/*:39*/


for(i= 0;i<PRNG_NUM_KINDS;i++){
if(0==strcmp(name,prng_name_map[i]))return i;
}
return PRNG_DEFAULT;
}

const char*
prng_name_from_kind(int kind){
/*39:*/


const static char*prng_name_map[PRNG_NUM_KINDS]= {
"gb_flip",
"nrand48"
};

/*:39*/


errorif(kind<0||kind>=PRNG_NUM_KINDS,
"No such kind of pseudo-random number generator: %d",kind);
return prng_name_map[kind];
}

/*:40*//*41:*/


int
prng_kind(prng_t*g){
return g->kind;
}

/*:41*/



/*:6*/
