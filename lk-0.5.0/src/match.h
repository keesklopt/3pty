/*11:*/


#if !defined(_MATCH_H_)
#define _MATCH_H_
extern const char*match_rcs_id;
#if !defined(_PRNG_H_)
#include "prng.h"
#endif
/*12:*/


void match_setup(int n);
void match_cleanup(void);

/*:12*//*18:*/


void match_show(FILE*out);
void match_ps_out(FILE*ps_out,const char*name);

/*:18*//*20:*/


void
match_validate(length_t*validate_len,double*double_validate_len,
double*ordered_double_len,double*raw_len);


/*:20*//*25:*/


length_t match_construct(int alg,long alg_param,const long random_seed);


/*:25*//*50:*/


void match_run(const int backtracking_levels,const int iterations,
prng_t*random_stream);

/*:50*/


#endif

/*:11*/
