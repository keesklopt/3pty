/*3:*/


#if !defined(_PQ_H_)
#define _PQ_H_
extern const char*pq_rcs_id;
/*6:*/


typedef int(*pq_cmp_func_t)(const void*,const void*);

typedef struct{
void**A;
int A_size;
int last_elem_i;
pq_cmp_func_t cmp;
void(*print_func)(FILE*,void*);
}pq_t;

/*:6*/


/*4:*/


pq_t*pq_create(pq_cmp_func_t cmp);
pq_t*pq_create_size(pq_cmp_func_t cmp,int n);
void pq_destroy(pq_t*pq);
void pq_make_empty(pq_t*pq);
void pq_insert(pq_t*pq,void*payload);
void*pq_delete_min(pq_t*pq);
void*pq_min(pq_t*pq);
int pq_empty_func(pq_t*pq);
int pq_size_func(pq_t*pq);
void pq_set_print_func(pq_t*pq,void(*print_func)(FILE*,void*));
void pq_print(pq_t*pq,FILE*out);

/*:4*/


/*5:*/


#define pq_empty(PQ) ((PQ)->last_elem_i == 0)
#define pq_size(PQ)  ((const int)((PQ)->last_elem_i))

/*:5*/


#endif


/*:3*/
