/*1:*/


#include <config.h>
#include "lkconfig.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "length.h"
#include "read.h"
#include "decluster.h"
#include "kdtree.h"
#include "dsort.h"

int noround= 0;
int verbose= 0;
void(*sort)(void*base,size_t nmemb,size_t size,
int(*compar)(const void*,const void*))= dsort;

int
main(int argc,char**argv)
{
char const*TSPLIB_in= "../data/lin105.tsp";
tsp_instance_t*tsp;
decluster_tree_t*mst;
FILE*TSPLIB_file;
int a,b;
if(argc>1){
TSPLIB_in= argv[1];
}
if(NULL==(TSPLIB_file= fopen(TSPLIB_in,"r"))){
fprintf(stderr,"Couldn't open %s for reading\n",TSPLIB_in);
exit(1);
}
tsp= read_tsp_file(TSPLIB_file,NULL,0);
if(E2_supports(tsp)){
E2_create(tsp);
}
mst= decluster_setup(tsp->n);
decluster_mst(tsp,mst);
decluster_preprocess(mst);

while(scanf(" %d %d",&a,&b)==2){
if(a<0||a>=tsp->n){
printf("a=%d is out of range [0,%d)\n",a,tsp->n);
continue;
}
if(b<0||b>=tsp->n){
printf("b=%d is out of range [0,%d)\n",b,tsp->n);
continue;
}
printf("(%d,%d) cost=="length_t_spec" cluster dist=="length_t_spec"\n",
a,b,cost(a,b),decluster_d(a,b));
}
return 0;
}

/*:1*/
