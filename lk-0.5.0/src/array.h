/*5:*/


extern const char*array_rcs_id;
/*14:*/


void array_setup(int num_vertices);
void array_cleanup(void);

/*:14*//*16:*/



int array_next(int a);
int array_prev(int a);

/*:16*//*18:*/


int array_between(int a,int b,int c);

/*:18*//*25:*/


void array_flip(int a,int b,int c,int d);

/*:25*//*27:*/


void array_set(int const*tour);

/*:27*/



/*:5*/
