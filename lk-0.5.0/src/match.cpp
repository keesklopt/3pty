#define make_mates(u,v) ((mate[(u) ]= (v) ) ,(mate[(v) ]= (u) ) ) 
#define dump_core(x) ((verbose%2) ?*(int*) 0= (x) :(s_x= (x) ) ) 
#define basic_swap_mates(a,b,c,d) (make_mates((a) ,(d) ) ,make_mates((b) ,(c) ) ) 
#define cautious_swap_mates(a,b,c,d) { \
s_a= a; \
s_b= b; \
s_c= c; \
s_d= d; \
if(a<0||a>=n) dump_core(41) ; \
if(b<0||b>=n) dump_core(42) ; \
if(c<0||c>=n) dump_core(43) ; \
if(d<0||d>=n) dump_core(44) ; \
if((mate[a]!=b&&mate[b]!=a) ||(mate[c]!=d&&mate[d]!=c) ) dump_core(45) ; \
errorif((mate[a]!=b&&mate[b]!=a) ||(mate[c]!=d&&mate[d]!=c) , \
"swap_mates(%d,%d,%d,%d) aren't pairwise mates",a,b,c,d) ; \
basic_make_mates(a,b,c,d) ; \
} \

#define city_name(I) (1+(original_city_num?original_city_num[I]:I) )  \

#define USE_DECLUSTER (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY \
)  \

#define mark_dirty(CITY) (dirty_add(dirty_set,(CITY) ) )  \

#define DEPTHS_BOUND (n/2+8)  \

#define write_log(A) (change_log[change_log_next++]= (A) )  \

/*4:*/


#include <config.h> 
#include "declevel.h"
#include "lkconfig.h"
/*16:*/


#include <stddef.h> 
#include <stdio.h> 
#include <stdlib.h> 

/*:16*//*37:*/


#include <limits.h> 

/*:37*/


/*13:*/


#include "error.h"
#include "memory.h"
#include "length.h"
#include "read.h"
#include "match.h"

/*:13*//*23:*/


#include "lk.h"


/*:23*//*26:*/


#include "construct.h"

/*:26*//*46:*/


#include "nn.h"

/*:46*//*51:*/


#include "dirty.h"

/*:51*//*63:*/


#include "decluster.h"

/*:63*//*93:*/


#include "milestone.h"

/*:93*//*116:*/


#include "resource.h"


/*:116*/



/*6:*/


#if defined(MATCH_DEBUG)
#define swap_mates(a,b,c,d) cautious_swap_mates(a,b,c,d)
#else
#define swap_mates(a,b,c,d) basic_swap_mates(a,b,c,d)
#endif

/*:6*//*33:*/


#define SPLIT_GAIN_VAR (!(LENGTH_TYPE_IS_EXACT))



/*:33*//*34:*/


#if !SPLIT_GAIN_VAR
#define CAREFUL_OP(LHS,OP,RHS) ((LHS) OP (RHS))
#else
#define CAREFUL_OP(LHS,OP,RHS) ((LHS##_pos) OP (RHS##_with_slop + LHS##_neg))
#endif

/*:34*/


/*44:*/


typedef struct{
length_t gain_for_comparison;
length_t cluster_distance;
#if SPLIT_GAIN_VAR
length_t gain_1_pos,gain_1_neg;
length_t gain_pos,gain_neg;
#else
length_t gain_1;
length_t gain;
#endif
int t2ip1,t2ip2;
int two_i;
}eligible_t;

/*:44*/


/*5:*/


static int*mate= NULL;
static int n= 0;

/*:5*//*28:*/


int*t;

/*:28*/


/*7:*/


#if defined(MATCH_DEBUG)
static int s_a= -1,s_b= -1,s_c= -1,s_d= -1,s_x= -1;
#endif
static void(*prev_cleanup_fn)(void);
static
void
dump_ds(void)
{
#if MATCH_DEBUG
int i;
printf("dumping core: %d\n",s_x);
printf("possibly swapping (%d,%d,%d,%d)\n",s_a,s_b,s_c,s_d);
for(i= 0;i<n;i++){
printf("  mate[%d] = %d\n",i,mate[i]);
}
#endif
if(prev_cleanup_fn)prev_cleanup_fn();
}

/*:7*//*67:*/


static int cmp_eligible(const void*a,const void*b);
static int
cmp_eligible(const void*a,const void*b)
{
const eligible_t*ea= (const eligible_t*)a;
const eligible_t*eb= (const eligible_t*)b;
length_t diff= ea->gain_for_comparison-eb->gain_for_comparison;
return diff<0?1:(diff> 0?-1:0);
}

/*:67*//*109:*/


static void
show_entry_with_string_at_verbose(const eligible_t*e_entry,const char*str,int the_verbose)
{
if(verbose>=the_verbose){
printf("\n%s: %p two_i %d  t2ip1 = %d  t2ip2 = %d "length_t_spec" -> "length_t_spec,
str,
e_entry,
e_entry->two_i,
e_entry->t2ip1,
e_entry->t2ip2,
#if SPLIT_GAIN_VAR
length_t_pcast(e_entry->gain_pos-e_entry->gain_neg),
#else
length_t_pcast(e_entry->gain),
#endif
length_t_pcast(e_entry->gain_for_comparison)
);
fflush(stdout);
}
}

/*:109*/


/*9:*/


void
match_setup(int the_n)
{
errorif(the_n%2,
"Perfect matchings require an even number of vertices; given %d\n",the_n);
n= the_n;
mate= new_arr_of(int,n);
mate[0]= -1;
/*29:*/


t= new_arr_of(int,n+1);

/*:29*/


}


/*:9*//*10:*/


void
match_cleanup(void)
{
if(mate){free_mem(mate);mem_deduct(n*sizeof(int));}
/*30:*/


if(t){free_mem(t);mem_deduct(sizeof(int)*(n+1));}

/*:30*/


n= 0;
}

/*:10*//*15:*/


void
match_show(FILE*out)
{
length_t weight= 0;
int i;
errorif(mate==NULL||mate[0]==-1,
"Tried to print a matching before it is initialized");
fprintf(out,"Perfect matching:\n");
for(i= 0;i<n;i++){
const int i_mate= mate[i];
if(i_mate> i){
fprintf(out,"%d %d\n",city_name(i),city_name(i_mate));
weight+= cost(i,i_mate);
}
}
fprintf(out,"Length: "length_t_spec"\n",length_t_pcast(weight));
}


/*:15*//*17:*/


void
match_ps_out(FILE*ps_out,const char*name)
{
length_t weight= 0;
int i;
errorif(mate==NULL||mate[0]==-1,
"Tried to print a matching before it is initialized");
fprintf(ps_out,"%%Here's a weighted perfect matching\n");
for(i= 0;i<n;i++){
const int i_mate= mate[i];
if(i_mate> i){
weight+= cost(i,i_mate);
fprintf(ps_out,"%f x %f y %f x %f y rawedge\n",
tsp_instance->coord[i].x[0],
tsp_instance->coord[i].x[1],
tsp_instance->coord[i_mate].x[0],
tsp_instance->coord[i_mate].x[1]);
}
}

fprintf(ps_out,"(%s matching, weight "length_t_native_spec") title\n",
name,length_t_native_pcast(weight));
fprintf(ps_out,"(%s) comment\n",tsp_instance->comment);
fprintf(ps_out,"showpage\n");fflush(ps_out);
}

/*:17*//*19:*/


void
match_validate(length_t*validate_len,double*double_validate_len,
double*ordered_double_len,double*raw_len)
{
length_t my_validate_len= 0;
double my_double_validate_len= 0,my_ordered_double_len= 0,my_raw_len= 0;
double*length= new_arr_of(double,n/2);
double*raw_length= new_arr_of(double,n/2);
length_t*length_t_length= new_arr_of(length_t,n/2);

int i,w;
errorif(mate==NULL||mate[0]==-1,
"Tried to validate a matching before it is initialized");

/*21:*/


{
int i,*visited= new_arr_of_zero(int,n);
for(i= 0;i<n;i++){
const int i_mate= mate[i];
errorif(i_mate<0||i_mate>=n,
"Mate of %d is %d, out of range\n",i_mate);
visited[i]++;
visited[i_mate]++;
}
for(i= 0;i<n;i++){
errorif(visited[i]!=2,"Vertex %d visited %d times, not 2 times",i,visited[i]);
}

free_mem(visited);
}


/*:21*/



for(i= 0,w= 0;i<n;i++){
const int i_mate= mate[i];
if(i_mate> i){
length_t_length[w]= cost(i,i_mate);
length[w]= (double)length_t_length[w];
my_double_validate_len+= length[w];
switch(tsp_instance->edge_weight_type){
case EUC_2D:
case CEIL_2D:
raw_length[w]= cost_from_euc2d_raw(i,i_mate);
break;
default:raw_length[w]= length[w];
}
w++;
}
}
/*22:*/


sort(length,(unsigned)n/2,sizeof(double),lk_double_cmp);
sort(raw_length,(unsigned)n/2,sizeof(double),lk_double_cmp);
sort(length_t_length,(unsigned)n/2,sizeof(length_t),lk_length_t_cmp);
my_validate_len= 0;
my_ordered_double_len= my_raw_len= 0.0;
for(i= 0;i<n/2;i++){
my_validate_len+= length_t_length[i];
my_ordered_double_len+= length[i];
my_raw_len+= raw_length[i];
}

/*:22*/


free_mem(length);
free_mem(raw_length);
free_mem(length_t_length);
mem_deduct((n/2)*sizeof(length_t)+sizeof(double)+sizeof(double));
*validate_len= my_validate_len;
*double_validate_len= my_double_validate_len;
*ordered_double_len= my_ordered_double_len;
*raw_len= my_raw_len;
}
/*:19*//*24:*/


length_t
match_construct(int alg,long alg_param,const long random_seed)
{
errorif(mate==NULL,
"Tried to construct a matching before space is allocated");
return construct_matching(n,mate,alg,alg_param,random_seed);
}

/*:24*//*49:*/


void match_run(const int backtracking_levels,const int iterations,
prng_t*random_stream)
{
int iteration;
/*31:*/


int two_i;


/*:31*//*32:*/


#if SPLIT_GAIN_VAR
length_t cum_gain_pos,cum_gain_neg;
#else
length_t cum_gain;
#endif


/*:32*//*35:*/


length_t best_gain;
int best_two_i,best_exit_a,best_exit_b;

/*:35*//*38:*/


#if !LENGTH_TYPE_IS_EXACT
length_t best_gain_with_slop;
length_t instance_epsilon;
#endif

/*:38*//*45:*/


eligible_t**e;
int*en,*ei;

/*:45*//*58:*/


int keep_going;

/*:58*//*71:*/


const int max_two_i= 
(max_generic_flips==INT_MAX?INT_MAX:2*(backtracking_levels+max_generic_flips));

/*:71*//*77:*/


#if MATCH_REPORT_DEPTHS
int probe_depth_less_2,move_depth;
#endif


/*:77*//*78:*/


#if MATCH_REPORT_DEPTHS
int*m_depths= new_arr_of_zero(int,DEPTHS_BOUND);
int*p_depths= new_arr_of_zero(int,DEPTHS_BOUND);
#endif

/*:78*//*86:*/


int*change_log= NULL,change_log_max_alloc,change_log_next= 0;

/*:86*//*88:*/


length_t previous_incumbent_len= 0;


/*:88*//*92:*/


milestone_state_t ms_lower_bound;
milestone_state_t ms_upper_bound;

/*:92*/


dirty_set_t*dirty_set;
/*54:*/


dirty_set= dirty_create(n,1,prng_unif_int(random_stream,n),__FILE__,__LINE__);

/*:54*/



/*8:*/


prev_cleanup_fn= error_precleanup_stats;
error_precleanup_stats= dump_ds;


/*:8*//*40:*/


/*39:*/


#if !LENGTH_TYPE_IS_EXACT
instance_epsilon= incumbent_len*LENGTH_MACHINE_EPSILON;
#endif

/*:39*/



/*:40*//*47:*/


{int l;
e= new_arr_of(eligible_t*,1+2*backtracking_levels);
en= new_arr_of(int,1+2*backtracking_levels);
ei= new_arr_of(int,1+2*backtracking_levels);
for(l= 2;l<=2*backtracking_levels;l+= 2)
e[l]= new_arr_of(eligible_t,nn_max_bound);
}

/*:47*//*84:*/


change_log_max_alloc= 10000;
change_log= new_arr_of(int,change_log_max_alloc);

/*:84*//*94:*/


milestone_initialize(&ms_lower_bound,lower_bound_name,(length_t)lower_bound_value,
begin_data_structures_mark);
milestone_initialize(&ms_upper_bound,upper_bound_name,(length_t)upper_bound_value,
begin_data_structures_mark);
milestone_show_initial(&ms_lower_bound,incumbent_len);
milestone_show_initial(&ms_upper_bound,incumbent_len);

/*:94*/


for(iteration= 0;iteration<iterations;iteration++){
int dirty;
while((dirty= dirty_remove(dirty_set))>=0){
t[1]= dirty;
t[2]= mate[t[1]];
two_i= 2;
/*36:*/


best_gain= 0;
best_two_i= 0;
best_exit_a= best_exit_b= INT_MAX;

/*:36*//*41:*/


#if SPLIT_GAIN_VAR
best_gain_with_slop= instance_epsilon;
#endif



/*:41*//*52:*/


#if SPLIT_GAIN_VAR
cum_gain_pos= cost(t[1],t[2]);
cum_gain_neg= 0;
#else
cum_gain= cost(t[1],t[2]);
#endif

/*:52*//*76:*/


#if MATCH_REPORT_DEPTHS
probe_depth_less_2= -2;
move_depth= 0;
#endif

/*:76*/


/*57:*/


/*110:*/


#if MATCH_MAX_VERBOSE >= 105
if(verbose>=105){
printf("*");
}
#endif

/*:110*/


ei[two_i]= -2;
en[two_i]= -1;
{keep_going= 1;
while(keep_going){
if(two_i<=2*backtracking_levels){
if(ei[two_i]==-2){
/*59:*/


{int j,num_candidates,*candidates;
candidates= nn_list(t[two_i],&num_candidates);
for(en[two_i]= j= 0;j<num_candidates;j++){
const int t2ip1= candidates[j],t2ip2= mate[t2ip1];
eligible_t*e_entry= &e[two_i][en[two_i]];
/*61:*/


{
int j,tabu= 0;
if(t2ip2==t[two_i]){/*104:*/


#if MATCH_MAX_VERBOSE >= 500
if(verbose>=500){
printf("X");
fflush(stdout);
}
#endif

/*:104*/

continue;}
for(j= 2;j<two_i;j+= 2){
if((t2ip1==t[j]&&t2ip2==t[j+1])
||(t2ip1==t[j+1]&&t2ip2==t[j])){tabu= 1;break;}
}
if(tabu){/*104:*/


#if MATCH_MAX_VERBOSE >= 500
if(verbose>=500){
printf("X");
fflush(stdout);
}
#endif

/*:104*/

continue;}
}


/*:61*/


/*62:*/


{
const length_t this_neg= cost(t[two_i],t2ip1),
this_pos= cost(t2ip1,t2ip2),this_net= this_pos-this_neg;

e_entry->t2ip1= t2ip1;
e_entry->t2ip2= t2ip2;
#if SPLIT_GAIN_VAR
e_entry->gain_1_neg= cum_gain_neg+this_neg;
e_entry->gain_1_pos= cum_gain_pos;
e_entry->gain_neg= cum_gain_neg+this_neg;
e_entry->gain_pos= cum_gain_pos+this_pos;
#else
e_entry->gain_1= cum_gain-this_neg;
e_entry->gain= cum_gain+this_net;
#endif
#if USE_DECLUSTER
e_entry->cluster_distance= decluster_d(t[1],t2ip2);
e_entry->gain_for_comparison= this_net-e_entry->cluster_distance;
#else
e_entry->gain_for_comparison= this_net;
#endif
e_entry->two_i= two_i;
}

/*:62*/


/*64:*/


{
const length_t cluster_dist= (USE_DECLUSTER?e_entry->cluster_distance:0);
if(CAREFUL_OP(e_entry->gain,<=,cluster_dist+best_gain)
||CAREFUL_OP(e_entry->gain_1,<=,best_gain)){
/*105:*/


#if MATCH_MAX_VERBOSE >= 500
if(verbose>=500){
printf("<");
fflush(stdout);
}
#endif

/*:105*/


continue;
}
}


/*:64*/


/*106:*/


#if MATCH_MAX_VERBOSE >= 500
show_entry_with_string_at_verbose(e_entry,"accept",500);
#endif

/*:106*/


/*65:*/


{
#if SPLIT_GAIN_VAR
const length_t possible_best_gain_pos= e_entry->gain_pos,
possible_best_gain_neg= e_entry->gain_neg+cost(t[1],e_entry->t2ip2);
#else
const length_t possible_best_gain= e_entry->gain-cost(t[1],e_entry->t2ip2);
#endif

if(CAREFUL_OP(possible_best_gain,> ,best_gain)){
best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
#if SPLIT_GAIN_VAR
best_gain= possible_best_gain_pos-possible_best_gain_neg;
#else
best_gain= possible_best_gain;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif
/*99:*/


#if MATCH_MAX_VERBOSE >= 125
if(verbose>=125){
printf(" verbose %d ===two_i %d best_two_i %d best_exit_a %d best_exit_b %d\n",
verbose,
two_i,best_two_i,best_exit_a,best_exit_b);
fflush(stdout);
}
#endif

/*:99*/


}
}

/*:65*/


en[two_i]++;
}
/*66:*/


sort(e[two_i],(size_t)en[two_i],sizeof(eligible_t),cmp_eligible);

/*:66*/


ei[two_i]= 0;
}

/*:59*//*60:*/


if(two_i<2*backtracking_levels){
ei[two_i+2]= -2;
en[two_i+2]= -1;
}


/*:60*/


}
if(ei[two_i]<en[two_i]){
const eligible_t*e_entry= e[two_i]+ei[two_i];
/*111:*/


#if MATCH_MAX_VERBOSE >= 109
if(verbose>=109){
printf("g");
}
#endif

/*:111*/


ei[two_i]++;
/*68:*/


/*108:*/


#if MATCH_MAX_VERBOSE >= 500
show_entry_with_string_at_verbose(e_entry,"doing move",500);
#endif

/*:108*/


#if SPLIT_GAIN_VAR
cum_gain_pos= e_entry->gain_pos;
cum_gain_neg= e_entry->gain_neg;
#else
cum_gain= e_entry->gain;
#endif
#if defined(MATCH_DEBUG)
if(e_entry->two_i!=two_i)dump_core(96);
#endif

t[two_i+1]= e_entry->t2ip1;
t[two_i+2]= e_entry->t2ip2;
swap_mates(t[1],t[two_i],t[two_i+1],t[two_i+2]);
two_i+= 2;
/*74:*/


#if MATCH_REPORT_DEPTHS
if(two_i> probe_depth_less_2)probe_depth_less_2= two_i;
#endif


/*:74*/


/*98:*/


#if MATCH_MAX_VERBOSE >= 125
if(verbose> 125){
int i;
printf("\n");
for(i= 0;i<two_i;i++)putchar(' ');
printf("+");
fflush(stdout);
}
#endif


/*:98*/


/*101:*/


#if MATCH_MAX_VERBOSE >= 150
if(verbose>=150){
int i;
printf("t:");
for(i= 1;i<=two_i;i++)printf(" %d",t[i]);
printf("\n");
fflush(stdout);
}
#endif

/*:101*/



/*:68*/


}else if(best_two_i){keep_going= 0;}
else{/*69:*/


if(two_i> 2){
if(two_i<=2*backtracking_levels){
ei[two_i]= -2;
en[two_i]= -1;
}
two_i-= 2;
/*97:*/


#if MATCH_MAX_VERBOSE >= 125
if(verbose> 125){
int i;
printf("\n");
for(i= 0;i<two_i;i++)putchar(' ');
printf("-");
fflush(stdout);
}
#endif

/*:97*/


swap_mates(t[1],t[two_i+2],t[two_i+1],t[two_i]);
/*101:*/


#if MATCH_MAX_VERBOSE >= 150
if(verbose>=150){
int i;
printf("t:");
for(i= 1;i<=two_i;i++)printf(" %d",t[i]);
printf("\n");
fflush(stdout);
}
#endif

/*:101*/


}else keep_going= 0;



/*:69*/

}
}else{
/*112:*/


#if MATCH_MAX_VERBOSE >= 109
if(verbose>=109){
printf("G");
}
#endif

/*:112*/


/*70:*/


{int none_eligible= 1;
if(two_i<=max_two_i){
/*72:*/


{
eligible_t best_move;
int j,num_candidates,*candidates= nn_list(t[two_i],&num_candidates);
for(j= 0;j<num_candidates;j++){
const int t2ip1= candidates[j],t2ip2= mate[t2ip1];
eligible_t trial_move,*e_entry= &trial_move;
/*61:*/


{
int j,tabu= 0;
if(t2ip2==t[two_i]){/*104:*/


#if MATCH_MAX_VERBOSE >= 500
if(verbose>=500){
printf("X");
fflush(stdout);
}
#endif

/*:104*/

continue;}
for(j= 2;j<two_i;j+= 2){
if((t2ip1==t[j]&&t2ip2==t[j+1])
||(t2ip1==t[j+1]&&t2ip2==t[j])){tabu= 1;break;}
}
if(tabu){/*104:*/


#if MATCH_MAX_VERBOSE >= 500
if(verbose>=500){
printf("X");
fflush(stdout);
}
#endif

/*:104*/

continue;}
}


/*:61*/


/*62:*/


{
const length_t this_neg= cost(t[two_i],t2ip1),
this_pos= cost(t2ip1,t2ip2),this_net= this_pos-this_neg;

e_entry->t2ip1= t2ip1;
e_entry->t2ip2= t2ip2;
#if SPLIT_GAIN_VAR
e_entry->gain_1_neg= cum_gain_neg+this_neg;
e_entry->gain_1_pos= cum_gain_pos;
e_entry->gain_neg= cum_gain_neg+this_neg;
e_entry->gain_pos= cum_gain_pos+this_pos;
#else
e_entry->gain_1= cum_gain-this_neg;
e_entry->gain= cum_gain+this_net;
#endif
#if USE_DECLUSTER
e_entry->cluster_distance= decluster_d(t[1],t2ip2);
e_entry->gain_for_comparison= this_net-e_entry->cluster_distance;
#else
e_entry->gain_for_comparison= this_net;
#endif
e_entry->two_i= two_i;
}

/*:62*/


/*64:*/


{
const length_t cluster_dist= (USE_DECLUSTER?e_entry->cluster_distance:0);
if(CAREFUL_OP(e_entry->gain,<=,cluster_dist+best_gain)
||CAREFUL_OP(e_entry->gain_1,<=,best_gain)){
/*105:*/


#if MATCH_MAX_VERBOSE >= 500
if(verbose>=500){
printf("<");
fflush(stdout);
}
#endif

/*:105*/


continue;
}
}


/*:64*/


/*65:*/


{
#if SPLIT_GAIN_VAR
const length_t possible_best_gain_pos= e_entry->gain_pos,
possible_best_gain_neg= e_entry->gain_neg+cost(t[1],e_entry->t2ip2);
#else
const length_t possible_best_gain= e_entry->gain-cost(t[1],e_entry->t2ip2);
#endif

if(CAREFUL_OP(possible_best_gain,> ,best_gain)){
best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
#if SPLIT_GAIN_VAR
best_gain= possible_best_gain_pos-possible_best_gain_neg;
#else
best_gain= possible_best_gain;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif
/*99:*/


#if MATCH_MAX_VERBOSE >= 125
if(verbose>=125){
printf(" verbose %d ===two_i %d best_two_i %d best_exit_a %d best_exit_b %d\n",
verbose,
two_i,best_two_i,best_exit_a,best_exit_b);
fflush(stdout);
}
#endif

/*:99*/


}
}

/*:65*/


/*106:*/


#if MATCH_MAX_VERBOSE >= 500
show_entry_with_string_at_verbose(e_entry,"accept",500);
#endif

/*:106*/


if(none_eligible||best_move.gain_for_comparison<trial_move.gain_for_comparison){
none_eligible= 0;
best_move= trial_move;
/*107:*/


#if MATCH_MAX_VERBOSE >= 500
show_entry_with_string_at_verbose(e_entry,"best move",500);
#endif


/*:107*/


}
}
if(!none_eligible){eligible_t*e_entry= &best_move;
/*68:*/


/*108:*/


#if MATCH_MAX_VERBOSE >= 500
show_entry_with_string_at_verbose(e_entry,"doing move",500);
#endif

/*:108*/


#if SPLIT_GAIN_VAR
cum_gain_pos= e_entry->gain_pos;
cum_gain_neg= e_entry->gain_neg;
#else
cum_gain= e_entry->gain;
#endif
#if defined(MATCH_DEBUG)
if(e_entry->two_i!=two_i)dump_core(96);
#endif

t[two_i+1]= e_entry->t2ip1;
t[two_i+2]= e_entry->t2ip2;
swap_mates(t[1],t[two_i],t[two_i+1],t[two_i+2]);
two_i+= 2;
/*74:*/


#if MATCH_REPORT_DEPTHS
if(two_i> probe_depth_less_2)probe_depth_less_2= two_i;
#endif


/*:74*/


/*98:*/


#if MATCH_MAX_VERBOSE >= 125
if(verbose> 125){
int i;
printf("\n");
for(i= 0;i<two_i;i++)putchar(' ');
printf("+");
fflush(stdout);
}
#endif


/*:98*/


/*101:*/


#if MATCH_MAX_VERBOSE >= 150
if(verbose>=150){
int i;
printf("t:");
for(i= 1;i<=two_i;i++)printf(" %d",t[i]);
printf("\n");
fflush(stdout);
}
#endif

/*:101*/



/*:68*/


}
}

/*:72*/


}
if(none_eligible){
if(best_two_i==0)
while(two_i> 2*backtracking_levels){/*69:*/


if(two_i> 2){
if(two_i<=2*backtracking_levels){
ei[two_i]= -2;
en[two_i]= -1;
}
two_i-= 2;
/*97:*/


#if MATCH_MAX_VERBOSE >= 125
if(verbose> 125){
int i;
printf("\n");
for(i= 0;i<two_i;i++)putchar(' ');
printf("-");
fflush(stdout);
}
#endif

/*:97*/


swap_mates(t[1],t[two_i+2],t[two_i+1],t[two_i]);
/*101:*/


#if MATCH_MAX_VERBOSE >= 150
if(verbose>=150){
int i;
printf("t:");
for(i= 1;i<=two_i;i++)printf(" %d",t[i]);
printf("\n");
fflush(stdout);
}
#endif

/*:101*/


}else keep_going= 0;



/*:69*/

}
else{keep_going= 0;}
}
}

/*:70*/


}
}
}

/*:57*/


if(best_two_i){/*73:*/


/*75:*/


#if MATCH_REPORT_DEPTHS
move_depth= best_two_i+2;
#endif

/*:75*/


while(best_two_i<two_i){/*69:*/


if(two_i> 2){
if(two_i<=2*backtracking_levels){
ei[two_i]= -2;
en[two_i]= -1;
}
two_i-= 2;
/*97:*/


#if MATCH_MAX_VERBOSE >= 125
if(verbose> 125){
int i;
printf("\n");
for(i= 0;i<two_i;i++)putchar(' ');
printf("-");
fflush(stdout);
}
#endif

/*:97*/


swap_mates(t[1],t[two_i+2],t[two_i+1],t[two_i]);
/*101:*/


#if MATCH_MAX_VERBOSE >= 150
if(verbose>=150){
int i;
printf("t:");
for(i= 1;i<=two_i;i++)printf(" %d",t[i]);
printf("\n");
fflush(stdout);
}
#endif

/*:101*/


}else keep_going= 0;



/*:69*/

}
/*103:*/


#if MATCH_MAX_VERBOSE >= 110
if(verbose>=110){
int i,m= (two_i<best_two_i?two_i:best_two_i);
printf("two_i %d best_two_i %d best_exit_a %d best_exit_b %d\n",
two_i,best_two_i,best_exit_a,best_exit_b);

printf("B:");
for(i= 1;i<=m;i++)printf(" %d",t[i]);
printf("\n");
fflush(stdout);
}
#endif

/*:103*/


swap_mates(t[1],t[best_two_i],best_exit_a,best_exit_b);
/*82:*/


if(iteration> 0){
const int more_log= 3+best_two_i;
/*83:*/


{const int need_size= more_log+change_log_next;
if(need_size>=change_log_max_alloc){
do{
change_log_max_alloc*= 2;
}while(need_size>=change_log_max_alloc);
change_log= (int*)mem_realloc(change_log,sizeof(int)*change_log_max_alloc);
}
}

/*:83*/


{int j;
for(j= 1;j<=best_two_i;j++){
write_log(t[j]);
}
}
write_log(best_exit_a);
write_log(best_exit_b);
write_log(2+best_two_i);
}

/*:82*/


/*55:*/


{int i;
for(i= 1;i<=best_two_i;i++){mark_dirty(t[i]);}
mark_dirty(best_exit_a);
mark_dirty(best_exit_b);
}

/*:55*/


/*102:*/


#if MATCH_MAX_VERBOSE >= 110
if(verbose>=110){
int i;
printf("T:");
for(i= 1;i<=best_two_i;i++)printf(" %d",t[i]);
printf(" %d",best_exit_a);
printf(" %d",best_exit_b);
printf("\n");
fflush(stdout);
}
#endif

/*:102*/


incumbent_len-= best_gain;
/*100:*/


#if MATCH_MAX_VERBOSE >= 100
if(verbose>=100){
printf("=== improve by "length_t_spec" to "length_t_spec"\n",
length_t_pcast(best_gain),length_t_pcast(incumbent_len));
fflush(stdout);
}
#endif

/*:100*/


/*95:*/


milestone_check(&ms_lower_bound,incumbent_len);
milestone_check(&ms_upper_bound,incumbent_len);

/*:95*/


/*39:*/


#if !LENGTH_TYPE_IS_EXACT
instance_epsilon= incumbent_len*LENGTH_MACHINE_EPSILON;
#endif

/*:39*/



/*:73*/

}
/*79:*/


#if MATCH_REPORT_DEPTHS
m_depths[move_depth/2]+= 1;
p_depths[probe_depth_less_2/2+1]+= 1;
#endif


/*:79*/


}
/*114:*/


#if MATCH_MAX_VERBOSE >= 40
if(verbose>=40){
char end_iter_message[100];
printf("End of LK step %d, incumbent_len = "length_t_spec"\n",
iteration+1,
length_t_pcast(incumbent_len));
sprintf(end_iter_message,"End of LK step %d",iteration+1);
milestone_show_request(&ms_lower_bound,end_iter_message,incumbent_len);
milestone_show_request(&ms_upper_bound,end_iter_message,incumbent_len);
}
#endif

/*:114*/


/*87:*/


if(iteration> 0&&change_log_next> 0&&previous_incumbent_len<incumbent_len){
/*113:*/


#if MATCH_MAX_VERBOSE >= 57
if(verbose>=57){
printf("Reverting to previous\n");
}
#endif

/*:113*/


while(change_log_next> 0){
/*89:*/


{
const int t1_pos= change_log_next-change_log[change_log_next-1]-1;
int j;
for(j= change_log_next-3;j>=t1_pos;j-= 2){
const int a= change_log[j],b= change_log[j+1];
make_mates(a,b);
}
errorif(j!=t1_pos-2,"Fencepost Bug!");
}


/*:89*/


change_log_next-= change_log[change_log_next-1]+1;
}
errorif(change_log_next!=0,"Bug!");
incumbent_len= previous_incumbent_len;
}
change_log_next= 0;

/*:87*/


/*90:*/


if(iteration+1<iterations){
int m[8],i;

/*91:*/


{
int count= 0,still_clashing;

do{
int j;
errorif(count++>=10000,"Ummm, 4-change matching mutation didn't terminate after 10000 tries!\n");
for(j= 0;j<8;j+= 2){
m[j]= prng_unif_int(random_stream,n);
m[j+1]= mate[m[j]];
}
still_clashing= 0;
for(j= 0;j<8;j+= 2){
int k;
for(k= j+2;k<8;k++){
if(m[j]==m[k]){
still_clashing= 1;
break;
}
}
}
}while(still_clashing);
}

/*:91*/


make_mates(m[0],m[7]);
make_mates(m[1],m[2]);
make_mates(m[3],m[4]);
make_mates(m[5],m[6]);
for(i= 0;i<8;i++)write_log(m[i]);
write_log(8);


previous_incumbent_len= incumbent_len;
incumbent_len+= 
cost(m[1],m[2])+cost(m[3],m[4])+cost(m[5],m[6])+cost(m[7],m[0])
-cost(m[0],m[1])-cost(m[2],m[3])-cost(m[4],m[5])-cost(m[6],m[7]);


for(i= 0;i<8;i++)mark_dirty(m[i]);
}

/*:90*/


}
/*115:*/


#if MATCH_MAX_VERBOSE >= 20
if(verbose>=20){
const double lk_time= resource_user_tick();
const double ds_time= 
resource_user_tick_from(begin_data_structures_mark);
printf("LK phase ended with incumbent_len == "length_t_spec
" after %.3f sec for LK and %.3f sec for ds+LK\n",
length_t_pcast(incumbent_len),lk_time,ds_time);
fflush(stdout);
}
#endif

/*:115*/


/*96:*/


milestone_show_final(&ms_lower_bound,incumbent_len);
milestone_show_final(&ms_upper_bound,incumbent_len);



/*:96*/


/*81:*/


#if MATCH_REPORT_DEPTHS
{int i,j;
for(i= DEPTHS_BOUND-1;p_depths[i]==0&&i> 0;i--);
for(j= 0;j<=i;j++){
printf("p %d citydeep %d\n",j*2,p_depths[j]);
}
for(i= DEPTHS_BOUND-1;m_depths[i]==0&&i> 0;i--);
for(j= 0;j<=i;j++){
printf("m %d citydeep %d\n",j*2,m_depths[j]);
}
}
#endif


/*:81*/


/*48:*/


{int l;
for(l= 2;l<=2*backtracking_levels;l+= 2){
free_mem(e[l]);
mem_deduct(sizeof(eligible_t)*nn_max_bound);
}
free_mem(e);mem_deduct(sizeof(eligible_t*)*(1+2*backtracking_levels));
free_mem(en);mem_deduct(sizeof(int)*(1+2*backtracking_levels));
free_mem(ei);mem_deduct(sizeof(int)*(1+2*backtracking_levels));
}



/*:48*//*80:*/


#if MATCH_REPORT_DEPTHS
free_mem(m_depths);mem_deduct(sizeof(int)*DEPTHS_BOUND);
free_mem(p_depths);mem_deduct(sizeof(int)*DEPTHS_BOUND);
#endif


/*:80*//*85:*/


free_mem(change_log);mem_deduct(sizeof(int)*change_log_max_alloc);


/*:85*/


}

/*:49*/


const char*match_rcs_id= "$Id: match.w,v 1.25 2000/09/17 03:10:18 neto Exp neto $";

/*:4*/
