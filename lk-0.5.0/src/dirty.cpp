#define VIRTUAL_BASE ((char*) 42)  \

/*2:*/


#include <config.h> 
#include "lkconfig.h"
/*3:*/


#include <stdio.h> 
#include <stdlib.h> 
#include <stddef.h> 

/*:3*/


#include "error.h"
#include "memory.h"
#include "dirty.h"

/*32:*/


#if DIRTY_SET == DIRTY_SET_FIFO
static dirty_queue_node_t sentinel;
#endif


/*:32*//*38:*/


#if DIRTY_SET == DIRTY_SET_FIFO
static int num_queues= 0;
#endif

/*:38*/


/*18:*/


#if DIRTY_SET == DIRTY_SET_SPLAY_ROOT
static int cmp_virtual_char(const void*a,const void*b);
static int
cmp_virtual_char(const void*a,const void*b)
{
return(const char*)a-(const char*)b;
}

static void prn_virtual_char(void*a);
static void
prn_virtual_char(void*a)
{
printf("%d",(const char*)a-VIRTUAL_BASE);
}
#endif

/*:18*//*31:*/


#if DIRTY_SET == DIRTY_SET_FIFO && DIRTY_DEBUG
void show_FIFO(dirty_set_t*ds);
void show_FIFO(dirty_set_t*ds)
{
if(0> 5000){
dirty_queue_node_t*p= ds->head;
while(p&&p!=&sentinel){
printf(" %d",p-ds->queue);
p= p->next;
}
printf(".\n");
}
fflush(stdout);
}
#endif

/*:31*/


/*7:*/


dirty_set_t*
dirty_create(int n,int full,int seed,const char*file,int line)
{
dirty_set_t*ds= new_of(dirty_set_t);
errorif(n<0,"Dirty set must be over non-empty range: n=%d",n);
ds->n= n;
ds->num_used= -1;
ds->file= file;
ds->line= line;
#if DIRTY_SET==DIRTY_SET_SPLAY_ROOT
/*17:*/


ds->dict= dict_create(cmp_virtual_char,prn_virtual_char);


/*:17*/


#else
/*27:*/


ds->queue= new_arr_of(dirty_queue_node_t,n);

/*:27*//*37:*/


ds->prng= prng_new(PRNG_DEFAULT,seed^21264^num_queues++);

/*:37*/


#endif
if(full){dirty_make_full(ds,seed);}
else{dirty_make_empty(ds);}
#if DIRTY_DEBUG

printf("dirty.w:%d:dirty_create(%d,%d,%d,%s,%d) = %8p\n",
__LINE__,n,full,seed,file,line,ds);
#endif
return ds;
}


/*:7*//*8:*/


void
dirty_destroy(dirty_set_t*ds)
{
if(ds){
#if DIRTY_SET == DIRTY_SET_SPLAY_ROOT
/*19:*/


if(ds->dict)dict_destroy(ds->dict,NULL);


/*:19*/


#else
/*28:*/


free_mem(ds->queue);mem_deduct(ds->n*sizeof(dirty_queue_node_t));
ds->head= NULL;
ds->tail= NULL;

/*:28*//*39:*/


prng_free(ds->prng);

/*:39*/


#endif
ds->n= -1;
free_mem(ds);mem_deduct(sizeof(dirty_set_t));
}
}


/*:8*//*9:*/


void
dirty_add(dirty_set_t*ds,const int v)
{
#if DIRTY_DEBUG
errorif(v<0||v>=ds->n,"arg v=%d not in range 0..%d",v,ds->n-1);
#endif
#if DIRTY_SET == DIRTY_SET_SPLAY_ROOT
/*22:*/


if(!dict_insert(ds->dict,VIRTUAL_BASE+v))ds->num_used++;

/*:22*/


#else
/*30:*/


{dirty_queue_node_t*dq= ds->queue;
#if DIRTY_DEBUG
if(verbose>=1000){printf("Before adding  : ");show_FIFO(ds);}
#endif
if(dq[v].next==NULL){
dq[v].next= &sentinel;
if(ds->tail)ds->tail->next= dq+v;
else ds->head= dq+v;
ds->tail= dq+v;
ds->num_used++;
}
#if DIRTY_DEBUG
if(verbose>=1000){printf("After adding   : ");show_FIFO(ds);}
#endif
}


/*:30*/


#endif
}

/*:9*//*10:*/


int
dirty_includes(dirty_set_t*ds,const int v)
{
#if DIRTY_SET == DIRTY_SET_SPLAY_ROOT
/*23:*/


return dict_find(ds->dict,VIRTUAL_BASE+v)!=NULL;

/*:23*/


#else
/*29:*/


return ds->queue[v].next!=NULL;

/*:29*/


#endif
}


/*:10*//*11:*/


int
dirty_remove(dirty_set_t*ds)
{
int return_value= -1;
#if DIRTY_SET == DIRTY_SET_SPLAY_ROOT
/*24:*/


{void*vp= dict_delete_any(ds->dict,NULL);
if(vp){ds->num_used--;return((char*)vp)-VIRTUAL_BASE;}
}


/*:24*/


#else
/*33:*/


#if DIRTY_DEBUG
if(verbose>=1000){printf("Before removing: ");show_FIFO(ds);}
#endif
if(ds->head){
dirty_queue_node_t*old_head= ds->head;
return_value= old_head-ds->queue;
if(old_head==ds->tail)ds->tail= ds->head= NULL;

else ds->head= old_head->next;
old_head->next= NULL;
ds->num_used--;
}
#if DIRTY_DEBUG
if(verbose>=1000){printf("After  removing: ");show_FIFO(ds);}
#endif

/*:33*/


#endif
return return_value;
}


/*:11*//*12:*/


void
dirty_make_full(dirty_set_t*ds,int seed)
{
#if DIRTY_SET == DIRTY_SET_SPLAY_ROOT
/*20:*/


{
int i,n= ds->n;
dict_t*d= ds->dict;
for(i= 0;i<n;i++){
dict_insert(d,VIRTUAL_BASE+i);
}
}

/*:20*/


#else
/*40:*/


{
const int n= ds->n;
prng_t*prng= ds->prng;
int i,*work= new_arr_of(int,n);

/*41:*/


for(i= 0;i<n;i++)work[i]= i;
for(i= 0;i<n;i++){
const int next= prng_unif_int(prng,n-i);
const int t= work[next];
work[next]= work[n-1-i];
work[n-1-i]= t;
}

/*:41*/


/*42:*/


{
dirty_queue_node_t*q= ds->queue;
ds->head= q+work[0];
ds->tail= q+work[n-1];
for(i= 0;i<n-1;i++){
q[work[i]].next= q+work[i+1];
}
q[work[n-1]].next= &sentinel;
}



/*:42*/



free_mem(work);mem_deduct(n*sizeof(int));
}


/*:40*/


#endif
ds->num_used= ds->n;
}

/*:12*//*13:*/


void
dirty_make_empty(dirty_set_t*ds)
{
#if DIRTY_SET == DIRTY_SET_SPLAY_ROOT
/*21:*/


dict_delete_all(ds->dict,NULL);

/*:21*/


#else
/*34:*/


if(ds->num_used<0||ds->num_used>=ds->n/2){
ds->head= ds->tail= NULL;
{int i;
const int n= ds->n;
dirty_queue_node_t*dq= ds->queue;
for(i= 0;i<n;i++)dq[i].next= NULL;
}
}else{
int return_value;
while(ds->head){/*33:*/


#if DIRTY_DEBUG
if(verbose>=1000){printf("Before removing: ");show_FIFO(ds);}
#endif
if(ds->head){
dirty_queue_node_t*old_head= ds->head;
return_value= old_head-ds->queue;
if(old_head==ds->tail)ds->tail= ds->head= NULL;

else ds->head= old_head->next;
old_head->next= NULL;
ds->num_used--;
}
#if DIRTY_DEBUG
if(verbose>=1000){printf("After  removing: ");show_FIFO(ds);}
#endif

/*:33*/

}
}

/*:34*/


#endif
ds->num_used= 0;
}


/*:13*//*14:*/


int
dirty_has_elements(dirty_set_t*ds)
{
return ds->num_used> 0;
}



/*:14*/


const char*dirty_rcs_id= "$Id: dirty.w,v 1.14 2000/09/17 03:08:33 neto Exp neto $";

/*:2*/
