/*5:*/


extern const char*twolevel_rcs_id;
/*13:*/


void twolevel_setup(const int num_vertices,const int start_seg_size);
void twolevel_cleanup(void);

/*:13*//*25:*/


void twolevel_set(int const*tour);


/*:25*//*32:*/


int twolevel_next(int a);
int twolevel_prev(int a);

/*:32*//*34:*/


int twolevel_between(int a,int b,int c);


/*:34*//*37:*/


void twolevel_flip(int a,int b,int c,int d);

/*:37*//*67:*/


#if defined(TWOLEVEL_DEBUG)
void twolevel_debug_setup(const int num_vertices,const int start_seg_size);
void twolevel_debug_cleanup(void);
void twolevel_debug_set(int const*tour);
int twolevel_debug_next(int a);
int twolevel_debug_prev(int a);
int twolevel_debug_between(int a,int b,int c);
void twolevel_debug_flip(int a,int b,int c,int d);
#endif


/*:67*/



/*:5*/
