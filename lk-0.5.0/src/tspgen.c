#define version(out) (fprintf(out,"%s (LK %s)\n",prog_name,VERSION_STRING) )  \

#define MAX_LINE_LENGTH (20000)  \

#define TSPLIB_NAME_PREFIX "c TSPLIB: NAME: "
#define TSPLIB_COMMENT_PREFIX "c TSPLIB: COMMENT: "
#define LK_LENGTH_T_PREFIX "c LK: length_t: " \

#define next_component_slot() (component+n+n-nc)  \

#define hull_city(hull_vertex_ptr) ((hull_vertex_ptr) -hull_vertex)  \

#define sqr(X) ((X) *(X) ) 
#define hull_edge_len(HP) (sqr((HP) ->x-(HP) ->next->x) +sqr((HP) ->y-(HP) ->next->y) )  \

#define LAY_FORWARD 1
#define LAY_BACKWARD 0 \

#define pi (3.1415926535897932384626433832795028841972) 
#define two_to_the_31 ((unsigned long) 0x80000000) 
#define scale_01(s,a,b) ((a) +((b) -(a) ) *(s) ) 
#define uniform_sample(g,a,b) scale_01((((double) gb_prng_next_rand(g) ) /two_to_the_31) ,(a) ,(b) )  \

#define uniform_sample_incl(g,a,b) scale_01(((double) gb_prng_next_rand(g) ) /(two_to_the_31-1) ,(a) ,(b) )  \
 \

/*5:*/



const char*prog_name= "tspgen";
const char*tspgen_rcs_id= "$Id: tspgen.w,v 1.31 1998/12/05 22:36:13 neto Exp neto $";
#include <config.h>
#include <lkconfig.h>
/*7:*/


#define _POSIX_C_SOURCE 2   
#if HAVE_UNISTD_H
#include <unistd.h>
#endif

/*:7*//*19:*/


#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>



/*:19*//*72:*/


#include <math.h>


/*:72*//*80:*/


#include <errno.h>

/*:80*/


/*8:*/


#define FIXINCLUDES_NEED_GETOPT
#include "fixincludes.h"
#undef FIXINCLUDES_NEED_GETOPT


/*:8*//*20:*/


#include "error.h"
#include "memory.h"
#include "length.h"

/*:20*//*28:*/


#include "dsort.h"

/*:28*//*39:*/


#include "gb_flip.h"

/*:39*/


/*67:*/


typedef struct{
double theta;
double v[2];
}rigid_motion_t;

/*:67*/


/*45:*/



typedef struct hull_vertex_s{
double x,y;
struct hull_vertex_s*next,*prev;
}hull_vertex_t;

/*:45*//*54:*/


typedef struct component_s{
struct component_s*left,*right,*ancestor;
hull_vertex_t*hull;
int hull_size;
rigid_motion_t transform;
}component_t;

/*:54*//*65:*/


typedef struct{
hull_vertex_t*vertex;
double len;
}hull_sortbox_t;

/*:65*//*90:*/


typedef struct{double x,y,angle;hull_vertex_t*home;}point_t;

/*:90*/



#if !defined(TSPGEN_DEBUG)
#define TSPGEN_DEBUG 0
#endif

/*15:*/


typedef struct{
int city[2];
length_t cost;
}edge_t;

typedef struct{
edge_t*edge;
int ne;
int num_filled;
}tree_t;

/*:15*/


/*13:*/


static long seed;
static double packing_factor,join_length_bias;
static int try_forcing_isomorphic,force_failures= 0;
static char*option_name= NULL;
static char*name= NULL;

/*:13*//*18:*/


static tree_t*in_tree= NULL;

/*:18*//*22:*/


static char*in_comment= NULL;
static char*in_name= NULL;

/*:22*//*25:*/


static int n,ne;


/*:25*//*34:*/


static int nc;

/*:34*//*38:*/


static gb_prng_t*angle_prng;

/*:38*//*49:*/


static hull_vertex_t*hull_vertex;

/*:49*//*57:*/


static component_t*component;


/*:57*//*64:*/


static hull_sortbox_t*hull_sortbox;

/*:64*//*89:*/


static point_t*hull_buf= NULL,west;
static int nh= 0;

/*:89*/


/*11:*/


static void usage(FILE*out,char**argv);
static void sample(FILE*out);


/*:11*/


/*10:*/


static void
usage(FILE*out,char**argv)
{
version(out);
fprintf(out,
"Generate TSPLIB instances from minimum spanning trees\n"
"\nCopyright (C) 1997, 1998 David M. Neto\n"
"LK comes with NO WARRANTY, to the extent permitted by law.\n"
"You may redistribute and/or modify copies of LK under the terms of the\n"
"GNU General Public License, version 2 or any later version.\n"
"For more information about these matters, see the file named COPYING.\n\n"
"Usage: %s [options] < <weighted-tree.edge> \n",
argv[0]);
fprintf(out,
" -h                    : Output this help and quit\n"
" --help                : Output this help and quit\n"
" -i                    : Try hard to make the output have a minimum spanning\n"
"                         tree that is isomorphic to the input weighted tree\n"
" -j <join-length-bias> : Bias choice of hull point based on associated\n"
"                         hull edge length.\n"
"                           negative means random vertex\n"
"                           0 to 1: use this as a fractional ordinal in\n"
"                             sorted hull edge lengths (read the source!)\n"
" -n <name>             : Use <name> as the instance name, overriding \n"
"                         the 'c TSPLIB: NAME: <name>', if any \n"
" -p <packing-factor>   : Non-negative <packing-factor> is a hint on how\n"
"                         tight to try to make the instance:\n"
"                           0 means try hard to stretch it loose,\n"
"                           large values means try to pack it tight\n"
"                           1 means join angles are uniform over legal range\n"
" -s <seed>             : Use integer <seed> as the random number seed\n"
" --version             : Print version number and exit successfully\n"
"\nThe weighted tree file is in DIMACS matching input format in 'edge' style.\n"
"SAMPLE BEGINS NEXT LINE\n");
sample(out);
fprintf(out,"SAMPLE ENDS PREVIOUS LINE\n");
}

/*:10*//*12:*/


static void
sample(FILE*out)
{
fprintf(out,
"c Minimum spanning tree generated by LK 0.4.9\n"
"c TSPLIB: NAME: foobar5\n"
"c TSPLIB: COMMENT: Sample bogus instance for tspgen.\n"
"c LK: option: -M\n"
"c LK: length_t: double\n"
"c LENGTH: 21876.000000000000000\n"
"p edge 5 4\n"
"e 1 2 4328.000000000000000\n"
"e 3 1 10055.000000000000000\n"
"e 1 4 5760.000000000000000\n"
"e 5 2 1733.000000000000000\n");
}



/*:12*//*29:*/


static int
cmp_edge(const void*ap,const void*bp)
{
const edge_t*a= (const edge_t*)ap,*b= (const edge_t*)bp;
if(a->cost<b->cost)return-1;
if(a->cost>b->cost)return 1;
return 0;
}


/*:29*//*50:*/


static void
hull_then_rotate(hull_vertex_t*h,double theta)
{
const double ct= cos(theta),st= sin(theta);
const hull_vertex_t*start= h;
do{
const double x= h->x,y= h->y;
h->x= x*ct-y*st;
h->y= x*st+y*ct;
h= h->next;
}while(h!=start);
}

static void
hull_then_translate(hull_vertex_t*h,double x,double y)
{
const hull_vertex_t*start= h;
do{
h->x+= x;
h->y+= y;
h= h->next;
}while(h!=start);
}

/*:50*//*66:*/


static int
cmp_hull_sortbox(const void*ap,const void*bp)
{
const double
al= ((const hull_sortbox_t*)ap)->len,
bl= ((const hull_sortbox_t*)bp)->len;
if(al<bl)return-1;
if(al>bl)return 1;
return 0;
}

/*:66*//*68:*/


static void
transform_init(rigid_motion_t*t)
{
t->theta= t->v[0]= t->v[1]= 0.0;
}

/*:68*//*70:*/


static void
transform_then_translate(rigid_motion_t*t,double x,double y)
{
t->v[0]+= x;
t->v[1]+= y;
}

/*:70*//*71:*/


static void
transform_then_rotate(rigid_motion_t*t,double theta)
{
const double ct= cos(theta),st= sin(theta);
const double x= t->v[0],y= t->v[1];
t->v[0]= x*ct-y*st;
t->v[1]= x*st+y*ct;
t->theta+= theta;
}

/*:71*//*73:*/


static void
transform_then_transform(rigid_motion_t*t,rigid_motion_t*t2)

{
rigid_motion_t copy;
if(t==t2){copy= *t2;t2= &copy;}
transform_then_rotate(t,t2->theta);
transform_then_translate(t,t2->v[0],t2->v[1]);
}

/*:73*//*74:*/


static void
component_then_rotate(component_t*c,double theta)
{
transform_then_rotate(&(c->transform),theta);
hull_then_rotate(c->hull,theta);
}

static void
component_then_translate(component_t*c,double x,double y)
{
transform_then_translate(&(c->transform),x,y);
hull_then_translate(c->hull,x,y);
}


/*:74*//*79:*/


void rotate_hull_with_bias(component_t*comp,int is_foward,double packing_factor);
void
rotate_hull_with_bias(component_t*comp,int is_forward,double packing_factor)
{
hull_vertex_t*comph= comp->hull;
if(errno){perror("main: about to rotate comp");errorif(1,"Oops!");}
if(comp->hull_size>1){
double rot;
double alpha= -atan2(comph->y-comph->prev->y,comph->x-comph->prev->x);
double beta= -atan2(comph->next->y-comph->y,comph->next->x-comph->x);
errorif(errno==EDOM,"Sorry, my call to atan2 had a bad argument.");
errorif(errno==ERANGE,"Sorry, the answer to my atan2 call was out of range.");
while(alpha<beta)beta-= 2*pi;

/*82:*/


/*81:*/


errno= 0;


/*:81*/


{double y= 1-uniform_sample(angle_prng,0,1),t= exp(packing_factor*log(y));
switch(errno){
case 0:break;
case ERANGE:t= 1.0;/*81:*/


errno= 0;


/*:81*/

;break;
default:perror("main: about to sample alpha beta");errorif(1,"Oops!");
}
errorif(t<0.0||1.0<t,"alpha-beta mediation is %f, not in [0,1]",t);
if(!is_forward)t= 1-t;
rot= t*alpha+(1-t)*beta;
/*110:*/


#if TSPGEN_DEBUG >= 200
{double frac= (rot-beta)/(alpha-beta);
fprintf(stderr," angle %10.5f%% alpha and %10.5f%% beta %s\n",
100*frac,100*(1-frac),(is_forward?"forward":"reverse"));
errorif(frac<0.0||1.0<frac,"Frac %f out of [0,1]",frac);
}
#endif

/*:110*/


}




/*:82*/



component_then_rotate(comp,rot);
}
}

/*:79*//*93:*/


#define abs(A) ((A)<0 ? -(A) : (A))
static int
cmp_point(const void*ap,const void*bp)
{
const point_t a= *(const point_t*)ap,b= *(const point_t*)bp;
if(a.angle<b.angle)return-1;
if(a.angle>b.angle)return 1;
if(a.x<b.x)return-1;
if(a.x>b.x)return 1;
if(abs(a.y-west.y)<abs(b.y-west.y))return-1;
if(abs(a.y-west.y)>abs(b.y-west.y))return 1;
return 0;
}

/*:93*//*99:*/


static int
left_turn(point_t u,point_t v,point_t w)
{
const double
ad= (u.x-w.x)*(v.y-w.y),
bc= (u.y-w.y)*(v.x-w.x);
return ad>bc;
}


/*:99*//*104:*/


static void
write_component(FILE*file,component_t*c,int*city_num)
{
if(c->left){
transform_then_transform(&(c->left->transform),&(c->transform));
write_component(file,c->left,city_num);
}
if(c->left==NULL&&c->right==NULL){
printf("%10d %.16g %.16g\n",
*city_num,
c->transform.v[0],c->transform.v[1]);
*city_num+= 1;
}
if(c->right){
transform_then_transform(&(c->right->transform),&(c->transform));
write_component(file,c->right,city_num);
}
}

/*:104*//*109:*/


#if TSPGEN_DEBUG >= 100
static double
euc2hull(hull_vertex_t*ah,hull_vertex_t*bh)
{
const double dx= ah->x-bh->x,dy= ah->y-bh->y;
return sqrt(dx*dx+dy*dy);
}
#endif


/*:109*/




int main(int argc,char**argv)
{
/*37:*/


gb_prng_t*component_prng,*hull_prng;

/*:37*//*95:*/


int*stack;

/*:95*/


/*6:*/


/*9:*/


{int i;
for(i= 1;i<argc;i++){
if(0==strcmp(argv[i],"--help")){
usage(stdout,argv);
exit(0);
}else if(0==strcmp(argv[i],"--version")){
version(stdout);
exit(0);
}
}
}


/*:9*/


seed= -42L;
packing_factor= 100;
join_length_bias= 1;
try_forcing_isomorphic= 0;
force_failures= 0;
option_name= NULL;
while(1){
extern char*optarg;
const int opt= getopt(argc,argv,"s:p:j:hin:");
if(opt==EOF)break;
switch(opt){
case's':seed= atol(optarg);break;
case'n':option_name= optarg;break;
case'p':
packing_factor= atof(optarg);
errorif(packing_factor<0,
"Packing factor should be non-negative, but given %f",packing_factor);
break;
case'j':
join_length_bias= atof(optarg);
errorif(join_length_bias>1,
"Join length bias should be no more than 1, but given %f",
join_length_bias);
break;
case'i':try_forcing_isomorphic= 1;break;
case'h':usage(stdout,argv);exit(0);break;
case':':errorif(1,"some option is missing an argument");break;
case'?':usage(stderr,argv);errorif(1,"Unrecognized option");break;
default:errorif(1,"getopt returned character 0%o",opt);
}
}

/*:6*/


/*36:*/


component_prng= gb_prng_new(seed);
hull_prng= gb_prng_new(seed^42);
angle_prng= gb_prng_new(seed^(42*42));
errorif(
component_prng==NULL
||hull_prng==NULL
||angle_prng==NULL
,"Couldn't allocate three random number generators");

/*:36*/


/*14:*/


/*81:*/


errno= 0;


/*:81*/


{int problem_line_read= 0;
char line[MAX_LINE_LENGTH];
while(fgets(line,MAX_LINE_LENGTH,stdin)){
switch(line[0]){
case'c':/*21:*/


{char*p,parsed_suffix[MAX_LINE_LENGTH];
if((p= strstr(line,TSPLIB_COMMENT_PREFIX))!=NULL){
if(in_comment){mem_deduct(strlen(in_comment));free_mem(in_comment);}
in_comment= dup_string(p+strlen(TSPLIB_COMMENT_PREFIX));
}else if(1==sscanf(line,TSPLIB_NAME_PREFIX"%s",parsed_suffix)){

if(in_name){mem_deduct(strlen(in_name));free_mem(in_name);}
in_name= dup_string(parsed_suffix);
}else if(1==sscanf(line,LK_LENGTH_T_PREFIX"%s",parsed_suffix)){
if(strcmp(LENGTH_TYPE_STRING,parsed_suffix)){
fprintf(stderr,
"tspgen:Warning: tspgen uses %s for lengths, input used %s\n",
LENGTH_TYPE_STRING,p+strlen(LK_LENGTH_T_PREFIX));
}
}
}

/*:21*/

break;
case'p':/*24:*/


errorif(problem_line_read,
"A problem line has already been read, but I now see a second one: %s",
line);
{int num_read;
num_read= sscanf(line,"p edge %d %d ",&n,&ne);
errorif(num_read!=2,"Couldn't understand the problem line."
"  It should be of form: p edge <n> <n-1>");
errorif(n<2,"n, the number vertices, is %d but must be at least 2.",n);
errorif(ne!=n-1,"The number of edges is %d but should be %d.",ne,n-1);
/*16:*/


in_tree= new_of(tree_t);
in_tree->ne= ne;
in_tree->num_filled= 0;
in_tree->edge= new_arr_of(edge_t,ne);

/*:16*/


}

/*:24*/

break;
case'e':/*26:*/


errorif(in_tree==NULL,"We got an edge (e) line before a problem (p) line");
errorif(in_tree->num_filled>=in_tree->ne,
"Too many edges specified on input.  Got this one as first extra line: %s",
line);
{
int num_read;
int u= 0,v= 0;
length_t len;
num_read= sscanf(line,"e %d %d "length_t_native_inspec,
&u,&v,length_t_native_incast(&len));
errorif(num_read!=(2+length_t_native_incount),
"Couldn't understand an edge line. Parsed %d items; line is: %s",
num_read,line);
errorif(u<1||u>n,"First vertex %d is out of bounds in: %s",u,line);
errorif(v<1||v>n,"Second vertex %d is out of bounds in: %s",v,line);
u--;
v--;
in_tree->edge[in_tree->num_filled].city[0]= u;
in_tree->edge[in_tree->num_filled].city[1]= v;
in_tree->edge[in_tree->num_filled].cost= len;
in_tree->num_filled++;
}

/*:26*/

break;
default:errorif(1,"Don't know how to parse input line:\n%s",line);
}
}
errorif(in_tree==NULL,"No tree found on the input");
errorif(in_tree->num_filled!=in_tree->ne,
"Read only %d edges when %d were expected",
in_tree->num_filled,in_tree->ne);
}
/*81:*/


errno= 0;


/*:81*/



/*:14*/


/*27:*/


dsort(in_tree->edge,(size_t)ne,sizeof(edge_t),cmp_edge);

/*:27*/


/*46:*/


hull_vertex= new_arr_of(hull_vertex_t,n);
/*47:*/


{int i;
for(i= 0;i<n;i++){
hull_vertex[i].x= 0;
hull_vertex[i].y= 0;
hull_vertex[i].next= hull_vertex[i].prev= hull_vertex+i;
}
}

/*:47*/



/*:46*//*55:*/


component= new_arr_of(component_t,n+ne);
{int i;
for(i= 0;i<n;i++){
component[i].hull= hull_vertex+i;
component[i].hull_size= 1;
transform_init(&component[i].transform);
}
for(i= 0;i<n+ne;i++)
component[i].left= component[i].right= component[i].ancestor= NULL;
}

/*:55*//*62:*/


hull_sortbox= new_arr_of(hull_sortbox_t,n);

/*:62*//*87:*/


hull_buf= new_arr_of(point_t,n);

/*:87*//*96:*/


stack= new_arr_of(int,n+1);

/*:96*/


/*31:*/


for(nc= n;nc>1;nc--){
component_t*a,*b;
const edge_t this_edge= in_tree->edge[n-nc];
const double this_length= this_edge.cost;
if(try_forcing_isomorphic){
const int u= this_edge.city[0],v= this_edge.city[1];
/*40:*/


{
component_t*d;
d= component+u;/*41:*/


{
component_t*updating= d;
while(d->ancestor)d= d->ancestor;
for(;updating!=d;updating= updating->ancestor)
updating->ancestor= d;
}


/*:41*/

a= d;
d= component+v;/*41:*/


{
component_t*updating= d;
while(d->ancestor)d= d->ancestor;
for(;updating!=d;updating= updating->ancestor)
updating->ancestor= d;
}


/*:41*/

b= d;
errorif(a==b,
"Input wasn't a tree.  It contained a cycle involving edge (%d,%d)",
u+1,v+1);
}

/*:40*/


/*42:*/


{component_t*c;int w;
c= a;w= u;/*43:*/


{
int found_w= 0;
hull_vertex_t*first= c->hull,*here= first;
do{
if(hull_city(here)==w){
c->hull= here;
found_w= 1;
/*115:*/


#if TSPGEN_DEBUG >= 200
fprintf(stderr,"Found %d and set c->hull = %p (city %d)\n",
w,c->hull,hull_city(c->hull));
#endif

/*:115*/


break;
}
here= here->next;
}while(here!=first);
if(!found_w){
const double old_join_length_bias= join_length_bias;
join_length_bias= 1.0;
/*61:*/


{hull_vertex_t*here= c->hull;
int i,index;
for(i= 0,here= c->hull;i<c->hull_size;i++,here= here->next){
hull_sortbox[i].vertex= here;
hull_sortbox[i].len= hull_edge_len(here);
}
dsort(hull_sortbox,(size_t)c->hull_size,sizeof(hull_sortbox_t),cmp_hull_sortbox);
index= floor(scale_01(join_length_bias,0,c->hull_size));
if(index==c->hull_size)index--;
c->hull= hull_sortbox[index].vertex;
}

/*:61*/


join_length_bias= old_join_length_bias;
force_failures++;
/*116:*/


#if TSPGEN_DEBUG >= 200
fprintf(stderr,"Force failed to find city %d on hull\n",w);
#endif

/*:116*/


}
}

/*:43*/


c= b;w= v;/*43:*/


{
int found_w= 0;
hull_vertex_t*first= c->hull,*here= first;
do{
if(hull_city(here)==w){
c->hull= here;
found_w= 1;
/*115:*/


#if TSPGEN_DEBUG >= 200
fprintf(stderr,"Found %d and set c->hull = %p (city %d)\n",
w,c->hull,hull_city(c->hull));
#endif

/*:115*/


break;
}
here= here->next;
}while(here!=first);
if(!found_w){
const double old_join_length_bias= join_length_bias;
join_length_bias= 1.0;
/*61:*/


{hull_vertex_t*here= c->hull;
int i,index;
for(i= 0,here= c->hull;i<c->hull_size;i++,here= here->next){
hull_sortbox[i].vertex= here;
hull_sortbox[i].len= hull_edge_len(here);
}
dsort(hull_sortbox,(size_t)c->hull_size,sizeof(hull_sortbox_t),cmp_hull_sortbox);
index= floor(scale_01(join_length_bias,0,c->hull_size));
if(index==c->hull_size)index--;
c->hull= hull_sortbox[index].vertex;
}

/*:61*/


join_length_bias= old_join_length_bias;
force_failures++;
/*116:*/


#if TSPGEN_DEBUG >= 200
fprintf(stderr,"Force failed to find city %d on hull\n",w);
#endif

/*:116*/


}
}

/*:43*/


}

/*:42*/


}else{
/*35:*/


{component_t temp;
int base= 2*(n-nc),r;
r= base+gb_prng_unif_rand(component_prng,nc);
temp= component[base];component[base]= component[r];component[r]= temp;
r= base+1+gb_prng_unif_rand(component_prng,nc-1);
temp= component[base+1];component[base+1]= component[r];component[r]= temp;
a= component+base;
b= component+base+1;
}

/*:35*/


/*58:*/


if(join_length_bias<0){
/*59:*/


{long steps;
for(steps= gb_prng_unif_rand(hull_prng,a->hull_size);steps;steps--)
a->hull= a->hull->next;
for(steps= gb_prng_unif_rand(hull_prng,b->hull_size);steps;steps--)
b->hull= b->hull->next;
}

/*:59*/


}else{
/*60:*/


{component_t*c;
c= a;/*61:*/


{hull_vertex_t*here= c->hull;
int i,index;
for(i= 0,here= c->hull;i<c->hull_size;i++,here= here->next){
hull_sortbox[i].vertex= here;
hull_sortbox[i].len= hull_edge_len(here);
}
dsort(hull_sortbox,(size_t)c->hull_size,sizeof(hull_sortbox_t),cmp_hull_sortbox);
index= floor(scale_01(join_length_bias,0,c->hull_size));
if(index==c->hull_size)index--;
c->hull= hull_sortbox[index].vertex;
}

/*:61*/


c= b;/*61:*/


{hull_vertex_t*here= c->hull;
int i,index;
for(i= 0,here= c->hull;i<c->hull_size;i++,here= here->next){
hull_sortbox[i].vertex= here;
hull_sortbox[i].len= hull_edge_len(here);
}
dsort(hull_sortbox,(size_t)c->hull_size,sizeof(hull_sortbox_t),cmp_hull_sortbox);
index= floor(scale_01(join_length_bias,0,c->hull_size));
if(index==c->hull_size)index--;
c->hull= hull_sortbox[index].vertex;
}

/*:61*/


}

/*:60*/


}

/*:58*/


}
/*75:*/


/*76:*/


rotate_hull_with_bias(a,LAY_FORWARD,packing_factor);
if(try_forcing_isomorphic){
const int to_prev_is_longer= 
hull_edge_len(b->hull->prev)>hull_edge_len(b->hull);
const int join_longer= join_length_bias>=0.5;
if(join_longer==to_prev_is_longer){
rotate_hull_with_bias(b,LAY_BACKWARD,packing_factor);
}else{
rotate_hull_with_bias(b,LAY_FORWARD,packing_factor);
}
}else{
b->hull= b->hull->next;
rotate_hull_with_bias(b,LAY_BACKWARD,packing_factor);
}
component_then_rotate(b,pi);
/*106:*/


#if TSPGEN_DEBUG >=120
{component_t*comp;
int is_forward= -9999;
fprintf(stderr," Component a\n");fflush(stderr);
comp= a;/*105:*/


#if TSPGEN_DEBUG >= 125
{int i;hull_vertex_t*here;
fprintf(stderr,"  Hull size %d (city %d), `bot-hull' point first.  is_forward=%d\n",
comp->hull_size,hull_city(comp->hull),is_forward);
for(i= 0,here= comp->hull;i<comp->hull_size;i++,here= here->next){
fprintf(stderr,"\t%10.5f  %10.5f (city %d)\n",here->x,here->y,
hull_city(here));
}
errorif(here!=comp->hull,"hull_size wrong");
}
#endif

/*:105*/


fprintf(stderr," Component b\n");fflush(stderr);
comp= b;/*105:*/


#if TSPGEN_DEBUG >= 125
{int i;hull_vertex_t*here;
fprintf(stderr,"  Hull size %d (city %d), `bot-hull' point first.  is_forward=%d\n",
comp->hull_size,hull_city(comp->hull),is_forward);
for(i= 0,here= comp->hull;i<comp->hull_size;i++,here= here->next){
fprintf(stderr,"\t%10.5f  %10.5f (city %d)\n",here->x,here->y,
hull_city(here));
}
errorif(here!=comp->hull,"hull_size wrong");
}
#endif

/*:105*/


}
#endif

/*:106*/



/*:76*/


{
component_t*c= next_component_slot();
hull_vertex_t*ah= a->hull,*bh= b->hull;
/*77:*/


component_then_translate(a,bh->x-ah->x,bh->y-ah->y+this_length);
/*108:*/


#if TSPGEN_DEBUG >= 100
{
hull_vertex_t*here,*ahere,*bhere;
const double d= euc2hull(ah,bh);
int fail= 0;
here= ah;


for(here= bh->next;here!=bh;here= here->next){
const double hd= euc2hull(ah,here);
if(d>hd)fail= 1,fprintf(stderr,"d = %f > hd = %f\n",d,hd);
fprintf(stderr,"a");
}

for(here= ah->next;here!=ah;here= here->next){
const double hd= euc2hull(bh,here);
if(d>hd)fail= 1,fprintf(stderr,"d = %f > hd = %f\n",d,hd);
fprintf(stderr,"b");
}


ahere= ah;
do{
bhere= bh;
do{
const double hd= euc2hull(ahere,bhere);
if((ahere!=ah||bhere!=bh)&&d>hd)
fail= 1,fprintf(stderr,"full d = %f > hd = %f\n",d,hd);
bhere= bhere->next;
fprintf(stderr,"c");
}while(bhere!=bh);
ahere= ahere->next;
}while(ahere!=ah);



if(d<this_length-1e-6)
fail= 1,fprintf(stderr,"d = %f < %f = this_length\n",d,this_length);
if(d>this_length+1e-6)
fail= 1,fprintf(stderr,"d = %f > %f = this_length\n",d,this_length);
fprintf(stderr,"\n");
errorif(fail,"distances mucked up!");
}
#endif

/*:108*/



/*:77*/


/*83:*/


c->left= a;
c->right= b;
a->ancestor= b->ancestor= c;
/*85:*/


/*86:*/


{hull_vertex_t*start,*here;
nh= 0;
here= start= a->hull;
do{
hull_buf[nh].x= here->x;
hull_buf[nh].y= here->y;
hull_buf[nh].home= here;
here= here->next;
nh++;
}while(here!=start);
here= start= b->hull;
do{
hull_buf[nh].x= here->x;
hull_buf[nh].y= here->y;
hull_buf[nh].home= here;
here= here->next;
nh++;
}while(here!=start);
}

/*:86*/


/*91:*/


{int i;
west= hull_buf[0];
for(i= 1;i<nh;i++)
if(hull_buf[i].x<west.x){
west= hull_buf[i];
hull_buf[i]= hull_buf[0];
hull_buf[0]= west;
}
}

/*:91*/


/*92:*/


{int i;
for(i= 1;i<nh;i++){
hull_buf[i].angle= atan2(hull_buf[i].y-west.y,hull_buf[i].x-west.x);
}
dsort(&hull_buf[1],(size_t)nh-1,sizeof(point_t),cmp_point);
}

/*:92*/


/*94:*/


{
int i;
int top= -1;
/*111:*/


#if TSPGEN_DEBUG >= 400
fprintf(stderr,"Graham scan: nh==%d\n",nh);fflush(stderr);
#endif

/*:111*/


for(i= 0;i<=nh;i++){
stack[++top]= (i<nh)?i:0;
/*112:*/


#if TSPGEN_DEBUG >= 500
fprintf(stderr," push   %d, top==%d\n",stack[top],top);fflush(stderr);
#endif


/*:112*/


while(top>=2&&stack[top-2]!=stack[top]
&&!left_turn(hull_buf[stack[top-2]],
hull_buf[stack[top-1]],
hull_buf[stack[top]])){
/*113:*/


#if TSPGEN_DEBUG>=500
fprintf(stderr," remove %d, top==%d\n",stack[top-1],top-1);fflush(stderr);
#endif

/*:113*/


stack[top-1]= stack[top];
top--;
}
}
/*114:*/


#if TSPGEN_DEBUG >=400
fprintf(stderr,"Done Graham scan: top==%d\n",top);fflush(stdout);
#endif

/*:114*/


/*98:*/


for(i= 0;i<top;i++){
hull_vertex_t*here= hull_buf[stack[i]].home;
here->x= hull_buf[stack[i]].x;
here->y= hull_buf[stack[i]].y;
here->prev= hull_buf[stack[(i-1+top)%top]].home;
here->next= hull_buf[stack[(i+1)%top]].home;
}
c->hull= hull_buf[stack[0]].home;
c->hull_size= top;

/*:98*/


}

/*:94*/



/*:85*/



/*:83*/


/*107:*/


#if TSPGEN_DEBUG >=100
fprintf(stderr,"New component\n");fflush(stderr);
{component_t*comp= c;
int is_forward= -9999;
/*105:*/


#if TSPGEN_DEBUG >= 125
{int i;hull_vertex_t*here;
fprintf(stderr,"  Hull size %d (city %d), `bot-hull' point first.  is_forward=%d\n",
comp->hull_size,hull_city(comp->hull),is_forward);
for(i= 0,here= comp->hull;i<comp->hull_size;i++,here= here->next){
fprintf(stderr,"\t%10.5f  %10.5f (city %d)\n",here->x,here->y,
hull_city(here));
}
errorif(here!=comp->hull,"hull_size wrong");
}
#endif

/*:105*/


}
fprintf(stderr,"\n\n\n");fflush(stderr);
#endif

/*:107*/


if(nc>2){/*78:*/


component_then_rotate(c,uniform_sample(angle_prng,-pi,pi));

/*:78*/

}
}

/*:75*/


}

/*:31*/


/*100:*/


{char*name= NULL;
if(!name)name= option_name;
if(!name)name= in_name;
if(!name){
char constructed_name[1000];
sprintf(constructed_name,"%s%d",prog_name,n);
name= dup_string(constructed_name);
}
printf("NAME: %s\n",name);
}


/*:100*//*102:*/


if(in_comment){
char*newline;
for(newline= strchr(in_comment,'\n');newline;newline= strchr(in_comment,'\n'))*newline= ' ';
}
printf("COMMENT: %s | (LK %s) %s ",
in_comment?in_comment:"<unknown source>",
VERSION_STRING,prog_name);
if(try_forcing_isomorphic){
printf("-i (%d or %.1f%% fail) ",force_failures,(100.0*force_failures)/ne);
}
printf("-s %ld -j %g -p %g\n",seed,join_length_bias,packing_factor);

/*:102*//*103:*/


/*47:*/


{int i;
for(i= 0;i<n;i++){
hull_vertex[i].x= 0;
hull_vertex[i].y= 0;
hull_vertex[i].next= hull_vertex[i].prev= hull_vertex+i;
}
}

/*:47*/


printf("TYPE: TSP\n");
printf("DIMENSION: %d\n",n);
printf("EDGE_WEIGHT_TYPE: EUC_2D\n");
printf("NODE_COORD_SECTION\n");
errorif(nc!=1,"Must be only one component, but nc==%d.",nc);
{int city_num= 1;
write_component(stdout,next_component_slot()-1,&city_num);
errorif(city_num-1!=n,"Wrote %d cities instead of %d.",city_num,n);
}
printf("EOF\n");

/*:103*/


/*17:*/


if(in_tree){
free_mem(in_tree->edge);mem_deduct(in_tree->ne*sizeof(edge_t));
in_tree->ne= -1;
in_tree->num_filled= 0;
free_mem(in_tree);
}

/*:17*//*23:*/


if(in_comment){mem_deduct(strlen(in_comment));free_mem(in_comment);}
if(in_name){mem_deduct(strlen(in_name));free_mem(in_name);}


/*:23*//*48:*/


free_mem(hull_vertex);

/*:48*//*56:*/


free_mem(component);

/*:56*//*63:*/


free_mem(hull_sortbox);

/*:63*//*88:*/


free_mem(hull_buf);

/*:88*//*97:*/


free_mem(stack);


/*:97*//*101:*/


if(name){mem_deduct(strlen(name));free_mem(name);}


/*:101*/


return 0;
}

/*:5*/
