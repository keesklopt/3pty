/*2:*/


#if !defined(_CONSTRUCT_H_)
#define _CONSTRUCT_H_
/*6:*/


#include "length.h"

/*:6*/


extern const char*construct_rcs_id;
/*9:*/


#define CONSTRUCT_CANONICAL 0




/*:9*//*11:*/


#define CONSTRUCT_RANDOM 1


/*:11*//*18:*/


#define CONSTRUCT_GREEDY   2
#define CONSTRUCT_GREEDY_RANDOM 3

/*:18*/


/*5:*/


length_t
construct(const int n,int*tour,const int heuristic,const long
heur_param,const long random_seed);

/*:5*//*63:*/


length_t construct_matching(int n,int*mate,int alg,long alg_param,const
long random_seed);


/*:63*/


#endif

/*:2*/
