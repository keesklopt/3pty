/*5:*/


#if !defined(_POOL_H_)
#define _POOL_H_
extern const char*pool_rcs_id;
/*7:*/


typedef struct pool_block_freelist_s{
struct pool_block_freelist_s*next;
}pool_block_freelist_t;

typedef struct pool_s{
struct pool_s*next;
size_t os;
int bs;
void*block;
pool_block_freelist_t*freelist_head;
}pool_t;

/*:7*/


/*10:*/


pool_t*pool_create(size_t os,int bs);

/*:10*//*13:*/


void pool_destroy(pool_t*p);

/*:13*//*15:*/


void*pool_alloc(pool_t*p);

/*:15*//*17:*/


void pool_free(pool_t*p,void*vp);

/*:17*/


#endif

/*:5*/
