
tsp_result_t *TspLk(int nlocs, coord_t *locs, coord_t *start,coord_t *end,int timeout, int loopout)
{
	micro_t timer;
	micro_t starttime;
	micro_t endtime;
	length_t validate_len;

	// just does not work well with low number, return optimum with LP
	if (nlocs<6) {
		return TspLp(nlocs,locs,start,end,timeout,loopout);
	}

	init_lk_main();

	tsp_result_t *solution = (tsp_result_t *)calloc(sizeof(tsp_result_t),nlocs+1);
	verbose=0;
	double double_validate_len,ordered_double_len,raw_len;

	last_resource_mark= resource_mark("Reading the instance");

	tsp_instance = (tsp_instance_t *)calloc(sizeof(tsp_instance_t),1);
	candidate_expr= CAND_NN;
	cand_nn_k= 2;
	cand_nq_k= 5;
	cand_del_d= 3;

	tour_next= array_next;
	tour_prev= array_prev;
	tour_between= array_between;
	tour_flip= array_flip;
	tour_set= array_set;
	tour_setup= array_setup;
	tour_cleanup= array_cleanup;

	jbmr_setup(nlocs);

	mem_usage_reset();  /* Start memory counter at zero. */
	resource_setup(50);

	tour_setup(nlocs);

	starttime= micro_now();
	mat_t *orig = HaversineMatrix(nlocs, locs, nlocs, locs);
	endtime= micro_now();

	timer = endtime - starttime;

	// now time tsp 
	starttime= micro_now();

	tsp_instance->name=(char *)"lk";
	tsp_instance->input_n = tsp_instance->n= nlocs;
	tsp_instance->edge_weight_type=EXPLICIT;
	//tsp_instance->edge_weight_format=LOWER_DIAG_ROW;
	tsp_instance->edge_weight_format=FULL_MATRIX; // would expect !!
	tsp_instance->edge_weights= new_arr_of(length_t*,tsp_instance->input_n);

	cost = cost_from_matrix_tsp;


	int row;
	for(row= 0;row<tsp_instance->input_n;row++){
		tsp_instance->edge_weights[row]= new_arr_of(length_t,tsp_instance->input_n);
	}
	
	// turn it into a symmetric problem 
	// lk con't do asymmetric,
	int col;
	for(row= 0;row<tsp_instance->input_n;row++){
		for(col= 0;col<tsp_instance->input_n;col++){
			tsp_instance->edge_weights[row][col] = mat_get(orig, row,col);
			tsp_instance->edge_weights[col][row] = mat_get(orig, row,col);
		}
	}

	//E2_create(tsp_instance);

	decluster_tree_t *mst;
	mst = decluster_setup(nlocs);
	length_t mst_len= decluster_mst(tsp_instance,mst);


/*
{
int i;
const int m= mst->n;
for(i= 0;i<m;i++) 
printf("e %d %d "length_t_native_spec"\n",
st->edge[i].city[0]+1,
mst->edge[i].city[1]+1,
length_t_native_pcast(mst->edge[i].cost));
}
*/

	decluster_preprocess(mst);


	nn_build(
	(candidate_expr&CAND_NN)?cand_nn_k:0,
	(candidate_expr&CAND_NQ)?cand_nq_k:0,
	(candidate_expr&CAND_DEL)?cand_del_d:0);

	tour= new_arr_of(int,nlocs);
incumbent_len=
construct(nlocs,tour,construction_algorithm,start_heuristic_param,random_seed);
//printf("Initial tour length: "length_t_spec"\n",length_t_pcast(incumbent_len));

	int i;
	for(i= 0;i<nlocs;i++){
		const int city= tour[i],next_city= tour[(i+1)%nlocs];
		//fprintf(stdout,"%f %f %f %f ue\n",
		//tsp_instance->coord[city].x[0],
		//tsp_instance->coord[city].x[1],
		//tsp_instance->coord[next_city].x[0],
		//tsp_instance->coord[next_city].x[1]);
	}

	tour_set(tour);

	prng_t*prng= prng_new(PRNG_DEFAULT,1998^random_seed);

	last_resource_mark= resource_mark("Lin-Kernighan");

	jbmr_run(2000,prng);

	int c;
//printf("Tour:\n");
for(i= 0,c= 0;i<nlocs;i++,c= tour_next(c)){
	solution[i].index = (original_city_num?original_city_num[c]:c);
//printf("%d ",(original_city_num?original_city_num[c]:c)+1);

//if((i%10)==9||i==nlocs-1)printf("\n");
}
	endtime= micro_now();
	solution->matrixtime=timer;
	solution->tsptime=endtime-starttime;

	return solution;
}
