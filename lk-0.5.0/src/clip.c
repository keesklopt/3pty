/*1:*/


#include <config.h>
#include "lkconfig.h"
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#include "length.h"
#include "read.h"

int noround= 0;

int
main(int argc,char**argv)
{
tsp_instance_t*tsp;
tsp= read_tsp_file(stdin,NULL,1);
write_tsp_file_clip(tsp,stdout,1);
return 0;
}

/*:1*/
