#include <config.h> 
#include "lkconfig.h"

#include <stdio.h> 
#include <stdlib.h> 
#include <stddef.h> 
#ifdef HAVE_UNISTD_H
#include <unistd.h> 
#endif
#if 0 
#if defined(__linux__)
size_t getpagesize(void);
#endif
#endif

#ifdef _WIN32
#include <time.h>
#else
#include <sys/time.h> 
#include <sys/resource.h> 
#endif
#define FIXINCLUDES_NEED_RESOURCE_STUFF
#include "fixincludes.h"
#undef FIXINCLUDES_NEED_RESOURCE_STUFF

#include "error.h"
#include "memory.h"
#include "resource.h"


#ifndef _WIN32
static struct rusage*mark= NULL;
#endif

static int max_marks= -1,next_mark= -1;
static char**mark_name= NULL;

#ifdef __linux__
int my_getrusage(unsigned int who,struct rusage*usage);
int my_getrusage(unsigned int who,struct rusage*usage)
{
int ret= getrusage(who,usage);
#if __linux__
char stat_file[200];
int dummyi;
unsigned dummyu,rss;
FILE*f;
sprintf(stat_file,"/proc/%d/stat",getpid());
f= fopen(stat_file,"r");
fscanf(f," %d %s %c %d  %d  %d  %d  %d %u %u %u %u %u %d %d %d %d %d %d %u %u %d %u %u ",
&dummyi,
stat_file,
stat_file,
&dummyi,
&dummyi,
&dummyi,
&dummyi,
&dummyi,
&dummyu,
&dummyu,
&dummyu,
&dummyu,
&dummyu,
&dummyi,
&dummyi,
&dummyi,
&dummyi,
&dummyi,
&dummyi,
&dummyu,
&dummyu,
&dummyi,
&dummyu,
&rss);
fclose(f);
usage->ru_maxrss= rss;
#endif
return ret;
}

#endif

void
resource_setup(const int m)
{
#ifdef __linux__
mark= new_arr_of(struct rusage,m);
mark_name= new_arr_of_zero(char*,m);
max_marks= m;
next_mark= 0;
#endif
}

/*:5*//*10:*/


void
resource_cleanup(void)
{
#ifdef __linux__
if(mark!=NULL){
free_mem(mark);
for (int i=0; i< max_marks; i++) {
    if (mark_name[i]) free(mark_name[i]);
}
free_mem(mark_name);
max_marks= next_mark= -1;
}
#endif
}

/*:10*//*12:*/


int
resource_mark(const char*name)
{
#ifdef __linux__
if(max_marks<0)resource_setup(10);
errorif(next_mark>=max_marks,
"Too many resource marks; you need at least %d",max_marks+1);
my_getrusage(RUSAGE_SELF,mark+next_mark);
printf("----Saving index [%d] : name is %s----\n", next_mark, name);
mark_name[next_mark]= dup_string(name);
next_mark++;
return next_mark-1;
#endif
return 0;
}

/*:12*//*15:*/


void
resource_report(FILE*out,int begin,int end)
{
#ifdef __linux__
float begin_s,end_s;
if(begin<0||begin> end||end>=next_mark){
fprintf(stderr,
"resource_report: bad begin==%d or end==%d (or both!); next_mark==%d.\n"
,begin,end,next_mark);
return;
}

begin_s= mark[begin].ru_utime.tv_sec+
mark[begin].ru_utime.tv_usec/((float)1000000);
end_s= mark[end].ru_utime.tv_sec+
mark[end].ru_utime.tv_usec/((float)1000000);
fprintf(out,"%s to\n\t%s\n\t%.2f user seconds",
mark_name[begin],mark_name[end],end_s-begin_s);

begin_s= mark[begin].ru_stime.tv_sec+
mark[begin].ru_stime.tv_usec/((float)1000000);
end_s= mark[end].ru_stime.tv_sec+
mark[end].ru_stime.tv_usec/((float)1000000);
fprintf(out," and %.2f system seconds\n",end_s-begin_s);

fprintf(out,"\tdelta max resident set size %8ld * %d bytes (ru_maxrss)\n",
mark[end].ru_maxrss-mark[begin].ru_maxrss,getpagesize());
fprintf(out,"\t      max resident set size %8ld * %d bytes = %ld\n",
mark[end].ru_maxrss,getpagesize(),mark[end].ru_maxrss*getpagesize());

fprintf(out,"\tdelta major page faults     %8ld (ru_majflt)\n",
mark[end].ru_majflt-mark[begin].ru_majflt);
fprintf(out,"\tdelta minor page faults     %8ld (ru_minflt)\n",
mark[end].ru_minflt-mark[begin].ru_minflt);
fflush(out);
#endif
}


double resource_user_tick(void){return resource_user_tick_from(next_mark-1);}

double
resource_user_tick_from(int marknum)
{
#ifdef __linux__
struct rusage r;
double begin_s,end_s;
errorif(max_marks<0,"resource_user_tick called before resource_setup");
errorif(next_mark<=0,"resource_mark must be called before resource_user_tick");
errorif(marknum<0||marknum>=next_mark,
"invalid mark number %d",marknum);
my_getrusage(RUSAGE_SELF,&r);
begin_s= mark[marknum].ru_utime.tv_sec+
mark[marknum].ru_utime.tv_usec/((double)1000000);
end_s= r.ru_utime.tv_sec+
r.ru_utime.tv_usec/((double)1000000);
return end_s-begin_s;
#endif
return 0.0;
}



void
resource_abnormal_exit_output(void)
{
#ifdef __linux__
int i;
if(mark!=NULL&&mark_name&&next_mark<max_marks){
resource_mark("abnormal ending");
}else{
fprintf(stdout,"Abnormal ending\n");
fprintf(stderr,"Abnormal ending\n");
}
for(i= 0;i<next_mark-1;i++){
resource_report(stdout,i,next_mark-1);
resource_report(stderr,i,next_mark-1);
}
#endif
}

const char*resource_rcs_id= "$Id: resource.w,v 1.126 2000/09/17 03:11:10 neto Exp neto $";

