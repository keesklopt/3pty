#define mark_dirty(CITY) (dirty_add(dirty_set,(CITY) ) )  \
 \

#define write_log(a) (change_log[change_log_next++]= a)  \

#define DEPTHS_BOUND (n+20) 
#define swap_bridge(a,b) {int temp= a[0];a[0]= b[0];b[0]= temp;temp= a[1];a[1]= b[1];b[1]= temp;}
#define bridge_less(a,b) (tour_inorder(edge[0][0],edge[0][1],a[0],b[0]) )  \
 \

#define bridge_t(X) (edge[(X-1) >>1][(X-1) &1]) 
#define bridge_move(a,b,c,d) (tour_flip_arb(bridge_t(a) ,bridge_t(b) ,bridge_t(c) ,bridge_t(d) ) )  \

#define mutate(a) (mutation[mutation_next++]= bridge_t(a) )  \

/*4:*/


#include <config.h> 
#include "lkconfig.h"
/*5:*/


#include <stdio.h> 
#if !defined(__USE_MISC)
#define __USE_MISC  
#endif
#include <stdlib.h> 
#include <stddef.h> 
#include <limits.h> 
#include "fixincludes.h"

/*:5*/


/*8:*/


#include "error.h"
#include "memory.h"
#include "length.h"
#include "read.h"
#include "nn.h"
#include "lk.h"


/*:8*/


/*7:*/


#include "prng.h"
#include "jbmr.h"


/*:7*//*16:*/


#include "dirty.h"

/*:16*//*65:*/


#include "decluster.h"
#include "declevel.h"

/*:65*//*117:*/


#include "pool.h"
#include "dict.h"


/*:117*//*127:*/


#include "tabuhash.h"

/*:127*//*134:*/


#include "milestone.h"

/*:134*//*142:*/


#include "resource.h"

/*:142*/



/*30:*/


#if LENGTH_TYPE_IS_EXACT || defined(JBMR_REQUIRE_JOINED_GAIN_VAR)
#define SPLIT_GAIN_VAR 0
#else
#define SPLIT_GAIN_VAR 1
#endif

/*:30*//*55:*/


#if LENGTH_TYPE_IS_EXACT
#define CAREFUL_OP(LHS,OP,RHS) ((LHS) OP (RHS))
#elif SPLIT_GAIN_VAR
#define CAREFUL_OP(LHS,OP,RHS) ((LHS##_pos) OP ((RHS##_with_slop)+(LHS##_neg)))
#else
#define CAREFUL_OP(LHS,OP,RHS) ((LHS) OP (RHS##_with_slop))
#endif




/*:55*//*169:*/


#if JBMR_MAX_VERBOSE||JBMR_REPORT_DEPTHS
#define TRACK_DEPTHS 1
#else
#define TRACK_DEPTHS 0
#endif

/*:169*/


/*46:*/


typedef struct{
length_t gain_for_comparison;
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
length_t cluster_dist;
#endif
#if !SPLIT_GAIN_VAR
length_t gain;
#else 
length_t gain_pos,gain_neg;
#endif 
int t2ip1,t2ip2,scheme_id;
}eligible_t;

/*:46*/


/*11:*/


static int n;

/*:11*//*40:*/


static int scheme[14][16]= {
{
1,2,5,6,
4,3,2,5},


{
1,2,6,5,
2,6,4,3,
1,5,4,6},


{
5,6,3,4,
1,2,6,3,
6,2,8,7,
1,3,2,8},


{
5,6,3,4,
8,7,6,3,
1,2,3,8},


{
1,2,3,4},


{
1,2,3,4,
1,4,6,5,
6,4,8,7,
1,5,4,8},


{
1,2,3,4,
6,5,8,7,
1,4,5,8},


{
1,2,3,4,
1,4,5,6},


{
1,2,5,6,
5,2,3,4},


{
6,5,8,7,
4,3,8,5,
1,2,3,8},


{
1,2,8,7,
1,7,6,5,
1,5,2,8,
4,3,2,5},


{
6,5,4,3,
6,3,8,7,
1,2,3,8},


{
6,5,8,7,
1,2,5,8,
5,2,3,4},


{-1}
};

static int scheme_max[14]= {8,12,16,12,4,16,12,8,8,12,16,12,12,0};
static int scheme_num_cities[14]= {6,6,8,8,4,8,8,6,6,8,8,8,8,0};

/*:40*//*89:*/


static int scheme_feas_check[14][10]= {
{-1},
{-1},
{1,3,3,6,2,6,1,8,2,8},
{3,8,1,8,3,6},
{-1},
{1,4,4,6,1,5,4,8},
{1,4,5,8,1,8},
{-1},
{-1},
{1,8,3,8,5,8},
{1,7,1,5,2,8,2,5,1,8},
{1,8,3,6,3,8},
{5,8,2,5,1,8},
{-1}
};

static int scheme_feas_n[14]= {0,0,10,6,0,8,6,0,0,6,10,6,6,0};

/*:89*//*144:*/


#if JBMR_MAX_VERBOSE >= 100 && defined(JBMR_WATCH_THIS_CITY)
static int old_verbose,old_verbose_is_set= 0;
#endif


/*:144*//*182:*/


#if TRACK_DEPTHS
static int*p_depths= NULL,*m_depths= NULL;
#endif

/*:182*/


/*27:*/


static int tour_inorder(int a,int b,int c,int d);
static int
tour_inorder(int a,int b,int c,int d){
if(tour_next(a)==b)return tour_between(b,c,d);
else if(tour_prev(a)==b)return tour_between(d,c,b);
else{
/*168:*/


{int i,c,cn;
printf("Tour: 0");
for(i= 0,c= 0;i<n;i++){
errorif(c==0&&i> 0,"Not a tour");
cn= tour_next(c);
printf(" %d",cn);
c= cn;
if(i%20==19)printf("\n");
}
printf("\n");
fflush(stdout);
errorif(c!=0,"Not a tour");
}

/*:168*/


errorif(1,"Bad tour_inorder(%d,%d,%d,%d)\n");
return-1;
}
}

/*:27*//*69:*/


static int
cmp_eligible(const void*a,const void*b)
{
length_t diff= ((const eligible_t*)a)->gain_for_comparison
-((const eligible_t*)b)->gain_for_comparison;
return diff> 0?-1:(diff<0?1:

#if defined(QSORT_DETERMINATE)
(int)(((eligible_t*)a)-((eligible_t*)b))
#else
0
#endif
);
}

/*:69*//*85:*/


static void tour_flip_arb(int a,int b,int c,int d);
static void
tour_flip_arb(int a,int b,int c,int d){
/*152:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("tour_flip_arb(%d,%d,%d,%d)",a,b,c,d);
fflush(stdout);
}
#endif

/*:152*/


if(b==d||a==c)return;
if(a==tour_next(b)&&d==tour_next(c)){
tour_flip(a,b,c,d);
/*154:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf(" case a\n");
fflush(stdout);
}
#endif

/*:154*/


}else if(a==tour_prev(b)&&d==tour_prev(c)){
tour_flip(b,a,d,c);
/*155:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf(" case b\n");
fflush(stdout);
}
#endif

/*:155*/


}else{
printf("\nNeighbour conditions not met\n");
/*168:*/


{int i,c,cn;
printf("Tour: 0");
for(i= 0,c= 0;i<n;i++){
errorif(c==0&&i> 0,"Not a tour");
cn= tour_next(c);
printf(" %d",cn);
c= cn;
if(i%20==19)printf("\n");
}
printf("\n");
fflush(stdout);
errorif(c!=0,"Not a tour");
}

/*:168*/


printf("\t(%d) %d (%d)",tour_prev(a),a,tour_next(a));
printf("\t(%d) %d (%d)",tour_prev(b),b,tour_next(b));
printf("\t(%d) %d (%d)",tour_prev(c),c,tour_next(c));
printf("\t(%d) %d (%d)",tour_prev(d),d,tour_next(d));
printf("\n");
errorif(1,"Neighbour conditions not met.");
}
}

/*:85*//*118:*/


#if defined(TABU_SPLAY)
static int cmp_pair(const void*a,const void*b);
static int
cmp_pair(const void*a,const void*b){
int a1= *((const int*)a),a2= *(((const int*)a)+1);
int b1= *((const int*)b),b2= *(((const int*)b)+1);
if(a1<a2){int t= a1;a1= a2;a2= t;}
if(b1<b2){int t= b1;b1= b2;b2= t;}
return a1==b1?a2-b2:a1-b1;
}
#endif


/*:118*//*124:*/


#if defined(TABU_SPLAY)
static void move_t(void*env,void**p);
static void
move_t(void*env,void**p){
int*old_t= ((int**)env)[0],*t= ((int**)env)[1];
*p= (void*)(t+((int*)(*p)-old_t));
}
#endif


/*:124*//*157:*/


#if JBMR_MAX_VERBOSE >= 300
#if !SPLIT_GAIN_VAR
#define put_city(X) \
if ( verbose >= 300 ) { \
 int i;\
 length_t cg =  cum_gain, bg =  best_gain; \
 for ( i= 0;i<(X);i++) {printf(" ");}\
 printf("t%d == %d p=%d n=%d",(X),t[(X)],tour_prev(t[(X)]),tour_next(t[(X)]));\
 if (X> 1) { \
  int c =  cost(t[(X)-1],t[X]); \
  printf(" c(t%d,t%d)=%d",(X)-1,(X),c); \
  c =  cost(t[(X)],t[1]); \
  printf(" c(t%d,t1)=%d",(X),c); \
 } \
 printf("\tcg "length_t_spec" bg "length_t_spec"\n",length_t_pcast(cg),\
  length_t_pcast(bg)); \
 fflush(stdout); \
}
#else
#define put_city(X) \
if ( verbose >= 300 ) { \
 int i;\
 length_t cg =  cum_gain_pos-cum_gain_neg, bg =  best_gain; \
 for ( i= 0;i<(X);i++) {printf(" ");}\
 printf("t%d == %d p=%d n=%d",(X),t[(X)],tour_prev(t[(X)]),tour_next(t[(X)]));\
 if (X> 1) { \
  int c =  cost(t[(X)-1],t[X]); \
  printf(" c(t%d,t%d)=%d",(X)-1,(X),c); \
  c =  cost(t[(X)],t[1]); \
  printf(" c(t%d,t1)=%d",(X),c); \
 } \
 printf("\tcg "length_t_spec" bg "length_t_spec"\n",length_t_pcast(cg),\
  length_t_pcast(bg)); \
 fflush(stdout); \
}

#endif
#else
#define put_city(X)
#endif

/*:157*/


/*9:*/


void
jbmr_setup(int the_n){
n= the_n;
/*180:*/


#if TRACK_DEPTHS && defined(TABU_JBMR)
p_depths= new_arr_of_zero(int,DEPTHS_BOUND);
m_depths= new_arr_of_zero(int,DEPTHS_BOUND);
#endif


/*:180*/


}

/*:9*//*12:*/


void
jbmr_cleanup(void)
{
n= 0;
/*181:*/


#if TRACK_DEPTHS
free_mem(p_depths);mem_deduct(DEPTHS_BOUND*sizeof(int));
free_mem(m_depths);mem_deduct(DEPTHS_BOUND*sizeof(int));
#endif

/*:181*/


}

/*:12*//*21:*/


void
jbmr_run(const int iterations,prng_t*random_stream)
{
/*15:*/


dirty_set_t*dirty_set;

/*:15*//*23:*/


int*t,t_max_alloc;

/*:23*//*31:*/


#if SPLIT_GAIN_VAR
length_t cum_gain_pos,cum_gain_neg;
#endif

/*:31*//*32:*/


length_t best_gain;

/*:32*//*33:*/


#if !SPLIT_GAIN_VAR
length_t cum_gain;
#endif

/*:33*//*34:*/


int best_two_i,best_exit_a,best_exit_b,best_scheme_id;


/*:34*//*35:*/


int more_backtracking;

/*:35*//*36:*/


int two_i;

/*:36*//*41:*/


int scheme_id,base_scheme[9];


/*:41*//*47:*/


eligible_t*(e[4]);

/*:47*//*50:*/


int en[4];

/*:50*//*51:*/


int ec[4];


/*:51*//*52:*/


#if !(LENGTH_TYPE_IS_EXACT)
length_t instance_epsilon= incumbent_len*LENGTH_MACHINE_EPSILON;
length_t best_gain_with_slop;
#endif

/*:52*//*57:*/


int num_reject_by_cum_1,num_reject_pre_e_build;
const int no_extra_backtrack= !extra_backtrack;

/*:57*//*61:*/


#if !defined(JBMR_UNROLL_PREV_NEXT_LOOP)
int(*tour_neighbour[2])(int);
#endif

/*:61*//*78:*/


int*change_log= NULL,change_log_max_alloc,change_log_next= 0;


/*:78*//*95:*/


int last_special_two_i;
int generic_flips_made;

/*:95*//*115:*/


#if defined(TABU_SPLAY)
dict_t*tabu= dict_create(cmp_pair,NULL);
#endif

/*:115*//*126:*/


#if defined(TABU_HASH)
tabu_hash_t*tabu_hash= NULL;
tabu_hash_t*(*tabu_hash_create)(int vertex_bound,int max_size)= NULL;
void(*tabu_hash_destroy)(tabu_hash_t*th)= NULL;
int(*tabu_hash_includes)(tabu_hash_t*th,int u,int v)= NULL;
void(*tabu_hash_add)(tabu_hash_t*th,int u,int v)= NULL;
void(*tabu_hash_make_empty)(tabu_hash_t*th)= NULL;
#endif

/*:126*//*135:*/


milestone_state_t ms_lower_bound;
milestone_state_t ms_upper_bound;

/*:135*//*161:*/


int num_reject_by_decluster;


/*:161*//*177:*/


#if TRACK_DEPTHS
int probe_depth,move_depth;
#endif

/*:177*//*179:*/


#if JBMR_MAX_VERBOSE >= 125
int probes= 0;
#endif

/*:179*//*186:*/


#if TRACK_DEPTHS
int last_probe_depth= 0;
#endif


/*:186*//*189:*/


length_t previous_incumbent_len= 0;

/*:189*//*199:*/


int mutation[12];

/*:199*/


int iteration;

/*58:*/


num_reject_by_cum_1= num_reject_pre_e_build= 0;


/*:58*//*62:*/


#if !defined(JBMR_UNROLL_PREV_NEXT_LOOP)
tour_neighbour[0]= tour_prev;
tour_neighbour[1]= tour_next;
#endif

/*:62*//*128:*/


#if defined(TABU_HASH)
{const int max_tabu_size
= max_generic_flips<(INT_MAX-4)?max_generic_flips+4:n;
if(max_tabu_size<=n){
tabu_hash_create= tabu_hash_bd_create;
tabu_hash_destroy= tabu_hash_bd_destroy;
tabu_hash_includes= tabu_hash_bd_includes;
tabu_hash_add= tabu_hash_bd_add;
tabu_hash_make_empty= tabu_hash_bd_make_empty;
}else{
tabu_hash_create= tabu_hash_unbd_create;
tabu_hash_destroy= tabu_hash_unbd_destroy;
tabu_hash_includes= tabu_hash_unbd_includes;
tabu_hash_add= tabu_hash_unbd_add;
tabu_hash_make_empty= tabu_hash_unbd_make_empty;
}
tabu_hash= tabu_hash_create(n,max_tabu_size);
}
#endif

/*:128*//*136:*/


milestone_initialize(&ms_lower_bound,lower_bound_name,(length_t)lower_bound_value,
begin_data_structures_mark);
milestone_initialize(&ms_upper_bound,upper_bound_name,(length_t)upper_bound_value,
begin_data_structures_mark);
milestone_show_initial(&ms_lower_bound,incumbent_len);
milestone_show_initial(&ms_upper_bound,incumbent_len);

/*:136*//*162:*/


num_reject_by_decluster= 0;

/*:162*/


/*24:*/


t_max_alloc= 128;
// KEES changed because of valgrind complaints
//t= new_arr_of(int,t_max_alloc);
//  IN :
t= new_arr_of_zero(int,t_max_alloc);

/*:24*//*48:*/


e[0]= new_arr_of(eligible_t,nn_max_bound*2);
e[1]= new_arr_of(eligible_t,nn_max_bound*2);
e[2]= new_arr_of(eligible_t,nn_max_bound*2);
e[3]= new_arr_of(eligible_t,nn_max_bound*2);

/*:48*//*76:*/


change_log_max_alloc= 10000;
change_log= new_arr_of(int,change_log_max_alloc);

/*:76*/


/*18:*/


dirty_set= dirty_create(n,1,prng_unif_int(random_stream,n),__FILE__,__LINE__);

/*:18*/



for(iteration= 0;iteration<iterations;iteration++){
int dirty;
/*96:*/


last_special_two_i= INT_MAX;
generic_flips_made= 0;




/*:96*/


while((dirty= dirty_remove(dirty_set))>=0){
/*42:*/


{
int t1_n[2],t1_i;
length_t t1_l[2];
/*143:*/


#if JBMR_MAX_VERBOSE >= 100
if(verbose>=100){
printf("Search for an improvement starting at city %d\n",dirty);
fflush(stdout);
#if defined(JBMR_WATCH_THIS_CITY)
if(dirty==JBMR_WATCH_THIS_CITY){
old_verbose= verbose;
old_verbose_is_set= 1;
verbose= 2000;
}else if(old_verbose_is_set){
verbose= old_verbose;
old_verbose_is_set= 0;
}
#endif
}
#endif

/*:143*/


t[1]= dirty;
t1_n[0]= tour_prev(t[1]);
t1_n[1]= tour_next(t[1]);
t1_l[0]= cost(t1_n[0],t[1]);
t1_l[1]= cost(t1_n[1],t[1]);
#if JBMR_FARTHER_T1_FIRST
if(t1_l[0]<t1_l[1]){
int tmp;
length_t tmp_l;
tmp= t1_n[0];t1_n[0]= t1_n[1];t1_n[1]= tmp;
tmp_l= t1_l[0];t1_l[0]= t1_l[1];t1_l[1]= tmp_l;
}
#endif

/*43:*/


#if !SPLIT_GAIN_VAR
cum_gain= 0;
#else
cum_gain_pos= cum_gain_neg= 0;
#endif
best_gain= 0;best_two_i= 0;best_exit_a= best_exit_b= -1;
more_backtracking= 1;scheme_id= best_scheme_id= -1;

/*:43*//*53:*/


#if !(LENGTH_TYPE_IS_EXACT)
best_gain_with_slop= instance_epsilon;
#endif



/*:53*//*176:*/


#if TRACK_DEPTHS
probe_depth= move_depth= 0;
#endif

/*:176*/


put_city(1);
for(t1_i= 0;t1_i<2&&more_backtracking;t1_i++){
t[2]= t1_n[t1_i];
/*185:*/


#if JBMR_MAX_VERBOSE >= 70
if(verbose>=70){
printf("(t1,t2)= (%d,%d) cost=%f, d=%f p=%d\n",t[1],t[2],(double)cost(t[1],t[2]),
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
(double)decluster_d(t[1],t[2])
#else
(double)-1
#endif
,last_probe_depth);
}
#endif

/*:185*/


#if !SPLIT_GAIN_VAR
cum_gain= t1_l[t1_i];
#else
cum_gain_pos= t1_l[t1_i];
cum_gain_neg= 0;
#endif
put_city(2);
/*45:*/


two_i= 2;
/*175:*/


#if TRACK_DEPTHS
if(probe_depth<two_i+2)(probe_depth= two_i+2);
#endif


/*:175*/


#define BL 0
{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
const length_t cluster_dist= decluster_d(t[1],t[2]);
#else
const length_t cluster_dist= 0;
#endif
/*56:*/


en[BL]= ec[BL]= 0;
if(CAREFUL_OP(cum_gain,> ,cluster_dist+best_gain)){
int i,t2ip1,t2ip2,enbl,*neighbour_list,nn_bound;
#if SPLIT_GAIN_VAR
length_t cum_1_pos,cum_1_neg,cum_2_pos,cum_2_neg;
#else
length_t cum_1,cum_2;
#endif
/*156:*/


#if JBMR_MAX_VERBOSE >= 400
if(verbose>=400){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d %d "length_t_spec" "length_t_spec" s%d\n",t[two_i-1],t[two_i],
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_gain),
#else
length_t_pcast(cum_gain_pos-cum_gain_neg),
#endif
length_t_pcast(best_gain),scheme_id);
fflush(stdout);
}
#endif



/*:156*/




#if SPLIT_GAIN_VAR
cum_1_pos= cum_gain_pos;
#endif

neighbour_list= nn_list(t[two_i],&nn_bound);
for(i= 0,enbl= 0;i<nn_bound;i++){
t2ip1= neighbour_list[i];
#if SPLIT_GAIN_VAR
cum_1_neg= cum_gain_neg+cost(t[two_i],t2ip1);
#else
cum_1= cum_gain-cost(t[two_i],t2ip1);
#endif

if(CAREFUL_OP(cum_1,<=,best_gain)){
/*163:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("Terminating |cum_1| "length_t_spec"\n",length_t_pcast(cum_1));
#else
printf("Terminating |cum_1| "length_t_spec" "
"(== "length_t_spec" - "length_t_spec")\n",
length_t_pcast(cum_1_pos-cum_1_neg),
length_t_pcast(cum_1_pos),
length_t_pcast(cum_1_neg));
#endif
fflush(stdout);
}
#endif

/*:163*/


num_reject_by_cum_1++;
break;
}

/*59:*/


#if BL==0
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


#endif

/*:59*//*79:*/


#if BL==1
if(t2ip1!=t[3]){
switch(e[0][ec[0]].scheme_id){
case 0:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[3])?0:2;break;
case 4:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[4])?5:8;break;
default:errorif(1,"Non exhaustive switch: %d",e[0][ec[0]].scheme_id);
}
/*165:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme5 == %d\n",base_scheme[5]);
fflush(stdout);
}
#endif

/*:165*/


/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
#endif


/*:79*//*86:*/


#if BL==2
if(t2ip1!=t[5]){
int i1,i2,i4;
switch(base_scheme[7]= e[1][ec[1]].scheme_id){
case 2:i1= 1;i2= 2;i4= 3;break;
case 5:i1= 6,i2= 5,i4= 4;break;
case 9:i1= 1,i2= 2,i4= 5;break;
default:errorif(1,"Got to 4-change in base scheme %d",base_scheme[7]);
i1= i2= i4= -1;
break;
}
if(tour_inorder(t[i1],t[i2],t2ip1,t[i4])){
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
}
#endif


/*:86*//*97:*/


#if BL==3
if(t[two_i]==tour_prev(t[1]))t2ip2= tour_next(t2ip1);
else t2ip2= tour_prev(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


#endif


/*:97*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
en[BL]= enbl;
}else{
num_reject_pre_e_build++;
}

/*:56*/


}
/*68:*/


sort(e[BL],(size_t)en[BL],sizeof(eligible_t),cmp_eligible);

/*:68*/


#undef BL
/*71:*/


for(ec[0]= 0;more_backtracking&&ec[0]<en[0];ec[0]++){
eligible_t*this_move= &e[0][ec[0]];
t[3]= this_move->t2ip1;
t[4]= this_move->t2ip2;
#if !SPLIT_GAIN_VAR
cum_gain= this_move->gain;
#else
cum_gain_pos= this_move->gain_pos;
cum_gain_neg= this_move->gain_neg;
#endif
two_i= 4;
/*175:*/


#if TRACK_DEPTHS
if(probe_depth<two_i+2)(probe_depth= two_i+2);
#endif


/*:175*/


#define BL 1
{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
const length_t cluster_dist= this_move->cluster_dist;
#else
const length_t cluster_dist= 0;
#endif
/*56:*/


en[BL]= ec[BL]= 0;
if(CAREFUL_OP(cum_gain,> ,cluster_dist+best_gain)){
int i,t2ip1,t2ip2,enbl,*neighbour_list,nn_bound;
#if SPLIT_GAIN_VAR
length_t cum_1_pos,cum_1_neg,cum_2_pos,cum_2_neg;
#else
length_t cum_1,cum_2;
#endif
/*156:*/


#if JBMR_MAX_VERBOSE >= 400
if(verbose>=400){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d %d "length_t_spec" "length_t_spec" s%d\n",t[two_i-1],t[two_i],
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_gain),
#else
length_t_pcast(cum_gain_pos-cum_gain_neg),
#endif
length_t_pcast(best_gain),scheme_id);
fflush(stdout);
}
#endif



/*:156*/




#if SPLIT_GAIN_VAR
cum_1_pos= cum_gain_pos;
#endif

neighbour_list= nn_list(t[two_i],&nn_bound);
for(i= 0,enbl= 0;i<nn_bound;i++){
t2ip1= neighbour_list[i];
#if SPLIT_GAIN_VAR
cum_1_neg= cum_gain_neg+cost(t[two_i],t2ip1);
#else
cum_1= cum_gain-cost(t[two_i],t2ip1);
#endif

if(CAREFUL_OP(cum_1,<=,best_gain)){
/*163:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("Terminating |cum_1| "length_t_spec"\n",length_t_pcast(cum_1));
#else
printf("Terminating |cum_1| "length_t_spec" "
"(== "length_t_spec" - "length_t_spec")\n",
length_t_pcast(cum_1_pos-cum_1_neg),
length_t_pcast(cum_1_pos),
length_t_pcast(cum_1_neg));
#endif
fflush(stdout);
}
#endif

/*:163*/


num_reject_by_cum_1++;
break;
}

/*59:*/


#if BL==0
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


#endif

/*:59*//*79:*/


#if BL==1
if(t2ip1!=t[3]){
switch(e[0][ec[0]].scheme_id){
case 0:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[3])?0:2;break;
case 4:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[4])?5:8;break;
default:errorif(1,"Non exhaustive switch: %d",e[0][ec[0]].scheme_id);
}
/*165:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme5 == %d\n",base_scheme[5]);
fflush(stdout);
}
#endif

/*:165*/


/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
#endif


/*:79*//*86:*/


#if BL==2
if(t2ip1!=t[5]){
int i1,i2,i4;
switch(base_scheme[7]= e[1][ec[1]].scheme_id){
case 2:i1= 1;i2= 2;i4= 3;break;
case 5:i1= 6,i2= 5,i4= 4;break;
case 9:i1= 1,i2= 2,i4= 5;break;
default:errorif(1,"Got to 4-change in base scheme %d",base_scheme[7]);
i1= i2= i4= -1;
break;
}
if(tour_inorder(t[i1],t[i2],t2ip1,t[i4])){
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
}
#endif


/*:86*//*97:*/


#if BL==3
if(t[two_i]==tour_prev(t[1]))t2ip2= tour_next(t2ip1);
else t2ip2= tour_prev(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


#endif


/*:97*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
en[BL]= enbl;
}else{
num_reject_pre_e_build++;
}

/*:56*/


}
/*68:*/


sort(e[BL],(size_t)en[BL],sizeof(eligible_t),cmp_eligible);

/*:68*/


#undef BL
/*82:*/


for(ec[1]= 0;more_backtracking&&ec[1]<en[1];ec[1]++){
eligible_t*this_move= &e[1][ec[1]];
t[5]= this_move->t2ip1;
t[6]= this_move->t2ip2;
#if !SPLIT_GAIN_VAR
cum_gain= this_move->gain;
#else
cum_gain_pos= this_move->gain_pos;
cum_gain_neg= this_move->gain_neg;
#endif
two_i= 6;
/*175:*/


#if TRACK_DEPTHS
if(probe_depth<two_i+2)(probe_depth= two_i+2);
#endif


/*:175*/


if(scheme_num_cities[this_move->scheme_id]==6){
scheme_id= this_move->scheme_id;
/*84:*/


{int i,n= scheme_max[scheme_id],*s= &scheme[scheme_id][0];
/*149:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Implement scheme %d\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:149*/


for(i= 0;i<n;i+= 4){
tour_flip_arb(t[s[i]],t[s[i+1]],t[s[i+2]],t[s[i+3]]);
}
}

/*:84*/


/*93:*/


{int go_deeper;
last_special_two_i= two_i;
generic_flips_made= 0;
/*111:*/



/*:111*//*120:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{
int i;
for(i= 2;i<two_i;i+= 2)dict_insert(tabu,t+i);
}
#endif

/*:120*//*130:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{
int i;
for(i= 2;i<two_i;i+= 2)tabu_hash_add(tabu_hash,t[i],t[i+1]);
}
#endif

/*:130*/


/*147:*/


#if JBMR_MAX_VERBOSE >= 175
if(verbose>=175){
printf("Start generic search\n");
fflush(stdout);
}
#endif


/*:147*/


for(go_deeper= 1;go_deeper;){
/*94:*/


#if defined(JBMR_LIMIT_PROBE_DEPTH)
if(generic_flips_made>=max_generic_flips){
/*187:*/


#if JBMR_MAX_VERBOSE >= 75
if(verbose>=75){
printf(" hit max generic flips: %d >= %d\n",
generic_flips_made,max_generic_flips);
}
#endif

/*:187*/


/*102:*/


if(best_gain==0){
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


}else if(best_scheme_id==13){
/*103:*/


{
int low_j,best_is_prefix;
errorif(best_gain<=0,"Bad best_scheme_id == 13");
best_is_prefix= t[best_two_i+1]==best_exit_a
&&t[best_two_i+2]==best_exit_b;
low_j= best_two_i+(best_is_prefix?2:0);
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


if(!best_is_prefix){
tour_flip_arb(t[1],t[best_two_i],best_exit_a,best_exit_b);
}
/*73:*/


{int i;
for(i= 1;i<=best_two_i;i++){
mark_dirty(t[i]);
}
mark_dirty(best_exit_a);
mark_dirty(best_exit_b);
}

/*:73*//*74:*/


if(iteration> 0){
const int more_log= 4+best_two_i;
/*75:*/


{const int need_size= more_log+change_log_next;
if(need_size>=change_log_max_alloc){
do{
change_log_max_alloc*= 2;
}while(need_size>=change_log_max_alloc);
change_log= (int*)mem_realloc(change_log,sizeof(int)*change_log_max_alloc);
}
}

/*:75*/


{int j;
for(j= 1;j<=best_two_i;j++){
write_log(t[j]);
}
}
write_log(best_exit_a);
write_log(best_exit_b);
write_log(scheme_id);
write_log(3+best_two_i);
}


/*:74*/


/*173:*/


#if TRACK_DEPTHS
move_depth= best_two_i+2;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:173*/


more_backtracking= 0;
}

/*:103*/


}

/*:102*/


go_deeper= 0;
break;
}
#endif

/*:94*/


/*175:*/


#if TRACK_DEPTHS
if(probe_depth<two_i+2)(probe_depth= two_i+2);
#endif


/*:175*/


/*26:*/


if(two_i+3>=t_max_alloc){
#if defined(TABU_SPLAY)
int*old_t= t;
#endif
do{
t_max_alloc*= 2;
}while(two_i+3>=t_max_alloc);
t= (int*)mem_realloc(t,sizeof(int)*t_max_alloc);
/*123:*/


#if defined(TABU_SPLAY)
{int*env[2];
env[0]= old_t;
env[1]= t;
dict_update_all(tabu,move_t,env);
}
#endif

/*:123*/


}


/*:26*/


#define BL 3
{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
const length_t cluster_dist= decluster_d(t[1],t[two_i]);
#else
const length_t cluster_dist= 0;
#endif
/*56:*/


en[BL]= ec[BL]= 0;
if(CAREFUL_OP(cum_gain,> ,cluster_dist+best_gain)){
int i,t2ip1,t2ip2,enbl,*neighbour_list,nn_bound;
#if SPLIT_GAIN_VAR
length_t cum_1_pos,cum_1_neg,cum_2_pos,cum_2_neg;
#else
length_t cum_1,cum_2;
#endif
/*156:*/


#if JBMR_MAX_VERBOSE >= 400
if(verbose>=400){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d %d "length_t_spec" "length_t_spec" s%d\n",t[two_i-1],t[two_i],
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_gain),
#else
length_t_pcast(cum_gain_pos-cum_gain_neg),
#endif
length_t_pcast(best_gain),scheme_id);
fflush(stdout);
}
#endif



/*:156*/




#if SPLIT_GAIN_VAR
cum_1_pos= cum_gain_pos;
#endif

neighbour_list= nn_list(t[two_i],&nn_bound);
for(i= 0,enbl= 0;i<nn_bound;i++){
t2ip1= neighbour_list[i];
#if SPLIT_GAIN_VAR
cum_1_neg= cum_gain_neg+cost(t[two_i],t2ip1);
#else
cum_1= cum_gain-cost(t[two_i],t2ip1);
#endif

if(CAREFUL_OP(cum_1,<=,best_gain)){
/*163:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("Terminating |cum_1| "length_t_spec"\n",length_t_pcast(cum_1));
#else
printf("Terminating |cum_1| "length_t_spec" "
"(== "length_t_spec" - "length_t_spec")\n",
length_t_pcast(cum_1_pos-cum_1_neg),
length_t_pcast(cum_1_pos),
length_t_pcast(cum_1_neg));
#endif
fflush(stdout);
}
#endif

/*:163*/


num_reject_by_cum_1++;
break;
}

/*59:*/


#if BL==0
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


#endif

/*:59*//*79:*/


#if BL==1
if(t2ip1!=t[3]){
switch(e[0][ec[0]].scheme_id){
case 0:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[3])?0:2;break;
case 4:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[4])?5:8;break;
default:errorif(1,"Non exhaustive switch: %d",e[0][ec[0]].scheme_id);
}
/*165:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme5 == %d\n",base_scheme[5]);
fflush(stdout);
}
#endif

/*:165*/


/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
#endif


/*:79*//*86:*/


#if BL==2
if(t2ip1!=t[5]){
int i1,i2,i4;
switch(base_scheme[7]= e[1][ec[1]].scheme_id){
case 2:i1= 1;i2= 2;i4= 3;break;
case 5:i1= 6,i2= 5,i4= 4;break;
case 9:i1= 1,i2= 2,i4= 5;break;
default:errorif(1,"Got to 4-change in base scheme %d",base_scheme[7]);
i1= i2= i4= -1;
break;
}
if(tour_inorder(t[i1],t[i2],t2ip1,t[i4])){
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
}
#endif


/*:86*//*97:*/


#if BL==3
if(t[two_i]==tour_prev(t[1]))t2ip2= tour_next(t2ip1);
else t2ip2= tour_prev(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


#endif


/*:97*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
en[BL]= enbl;
}else{
num_reject_pre_e_build++;
}

/*:56*/


}
#undef BL
if(en[3]> 0){
/*101:*/


{int i,best_i;
length_t best_len;
/*164:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
int i;
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("go deeper candidates begin\n");
for(i= 0;i<en[3];i++){
#if !SPLIT_GAIN_VAR
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("%d %d "length_t_spec" s%d\n",
e[3][i].t2ip1,
e[3][i].t2ip2,
length_t_pcast(e[3][i].gain),
e[3][i].scheme_id);
#else
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("%d %d "length_t_spec" (=="length_t_spec"-"
length_t_spec") s%d\n",
e[3][i].t2ip1,
e[3][i].t2ip2,
length_t_pcast(e[3][i].gain_pos-e[3][i].gain_neg),
length_t_pcast(e[3][i].gain_pos),
length_t_pcast(e[3][i].gain_neg),
e[3][i].scheme_id);
#endif
}
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("go deeper candidates end\n");
fflush(stdout);
}
#endif

/*:164*/


best_i= 0;
best_len= e[3][0].gain_for_comparison;
for(i= 1;i<en[3];i++){
if(best_len<e[3][i].gain_for_comparison){
best_i= i;
best_len= e[3][i].gain_for_comparison;
}
}
if(best_len==0){errorif(1,"Shouldn't be going deeper.");}
#if !SPLIT_GAIN_VAR
cum_gain= e[3][best_i].gain;
#else
cum_gain_pos= e[3][best_i].gain_pos;
cum_gain_neg= e[3][best_i].gain_neg;
#endif
t[two_i+1]= e[3][best_i].t2ip1;
t[two_i+2]= e[3][best_i].t2ip2;
tour_flip_arb(t[1],t[two_i],t[two_i+1],t[two_i+2]);
/*112:*/



/*:112*//*121:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
dict_insert(tabu,t+two_i);
#endif

/*:121*//*131:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
tabu_hash_add(tabu_hash,t[two_i],t[two_i+1]);
#endif


/*:131*/


two_i+= 2;
generic_flips_made++;
/*145:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips made: %d\n",generic_flips_made);
fflush(stdout);
}
#endif

/*:145*/


}

/*:101*/


}else{
/*102:*/


if(best_gain==0){
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


}else if(best_scheme_id==13){
/*103:*/


{
int low_j,best_is_prefix;
errorif(best_gain<=0,"Bad best_scheme_id == 13");
best_is_prefix= t[best_two_i+1]==best_exit_a
&&t[best_two_i+2]==best_exit_b;
low_j= best_two_i+(best_is_prefix?2:0);
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


if(!best_is_prefix){
tour_flip_arb(t[1],t[best_two_i],best_exit_a,best_exit_b);
}
/*73:*/


{int i;
for(i= 1;i<=best_two_i;i++){
mark_dirty(t[i]);
}
mark_dirty(best_exit_a);
mark_dirty(best_exit_b);
}

/*:73*//*74:*/


if(iteration> 0){
const int more_log= 4+best_two_i;
/*75:*/


{const int need_size= more_log+change_log_next;
if(need_size>=change_log_max_alloc){
do{
change_log_max_alloc*= 2;
}while(need_size>=change_log_max_alloc);
change_log= (int*)mem_realloc(change_log,sizeof(int)*change_log_max_alloc);
}
}

/*:75*/


{int j;
for(j= 1;j<=best_two_i;j++){
write_log(t[j]);
}
}
write_log(best_exit_a);
write_log(best_exit_b);
write_log(scheme_id);
write_log(3+best_two_i);
}


/*:74*/


/*173:*/


#if TRACK_DEPTHS
move_depth= best_two_i+2;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:173*/


more_backtracking= 0;
}

/*:103*/


}

/*:102*/


go_deeper= 0;
}
}
/*113:*/



/*:113*//*122:*/


#if defined(TABU_SPLAY)
dict_delete_all(tabu,NULL);
#endif

/*:122*//*132:*/


#if defined(TABU_HASH)
tabu_hash_make_empty(tabu_hash);
#endif

/*:132*/


}

/*:93*/


/*83:*/


if(more_backtracking){
if(best_gain> 0&&scheme_num_cities[best_scheme_id]==6){
if(best_exit_a==t[5]&&best_exit_b==t[6]){
/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


}else{
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


t[5]= best_exit_a;t[6]= best_exit_b;
scheme_id= best_scheme_id;
/*84:*/


{int i,n= scheme_max[scheme_id],*s= &scheme[scheme_id][0];
/*149:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Implement scheme %d\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:149*/


for(i= 0;i<n;i+= 4){
tour_flip_arb(t[s[i]],t[s[i+1]],t[s[i+2]],t[s[i+3]]);
}
}

/*:84*/


}
/*73:*/


{int i;
for(i= 1;i<=best_two_i;i++){
mark_dirty(t[i]);
}
mark_dirty(best_exit_a);
mark_dirty(best_exit_b);
}

/*:73*//*74:*/


if(iteration> 0){
const int more_log= 4+best_two_i;
/*75:*/


{const int need_size= more_log+change_log_next;
if(need_size>=change_log_max_alloc){
do{
change_log_max_alloc*= 2;
}while(need_size>=change_log_max_alloc);
change_log= (int*)mem_realloc(change_log,sizeof(int)*change_log_max_alloc);
}
}

/*:75*/


{int j;
for(j= 1;j<=best_two_i;j++){
write_log(t[j]);
}
}
write_log(best_exit_a);
write_log(best_exit_b);
write_log(scheme_id);
write_log(3+best_two_i);
}


/*:74*/


/*171:*/


#if TRACK_DEPTHS
move_depth= 6;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:171*/


more_backtracking= 0;
}else{
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


}
}

/*:83*/


}else{
#define BL 2
{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
const length_t cluster_dist= this_move->cluster_dist;
#else
const length_t cluster_dist= 0;
#endif
/*56:*/


en[BL]= ec[BL]= 0;
if(CAREFUL_OP(cum_gain,> ,cluster_dist+best_gain)){
int i,t2ip1,t2ip2,enbl,*neighbour_list,nn_bound;
#if SPLIT_GAIN_VAR
length_t cum_1_pos,cum_1_neg,cum_2_pos,cum_2_neg;
#else
length_t cum_1,cum_2;
#endif
/*156:*/


#if JBMR_MAX_VERBOSE >= 400
if(verbose>=400){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d %d "length_t_spec" "length_t_spec" s%d\n",t[two_i-1],t[two_i],
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_gain),
#else
length_t_pcast(cum_gain_pos-cum_gain_neg),
#endif
length_t_pcast(best_gain),scheme_id);
fflush(stdout);
}
#endif



/*:156*/




#if SPLIT_GAIN_VAR
cum_1_pos= cum_gain_pos;
#endif

neighbour_list= nn_list(t[two_i],&nn_bound);
for(i= 0,enbl= 0;i<nn_bound;i++){
t2ip1= neighbour_list[i];
#if SPLIT_GAIN_VAR
cum_1_neg= cum_gain_neg+cost(t[two_i],t2ip1);
#else
cum_1= cum_gain-cost(t[two_i],t2ip1);
#endif

if(CAREFUL_OP(cum_1,<=,best_gain)){
/*163:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("Terminating |cum_1| "length_t_spec"\n",length_t_pcast(cum_1));
#else
printf("Terminating |cum_1| "length_t_spec" "
"(== "length_t_spec" - "length_t_spec")\n",
length_t_pcast(cum_1_pos-cum_1_neg),
length_t_pcast(cum_1_pos),
length_t_pcast(cum_1_neg));
#endif
fflush(stdout);
}
#endif

/*:163*/


num_reject_by_cum_1++;
break;
}

/*59:*/


#if BL==0
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


#endif

/*:59*//*79:*/


#if BL==1
if(t2ip1!=t[3]){
switch(e[0][ec[0]].scheme_id){
case 0:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[3])?0:2;break;
case 4:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[4])?5:8;break;
default:errorif(1,"Non exhaustive switch: %d",e[0][ec[0]].scheme_id);
}
/*165:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme5 == %d\n",base_scheme[5]);
fflush(stdout);
}
#endif

/*:165*/


/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
#endif


/*:79*//*86:*/


#if BL==2
if(t2ip1!=t[5]){
int i1,i2,i4;
switch(base_scheme[7]= e[1][ec[1]].scheme_id){
case 2:i1= 1;i2= 2;i4= 3;break;
case 5:i1= 6,i2= 5,i4= 4;break;
case 9:i1= 1,i2= 2,i4= 5;break;
default:errorif(1,"Got to 4-change in base scheme %d",base_scheme[7]);
i1= i2= i4= -1;
break;
}
if(tour_inorder(t[i1],t[i2],t2ip1,t[i4])){
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
}
#endif


/*:86*//*97:*/


#if BL==3
if(t[two_i]==tour_prev(t[1]))t2ip2= tour_next(t2ip1);
else t2ip2= tour_prev(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


#endif


/*:97*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
en[BL]= enbl;
}else{
num_reject_pre_e_build++;
}

/*:56*/


}
/*68:*/


sort(e[BL],(size_t)en[BL],sizeof(eligible_t),cmp_eligible);

/*:68*/


#undef BL
/*91:*/


for(ec[2]= 0;more_backtracking&&ec[2]<en[2];ec[2]++){
eligible_t*this_move= &e[2][ec[2]];
t[7]= this_move->t2ip1;
t[8]= this_move->t2ip2;
#if !SPLIT_GAIN_VAR
cum_gain= this_move->gain;
#else
cum_gain_pos= this_move->gain_pos;
cum_gain_neg= this_move->gain_neg;
#endif
two_i= 8;
/*175:*/


#if TRACK_DEPTHS
if(probe_depth<two_i+2)(probe_depth= two_i+2);
#endif


/*:175*/


scheme_id= this_move->scheme_id;
/*84:*/


{int i,n= scheme_max[scheme_id],*s= &scheme[scheme_id][0];
/*149:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Implement scheme %d\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:149*/


for(i= 0;i<n;i+= 4){
tour_flip_arb(t[s[i]],t[s[i+1]],t[s[i+2]],t[s[i+3]]);
}
}

/*:84*/


/*93:*/


{int go_deeper;
last_special_two_i= two_i;
generic_flips_made= 0;
/*111:*/



/*:111*//*120:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{
int i;
for(i= 2;i<two_i;i+= 2)dict_insert(tabu,t+i);
}
#endif

/*:120*//*130:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{
int i;
for(i= 2;i<two_i;i+= 2)tabu_hash_add(tabu_hash,t[i],t[i+1]);
}
#endif

/*:130*/


/*147:*/


#if JBMR_MAX_VERBOSE >= 175
if(verbose>=175){
printf("Start generic search\n");
fflush(stdout);
}
#endif


/*:147*/


for(go_deeper= 1;go_deeper;){
/*94:*/


#if defined(JBMR_LIMIT_PROBE_DEPTH)
if(generic_flips_made>=max_generic_flips){
/*187:*/


#if JBMR_MAX_VERBOSE >= 75
if(verbose>=75){
printf(" hit max generic flips: %d >= %d\n",
generic_flips_made,max_generic_flips);
}
#endif

/*:187*/


/*102:*/


if(best_gain==0){
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


}else if(best_scheme_id==13){
/*103:*/


{
int low_j,best_is_prefix;
errorif(best_gain<=0,"Bad best_scheme_id == 13");
best_is_prefix= t[best_two_i+1]==best_exit_a
&&t[best_two_i+2]==best_exit_b;
low_j= best_two_i+(best_is_prefix?2:0);
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


if(!best_is_prefix){
tour_flip_arb(t[1],t[best_two_i],best_exit_a,best_exit_b);
}
/*73:*/


{int i;
for(i= 1;i<=best_two_i;i++){
mark_dirty(t[i]);
}
mark_dirty(best_exit_a);
mark_dirty(best_exit_b);
}

/*:73*//*74:*/


if(iteration> 0){
const int more_log= 4+best_two_i;
/*75:*/


{const int need_size= more_log+change_log_next;
if(need_size>=change_log_max_alloc){
do{
change_log_max_alloc*= 2;
}while(need_size>=change_log_max_alloc);
change_log= (int*)mem_realloc(change_log,sizeof(int)*change_log_max_alloc);
}
}

/*:75*/


{int j;
for(j= 1;j<=best_two_i;j++){
write_log(t[j]);
}
}
write_log(best_exit_a);
write_log(best_exit_b);
write_log(scheme_id);
write_log(3+best_two_i);
}


/*:74*/


/*173:*/


#if TRACK_DEPTHS
move_depth= best_two_i+2;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:173*/


more_backtracking= 0;
}

/*:103*/


}

/*:102*/


go_deeper= 0;
break;
}
#endif

/*:94*/


/*175:*/


#if TRACK_DEPTHS
if(probe_depth<two_i+2)(probe_depth= two_i+2);
#endif


/*:175*/


/*26:*/


if(two_i+3>=t_max_alloc){
#if defined(TABU_SPLAY)
int*old_t= t;
#endif
do{
t_max_alloc*= 2;
}while(two_i+3>=t_max_alloc);
t= (int*)mem_realloc(t,sizeof(int)*t_max_alloc);
/*123:*/


#if defined(TABU_SPLAY)
{int*env[2];
env[0]= old_t;
env[1]= t;
dict_update_all(tabu,move_t,env);
}
#endif

/*:123*/


}


/*:26*/


#define BL 3
{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
const length_t cluster_dist= decluster_d(t[1],t[two_i]);
#else
const length_t cluster_dist= 0;
#endif
/*56:*/


en[BL]= ec[BL]= 0;
if(CAREFUL_OP(cum_gain,> ,cluster_dist+best_gain)){
int i,t2ip1,t2ip2,enbl,*neighbour_list,nn_bound;
#if SPLIT_GAIN_VAR
length_t cum_1_pos,cum_1_neg,cum_2_pos,cum_2_neg;
#else
length_t cum_1,cum_2;
#endif
/*156:*/


#if JBMR_MAX_VERBOSE >= 400
if(verbose>=400){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d %d "length_t_spec" "length_t_spec" s%d\n",t[two_i-1],t[two_i],
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_gain),
#else
length_t_pcast(cum_gain_pos-cum_gain_neg),
#endif
length_t_pcast(best_gain),scheme_id);
fflush(stdout);
}
#endif



/*:156*/




#if SPLIT_GAIN_VAR
cum_1_pos= cum_gain_pos;
#endif

neighbour_list= nn_list(t[two_i],&nn_bound);
for(i= 0,enbl= 0;i<nn_bound;i++){
t2ip1= neighbour_list[i];
#if SPLIT_GAIN_VAR
cum_1_neg= cum_gain_neg+cost(t[two_i],t2ip1);
#else
cum_1= cum_gain-cost(t[two_i],t2ip1);
#endif

if(CAREFUL_OP(cum_1,<=,best_gain)){
/*163:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("Terminating |cum_1| "length_t_spec"\n",length_t_pcast(cum_1));
#else
printf("Terminating |cum_1| "length_t_spec" "
"(== "length_t_spec" - "length_t_spec")\n",
length_t_pcast(cum_1_pos-cum_1_neg),
length_t_pcast(cum_1_pos),
length_t_pcast(cum_1_neg));
#endif
fflush(stdout);
}
#endif

/*:163*/


num_reject_by_cum_1++;
break;
}

/*59:*/


#if BL==0
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


#endif

/*:59*//*79:*/


#if BL==1
if(t2ip1!=t[3]){
switch(e[0][ec[0]].scheme_id){
case 0:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[3])?0:2;break;
case 4:base_scheme[5]= tour_inorder(t[1],t[2],t2ip1,t[4])?5:8;break;
default:errorif(1,"Non exhaustive switch: %d",e[0][ec[0]].scheme_id);
}
/*165:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme5 == %d\n",base_scheme[5]);
fflush(stdout);
}
#endif

/*:165*/


/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
#endif


/*:79*//*86:*/


#if BL==2
if(t2ip1!=t[5]){
int i1,i2,i4;
switch(base_scheme[7]= e[1][ec[1]].scheme_id){
case 2:i1= 1;i2= 2;i4= 3;break;
case 5:i1= 6,i2= 5,i4= 4;break;
case 9:i1= 1,i2= 2,i4= 5;break;
default:errorif(1,"Got to 4-change in base scheme %d",base_scheme[7]);
i1= i2= i4= -1;
break;
}
if(tour_inorder(t[i1],t[i2],t2ip1,t[i4])){
/*60:*/


#if defined(JBMR_UNROLL_PREV_NEXT_LOOP)
#error "JBMR_UNROLL_PREV_NEXT_LOOP is not implemented"
#else
{int which_neighbour;
for(which_neighbour= 0;which_neighbour<2;which_neighbour++){
t2ip2= (tour_neighbour[which_neighbour])(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
}
#endif

/*:60*/


}
}
#endif


/*:86*//*97:*/


#if BL==3
if(t[two_i]==tour_prev(t[1]))t2ip2= tour_next(t2ip1);
else t2ip2= tour_prev(t2ip1);
/*63:*/


#if BL==0
if(t[2]!=t2ip2){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
#endif

/*:63*//*80:*/


#if BL==1
if(!(
(t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int is_illegal= 0;
switch(base_scheme[5]){
case 0:

if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 0;




is_illegal= t[2]==t2ip1||t[1]==t2ip2;
}else{
base_scheme[6]= 1;





is_illegal= t[2]==t2ip2||t[1]==t2ip1||t[4]==t2ip2
||t[1]==t2ip2;
}
break;
case 2:
base_scheme[6]= 2;
is_illegal= tour_inorder(t[3],t[4],t2ip1,t2ip2);







break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[6]= 5;











}else{
base_scheme[6]= 7;




is_illegal= t2ip2==t[1]||t[1]==t[4];
}
break;
case 8:
if(tour_inorder(t[4],t[3],t2ip2,t2ip1)){
base_scheme[6]= 8;













}else{
base_scheme[6]= 9;










is_illegal= t2ip2==t[2];
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[5]);
}
if(!is_illegal){
/*166:*/


#if JBMR_MAX_VERBOSE >= 1000
if(verbose>=1000){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("base_scheme6 == %d\n",base_scheme[6]);
fflush(stdout);
}
#endif


/*:166*/


/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif 

/*:80*//*87:*/


#if BL==2
if(!(
(t2ip2==t[6])
||(t2ip1==t[4]&&t2ip2==t[5])
||(t2ip1==t[5]&&t2ip2==t[4])
||(t2ip1==t[2]&&t2ip2==t[3])
||(t2ip1==t[3]&&t2ip2==t[2]))){
int infeasible_4_change,is_illegal= 0;
switch(base_scheme[7]){
case 2:
if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 2;
is_illegal= t2ip2==t[4]||t2ip2==t[1];
}else{
base_scheme[8]= 3;


}
break;
case 5:
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 5;


}else{
base_scheme[8]= 6;
is_illegal= t2ip2==t[3];


}
break;
case 9:
if(tour_inorder(t[1],t[2],t2ip1,t[4])){
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
base_scheme[8]= 9;





}else{
base_scheme[8]= 10;
is_illegal= t2ip2==t[3];



}
}else{




if(tour_inorder(t[1],t[2],t2ip1,t2ip2)){
base_scheme[8]= 11;
is_illegal= t2ip2==t[6];


}else{
base_scheme[8]= 12;
is_illegal= t2ip2==t[4];


}
}
break;
default:errorif(1,"Non-exhaustive switch: %d",base_scheme[7]);
}
if(!is_illegal){
/*88:*/


{int i,*sc,sn;
t[7]= t2ip1;
t[8]= t2ip2;
infeasible_4_change= 0;
sc= &scheme_feas_check[base_scheme[8]][0];
sn= scheme_feas_n[base_scheme[8]];
for(i= 0;i<sn;i+= 2){
if(t[sc[i]]==t[sc[i+1]]){infeasible_4_change= 1;break;}
}
}

/*:88*/


if(!infeasible_4_change){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
}
#endif

/*:87*//*99:*/


#if BL==3
if(t2ip2!=t[1]&&t2ip2!=t[two_i]){
int is_tabu= 0;
/*110:*/


#if defined(TABU_LINEAR)
#if defined(TABU_JBMR)
{
int i;
for(i= 2,is_tabu= 0;i<two_i;i+= 2){
if((t2ip1==t[i]&&t2ip2==t[i+1])
||(t2ip1==t[i+1]&&t2ip2==t[i])){
is_tabu= 1;
break;
}
}
}
#elif defined(TABU_Papadimitriou)
{errorif(1,"TABU_Papadimitriou is not implemented yet");}
#else
#error "Need one of TABU_JBMR or TABU_Papadimitriou defined"
#endif
#endif 

/*:110*//*119:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
{int edge[2];
if(t2ip1<t2ip2){edge[0]= t2ip2;edge[1]= t2ip1;}
else{edge[0]= t2ip1;edge[1]= t2ip2;}
is_tabu= dict_find(tabu,edge)!=NULL;
}
#endif

/*:119*//*129:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
{is_tabu= tabu_hash_includes(tabu_hash,t2ip1,t2ip2);}
#endif

/*:129*/


if(!is_tabu){
/*64:*/


{
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST || JBMR_DECLUSTER_IN_GREEDY
const length_t cluster_dist= decluster_d(t[1],t2ip2);
/*159:*/


#if JBMR_MAX_VERBOSE >= 501 && (JBMR_DECLUSTER_IN_ELIGIBILITY_TEST||JBMR_DECLUSTER_IN_GREEDY)
if(verbose>=501){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf(" v---- next clust_dist==%f\n",(double)cluster_dist);
}
#endif

/*:159*/


#endif

#if !SPLIT_GAIN_VAR
cum_2= cum_1+cost(t2ip1,t2ip2);
#else 
cum_2_pos= cum_1_pos+cost(t2ip1,t2ip2);
cum_2_neg= cum_1_neg;
#endif 

#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
if(CAREFUL_OP(cum_2,<=,cluster_dist+best_gain)){
/*160:*/


#if JBMR_MAX_VERBOSE
num_reject_by_decluster++;
#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("%d: %d %d "length_t_spec" s%d rejected (#%d), clust_dist==%f\n",
enbl,t2ip1,t2ip2,
#if !SPLIT_GAIN_VAR
length_t_pcast(cum_2),
#else
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id,
num_reject_by_decluster,
(double)cluster_dist);
fflush(stdout);
}
#endif
#endif

/*:160*/


}else
#endif

{
/*66:*/


#if BL==0
if(tour_inorder(t[1],t[2],t2ip2,t2ip1)){
e[BL][enbl].scheme_id= 4;
if(t[1]!=t[4]){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
}else{
e[BL][enbl].scheme_id= 0;
}
#endif



/*:66*//*81:*/


#if BL==1
if(scheme_num_cities[e[BL][enbl].scheme_id= base_scheme[6]]==6){
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


}
#endif

/*:81*//*90:*/


#if BL==2
e[BL][enbl].scheme_id= base_scheme[8];
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:90*//*100:*/


#if BL==3
e[BL][enbl].scheme_id= 13;
/*67:*/


{const length_t cost_phantom= cost(t[1],t2ip2);
#if !SPLIT_GAIN_VAR
const length_t cum_exit_now= cum_2-cost_phantom;
#endif

if(
#if LENGTH_TYPE_IS_EXACT
cum_exit_now> best_gain
#elif SPLIT_GAIN_VAR
cum_2_pos> best_gain_with_slop+cum_2_neg+cost_phantom
#else
cum_exit_now> best_gain_with_slop
#endif
)
{

#if SPLIT_GAIN_VAR
best_gain= cum_2_pos-cum_2_neg-cost_phantom;
#else
best_gain= cum_exit_now;
#endif
#if !LENGTH_TYPE_IS_EXACT
best_gain_with_slop= best_gain+instance_epsilon;
#endif

best_two_i= two_i;
best_exit_a= t2ip1;
best_exit_b= t2ip2;
best_scheme_id= e[BL][enbl].scheme_id;
/*148:*/


#if JBMR_MAX_VERBOSE >= 200
if(verbose>=200){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


printf("best_gain = "length_t_spec" %d %d s%d\n",
length_t_pcast(best_gain),best_exit_a,best_exit_b,best_scheme_id);
fflush(stdout);
}
#endif

/*:148*/


}
}

/*:67*/


#endif

/*:100*/


e[BL][enbl].t2ip1= t2ip1;
e[BL][enbl].t2ip2= t2ip2;

#if !SPLIT_GAIN_VAR
e[BL][enbl].gain_for_comparison= e[BL][enbl].gain= cum_2;
#else
e[BL][enbl].gain_for_comparison= cum_2_pos-cum_2_neg;
e[BL][enbl].gain_pos= cum_2_pos;
e[BL][enbl].gain_neg= cum_2_neg;
#endif

#if JBMR_DECLUSTER_IN_GREEDY
e[BL][enbl].gain_for_comparison-= cluster_dist;
#endif
#if JBMR_DECLUSTER_IN_ELIGIBILITY_TEST
e[BL][enbl].cluster_dist= cluster_dist;
#endif

/*158:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/


#if !SPLIT_GAIN_VAR
printf("%d: %d %d "length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2),
#else
printf("%d: %d %d "length_t_spec"-"length_t_spec"="length_t_spec" s%d\n",
enbl,t2ip1,t2ip2,
length_t_pcast(cum_2_pos),
length_t_pcast(cum_2_neg),
length_t_pcast(cum_2_pos-cum_2_neg),
#endif
e[BL][enbl].scheme_id);
fflush(stdout);
}
#endif

/*:158*/


enbl++;
}
}

/*:64*/


}
}
#endif


/*:99*/


#endif


/*:97*/


if(BL==2&&no_extra_backtrack&&enbl)break;
}
en[BL]= enbl;
}else{
num_reject_pre_e_build++;
}

/*:56*/


}
#undef BL
if(en[3]> 0){
/*101:*/


{int i,best_i;
length_t best_len;
/*164:*/


#if JBMR_MAX_VERBOSE >= 500
if(verbose>=500){
int i;
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("go deeper candidates begin\n");
for(i= 0;i<en[3];i++){
#if !SPLIT_GAIN_VAR
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("%d %d "length_t_spec" s%d\n",
e[3][i].t2ip1,
e[3][i].t2ip2,
length_t_pcast(e[3][i].gain),
e[3][i].scheme_id);
#else
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("%d %d "length_t_spec" (=="length_t_spec"-"
length_t_spec") s%d\n",
e[3][i].t2ip1,
e[3][i].t2ip2,
length_t_pcast(e[3][i].gain_pos-e[3][i].gain_neg),
length_t_pcast(e[3][i].gain_pos),
length_t_pcast(e[3][i].gain_neg),
e[3][i].scheme_id);
#endif
}
/*167:*/


#if JBMR_MAX_VERBOSE
{int i;for(i= 0;i<two_i;i++)printf(" ");}
#endif

/*:167*/

printf("go deeper candidates end\n");
fflush(stdout);
}
#endif

/*:164*/


best_i= 0;
best_len= e[3][0].gain_for_comparison;
for(i= 1;i<en[3];i++){
if(best_len<e[3][i].gain_for_comparison){
best_i= i;
best_len= e[3][i].gain_for_comparison;
}
}
if(best_len==0){errorif(1,"Shouldn't be going deeper.");}
#if !SPLIT_GAIN_VAR
cum_gain= e[3][best_i].gain;
#else
cum_gain_pos= e[3][best_i].gain_pos;
cum_gain_neg= e[3][best_i].gain_neg;
#endif
t[two_i+1]= e[3][best_i].t2ip1;
t[two_i+2]= e[3][best_i].t2ip2;
tour_flip_arb(t[1],t[two_i],t[two_i+1],t[two_i+2]);
/*112:*/



/*:112*//*121:*/


#if defined(TABU_SPLAY) && defined(TABU_JBMR)
dict_insert(tabu,t+two_i);
#endif

/*:121*//*131:*/


#if defined(TABU_HASH) && defined(TABU_JBMR)
tabu_hash_add(tabu_hash,t[two_i],t[two_i+1]);
#endif


/*:131*/


two_i+= 2;
generic_flips_made++;
/*145:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips made: %d\n",generic_flips_made);
fflush(stdout);
}
#endif

/*:145*/


}

/*:101*/


}else{
/*102:*/


if(best_gain==0){
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


}else if(best_scheme_id==13){
/*103:*/


{
int low_j,best_is_prefix;
errorif(best_gain<=0,"Bad best_scheme_id == 13");
best_is_prefix= t[best_two_i+1]==best_exit_a
&&t[best_two_i+2]==best_exit_b;
low_j= best_two_i+(best_is_prefix?2:0);
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


if(!best_is_prefix){
tour_flip_arb(t[1],t[best_two_i],best_exit_a,best_exit_b);
}
/*73:*/


{int i;
for(i= 1;i<=best_two_i;i++){
mark_dirty(t[i]);
}
mark_dirty(best_exit_a);
mark_dirty(best_exit_b);
}

/*:73*//*74:*/


if(iteration> 0){
const int more_log= 4+best_two_i;
/*75:*/


{const int need_size= more_log+change_log_next;
if(need_size>=change_log_max_alloc){
do{
change_log_max_alloc*= 2;
}while(need_size>=change_log_max_alloc);
change_log= (int*)mem_realloc(change_log,sizeof(int)*change_log_max_alloc);
}
}

/*:75*/


{int j;
for(j= 1;j<=best_two_i;j++){
write_log(t[j]);
}
}
write_log(best_exit_a);
write_log(best_exit_b);
write_log(scheme_id);
write_log(3+best_two_i);
}


/*:74*/


/*173:*/


#if TRACK_DEPTHS
move_depth= best_two_i+2;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:173*/


more_backtracking= 0;
}

/*:103*/


}

/*:102*/


go_deeper= 0;
}
}
/*113:*/



/*:113*//*122:*/


#if defined(TABU_SPLAY)
dict_delete_all(tabu,NULL);
#endif

/*:122*//*132:*/


#if defined(TABU_HASH)
tabu_hash_make_empty(tabu_hash);
#endif

/*:132*/


}

/*:93*/


/*92:*/


if(more_backtracking){
if(best_gain> 0&&scheme_num_cities[best_scheme_id]==8){
if(best_exit_a==t[7]&&best_exit_b==t[8]){
/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


}else{
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


t[7]= best_exit_a;t[8]= best_exit_b;
scheme_id= best_scheme_id;
/*84:*/


{int i,n= scheme_max[scheme_id],*s= &scheme[scheme_id][0];
/*149:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Implement scheme %d\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:149*/


for(i= 0;i<n;i+= 4){
tour_flip_arb(t[s[i]],t[s[i+1]],t[s[i+2]],t[s[i+3]]);
}
}

/*:84*/


}
/*73:*/


{int i;
for(i= 1;i<=best_two_i;i++){
mark_dirty(t[i]);
}
mark_dirty(best_exit_a);
mark_dirty(best_exit_b);
}

/*:73*//*74:*/


if(iteration> 0){
const int more_log= 4+best_two_i;
/*75:*/


{const int need_size= more_log+change_log_next;
if(need_size>=change_log_max_alloc){
do{
change_log_max_alloc*= 2;
}while(need_size>=change_log_max_alloc);
change_log= (int*)mem_realloc(change_log,sizeof(int)*change_log_max_alloc);
}
}

/*:75*/


{int j;
for(j= 1;j<=best_two_i;j++){
write_log(t[j]);
}
}
write_log(best_exit_a);
write_log(best_exit_b);
write_log(scheme_id);
write_log(3+best_two_i);
}


/*:74*/


/*172:*/


#if TRACK_DEPTHS
move_depth= 8;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:172*/


more_backtracking= 0;
}else{
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


}
}

/*:92*/


}

/*:91*/


}
}


/*:82*/


/*72:*/


if(best_scheme_id==4){
/*106:*/


/*105:*/


if(generic_flips_made){
int low_j= last_special_two_i;
/*104:*/


{int j;
errorif(low_j<last_special_two_i,"Generic broken! caught at unrolling time");
for(j= two_i;j> low_j;j-= 2){
/*153:*/


#if JBMR_MAX_VERBOSE >= 350
if(verbose>=350){
printf("Rollback generic flip(t%d t%d t%d t%d)\n",1,j,j-1,j-2);
fflush(stdout);
}
#endif

/*:153*/


tour_flip_arb(t[1],t[j],t[j-1],t[j-2]);
}
generic_flips_made= (low_j-last_special_two_i)/2;
/*146:*/


#if JBMR_MAX_VERBOSE >= 150
if(verbose>=150){
printf("Generic flips remain after rollback: %d\n",generic_flips_made);
fflush(stdout);
}
#endif


/*:146*/


}

/*:104*/


}

/*:105*/


/*107:*/


if(scheme_id>=0){
int j,*s= scheme[scheme_id];
/*150:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Unrolling the scheme %d changes\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:150*/


for(j= scheme_max[scheme_id]-4;j>=0;j-= 4){
tour_flip_arb(t[s[j]],t[s[j+3]],t[s[j+2]],t[s[j+1]]);
}
scheme_id= -1;
/*174:*/


#if TRACK_DEPTHS
move_depth= 0;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:174*/


}

/*:107*/



/*:106*/


t[3]= best_exit_a;t[4]= best_exit_b;
scheme_id= 4;
/*84:*/


{int i,n= scheme_max[scheme_id],*s= &scheme[scheme_id][0];
/*149:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
printf("Implement scheme %d\n",scheme_id);
/*151:*/


#if JBMR_MAX_VERBOSE >= 300
if(verbose>=300){
int i;
length_t c= 0,b= 0;
printf("t: ");
for(i= 1;i<=two_i;i++){
if(i%2){
if(i> 1)c-= cost(t[i],t[i-1]);
}else{
c+= cost(t[i],t[i-1]);
}
printf("%d ",t[i]);
}
b= c-cost(t[1],t[two_i]);
printf(length_t_spec" ",length_t_pcast(c));
printf(length_t_spec"\n",length_t_pcast(b));
fflush(stdout);
}
#endif



/*:151*/


fflush(stdout);
}
#endif

/*:149*/


for(i= 0;i<n;i+= 4){
tour_flip_arb(t[s[i]],t[s[i+1]],t[s[i+2]],t[s[i+3]]);
}
}

/*:84*/


/*73:*/


{int i;
for(i= 1;i<=best_two_i;i++){
mark_dirty(t[i]);
}
mark_dirty(best_exit_a);
mark_dirty(best_exit_b);
}

/*:73*//*74:*/


if(iteration> 0){
const int more_log= 4+best_two_i;
/*75:*/


{const int need_size= more_log+change_log_next;
if(need_size>=change_log_max_alloc){
do{
change_log_max_alloc*= 2;
}while(need_size>=change_log_max_alloc);
change_log= (int*)mem_realloc(change_log,sizeof(int)*change_log_max_alloc);
}
}

/*:75*/


{int j;
for(j= 1;j<=best_two_i;j++){
write_log(t[j]);
}
}
write_log(best_exit_a);
write_log(best_exit_b);
write_log(scheme_id);
write_log(3+best_two_i);
}


/*:74*/


/*170:*/


#if TRACK_DEPTHS
move_depth= 4;
/*178:*/


#if JBMR_MAX_VERBOSE >= 125
if(verbose>=125){
printf("Move_stats: %d %d %d\n",probes,move_depth,probe_depth);
fflush(stdout);
probes++;
}
#endif
#if TRACK_DEPTHS
last_probe_depth= probe_depth;
p_depths[probe_depth]++;
m_depths[move_depth]++;
move_depth= probe_depth= 0;
#endif


/*:178*/


#endif

/*:170*/


more_backtracking= 0;
}

/*:72*/


}

/*:71*/



/*:45*/


}
if(best_gain> 0){
incumbent_len-= best_gain;
/*140:*/


#if JBMR_MAX_VERBOSE >= 50
if(verbose>=50){
static double last_time= 0.0;
const double this_time= resource_user_tick();
printf("=== improve by "length_t_spec" to "length_t_spec,
length_t_pcast(best_gain),length_t_pcast(incumbent_len));
printf("  after %.3f (+ %.3f) sec\n",this_time,this_time-last_time);
last_time= this_time;
fflush(stdout);
}
#endif


/*:140*/


/*137:*/


milestone_check(&ms_lower_bound,incumbent_len);
milestone_check(&ms_upper_bound,incumbent_len);

/*:137*/


/*54:*/


#if !(LENGTH_TYPE_IS_EXACT)
instance_epsilon= incumbent_len*LENGTH_MACHINE_EPSILON;
#endif

/*:54*/


}
}

/*:42*/


}
/*201:*/


#if JBMR_MAX_VERBOSE >= 40
if(verbose>=40){
char end_iter_message[100];
sprintf(end_iter_message,"End of LK step %d",iteration+1);
milestone_show_request(&ms_lower_bound,end_iter_message,incumbent_len);
milestone_show_request(&ms_upper_bound,end_iter_message,incumbent_len);
}
#endif

/*:201*/


/*188:*/


if(iteration> 0&&change_log_next> 0&&previous_incumbent_len<incumbent_len){
/*203:*/


#if JBMR_MAX_VERBOSE >= 57
if(verbose>=57){
printf("Reverting to previous\n");
}
#endif

/*:203*/


while(change_log_next> 0){
/*190:*/


{
const int len= change_log[change_log_next-1],
revert_scheme_id= change_log[change_log_next-2],
first_pos= change_log_next-1-len,
first_generic_pos= first_pos+scheme_num_cities[revert_scheme_id],
t1= change_log[first_pos],*st= (&change_log[first_pos])-1;
int j,si,*s= &scheme[revert_scheme_id][0];
/*204:*/


#if JBMR_MAX_VERBOSE >= 65
if(verbose>=65){
printf("  first_pos = %d change_log_next = %d\n",first_pos,change_log_next);
}
#endif

/*:204*/


errorif(first_pos<0,"Bug!");
for(j= change_log_next-4;j>=first_generic_pos;j-= 2){
tour_flip_arb(t1,change_log[j+1],change_log[j],change_log[j-1]);
}
for(si= scheme_max[revert_scheme_id]-4;si>=0;si-= 4){
tour_flip_arb(st[s[si]],st[s[si+3]],st[s[si+2]],st[s[si+1]]);
}
}

/*:190*/


change_log_next-= change_log[change_log_next-1]+1;
}
errorif(change_log_next!=0,"Bug!");
/*200:*/


{int i;
for(i= 8;i>=0;i-= 4)
tour_flip_arb(mutation[i],mutation[i+3],mutation[i+2],mutation[i+1]);
}

/*:200*/


incumbent_len= previous_incumbent_len;
}
change_log_next= 0;

/*:188*/


/*191:*/


if(iteration<iterations-1){
int edge[4][2];
previous_incumbent_len= incumbent_len;
/*192:*/


{
int ok,count= 0;
do{
int i,j;
errorif(count++> 1000,"Ummm, random double-bridge search didn't stop after 1000 tries");
for(i= 0,ok= 1;ok&&i<4;i++){
edge[i][0]= prng_unif_int(random_stream,(long)n);
edge[i][1]= tour_next(edge[i][0]);
for(j= 0;ok&&j<i;j++){
if(edge[j][0]==edge[i][0])ok= 0;
}
}
}while(!ok);
}

/*:192*//*193:*/


#if JBMR_MAX_VERBOSE >= 60
if(verbose>=60){
int i;
printf("Doing double-bridge: ");
for(i= 0;i<4;i++)printf("%d(%d,%d) ",i,edge[i][0],edge[i][1]);
printf("\n");
}
#endif

/*:193*/


/*194:*/


{int bottom,rock;
for(bottom= 3;bottom> 1;bottom--){
for(rock= 1;rock<bottom;rock++){
if(bridge_less(edge[rock],edge[rock+1])){
swap_bridge(edge[rock],edge[rock+1]);
}
}
}
}


/*:194*/


/*195:*/


bridge_move(1,2,6,5);
bridge_move(8,7,4,3);
bridge_move(1,5,2,6);

/*:195*//*196:*/


{int mutation_next= 0;
mutate(1);
mutate(2);
mutate(6);
mutate(5);

mutate(8);
mutate(7);
mutate(4);
mutate(3);

mutate(1);
mutate(5);
mutate(2);
mutate(6);
}

/*:196*//*197:*/


#if JBMR_MAX_VERBOSE >= 60
#define verbose_print_double_bridge_edge(a,b) \
(verbose >= 60  \
 ? printf("cost(%d,%d)="length_t_spec"\n", \
  bridge_t(a),bridge_t(b),length_t_pcast(cost(bridge_t(a),bridge_t(b)))) \
 : 0 \
)
#define bc(a,b) (verbose_print_double_bridge_edge(a,b),cost(bridge_t(a),bridge_t(b)))
#else
#define bc(a,b) (cost(bridge_t(a),bridge_t(b)))
#endif


incumbent_len+= bc(1,6);
incumbent_len+= bc(2,5);
incumbent_len+= bc(4,7);
incumbent_len+= bc(3,8);
incumbent_len-= bc(1,2);
incumbent_len-= bc(3,4);
incumbent_len-= bc(5,6);
incumbent_len-= bc(7,8);


/*:197*//*198:*/


{int i;
for(i= 1;i<=8;i++)
mark_dirty(bridge_t(i));
}


/*:198*/


}
/*202:*/


#if JBMR_MAX_VERBOSE >= 60
if(verbose>=60){
printf("+++incumbent_len is now "length_t_spec"\n",length_t_pcast(incumbent_len));
}
#endif


/*:202*/





/*:191*/


}
/*141:*/


#if JBMR_MAX_VERBOSE >= 20
if(verbose>=20){
const double lk_time= resource_user_tick();
const double ds_time= resource_user_tick_from(begin_data_structures_mark);
printf("LK phase ended with incumbent_len == "length_t_spec
" after %.3f sec for LK and %.3f sec for ds+LK\n",
length_t_pcast(incumbent_len),lk_time,ds_time);
fflush(stdout);
}
#endif

/*:141*/


/*138:*/


milestone_show_final(&ms_lower_bound,incumbent_len);
milestone_show_final(&ms_upper_bound,incumbent_len);

/*:138*/


/*19:*/


dirty_destroy(dirty_set);

/*:19*//*25:*/


free_mem(t);

/*:25*//*49:*/


free_mem(e[0]);
free_mem(e[1]);
free_mem(e[2]);
free_mem(e[3]);

/*:49*//*77:*/


free_mem(change_log);


/*:77*//*116:*/


#if defined(TABU_SPLAY)
dict_destroy(tabu);
#endif

/*:116*//*133:*/


#if defined(TABU_HASH)
if(tabu_hash_destroy)tabu_hash_destroy(tabu_hash);
#endif

/*:133*/


/*184:*/


/*183:*/


#ifdef BLAB
#if TRACK_DEPTHS
{int i,j;
for(i= DEPTHS_BOUND-1;p_depths[i]==0&&i> 0;i--);
for(j= 0;j<=i;j++){
printf("p %d citydeep %d\n",j,p_depths[j]);
}
for(i= DEPTHS_BOUND-1;m_depths[i]==0&&i> 0;i--);
for(j= 0;j<=i;j++){
printf("m %d citydeep %d\n",j,m_depths[j]);
}
}
#endif


/*:183*/


printf("Statistics: num_reject_by_cum_1 %d\n",num_reject_by_cum_1);
printf("Statistics: num_reject_pre_e_build %d\n",num_reject_pre_e_build);
printf("Statistics: num_reject_by_decluster %d\n",num_reject_by_decluster);
#endif


/*:184*/


}

/*:21*/


const char*jbmr_rcs_id= "$Id: jbmr.w,v 1.206 2000/09/17 03:09:57 neto Exp neto $";

/*:4*/
