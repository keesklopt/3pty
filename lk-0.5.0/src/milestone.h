/*2:*/


typedef struct{
int current;
length_t target_value;
length_t lower_bound_value;
char*name;
double*percentage;
int num;
double time;
int tick_from;
}milestone_state_t;


/*:2*//*7:*/


#define MILESTONE_VERBOSITY_LEVEL 25
#if MILESTONE_MAX_VERBOSE >= MILESTONE_VERBOSITY_LEVEL
#define milestone_check(ms,len) \
 do { \
  if ((ms) && (ms)->name && verbose >= MILESTONE_VERBOSITY_LEVEL) \
   milestone_check_func((ms),(len)); \
 } while(0)
#else
#define milestone_check(ms,len)
#endif

/*:7*//*12:*/


void milestone_initialize(milestone_state_t*ms,char*name,length_t bound_value,int tick_from);
void milestone_check_func(milestone_state_t*ms,length_t this_len);
void milestone_show_initial(milestone_state_t*ms,length_t this_len);
void milestone_show_request(milestone_state_t*ms,char*str,length_t this_len);
void milestone_show_final(milestone_state_t*ms,length_t this_len);

/*:12*/
