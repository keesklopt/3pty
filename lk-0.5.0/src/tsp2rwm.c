#define x(v) ((int) (tsp->coord?tsp->coord[(v) ].x[0]:0) ) 
#define y(v) ((int) (tsp->coord?tsp->coord[(v) ].x[1]:0) )  \

/*2:*/



const char*prog_name= "tspps";
const char*tspps_rcs_id= "$Id: tspps.w,v 1.3 1998/07/24 20:12:19 neto Exp neto $";
#include <config.h>
#include "lkconfig.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "length.h"
#include "read.h"

/*3:*/


int noround= 0;

int main(int argc,char**argv)
{
tsp_instance_t*tsp;
if(!(LENGTH_TYPE_IS_INTEGRAL)){
fprintf(stderr,"%s: Warning: Length type is not integral. Truncating lengths.\n",argv[0]);
}
tsp= read_tsp_file(stdin,NULL,1);
/*4:*/


{const int n= tsp->n;
int i,j;
printf("%d %d U\n",n,n*(n-1)/2);
for(i= 0;i<n;i++){
printf("%d %d %d %d\n",n-1,i+1,x(i),y(i));
for(j= 0;j<n;j++){
printf("%d %d\n",j+1,(int)cost(i,j));
}
}
}


/*:4*/


return 0;
}


/*:3*/



/*:2*/
