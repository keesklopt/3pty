/*4:*/


#if !defined(_DIRTY_H_)
#define _DIRTY_H_
extern const char*dirty_rcs_id;
/*16:*/


#if DIRTY_SET==DIRTY_SET_SPLAY_ROOT
#include "dict.h"
#endif

/*:16*//*36:*/


#if DIRTY_SET==DIRTY_SET_FIFO
#include "prng.h"
#endif

/*:36*/


/*26:*/


typedef struct dirty_queue_node_s{
struct dirty_queue_node_s*next;
}dirty_queue_node_t;

/*:26*/


/*6:*/


typedef struct{
#if DIRTY_SET == DIRTY_SET_SPLAY_ROOT
/*15:*/


dict_t*dict;

/*:15*/


#else 
/*25:*/


dirty_queue_node_t*queue,*head,*tail;


/*:25*//*35:*/


prng_t*prng;

/*:35*/


#endif
int n;
int num_used;
const char*file;
int line;
}dirty_set_t;

/*:6*/


/*5:*/


dirty_set_t*dirty_create(int n,int full,int seed,const char*file,int line);
void dirty_make_full(dirty_set_t*ds,int seed);
void dirty_make_empty(dirty_set_t*ds);
int dirty_has_elements(dirty_set_t*ds);
int dirty_includes(dirty_set_t*ds,const int v);
void dirty_add(dirty_set_t*ds,const int v);
int dirty_remove(dirty_set_t*ds);
void dirty_destroy(dirty_set_t*ds);

/*:5*/


#endif

/*:4*/
