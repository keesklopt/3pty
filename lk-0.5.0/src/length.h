/*4:*/



#if !defined(_LENGTH_H_)
#define _LENGTH_H_

#if defined(LENGTH_USE_RCS_ID)
static const char*length_rcs_id= "$Id: length.w,v 1.124 1998/08/23 21:38:50 neto Exp neto $";
#endif

#if defined(LENGTH_LONG_LONG)

typedef long long length_t;
# define LENGTH_TYPE_STRING "long long"
# define LENGTH_TYPE_IS_EXACT (1)
# define LENGTH_TYPE_IS_INTEGRAL (1)
# define INFINITY \
 ((( ((long long)0x7FFF) << 16   \
     |((long long)0xFFFF)) << 16 \
     |((long long)0xFFFF)) << 16 \
     |((long long)0xFFFF))  

# define length_t_pcast(Y) (long)((Y)>>32),(unsigned long)((Y)&0xffffffff)
# define length_t_spec "(0000%ld*2^32+%ld)"
# define length_t_native_pcast(Y) (Y)
# define length_t_native_spec "%qd"



# if WORDS_BIGENDIAN
#  define length_t_native_incast(PTR) ((int *)(PTR)),((int *)(((char *)(PTR))+sizeof(int))),
# else
#  define length_t_native_incast(PTR) ((int *)(((char *)(PTR))+sizeof(int))),((int *)(PTR))
# endif
# define length_t_inspec "(0000%ld*2^32+%ld)"
# define length_t_incount (2)
# define length_t_native_incast(PTR) (PTR)
# define length_t_native_inspec "%qd"
# define length_t_native_incount (1)


#else 
#if defined(LENGTH_DOUBLE)

typedef double length_t;
# define LENGTH_TYPE_STRING "double"
# define LENGTH_TYPE_IS_EXACT (0)
# define LENGTH_TYPE_IS_INTEGRAL (0)
#   include <math.h>
#   include <float.h>
#   define LENGTH_MACHINE_EPSILON  DBL_EPSILON
# undef INFINITY
# define INFINITY DBL_MAX
# define length_t_pcast(Y) ((double)(Y))
# define length_t_spec "(0*2^32+%f)"
# define length_t_native_pcast(Y) (Y)
# define length_t_native_spec "%0.15f"

# define length_t_incast(PTR) (PTR)
# define length_t_inspec "%lf"
# define length_t_incount (1)
# define length_t_native_incast(PTR) (PTR)
# define length_t_native_inspec "%lf"
# define length_t_native_incount (1)

#else 
#if defined(LENGTH_FLOAT)

typedef float length_t;
# define LENGTH_TYPE_STRING "float"
# define LENGTH_TYPE_IS_EXACT (0)
# define LENGTH_TYPE_IS_INTEGRAL (0)
#   include <math.h>
#   include <float.h>
#   define LENGTH_MACHINE_EPSILON  FLT_EPSILON
# define INFINITY FLT_MAX
# define length_t_pcast(Y) ((double)(Y))
# define length_t_spec "(000*2^32+%.3f)"
# define length_t_native_pcast(Y) (Y)
# define length_t_native_spec "%.6f"

# define length_t_incast(PTR) (PTR)
# define length_t_inspec "%f"
# define length_t_incount (1)
# define length_t_native_incast(PTR) (PTR)
# define length_t_native_inspec "%f"
# define length_t_native_incount (1)
#else 

# define LENGTH_TYPE_STRING "int"
typedef int length_t;
# define LENGTH_TYPE_IS_EXACT (1)
# define LENGTH_TYPE_IS_INTEGRAL (1)
# define INFINITY (0x7FFFFFFF)
# define length_t_pcast(Y) (Y)
# define length_t_spec "(00*2^32+%d)"
# define length_t_native_pcast(Y) (Y)
# define length_t_native_spec "%d"

# define length_t_incast(PTR) (PTR)
# define length_t_inspec "%d"
# define length_t_incount (1)
# define length_t_native_incast(PTR) (PTR)
# define length_t_native_inspec "%d"
# define length_t_native_incount (1)
#endif 
#endif 
#endif 

#endif 

/*:4*/
