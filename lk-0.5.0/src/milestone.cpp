#define MAX(x,y) ((x) <(y) ?(y) :(x) ) 
#define epsilon (1e-5)  \

/*1:*/


#include "config.h"
#include "lkconfig.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "length.h"
#include "resource.h"
#include "milestone.h"

extern int verbose;
/*3:*/


double default_percentages[]= 
{.10,
.099,.098,.097,.096,.095,.094,.093,.092,.091,.090,
.089,.088,.087,.086,.085,.084,.083,.082,.081,.080,
.079,.078,.077,.076,.075,.074,.073,.072,.071,.070,
.069,.068,.067,.066,.065,.064,.063,.062,.061,.060,
.059,.058,.057,.056,.055,.054,.053,.052,.051,.050,
.049,.048,.047,.046,.045,.044,.043,.042,.041,.040,
.039,.038,.037,.036,.035,.034,.033,.032,.031,.030,
.029,.028,.027,.026,.025,.024,.023,.022,.021,.020,
.019,.018,.017,.016,.015,.014,.013,.012,.011,.010,
.009,.008,.007,.006,.005,.004,.003,.002,.001,.000,
-.001,-.002,-.003,-.004,-.005,-.006,-.007,-.008,-.009,-.010,
-.011,-.012,-.013,-.014,-.015,-.016,-.017,-.018,-.019,-.020,
-.021,-.022,-.023,-.024,-.025,-.026,-.027,-.028,-.029,-.030,
-.031,-.032,-.033,-.034,-.035,-.036,-.037,-.038,-.039,-.040,
-.041,-.042,-.043,-.044,-.045,-.046,-.047,-.048,-.049,-.050,
-.051,-.052,-.053,-.054,-.055,-.056,-.057,-.058,-.059,-.060,
-.061,-.062,-.063,-.064,-.065,-.066,-.067,-.068,-.069,-.070,
-.071,-.072,-.073,-.074,-.075,-.076,-.077,-.078,-.079,-.080,
-.081,-.082,-.083,-.084,-.085,-.086,-.087,-.088,-.089,-.090,
-.091,-.092,-.093,-.094,-.095,-.096,-.097,-.098,-.099,-.100
};
const int num_percentages= sizeof(default_percentages)/sizeof(double);

/*:3*/


/*4:*/


void
milestone_initialize(milestone_state_t*ms,char*name,length_t lower_bound_value,int tick_from)
{
if(!ms)return;
ms->current= 0;
ms->name= name;
ms->lower_bound_value= lower_bound_value;
ms->percentage= default_percentages;
ms->num= num_percentages;
ms->target_value= lower_bound_value*(1+ms->percentage[ms->current]);
ms->time= resource_user_tick_from(tick_from);
ms->tick_from= tick_from;
}

/*:4*//*5:*/


void
milestone_check_func(milestone_state_t*ms,length_t this_len)
{
if(!ms||!ms->name||ms->lower_bound_value<epsilon)return;
if(this_len<=ms->target_value){
const double this_time= resource_user_tick_from(ms->tick_from);
char const kind[]= "Intermediate";
if(ms->current==ms->num&&this_len<ms->target_value){

const double pct= 
100*(this_len-(double)ms->lower_bound_value)/
(double)ms->lower_bound_value;
/*6:*/


printf("Milestone:%s: length "length_t_native_spec
" %5.2f%% %s %s after %.2f (+ %.2f) sec\n",
kind,
length_t_pcast(this_len),
pct,
(this_len>=ms->lower_bound_value?"above":"below"),
ms->name,
this_time,
MAX(0,this_time-ms->time));
fflush(stdout);

/*:6*/


ms->time= this_time;
}else{
while(this_len<=ms->target_value&&ms->current<ms->num){
const double pct= 100*ms->percentage[ms->current];
/*6:*/


printf("Milestone:%s: length "length_t_native_spec
" %5.2f%% %s %s after %.2f (+ %.2f) sec\n",
kind,
length_t_pcast(this_len),
pct,
(this_len>=ms->lower_bound_value?"above":"below"),
ms->name,
this_time,
MAX(0,this_time-ms->time));
fflush(stdout);

/*:6*/


ms->time= this_time;
ms->current++;
if(ms->current<ms->num){
ms->target_value
= ms->lower_bound_value*(1+ms->percentage[ms->current]);
}
}
}
}
}


/*:5*//*9:*/


void
milestone_show_initial(milestone_state_t*ms,length_t this_len)
{
#if MILESTONE_MAX_VERBOSE >= MILESTONE_VERBOSITY_LEVEL
if(ms&&ms->name&&verbose>=MILESTONE_VERBOSITY_LEVEL&&ms->lower_bound_value>epsilon){
const double this_time= resource_user_tick_from(ms->tick_from);
const double pct= 
100*(this_len-(double)ms->lower_bound_value)/
(double)ms->lower_bound_value;
char const kind[]= "Initial";
/*6:*/


printf("Milestone:%s: length "length_t_native_spec
" %5.2f%% %s %s after %.2f (+ %.2f) sec\n",
kind,
length_t_pcast(this_len),
pct,
(this_len>=ms->lower_bound_value?"above":"below"),
ms->name,
this_time,
MAX(0,this_time-ms->time));
fflush(stdout);

/*:6*/


}
#endif
}

/*:9*//*10:*/


void
milestone_show_request(milestone_state_t*ms,char*str,length_t this_len)
{
#if MILESTONE_MAX_VERBOSE >= MILESTONE_VERBOSITY_LEVEL
if(str&&ms&&ms->name&&verbose>=MILESTONE_VERBOSITY_LEVEL
&&ms->lower_bound_value>epsilon){
const double this_time= resource_user_tick_from(ms->tick_from);
const double pct= 
100*(this_len-(double)ms->lower_bound_value)/(double)ms->lower_bound_value;
char kind[1000];
strcpy(kind,"Requested:");
strncpy(kind+10,str,1000-10-1);
/*6:*/


printf("Milestone:%s: length "length_t_native_spec
" %5.2f%% %s %s after %.2f (+ %.2f) sec\n",
kind,
length_t_pcast(this_len),
pct,
(this_len>=ms->lower_bound_value?"above":"below"),
ms->name,
this_time,
MAX(0,this_time-ms->time));
fflush(stdout);

/*:6*/


}
#endif
}

/*:10*//*11:*/


void
milestone_show_final(milestone_state_t*ms,length_t this_len)
{
#if MILESTONE_MAX_VERBOSE >= MILESTONE_VERBOSITY_LEVEL
if(ms&&ms->name&&verbose>=MILESTONE_VERBOSITY_LEVEL&&ms->lower_bound_value>epsilon){
double this_time= resource_user_tick_from(ms->tick_from);
const double pct= 100*(this_len-(double)ms->lower_bound_value)/(double)ms->lower_bound_value;
char const kind[]= "Final";
/*6:*/


printf("Milestone:%s: length "length_t_native_spec
" %5.2f%% %s %s after %.2f (+ %.2f) sec\n",
kind,
length_t_pcast(this_len),
pct,
(this_len>=ms->lower_bound_value?"above":"below"),
ms->name,
this_time,
MAX(0,this_time-ms->time));
fflush(stdout);

/*:6*/


}
#endif
}

/*:11*/



/*:1*/
