/*2:*/


extern const char*dsort_rcs_id;
/*9:*/


extern void
dsort(void*a,size_t n,size_t es,int(*cmp)(const void*,const void*));


/*:9*//*13:*/


extern void*
select_range(void*a,size_t n,size_t es,int(*cmp)(const void*,const void*),
int lo,int hi,int sorted);

/*:13*/



/*:2*/
