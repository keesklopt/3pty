#define version(out) (fprintf(out,"%s (LK %s)\n",prog_name,VERSION_STRING) )  \

/*2:*/



const char*prog_name= "tspps";
const char*tspps_rcs_id= "$Id: tspps.w,v 1.5 1999/01/21 21:31:56 neto Exp neto $";
#include <config.h>
#include "lkconfig.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
/*5:*/


#define _POSIX_C_SOURCE 2   
#if HAVE_UNISTD_H
#include <unistd.h>
#endif

/*:5*//*12:*/


#include <ctype.h>

/*:12*/


/*6:*/


#define FIXINCLUDES_NEED_GETOPT
#include "fixincludes.h"
#undef FIXINCLUDES_NEED_GETOPT



/*:6*//*9:*/


#include "error.h"

/*:9*//*13:*/


#include "length.h"
#include "read.h"

/*:13*/



/*14:*/


int noround= 1;

/*:14*/


/*4:*/


static int EPS= 0;
static double squeeze_factor= 1;


/*:4*/



/*7:*/


static void
usage(FILE*out,char**argv)
{
version(out);
fprintf(out,
"Generate PostScript figures of geometric TSPLIB instances\n"
"\nCopyright (C) 1997, 1998 David M. Neto\n"
"LK comes with NO WARRANTY, to the extent permitted by law.\n"
"You may redistribute and/or modify copies of LK under the terms of the\n"
"GNU General Public License, version 2 or any later version.\n"
"For more information about these matters, see the file named COPYING.\n\n"
"Usage: %s [options] < <tsplib-instance-file> \n",
argv[0]);
fprintf(out,
" -e                  : Output should be Encapsulated PostScript\n"
" -h                  : Output this help and quit\n"
" -s <squeeze-factor> : Scale points to fit only <squeeze-factor> of the frame;\n"
"                       Should be in (0,1]; the default is 1 (full frame).\n"
"                       This is used only when emitting encapsulated PostScript.\n"
" -v                  : Print version number and exit successfully\n");
}


/*:7*//*11:*/


static void write_PS_string(FILE*out,char*str);
static void
write_PS_string(FILE*out,char*str)
{
char*p;
fputc('(',out);
for(p= str;*p;p++){
if(*p=='('||*p==')'||*p=='\\')fprintf(out,"\\%c",*p);
else if(isprint(*p))fputc(*p,out);
else fprintf(out,"\\%3o",*p);
}
fputc(')',out);
}

/*:11*/



int main(int argc,char**argv)
{
/*3:*/


EPS= 0;
squeeze_factor= 1.0;
while(1){
extern char*optarg;
const int opt= getopt(argc,argv,"ehvs:");
if(opt==EOF)break;
switch(opt){
case'e':EPS= 1;break;
case'h':usage(stdout,argv);exit(0);break;
case'v':version(stdout);exit(0);break;
case's':
squeeze_factor= atof(optarg);
errorif(squeeze_factor<=0,
"Squeeze factor should be positive, but given %f",squeeze_factor);
errorif(squeeze_factor>1,
"Squeeze factor should be no more than 1, but given %f",squeeze_factor);
break;
case':':errorif(1,"some option is missing an argument");break;
case'?':usage(stderr,argv);errorif(1,"Unrecognized option");break;
default:errorif(1,"getopt returned character 0%o",opt);
}
}


/*:3*/


/*8:*/


{
FILE*prolog;
char*prolog_name= (EPS?"prolog.eps":"prolog.ps");
char buf[8192];
size_t countin,countout;
prolog= fopen(prolog_name,"r");
errorif(prolog==NULL,"Couldn't open %s for reading",prolog_name);
while((countin= fread(buf,1,8192,prolog))>0){
char*p= buf;
while((countout= fwrite(p,1,countin,stdout))<countin){
countin-= countout;p+= countout;
}
}
fclose(prolog);
}

/*:8*/


/*10:*/


{tsp_instance_t*tsp;
printf("/squeezefactor %f def\n",squeeze_factor);
tsp= read_tsp_file(stdin,stdout,0);
if(!EPS){
if(tsp->name)write_PS_string(stdout,tsp->name),printf(" title\n");
if(tsp->comment)write_PS_string(stdout,tsp->comment),printf(" comment\n");
}
}

/*:10*/


/*15:*/


fputs("end\n",stdout);
fputs("grestore\n",stdout);
if(!EPS){fputs("showpage\n",stdout);}
fputs("%%EOF\n",stdout);

/*:15*/


return 0;
}

/*:2*/
