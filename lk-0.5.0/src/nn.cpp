#define MAX(A,B) ((A) >(B) ?(A) :(B) )  \

/*5:*/


#include <config.h>
#include "lkconfig.h"
/*6:*/


#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

/*:6*/


/*12:*/


#include "length.h"

/*:12*//*24:*/


#include "read.h"
#include "lk.h"


/*:24*//*36:*/


#include "kdtree.h"

/*:36*/


/*8:*/


#include "nn.h"

/*:8*//*17:*/


#include "error.h"
#include "memory.h"

/*:17*//*40:*/


#include "dsort.h"


/*:40*/



/*9:*/


int nn_max_bound;

/*:9*/


/*11:*/


typedef struct{
length_t len;
int city;
}nn_entry_t;

/*:11*/


/*15:*/


static int*list,*begin,n;

/*:15*//*28:*/


static int list_size;

/*:28*/


/*13:*/


int*
nn_list(int city,int*list_len)
{
*list_len= begin[city+1]-begin[city];
return list+begin[city];
}


/*:13*//*18:*/


void
nn_cleanup(void){
/*19:*/


if(begin){free_mem(begin);mem_deduct(sizeof(int)*(n+1));}

/*:19*//*27:*/


if(list){free_mem(begin);mem_deduct(sizeof(int)*list_size);}

/*:27*/


}

/*:18*//*21:*/


void
nn_build(int num_pure,int num_quadrant,int num_delauney)
{
/*31:*/


kd_bin_t*work;

/*:31*/


int i;
n= tsp_instance->n;
/*23:*/


/*50:*/


if(verbose>=500){
printf("nn: build nn %d nq %d del %d\n",num_pure,num_quadrant,num_delauney);
fflush(stdout);
}

/*:50*/


errorif(num_pure<0,
"Need positive number of nearest neighbours; %d specified",num_pure);
errorif(num_quadrant<0,
"Need positive number of quadrant neighbours; %d specified",num_quadrant);
errorif(num_delauney<0,
"Need positive Delauney depth; %d specified",num_delauney);
errorif(num_pure<=0&&num_quadrant<=0&&num_delauney<=0,
"Must specify some candidates");
errorif(num_pure>=n,
"%d nearest neighbours specified, but there are only %d cities",num_pure,n);

/*:23*//*41:*/


errorif(num_quadrant>0&&!E2_supports(tsp_instance),
"Quadrant lists supported only when 2-d trees supported",num_pure);

/*:41*//*47:*/


errorif(num_delauney,"Delauney neighbours not supported (yet)");

/*:47*/



/*16:*/


begin= new_arr_of(int,n+1);
begin[0]= 0;

/*:16*//*26:*/


{int guess_avg;
/*25:*/


if(num_pure)
guess_avg= num_pure+num_quadrant+num_delauney;
else if(num_quadrant)
guess_avg= 4*num_quadrant+num_delauney;
else
guess_avg= 3*num_delauney*num_delauney;

/*:25*/


list_size= guess_avg*n;
list= new_arr_of(int,list_size);
}

/*:26*//*29:*/


work= new_arr_of(kd_bin_t,3*n);

/*:29*/


nn_max_bound= 0;
for(i= 0;i<n;i++){
/*33:*/


int work_next= 0;


/*:33*/


/*52:*/


if(verbose>=1250){
printf("nn: about to build for %d; work_next=%d list_size=%d\n",
i,work_next,list_size);
fflush(stdout);
}

/*:52*/


/*35:*/


if(num_pure){
int start_work= work_next;
if(E2_supports(tsp_instance)){
/*37:*/


work_next+= E2_nn_bulk(i,num_pure,work+work_next);

/*:37*/


}else{
/*38:*/


{int j;
for(j= 0;j<i;j++){
kd_bin_city(&work[start_work+j])= j;
kd_bin_monotonic_len(&work[start_work+j])= cost(i,j);
}
for(j= i+1;j<n;j++){
kd_bin_city(&work[start_work+j-1])= j;
kd_bin_monotonic_len(&work[start_work+j-1])= cost(i,j);
}
}


/*:38*/


/*39:*/


select_range((void*)work,(size_t)n-1,sizeof(kd_bin_t),kd_bin_cmp_increasing,
0,num_pure,0);
work_next+= num_pure;

/*:39*/


}
}

/*:35*/


/*42:*/


if(num_quadrant){
int quadrant;
int q_count[5]= {0,0,0,0,0};
/*43:*/


{int j;
coord_2d*coord= tsp_instance->coord;
for(j= 0;j<work_next;j++){
const double diff_x= coord[i].x[0]-coord[j].x[0];
const double diff_y= coord[i].x[1]-coord[j].x[1];
q_count[E2_quadrant(diff_x,diff_y)]++;
}
}


/*:43*/


if(0==q_count[1]+q_count[2]+q_count[3]+q_count[4]){
/*45:*/


{const int num_quadrant= n-1,quadrant= 0;
/*44:*/


work_next+= E2_nn_quadrant_bulk(i,num_quadrant,work+work_next,1<<quadrant);

/*:44*/


}


/*:45*/


}
for(quadrant= 1;quadrant<=4;quadrant++){
if(q_count[quadrant]<num_quadrant){
/*44:*/


work_next+= E2_nn_quadrant_bulk(i,num_quadrant,work+work_next,1<<quadrant);

/*:44*/


}
}
}

/*:42*/


/*48:*/



/*:48*/


/*32:*/


errorif(work_next<1,"Must have nonempty candidate list");
{
int r,w,last_city;
sort(work,(size_t)work_next,sizeof(kd_bin_t),kd_bin_cmp_increasing);
for(r= w= 0,last_city= kd_bin_city(&work[r])-1;r<work_next;r++){
if(kd_bin_city(&work[r])!=last_city)
last_city= kd_bin_city(&work[w++])= kd_bin_city(&work[r]);
}
/*34:*/


if(begin[i]+w>list_size){
int new_size= list_size;
while(begin[i]+w>new_size)new_size*= 2;
/*49:*/


if(verbose>=750){
printf("nn: Resize list from %d elements to %d elements; begin[i]=%d, w=%d\n",
list_size,new_size,begin[i],w);
fflush(stdout);
}

/*:49*/


list= (int *)mem_realloc(list,sizeof(int)*new_size);
mem_deduct(sizeof(int)*list_size);
list_size= new_size;
}

/*:34*/


for(r= 0;r<w;r++)list[begin[i]+r]= kd_bin_city(&work[r]);
begin[i+1]= begin[i]+w;
}

/*:32*/


nn_max_bound= MAX(nn_max_bound,begin[i+1]-begin[i]);
}
/*30:*/


free_mem(work);mem_deduct(sizeof(kd_bin_t)*3*n);

/*:30*/


/*51:*/


if(verbose>=75){
printf("nn: build nn %d nq %d del %d got %d total neighbours\n",
num_pure,num_quadrant,num_delauney,begin[n]);
fflush(stdout);
}

/*:51*/


}

/*:21*/


const char*nn_rcs_id= "$Id: nn.w,v 1.134 1998/10/10 19:27:39 neto Exp neto $";

/*:5*/
