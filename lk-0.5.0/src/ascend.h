/*9:*/


#if !defined(_ASCEND_H_)
#define _ASCEND_H_
extern const char*ascend_rcs_id;
/*14:*/


void ascend_setup(int the_n);
void ascend_cleanup(void);
double*const ascend_best_lambda(void);

/*:14*//*27:*/


length_t ascend(const int n,length_t upper_bound_len);
length_t ascend_alpha_beta(const int n,length_t upper_bound_len,double alpha,double beta);

/*:27*/


#endif


/*:9*/
