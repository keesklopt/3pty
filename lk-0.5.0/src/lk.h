/*4:*/


extern const char*lk_rcs_id;
/*132:*/


/*:132*/


/*17:*/


#define REP_ARRAY  1 
#define REP_TWO_LEVEL 2
#define REP_SPLAY_0  3
#define REP_SPLAY_1  4
#define REP_SPLAY_2  5
#define REP_SPLAY_3  6
#define REP_TWO_LEVEL_DEBUG  7


#define CAND_NN   1 
#define CAND_NQ   2
#define CAND_DEL  4



/*:17*/


/*16:*/


extern int verbose,iterations,should_show_tour,should_show_version;
extern int representation,construction_algorithm;
extern long start_heuristic_param;
extern int candidate_expr,cand_nn_k,cand_nq_k,cand_del_d;
extern char*PostScript_filename,*lower_bound_name,*upper_bound_name;
extern void(*sort)(void*a,size_t n,size_t es,int(*cmp)(const void*,const void*));
extern int noround;
extern double lower_bound_value,upper_bound_value;
extern int extra_backtrack;
extern long random_seed;

/*:16*//*39:*/


extern int max_generic_flips;

/*:39*//*65:*/


extern tsp_instance_t*tsp_instance;

/*:65*//*70:*/


extern int*original_city_num;


/*:70*//*80:*/


extern int begin_data_structures_mark;


/*:80*//*97:*/


extern int(*tour_next)(int);
extern int(*tour_prev)(int);
extern int(*tour_between)(int,int,int);
extern void(*tour_flip)(int,int,int,int);
extern void(*tour_set)(int const*);
extern void(*tour_setup)(int n);
extern void(*tour_cleanup)(void);

/*:97*//*109:*/


extern length_t incumbent_len;

/*:109*//*133:*/


/*:133*/


/*7:*/


int lk_main(int argc,char**argv);


/*:7*//*134:*/



/*:134*//*140:*/


int lk_double_cmp(const void*a,const void*b);
int lk_length_t_cmp(const void*a,const void*b);

/*:140*/



/*:4*/
