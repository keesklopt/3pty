/*2:*/


#include <stdio.h>
#include "gb_flip.h"   


int main(int argc,char**argv);
int main(int argc,char**argv)

{long j;
gb_init_rand(-314159L);
if(gb_next_rand()!=119318998){
fprintf(stderr,"Failure on the first try!\n");return-1;
}
for(j= 1;j<=133;j++)
gb_next_rand();

if(gb_unif_rand(0x55555555L)!=748103812){
fprintf(stderr,"Failure on the second try!\n");return-2;
}
/*19:*/


{gb_prng_t*a,*b;
gb_init_rand(-314159L);
a= gb_prng_new(-314159L);
b= gb_prng_new(-314159L);
if(gb_next_rand()!=119318998
||gb_prng_next_rand(a)!=119318998
||gb_prng_next_rand(b)!=119318998){
fprintf(stderr,"OO Failure on the first try!\n");return-1;
}
for(j= 1;j<=133;j++){
gb_next_rand();
gb_prng_next_rand(a);
gb_prng_next_rand(b);
}
if(gb_unif_rand(0x55555555L)!=748103812
||gb_prng_unif_rand(a,0x55555555L)!=748103812
||gb_prng_unif_rand(b,0x55555555L)!=748103812){
fprintf(stderr,"OO Failure on the second try!\n");return-2;
}
gb_prng_free(a);
gb_prng_free(b);
}


/*:19*/


fprintf(stderr,"OK, the gb_flip routines seem to work!\n");
return 0;

}

/*:2*/
