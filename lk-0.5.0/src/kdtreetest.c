/*114:*/


#include <stdio.h> 
#include <stdlib.h> 
#include <stddef.h> 

#include "length.h"
#include "memory.h"
#include "error.h"
#include "read.h"
#include "kdtree.h"

int verbose= 0;
int noround= 0;

int
main(int argc,char**argv)
{
kd_bin_t*work[6];
int work_num[6],q,i;
int errors= 0;
const int num_max_sizes= 3;
int max_sizes[3],ms;
tsp_instance_t*p= read_tsp_file(stdin,NULL,0);
printf("n=%d\n",p->n);
max_sizes[0]= 1;
max_sizes[1]= 3;
max_sizes[2]= 10;

for(q= 0;q<6;q++){
work[q]= new_arr_of(kd_bin_t,p->n);
printf("work[%d]=%p\n",q,work[q]);
}
E2_create(p);

for(ms= 0;ms<num_max_sizes;ms++){
int max_size= max_sizes[ms];
printf("\n\nmax_size in each quadrant is %d\n",max_size);
for(i= 0;i<p->n;i++){
work_num[5]= E2_nn_bulk(i,max_size,work[5]);

for(q= 0;q<5;q++){
printf("%d search quad %d\n",i,q);
work_num[q]= E2_nn_quadrant_bulk(i,max_size,work[q],1<<q);
printf("%d found quad %d %d cities\n",i,q,work_num[q]);
}
for(q= 0;q<6;q++){
printf(" Quadrant %d\n",q);


if(q==5&&max_size==p->n&&work_num[q]!=(max_size-1)){
printf("error: (a)nn bulk answered %d instead of %d neighbours\n",
work_num[q],max_size-1);
errors++;
}
if(q==5&&max_size<p->n&&work_num[q]!=max_size){
printf("error: (b)nn bulk answered %d instead of %d neighbours\n",
work_num[q],max_size);
errors++;
}
if(q!=5&&max_size<p->n&&work_num[q]> max_size){
printf("error: (c)nn bulk answered %d instead of %d neighbours\n",
work_num[q],max_size);
errors++;
}


if(work_num[q]> 0){
int j;
length_t last_len;
for(j= 0;j<work_num[q];j++){
printf("  _ (%d,%d) ~ %f\n",
i,kd_bin_city(&work[q][j]),
kd_bin_monotonic_len(&work[q][j]));
}
qsort(work[q],(size_t)work_num[q],sizeof(kd_bin_t),kd_bin_cmp_increasing);
for(j= 0;j<work_num[q];j++){
printf("    (%d,%d) ~ %f\n",
i,kd_bin_city(&work[q][j]),
kd_bin_monotonic_len(&work[q][j]));
}
last_len= cost(i,kd_bin_city(work[q]+0));
for(j= 1;j<work_num[q];j++){
int there= kd_bin_city(work[q]+j);
length_t this_len;
if(there<0||there>=p->n){
printf("        there=%d is bad\n",there);
fflush(stdout);
errors++;
}
this_len= cost(i,there);
printf("         %.0f :: %.0f\n",(double)last_len,(double)this_len);
if(this_len<last_len){
printf("error: i %d j %d (%d) mono %f thislen %f < last_len %f\n",
i,j,there,kd_bin_monotonic_len(work[q]+j),
(double)this_len,(double)last_len);
errors++;
}
last_len= this_len;
if((j%1000)==999)printf(".\n");
}
}
}
if((i%1000)==999)printf(";\n");
}

}
E2_destroy();
return errors;
}

/*:114*/
