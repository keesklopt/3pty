head	1.4;
access;
symbols
	zero-five-zero:1.4;
locks
	neto:1.4; strict;
comment	@# @;


1.4
date	98.12.05.22.38.10;	author neto;	state Exp;
branches;
next	1.3;

1.3
date	98.12.05.20.28.02;	author neto;	state Exp;
branches;
next	1.2;

1.2
date	98.12.05.20.24.20;	author neto;	state Exp;
branches;
next	1.1;

1.1
date	98.12.05.20.11.23;	author neto;	state Exp;
branches;
next	;


desc
@Cluster discount randomized transform on a TSPLIB instance
@


1.4
log
@Shorten the line for CWEB digestability
@
text
@@@i myboiler.w

\noindent Copyright \copyright 1994, 1995, 1996, 1997, 1998 David Neto
\smallskip

\noindent 
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
\smallskip

\noindent 
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
\smallskip

\noindent   
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA  02111-1307, USA.
\smallskip

\noindent   
   You may contact David Neto via email at {\tt netod@@@@acm.org}, or with
   greater latency at
\smallskip
\noindent{\obeylines
     Department of Computer Science
     University of Toronto
     10 King's College Rd.
     Toronto, Ontario
     M5S 3G4
     Canada
}
\medskip


\noindent\hbox{}\hrule\hbox{}\penalty-1000
\vskip0.5cm\relax



@@i webdefs.w
@@i types.w

\def\9#1{{\sl #1}}
{\obeylines
$Log: clusterdiscount.w,v $
Revision 1.3  1998/12/05 20:28:02  neto
Fixed output instance name

Revision 1.2  1998/12/05 20:24:20  neto
Fix the COMMENT augmentation

Revision 1.1  1998/12/05 20:11:23  neto
Initial revision

Revision 1.1  1998/12/05 19:27:56  neto
Initial revision

}

@@*CLUSTERDISCOUNT.
This program implements algorithm \alg{cluster-discount}.

The input is a TSPLIB instance taken from standard input.
The output is a matrix-form TSPLIB instance written to standard output.
@@^TSPLIB@@>

@@ We take one parameter, a random number seed.

@@ The outline of this program is as follows, and it is very much like
\alg{cluster-noise}.

@@c

const char *prog_name = "clusterdiscount";
const char *clusterdiscount_rcs_id = 
"$Id: clusterdiscount.w,v 1.3 1998/12/05 20:28:02 neto Exp neto $";
#include <config.h>
#include "lkconfig.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "length.h"
#include "read.h"
#include "decluster.h"
#include "kdtree.h"
#include "dsort.h"

static double dsj_random(const int i, const int j);
static length_t noisy_cost(int u, int v);

int noround=0;
int verbose=0;
long seed = -1998;
void (*sort)(void *base, size_t nmemb, size_t size,
              int (*compar)(const void *, const void *)) = dsort;


@@<Module definitions@@>@@;
@@<Module variables@@>@@;
@@<Module subroutines@@>@@;

int main(int argc, char **argv) 
{
	tsp_instance_t *tsp;
	decluster_tree_t *mst;
	if ( argc >= 2 ) { seed = atoi(argv[1]); }

	tsp=read_tsp_file(stdin,NULL,0);
	if ( E2_supports(tsp) ) {
		E2_create(tsp); /* Forget about freeing it, though. */
	}
	mst=decluster_setup(tsp->n);
	decluster_mst(tsp,mst);
	decluster_preprocess(mst);
	@@<Now write out the cost function, with discount@@>@@;
	return 0;
}


@@ Cost function |dsj_random| generates random 
scaled weights based only on a scale, a seed, and the two indices $i$
and $j$.  It is used by David Johnson and colleagues to produce random
matrices in a space-efficient manner.

(Our vertex numbers are zero-based, so we have to add 1
before generating the salt.)

@@<Module subroutines@@>=

static double
dsj_random(const int ii, const int jj)
{
	const int32 i=ii, j=jj;
	const int32 salt1 = 0x12345672*(i+1) + 1;
	const int32 salt2 = 0x12345672*(j+1) + 1;
	int32 x,y,z;

    x = salt1 & salt2;
    y = salt1 | salt2;
    z = dsj_random_param;

    x *= z;
    y *= x;
    z *= y;

    z ^= dsj_random_param;

    x *= z;
    y *= x;
    z *= y;

    x = ((salt1 + salt2) ^ z) &0x7fffffff;
    return (double) (x*dsj_random_factor);
}


@@ For consistent results, we need a 32 bit integer type.
@@<Module definitions@@>=
#if SIZEOF_INT==4
typedef int int32;
#elif SIZEOF_SHORT==4
typedef short int int32;
#elif SIZEOF_LONG==4
typedef long int32;
#else
#error "I need a 32 bit integer for consistent results with DSJ_RANDOM"
#endif

@@
@@<Module variables@@>=
static double dsj_random_factor = 1.0/2147483648.0;
static int32 dsj_random_param = 99163; /* Arbitrary junk. */

@@ Function |noisy_cost| actually computes the noisy cluster function.

@@<Module subroutines@@>=
static length_t 
noisy_cost(int u, int v) 
{
	return cost(u,v)-decluster_d(u,v)*dsj_random(u,v);
}

@@ Now we can actually generate the new instance.  We output it in
upper triangular matrix form.

@@<Now write out the cost function, with discount@@>=
dsj_random_factor = 1.0/2147483648.0;
dsj_random_param = 1+104*seed;  /* Inherit this$\ldots$ */
{ FILE *out=stdout; int row, col, n=tsp->n;
fprintf(out,"NAME: cd.%ld.%s\n",seed,tsp->name);
fprintf(out,"TYPE: TSP\n");
fprintf(out,"COMMENT: %s | clusterdiscount %ld\n",
    (tsp->comment?tsp->comment:""), seed);
fprintf(out,"DIMENSION: %d\n",tsp->n);
fprintf(out,"EDGE_WEIGHT_TYPE: EXPLICIT\n");
fprintf(out,"EDGE_WEIGHT_FORMAT: UPPER_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for (row=0;row<n;row++) {
	for (col=row+1;col<n;col++) {
		fprintf(out," %ld",(long)noisy_cost(row,col));
	}
	fprintf(out,"\n");
}
fprintf(out,"EOF\n");
}


@@*Index.

@


1.3
log
@Fixed output instance name
@
text
@d53 3
d82 2
a83 1
const char *clusterdiscount_rcs_id = "$Id: clusterdiscount.w,v 1.2 1998/12/05 20:24:20 neto Exp neto $";
@


1.2
log
@Fix the COMMENT augmentation
@
text
@d53 3
d79 1
a79 1
const char *clusterdiscount_rcs_id = "$Id: clusterdiscount.w,v 1.1 1998/12/05 20:11:23 neto Exp neto $";
d194 1
a194 1
fprintf(out,"NAME: cn.%ld.%s\n",seed,tsp->name);
@


1.1
log
@Initial revision
@
text
@d52 4
a55 1
$Log: clusternoise.w,v $
d76 1
a76 1
const char *clusterdiscount_rcs_id = "$Id$";
d193 1
a193 1
fprintf(out,"COMMENT: %s | clusternoise %ld\n",
@
