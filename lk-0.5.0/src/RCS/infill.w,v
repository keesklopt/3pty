head	1.1;
access;
symbols
	zero-five-zero:1.1;
locks
	neto:1.1; strict;
comment	@# @;


1.1
date	98.12.05.22.04.07;	author neto;	state Exp;
branches;
next	;


desc
@Destroy cluster structure by infilling.
@


1.1
log
@Initial revision
@
text
@@@i myboiler.w

\noindent Copyright \copyright 1994, 1995, 1996, 1997, 1998 David Neto
\smallskip

\noindent 
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
\smallskip

\noindent 
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
\smallskip

\noindent   
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA  02111-1307, USA.
\smallskip

\noindent   
   You may contact David Neto via email at {\tt netod@@@@acm.org}, or with
   greater latency at
\smallskip
\noindent{\obeylines
     Department of Computer Science
     University of Toronto
     10 King's College Rd.
     Toronto, Ontario
     M5S 3G4
     Canada
}
\medskip


\noindent\hbox{}\hrule\hbox{}\penalty-1000
\vskip0.5cm\relax



@@i webdefs.w
@@i types.w

\def\9#1{{\sl #1}}
{\obeylines
$Log: clusterdiscount.w,v $
Revision 1.1  1998/12/05 20:11:23  neto
Initial revision

Revision 1.1  1998/12/05 19:27:56  neto
Initial revision

}

@@*CLUSTERDISCOUNT.
This program implements algorithm \alg{cluster-discount}.

The input is a TSPLIB instance taken from standard input.
The output is a matrix-form TSPLIB instance written to standard output.
@@^TSPLIB@@>


@@ The outline of this program is as follows, and it is very much like
\alg{cluster-noise}.

@@c

const char *prog_name = "infill";
const char *infill_rcs_id = "$Id$";
#include <config.h>
#include "lkconfig.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "length.h"
#include "read.h"
#include "decluster.h"
#include "kdtree.h"
#include "dsort.h"

int noround=0;
int verbose=0; /* Make it positive to get debugging output. */
long seed = -1998;
void (*sort)(void *base, size_t nmemb, size_t size,
              int (*compar)(const void *, const void *)) = dsort;

@@<Module variables@@>@@;
@@<Module subroutines@@>@@;
@@<Subroutines@@>@@;

@@ We take an optional parameter, a filling fraction.

The filling fraction determines what fraction of the $n-1$ longest
edges in a MST should be bisected.  The default is $0.1$, or 10 \%.

|long_edges[0]| through |long_edges[num_extra-1]| are the longest
edges in the MST.

@@<Module variables@@>=
static double fill_fraction = 0.1;
static decluster_edge_t *long_edges=NULL;

@@
@@<Subroutines@@>=
int main(int argc, char **argv) 
{
	tsp_instance_t *tsp;
	decluster_tree_t *mst;
	int num_extra, new_n;
	if ( argc >= 2 ) { fill_fraction = atof(argv[1]); }
	errorif(fill_fraction <0 || fill_fraction >=1, 
		"Fill fraction should be between 0 and 1\n");

	tsp=read_tsp_file(stdin,NULL,0);
	n = tsp->n;
	num_extra = n*fill_fraction, new_n = n+num_extra;
	errorif(num_extra <0 || num_extra > n-1,
 		"Number of extra points must be between 0 and %s: adjust fill fraction",n-1);
	if ( E2_supports(tsp) ) {
		E2_create(tsp); /* Forget about freeing it, though. */
	}
	mst=decluster_setup(tsp->n);
	decluster_mst(tsp,mst);
	select_range(mst->edge,(size_t)(n-1),sizeof(decluster_edge_t),decluster_edge_cmp,
			((n-1)-num_extra),n-1,1);
	long_edges=mst->edge+((n-1)-num_extra);
	@@<Now output a filled-in version of the instance.@@>@@;
	return 0;
}


@@ Now we can actually generate the new instance.  We split the task
into two cases, depending on whether the source instance is a geometric
one, or whether it is in general matrix form.
In the latter case, the output is in upper triangular matrix form.

@@<Now output a filled-in version of the instance.@@>=
{ FILE *out=stdout; int row, col;
	
fprintf(out,"NAME: infill.%d.%s\n",num_extra,tsp->name);
fprintf(out,"TYPE: TSP\n");
fprintf(out,"COMMENT: %s | infill %f\n",
    (tsp->comment?tsp->comment:""), fill_fraction);
fprintf(out,"DIMENSION: %d\n",new_n);
if ( E2_supports(tsp) ) {
	@@<Do the geometric case@@>@@;
} else {
	@@<Do the generic case@@>@@;
}
fprintf(out,"EOF\n");
}

@@
@@<Module variables@@>=
static int n=0;


@@
@@<Do the generic case@@>=
fprintf(out,"EDGE_WEIGHT_TYPE: EXPLICIT\n");
fprintf(out,"EDGE_WEIGHT_FORMAT: UPPER_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for (row=0;row<new_n;row++) {
	for (col=row+1;col<new_n;col++) {
		fprintf(out," %ld",(long)extended_cost(row,col));
	}
	fprintf(out,"\n");
}


@@ We need a minimum length function.  We use a function and not a macro
because we don't want to evaluate the arguments too many times.

@@<Module subroutines@@>=
static inline length_t
min_len(length_t a, length_t b)
{
	if ( a <= b ) return a;
	else return b;
}


@@ Function |noisy_cost| actually computes the noisy cluster function.
It recurses to at most one level below the top call.

The vertices from 0 through $n-1$ are all present in the original
graph.  Those from $n$ through |n+num_extra| are the new vertices.

The topology tree |T| has an edge array with 0 through $n-1$ being
the original vertices of the graph, and $n$ through $2n-2$ being
the $n-1$ edges of the MST, in oder from shortest to longest.

@@<Module subroutines@@>=

static length_t 
extended_cost(int w, int uv) 
{
	decluster_edge_t e;
	if ( w < n && uv < n ) return cost(w,uv);
	if ( uv < n ) return extended_cost(uv,w);
	e = long_edges[uv-n];
	return e.cost/2 + 
			min_len(@@|
				((w<n ? cost : extended_cost)(e.city[0],w)),@@|
				((w<n ? cost : extended_cost)(e.city[1],w)));
}


@@ The geometric case is simpler.  We just put an extra point in
the middle of the longest |num_extra| edges.

@@<Do the geometric case@@>=
switch(tsp->edge_weight_type) {
case CEIL_2D: fprintf(out,"EDGE_WEIGHT_TYPE: CEIL_2D\n");
	break;
case EUC_2D:  fprintf(out,"EDGE_WEIGHT_TYPE: EUC_2D\n");
	break;
case GEO:  fprintf(out,"EDGE_WEIGHT_TYPE: GEO\n");
	break;
case ATT:  fprintf(out,"EDGE_WEIGHT_TYPE: GEO\n");
	break;
default:
	errorif(1,"Doh! what's that edge weight type again?");
	break;
}
fprintf(out,"NODE_COORD_SECTION\n");
{ int i;
for ( i=0; i<n ; i++ ) {
	fprintf(out,"%d %g %g\n",i+1,tsp->coord[i].x[0],tsp->coord[i].x[1]);
}
for (i=n; i<new_n;i++) {
	const decluster_edge_t e = long_edges[i-n];
	if (verbose>0) {
		fprintf(out,"   Halfway between %d (%g,%g) and %d (%g,%g) len "length_t_spec"\n",
			e.city[0],
			tsp->coord[e.city[0]].x[0],tsp->coord[e.city[0]].x[1],
			e.city[1],
			tsp->coord[e.city[1]].x[0],tsp->coord[e.city[1]].x[1],
			e.cost);
	}
	fprintf(out,"%d %g %g\n",i+1,
			(tsp->coord[e.city[0]].x[0]+tsp->coord[e.city[1]].x[0])/2,
			(tsp->coord[e.city[0]].x[1]+tsp->coord[e.city[1]].x[1])/2);
}
}


@@*Index.
@
