#define norm(x) ((x) <n?(x) :(x) -n)  \

#include <config.h>
#include "lkconfig.h"
/*4:*/

//#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>

/*:4*/


/*6:*/


#include "array.h"



/*:6*//*11:*/


#include "memory.h"

/*:11*//*22:*/


#include "error.h"

/*:22*/



/*21:*/


#if !defined(ARRAY_DEBUG)
#define ARRAY_DEBUG 0
#endif

/*:21*/


/*7:*/


static int*A= NULL,*B= NULL;

/*:7*//*8:*/


static int n,nm1;

/*:8*/


/*9:*/


void
array_setup(int num_vertices){
/*10:*/


n= num_vertices;
nm1= n-1;
A= new_arr_of(int,n);
B= new_arr_of(int,n);

/*:10*/


}

/*:9*//*12:*/


void
array_cleanup(void){
/*13:*/


free_mem(A);
free_mem(B);

/*:13*/


}

/*:12*//*15:*/


int
array_next(int a){
int ra;
/*28:*/


/*:28*/


ra= B[a];
if(ra==nm1)return A[0];
else return A[ra+1];
}

int
array_prev(int a){
int ra;
/*29:*/


/*:29*/


ra= B[a];
if(ra==0)return A[nm1];
else return A[ra-1];
}

/*:15*//*17:*/


int
array_between(int a,int b,int c){
int ra,rb,rc;
ra= B[a];
rb= B[b];
rc= B[c];
/*30:*/


/*:30*/


if(rb<ra)rb+= n;
if(rc<ra)rc+= n;
return rb<=rc;
}

/*:17*//*19:*/


void
array_flip(int a,int b,int c,int d){
int ra,rb,rc,rd;
/*20:*/


#if ARRAY_DEBUG
errorif(a!=array_next(b),"a != array_next(b)");
errorif(d!=array_next(c),"d != array_next(c)");
#endif 

/*:20*/


/*23:*/


ra= B[a];
rb= B[b];
rc= B[c];
rd= B[d];
if(rc<ra)rc+= n;
if(rb<rd)rb+= n;
if(rc-ra>rb-rd){
int t;
t= a;a= d;d= t;
t= b;b= c;c= t;
t= ra;ra= rd;rd= t;
t= rb;rb= rc;rc= t;
}

/*:23*/


/*31:*/



/*:31*/


/*24:*/


{
int ri,rj,t;
for(ri= ra,rj= rc;ri<rj;ri++,rj--){
B[A[norm(ri)]]= norm(rj);
B[A[norm(rj)]]= norm(ri);
t= A[norm(ri)];A[norm(ri)]= A[norm(rj)];A[norm(rj)]= t;
}
#if ARRAY_DEBUG>1000 
for(i= 0;i<n;i++){
assert(B[A[i]]==i);
assert(A[B[i]]==i);
}
#endif
}

/*:24*/


}


/*:19*//*26:*/


void
array_set(int const*tour){
int i;
for(i= 0;i<n;i++){
A[i]= tour[i];
B[A[i]]= i;
}
}

/*:26*/


const char*array_rcs_id= "$Id: array.w,v 1.112 1998/07/16 21:58:55 neto Exp neto $";

