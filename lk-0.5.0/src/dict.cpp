#define SETLINK(N,L,C) {(N) ->link[L]= (C) ;if(C) (C) ->parent= (N) ;} \

/*2:*/


#include <config.h>
#include "lkconfig.h"
/*3:*/


#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include "fixincludes.h"

/*:3*/


/*5:*/


#include "dict.h"

/*:5*//*7:*/


#include "error.h"
#include "memory.h"

/*:7*/



/*11:*/


#define NILLINK (-2) 
#define SELF (-1)
#define PARENT (0)
#define LEFT (1)
#define RIGHT (2)
#define parent link[PARENT]
#define left link[LEFT]
#define right link[RIGHT]

/*:11*/


/*12:*/


dict_t*dict_create(int(*the_cmp)(const void*a,const void*b),void(*the_prn)(void*a)){
dict_t*d= new_of(dict_t);
d->root= NULL;
errorif(the_cmp==NULL,"Need a non-null comparison function");
d->cmp= the_cmp;
d->prn= the_prn;
d->pool= pool_create(sizeof(dict_node_t),1000);
d->size= 0;
return d;
}

/*:12*//*13:*/


void
dict_destroy(dict_t*d,void(*action)(void*)){
if(d!=NULL){
/*14:*/


{
dict_node_t*here,*prev;
here= d->root;
prev= NULL;
while(here!=NULL){
if(here->left!=NULL&&prev==here->parent){
prev= here;
here= here->left;
}else if(here->right!=NULL&&prev!=here->right){
prev= here;
here= here->right;
}else{
if(action)action(here->payload);
prev= here;
here= here->parent;
pool_free(d->pool,prev);
}
}
}


/*:14*/


d->cmp= NULL;
d->prn= NULL;
d->root= NULL;
pool_destroy(d->pool);
d->pool= NULL;
d->size= 0;
free_mem(d);
}
}

/*:13*//*15:*/


void
dict_delete_all(dict_t*d,void(*action)(void*)){
/*14:*/


{
dict_node_t*here,*prev;
here= d->root;
prev= NULL;
while(here!=NULL){
if(here->left!=NULL&&prev==here->parent){
prev= here;
here= here->left;
}else if(here->right!=NULL&&prev!=here->right){
prev= here;
here= here->right;
}else{
if(action)action(here->payload);
prev= here;
here= here->parent;
pool_free(d->pool,prev);
}
}
}


/*:14*/


d->root= NULL;
d->size= 0;
}

/*:15*//*16:*/


int
dict_insert(dict_t*d,void*e){
dict_node_t*here,*next;
int l;
int was_absent;

next= d->root;
/*17:*/


l= PARENT;
for(here= NULL;next&&next!=here;){
int compared_as= d->cmp(e,next->payload);
here= next;
if(compared_as<0){
next= here->link[l= LEFT];
}else if(compared_as>0){
next= here->link[l= RIGHT];
}else if(compared_as==0){
l= SELF;
break;
}
}


/*:17*/


if(0!=(was_absent= (l!=SELF))){
/*18:*/


{
dict_node_t*node= (dict_node_t*)pool_alloc(d->pool);
node->payload= e;
node->left= node->right= NULL;
node->parent= here;
if(here==NULL)d->root= node;
else here->link[l]= node;
here= node;
}

/*:18*/


d->size++;
}
/*19:*/


if(here){
dict_node_t*p,*gp,*ggp= NULL;
int pl,gpl;
int ggpl= NILLINK;

dict_node_t*subtree[2];
do{
errorif(here==NULL,"|here| must not be NULL");
/*20:*/


gpl= pl= SELF;
p= here->parent;
if(p){
if(p->left==here)pl= LEFT;
else if(p->right==here)pl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
gp= p->parent;
if(gp){
if(gp->left==p)gpl= LEFT;
else if(gp->right==p)gpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
ggp= gp->parent;
if(ggp){
if(ggp->left==gp)ggpl= LEFT;
else if(ggp->right==gp)ggpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
}
}
}else{
break;
}


/*:20*/


/*21:*/


if(gp){
/*22:*/


switch(gpl){
case LEFT:
switch(pl){
case LEFT:
subtree[0]= here->right;
subtree[1]= p->right;
SETLINK(here,RIGHT,p);
SETLINK(p,RIGHT,gp);
SETLINK(p,LEFT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,p);
SETLINK(here,RIGHT,gp);
SETLINK(p,RIGHT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
}
break;
case RIGHT:
switch(pl){
case LEFT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,gp);
SETLINK(here,RIGHT,p);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= p->left;
subtree[1]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,LEFT,gp);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,RIGHT,subtree[1]);
break;
}
break;
}
if(ggp){
if(ggpl!=NILLINK){
SETLINK(ggp,ggpl,here);
}else{
errorif(1,"Uninitialized great grandparent link.");
}
}else{here->parent= NULL;d->root= here;}


/*:22*/


}else{
/*23:*/


switch(pl){
case LEFT:
subtree[0]= here->right;
SETLINK(here,RIGHT,p);
SETLINK(p,LEFT,subtree[0]);
break;
case RIGHT:
subtree[0]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,RIGHT,subtree[0]);
break;
}
here->parent= NULL;
d->root= here;

/*:23*/


}

/*:21*/


}while(1);
}

/*:19*/


return!was_absent;
}

/*:16*//*24:*/


void*
dict_find(dict_t*d,void*e){
dict_node_t*here,*next;
int l;
void*found= NULL;

next= d->root;
/*17:*/


l= PARENT;
for(here= NULL;next&&next!=here;){
int compared_as= d->cmp(e,next->payload);
here= next;
if(compared_as<0){
next= here->link[l= LEFT];
}else if(compared_as>0){
next= here->link[l= RIGHT];
}else if(compared_as==0){
l= SELF;
break;
}
}


/*:17*/


if(next!=NULL){
found= next->payload;
}
/*19:*/


if(here){
dict_node_t*p,*gp,*ggp= NULL;
int pl,gpl;
int ggpl= NILLINK;

dict_node_t*subtree[2];
do{
errorif(here==NULL,"|here| must not be NULL");
/*20:*/


gpl= pl= SELF;
p= here->parent;
if(p){
if(p->left==here)pl= LEFT;
else if(p->right==here)pl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
gp= p->parent;
if(gp){
if(gp->left==p)gpl= LEFT;
else if(gp->right==p)gpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
ggp= gp->parent;
if(ggp){
if(ggp->left==gp)ggpl= LEFT;
else if(ggp->right==gp)ggpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
}
}
}else{
break;
}


/*:20*/


/*21:*/


if(gp){
/*22:*/


switch(gpl){
case LEFT:
switch(pl){
case LEFT:
subtree[0]= here->right;
subtree[1]= p->right;
SETLINK(here,RIGHT,p);
SETLINK(p,RIGHT,gp);
SETLINK(p,LEFT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,p);
SETLINK(here,RIGHT,gp);
SETLINK(p,RIGHT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
}
break;
case RIGHT:
switch(pl){
case LEFT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,gp);
SETLINK(here,RIGHT,p);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= p->left;
subtree[1]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,LEFT,gp);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,RIGHT,subtree[1]);
break;
}
break;
}
if(ggp){
if(ggpl!=NILLINK){
SETLINK(ggp,ggpl,here);
}else{
errorif(1,"Uninitialized great grandparent link.");
}
}else{here->parent= NULL;d->root= here;}


/*:22*/


}else{
/*23:*/


switch(pl){
case LEFT:
subtree[0]= here->right;
SETLINK(here,RIGHT,p);
SETLINK(p,LEFT,subtree[0]);
break;
case RIGHT:
subtree[0]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,RIGHT,subtree[0]);
break;
}
here->parent= NULL;
d->root= here;

/*:23*/


}

/*:21*/


}while(1);
}

/*:19*/


return found;
}

/*:24*//*25:*/


void*
dict_delete(dict_t*d,void*e,void(*action)(void*)){
dict_node_t*next,*here,*splay_this;void*deleted_payload= NULL;
int l;
next= d->root;
/*17:*/


l= PARENT;
for(here= NULL;next&&next!=here;){
int compared_as= d->cmp(e,next->payload);
here= next;
if(compared_as<0){
next= here->link[l= LEFT];
}else if(compared_as>0){
next= here->link[l= RIGHT];
}else if(compared_as==0){
l= SELF;
break;
}
}


/*:17*/


if(next)deleted_payload= next->payload;
/*26:*/


{
dict_node_t*e_node,*ep;
int l;
e_node= next;
if(e_node){
if(e_node->left==NULL&&e_node->right==NULL){
/*27:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){ep->link[l]= NULL;}
else{d->root= NULL;}



/*:27*/


splay_this= e_node->parent;
}else if(e_node->left==NULL){
/*29:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->right);}
else{e_node->right->parent= NULL;d->root= e_node->right;}

/*:29*/


splay_this= e_node->right;
}else if(e_node->right==NULL){
/*30:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->left);}
else{e_node->left->parent= NULL;d->root= e_node->left;}

/*:30*/


splay_this= e_node->left;
}else{
/*31:*/


{dict_node_t*erL,*save_e_node= e_node;

for(erL= e_node->right;erL->left;erL= erL->left)
;
if(erL->right){
splay_this= erL->right;
}else{
if(erL->parent!=e_node)splay_this= erL->parent;
else splay_this= erL;
}
e_node= erL;
/*29:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->right);}
else{e_node->right->parent= NULL;d->root= e_node->right;}

/*:29*/


e_node= save_e_node;


SETLINK(erL,LEFT,e_node->left);
if(e_node->right!=erL){SETLINK(erL,RIGHT,e_node->right);}


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,erL);}
else{erL->parent= NULL;d->root= erL;}
}

/*:31*/


}
if(action)action(e_node->payload);
pool_free(d->pool,e_node);
d->size--;
here= splay_this;
}
}

/*:26*/


/*19:*/


if(here){
dict_node_t*p,*gp,*ggp= NULL;
int pl,gpl;
int ggpl= NILLINK;

dict_node_t*subtree[2];
do{
errorif(here==NULL,"|here| must not be NULL");
/*20:*/


gpl= pl= SELF;
p= here->parent;
if(p){
if(p->left==here)pl= LEFT;
else if(p->right==here)pl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
gp= p->parent;
if(gp){
if(gp->left==p)gpl= LEFT;
else if(gp->right==p)gpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
ggp= gp->parent;
if(ggp){
if(ggp->left==gp)ggpl= LEFT;
else if(ggp->right==gp)ggpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
}
}
}else{
break;
}


/*:20*/


/*21:*/


if(gp){
/*22:*/


switch(gpl){
case LEFT:
switch(pl){
case LEFT:
subtree[0]= here->right;
subtree[1]= p->right;
SETLINK(here,RIGHT,p);
SETLINK(p,RIGHT,gp);
SETLINK(p,LEFT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,p);
SETLINK(here,RIGHT,gp);
SETLINK(p,RIGHT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
}
break;
case RIGHT:
switch(pl){
case LEFT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,gp);
SETLINK(here,RIGHT,p);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= p->left;
subtree[1]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,LEFT,gp);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,RIGHT,subtree[1]);
break;
}
break;
}
if(ggp){
if(ggpl!=NILLINK){
SETLINK(ggp,ggpl,here);
}else{
errorif(1,"Uninitialized great grandparent link.");
}
}else{here->parent= NULL;d->root= here;}


/*:22*/


}else{
/*23:*/


switch(pl){
case LEFT:
subtree[0]= here->right;
SETLINK(here,RIGHT,p);
SETLINK(p,LEFT,subtree[0]);
break;
case RIGHT:
subtree[0]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,RIGHT,subtree[0]);
break;
}
here->parent= NULL;
d->root= here;

/*:23*/


}

/*:21*/


}while(1);
}

/*:19*/


return deleted_payload;
}

/*:25*//*32:*/


void*
dict_delete_any(dict_t*d,void(*action)(void*)){
if(d->root){
dict_node_t*next,*here= NULL,*splay_this;
void*ret;
next= d->root;
ret= next->payload;
/*26:*/


{
dict_node_t*e_node,*ep;
int l;
e_node= next;
if(e_node){
if(e_node->left==NULL&&e_node->right==NULL){
/*27:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){ep->link[l]= NULL;}
else{d->root= NULL;}



/*:27*/


splay_this= e_node->parent;
}else if(e_node->left==NULL){
/*29:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->right);}
else{e_node->right->parent= NULL;d->root= e_node->right;}

/*:29*/


splay_this= e_node->right;
}else if(e_node->right==NULL){
/*30:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->left);}
else{e_node->left->parent= NULL;d->root= e_node->left;}

/*:30*/


splay_this= e_node->left;
}else{
/*31:*/


{dict_node_t*erL,*save_e_node= e_node;

for(erL= e_node->right;erL->left;erL= erL->left)
;
if(erL->right){
splay_this= erL->right;
}else{
if(erL->parent!=e_node)splay_this= erL->parent;
else splay_this= erL;
}
e_node= erL;
/*29:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->right);}
else{e_node->right->parent= NULL;d->root= e_node->right;}

/*:29*/


e_node= save_e_node;


SETLINK(erL,LEFT,e_node->left);
if(e_node->right!=erL){SETLINK(erL,RIGHT,e_node->right);}


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,erL);}
else{erL->parent= NULL;d->root= erL;}
}

/*:31*/


}
if(action)action(e_node->payload);
pool_free(d->pool,e_node);
d->size--;
here= splay_this;
}
}

/*:26*/


/*19:*/


if(here){
dict_node_t*p,*gp,*ggp= NULL;
int pl,gpl;
int ggpl= NILLINK;

dict_node_t*subtree[2];
do{
errorif(here==NULL,"|here| must not be NULL");
/*20:*/


gpl= pl= SELF;
p= here->parent;
if(p){
if(p->left==here)pl= LEFT;
else if(p->right==here)pl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
gp= p->parent;
if(gp){
if(gp->left==p)gpl= LEFT;
else if(gp->right==p)gpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
ggp= gp->parent;
if(ggp){
if(ggp->left==gp)ggpl= LEFT;
else if(ggp->right==gp)ggpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
}
}
}else{
break;
}


/*:20*/


/*21:*/


if(gp){
/*22:*/


switch(gpl){
case LEFT:
switch(pl){
case LEFT:
subtree[0]= here->right;
subtree[1]= p->right;
SETLINK(here,RIGHT,p);
SETLINK(p,RIGHT,gp);
SETLINK(p,LEFT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,p);
SETLINK(here,RIGHT,gp);
SETLINK(p,RIGHT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
}
break;
case RIGHT:
switch(pl){
case LEFT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,gp);
SETLINK(here,RIGHT,p);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= p->left;
subtree[1]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,LEFT,gp);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,RIGHT,subtree[1]);
break;
}
break;
}
if(ggp){
if(ggpl!=NILLINK){
SETLINK(ggp,ggpl,here);
}else{
errorif(1,"Uninitialized great grandparent link.");
}
}else{here->parent= NULL;d->root= here;}


/*:22*/


}else{
/*23:*/


switch(pl){
case LEFT:
subtree[0]= here->right;
SETLINK(here,RIGHT,p);
SETLINK(p,LEFT,subtree[0]);
break;
case RIGHT:
subtree[0]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,RIGHT,subtree[0]);
break;
}
here->parent= NULL;
d->root= here;

/*:23*/


}

/*:21*/


}while(1);
}

/*:19*/


return ret;
}else return NULL;
}

/*:32*//*33:*/


void*
dict_min(dict_t*d){
dict_node_t*here;
if(d->root){
for(here= d->root;here->left;here= here->left)
;
/*19:*/


if(here){
dict_node_t*p,*gp,*ggp= NULL;
int pl,gpl;
int ggpl= NILLINK;

dict_node_t*subtree[2];
do{
errorif(here==NULL,"|here| must not be NULL");
/*20:*/


gpl= pl= SELF;
p= here->parent;
if(p){
if(p->left==here)pl= LEFT;
else if(p->right==here)pl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
gp= p->parent;
if(gp){
if(gp->left==p)gpl= LEFT;
else if(gp->right==p)gpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
ggp= gp->parent;
if(ggp){
if(ggp->left==gp)ggpl= LEFT;
else if(ggp->right==gp)ggpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
}
}
}else{
break;
}


/*:20*/


/*21:*/


if(gp){
/*22:*/


switch(gpl){
case LEFT:
switch(pl){
case LEFT:
subtree[0]= here->right;
subtree[1]= p->right;
SETLINK(here,RIGHT,p);
SETLINK(p,RIGHT,gp);
SETLINK(p,LEFT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,p);
SETLINK(here,RIGHT,gp);
SETLINK(p,RIGHT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
}
break;
case RIGHT:
switch(pl){
case LEFT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,gp);
SETLINK(here,RIGHT,p);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= p->left;
subtree[1]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,LEFT,gp);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,RIGHT,subtree[1]);
break;
}
break;
}
if(ggp){
if(ggpl!=NILLINK){
SETLINK(ggp,ggpl,here);
}else{
errorif(1,"Uninitialized great grandparent link.");
}
}else{here->parent= NULL;d->root= here;}


/*:22*/


}else{
/*23:*/


switch(pl){
case LEFT:
subtree[0]= here->right;
SETLINK(here,RIGHT,p);
SETLINK(p,LEFT,subtree[0]);
break;
case RIGHT:
subtree[0]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,RIGHT,subtree[0]);
break;
}
here->parent= NULL;
d->root= here;

/*:23*/


}

/*:21*/


}while(1);
}

/*:19*/


return here->payload;
}else return NULL;
}

/*:33*//*34:*/


void*
dict_max(dict_t*d)
{
dict_node_t*here;
if(d->root){
for(here= d->root;here->right;here= here->right)
;
/*19:*/


if(here){
dict_node_t*p,*gp,*ggp= NULL;
int pl,gpl;
int ggpl= NILLINK;

dict_node_t*subtree[2];
do{
errorif(here==NULL,"|here| must not be NULL");
/*20:*/


gpl= pl= SELF;
p= here->parent;
if(p){
if(p->left==here)pl= LEFT;
else if(p->right==here)pl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
gp= p->parent;
if(gp){
if(gp->left==p)gpl= LEFT;
else if(gp->right==p)gpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
ggp= gp->parent;
if(ggp){
if(ggp->left==gp)ggpl= LEFT;
else if(ggp->right==gp)ggpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
}
}
}else{
break;
}


/*:20*/


/*21:*/


if(gp){
/*22:*/


switch(gpl){
case LEFT:
switch(pl){
case LEFT:
subtree[0]= here->right;
subtree[1]= p->right;
SETLINK(here,RIGHT,p);
SETLINK(p,RIGHT,gp);
SETLINK(p,LEFT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,p);
SETLINK(here,RIGHT,gp);
SETLINK(p,RIGHT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
}
break;
case RIGHT:
switch(pl){
case LEFT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,gp);
SETLINK(here,RIGHT,p);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= p->left;
subtree[1]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,LEFT,gp);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,RIGHT,subtree[1]);
break;
}
break;
}
if(ggp){
if(ggpl!=NILLINK){
SETLINK(ggp,ggpl,here);
}else{
errorif(1,"Uninitialized great grandparent link.");
}
}else{here->parent= NULL;d->root= here;}


/*:22*/


}else{
/*23:*/


switch(pl){
case LEFT:
subtree[0]= here->right;
SETLINK(here,RIGHT,p);
SETLINK(p,LEFT,subtree[0]);
break;
case RIGHT:
subtree[0]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,RIGHT,subtree[0]);
break;
}
here->parent= NULL;
d->root= here;

/*:23*/


}

/*:21*/


}while(1);
}

/*:19*/


return here->payload;
}else return NULL;
}

/*:34*//*35:*/



void*
dict_delete_min(dict_t*d)
{
if(d->root){
dict_node_t*m,*next,*here= NULL,*splay_this;
void(*action)(void*)= NULL,*payload;
for(m= d->root;m->left;m= m->left)
;
next= m;
payload= m->payload;
/*26:*/


{
dict_node_t*e_node,*ep;
int l;
e_node= next;
if(e_node){
if(e_node->left==NULL&&e_node->right==NULL){
/*27:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){ep->link[l]= NULL;}
else{d->root= NULL;}



/*:27*/


splay_this= e_node->parent;
}else if(e_node->left==NULL){
/*29:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->right);}
else{e_node->right->parent= NULL;d->root= e_node->right;}

/*:29*/


splay_this= e_node->right;
}else if(e_node->right==NULL){
/*30:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->left);}
else{e_node->left->parent= NULL;d->root= e_node->left;}

/*:30*/


splay_this= e_node->left;
}else{
/*31:*/


{dict_node_t*erL,*save_e_node= e_node;

for(erL= e_node->right;erL->left;erL= erL->left)
;
if(erL->right){
splay_this= erL->right;
}else{
if(erL->parent!=e_node)splay_this= erL->parent;
else splay_this= erL;
}
e_node= erL;
/*29:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->right);}
else{e_node->right->parent= NULL;d->root= e_node->right;}

/*:29*/


e_node= save_e_node;


SETLINK(erL,LEFT,e_node->left);
if(e_node->right!=erL){SETLINK(erL,RIGHT,e_node->right);}


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,erL);}
else{erL->parent= NULL;d->root= erL;}
}

/*:31*/


}
if(action)action(e_node->payload);
pool_free(d->pool,e_node);
d->size--;
here= splay_this;
}
}

/*:26*/


/*19:*/


if(here){
dict_node_t*p,*gp,*ggp= NULL;
int pl,gpl;
int ggpl= NILLINK;

dict_node_t*subtree[2];
do{
errorif(here==NULL,"|here| must not be NULL");
/*20:*/


gpl= pl= SELF;
p= here->parent;
if(p){
if(p->left==here)pl= LEFT;
else if(p->right==here)pl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
gp= p->parent;
if(gp){
if(gp->left==p)gpl= LEFT;
else if(gp->right==p)gpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
ggp= gp->parent;
if(ggp){
if(ggp->left==gp)ggpl= LEFT;
else if(ggp->right==gp)ggpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
}
}
}else{
break;
}


/*:20*/


/*21:*/


if(gp){
/*22:*/


switch(gpl){
case LEFT:
switch(pl){
case LEFT:
subtree[0]= here->right;
subtree[1]= p->right;
SETLINK(here,RIGHT,p);
SETLINK(p,RIGHT,gp);
SETLINK(p,LEFT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,p);
SETLINK(here,RIGHT,gp);
SETLINK(p,RIGHT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
}
break;
case RIGHT:
switch(pl){
case LEFT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,gp);
SETLINK(here,RIGHT,p);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= p->left;
subtree[1]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,LEFT,gp);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,RIGHT,subtree[1]);
break;
}
break;
}
if(ggp){
if(ggpl!=NILLINK){
SETLINK(ggp,ggpl,here);
}else{
errorif(1,"Uninitialized great grandparent link.");
}
}else{here->parent= NULL;d->root= here;}


/*:22*/


}else{
/*23:*/


switch(pl){
case LEFT:
subtree[0]= here->right;
SETLINK(here,RIGHT,p);
SETLINK(p,LEFT,subtree[0]);
break;
case RIGHT:
subtree[0]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,RIGHT,subtree[0]);
break;
}
here->parent= NULL;
d->root= here;

/*:23*/


}

/*:21*/


}while(1);
}

/*:19*/


return payload;
}else return NULL;
}

void*
dict_delete_max(dict_t*d)
{
if(d->root){
dict_node_t*m,*next,*here= NULL,*splay_this;
void(*action)(void*)= NULL,*payload;
for(m= d->root;m->right;m= m->right)
;
next= m;
payload= m->payload;
/*26:*/


{
dict_node_t*e_node,*ep;
int l;
e_node= next;
if(e_node){
if(e_node->left==NULL&&e_node->right==NULL){
/*27:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){ep->link[l]= NULL;}
else{d->root= NULL;}



/*:27*/


splay_this= e_node->parent;
}else if(e_node->left==NULL){
/*29:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->right);}
else{e_node->right->parent= NULL;d->root= e_node->right;}

/*:29*/


splay_this= e_node->right;
}else if(e_node->right==NULL){
/*30:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->left);}
else{e_node->left->parent= NULL;d->root= e_node->left;}

/*:30*/


splay_this= e_node->left;
}else{
/*31:*/


{dict_node_t*erL,*save_e_node= e_node;

for(erL= e_node->right;erL->left;erL= erL->left)
;
if(erL->right){
splay_this= erL->right;
}else{
if(erL->parent!=e_node)splay_this= erL->parent;
else splay_this= erL;
}
e_node= erL;
/*29:*/


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,e_node->right);}
else{e_node->right->parent= NULL;d->root= e_node->right;}

/*:29*/


e_node= save_e_node;


SETLINK(erL,LEFT,e_node->left);
if(e_node->right!=erL){SETLINK(erL,RIGHT,e_node->right);}


/*28:*/


ep= e_node->parent;
l= NILLINK;
if(ep){
if(ep->left==e_node)l= LEFT;
else if(ep->right==e_node)l= RIGHT;
else{errorif(1,"Bug");}
}

/*:28*/


if(ep){SETLINK(ep,l,erL);}
else{erL->parent= NULL;d->root= erL;}
}

/*:31*/


}
if(action)action(e_node->payload);
pool_free(d->pool,e_node);
d->size--;
here= splay_this;
}
}

/*:26*/


/*19:*/


if(here){
dict_node_t*p,*gp,*ggp= NULL;
int pl,gpl;
int ggpl= NILLINK;

dict_node_t*subtree[2];
do{
errorif(here==NULL,"|here| must not be NULL");
/*20:*/


gpl= pl= SELF;
p= here->parent;
if(p){
if(p->left==here)pl= LEFT;
else if(p->right==here)pl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
gp= p->parent;
if(gp){
if(gp->left==p)gpl= LEFT;
else if(gp->right==p)gpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
ggp= gp->parent;
if(ggp){
if(ggp->left==gp)ggpl= LEFT;
else if(ggp->right==gp)ggpl= RIGHT;
else{dict_doall(d,d->prn);errorif(1,"Bug");}
}
}
}else{
break;
}


/*:20*/


/*21:*/


if(gp){
/*22:*/


switch(gpl){
case LEFT:
switch(pl){
case LEFT:
subtree[0]= here->right;
subtree[1]= p->right;
SETLINK(here,RIGHT,p);
SETLINK(p,RIGHT,gp);
SETLINK(p,LEFT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,p);
SETLINK(here,RIGHT,gp);
SETLINK(p,RIGHT,subtree[0]);
SETLINK(gp,LEFT,subtree[1]);
break;
}
break;
case RIGHT:
switch(pl){
case LEFT:
subtree[0]= here->left;
subtree[1]= here->right;
SETLINK(here,LEFT,gp);
SETLINK(here,RIGHT,p);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,LEFT,subtree[1]);
break;
case RIGHT:
subtree[0]= p->left;
subtree[1]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,LEFT,gp);
SETLINK(gp,RIGHT,subtree[0]);
SETLINK(p,RIGHT,subtree[1]);
break;
}
break;
}
if(ggp){
if(ggpl!=NILLINK){
SETLINK(ggp,ggpl,here);
}else{
errorif(1,"Uninitialized great grandparent link.");
}
}else{here->parent= NULL;d->root= here;}


/*:22*/


}else{
/*23:*/


switch(pl){
case LEFT:
subtree[0]= here->right;
SETLINK(here,RIGHT,p);
SETLINK(p,LEFT,subtree[0]);
break;
case RIGHT:
subtree[0]= here->left;
SETLINK(here,LEFT,p);
SETLINK(p,RIGHT,subtree[0]);
break;
}
here->parent= NULL;
d->root= here;

/*:23*/


}

/*:21*/


}while(1);
}

/*:19*/


return payload;
}else return NULL;
}

/*:35*//*36:*/


void
dict_update_all(dict_t*d,void(*proc)(void*env2,void**payload_p),void*env1)
{
dict_node_t*here,*prev;
here= d->root;
prev= NULL;
while(here!=NULL){
if(here->left!=NULL&&prev==here->parent){
prev= here;
here= here->left;
}else if(here->right!=NULL&&prev!=here->right){
prev= here;
here= here->right;
}else{
proc(env1,&(here->payload));
prev= here;
here= here->parent;
}
}
}

/*:36*//*37:*/


size_t
dict_size(dict_t*d)
{
return d->size;
}

/*:37*//*38:*/



void visit(dict_node_t*n,void(*action)(void*));
void visit(dict_node_t*n,void(*action)(void*)){
if(n){
printf("(");
visit(n->left,action);
errorif(n->left!=NULL&&n->left->parent!=n,"Bug");
if(action)action(n->payload);
visit(n->right,action);
errorif(n->right!=NULL&&n->right->parent!=n,"Bug");
printf(")");
}
}

void
dict_doall(dict_t*d,void(*action)(void*)){
visit(d->root,action);
errorif(d->root!=NULL&&d->root->parent!=NULL,"Bug");
}

void
dict_show_node(dict_t*d,dict_node_t*h){
printf("%lx",(unsigned long)h);
if(h){
printf("={\"");
d->prn(h->payload);
printf("\" p=%lx l=%lx r=%lx}",
(unsigned long)h->parent,
(unsigned long)h->left,
(unsigned long)h->right);
}
}

/*:38*/


const char*dict_rcs_id= "$Id: dict.w,v 1.128 1998/10/16 20:43:58 neto Exp neto $";

/*:2*/
