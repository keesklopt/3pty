/*13:*/


extern const char*decluster_rcs_id;
/*19:*/


typedef struct{
int city[2];
length_t cost;
}decluster_edge_t;

typedef struct{
int n;
decluster_edge_t*edge;
}decluster_tree_t;

/*:19*/


/*37:*/


extern int decluster_discard_topology_tree;



/*:37*/


/*11:*/


decluster_tree_t*decluster_setup(int n);
void decluster_cleanup_tree(decluster_tree_t*T);
void decluster_cleanup(void);
length_t decluster_mst(tsp_instance_t*tsp_instance,decluster_tree_t*T);
length_t decluster_mst_custom(decluster_tree_t*T,int*from,
length_t*dist,length_t(*cost)(int,int));
int decluster_edge_cmp(const void*a,const void*b);
void decluster_preprocess(decluster_tree_t*T);
length_t decluster_d(int u,int v);

decluster_tree_t*decluster_topology_tree(void);
void decluster_print_tree(FILE*out,decluster_tree_t const*t,const char*name);


/*:11*/



/*:13*/
