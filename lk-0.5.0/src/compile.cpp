/* Generated automatically from compile.c.in by configure. */
/* This file lets the program LK know at what compiler and flags were used
 * to make the program binary.  The items between at signs `@' are
 * substituted for at configuration time.
 */

const char *compile_compile="gcc -c -DHAVE_CONFIG_H  -ansi -pedantic -Wall -Wpointer-arith -Wcast-qual -Wcast-align -Wwrite-strings -Wconversion -Wstrict-prototypes -Wmissing-prototypes -g -O2"
" (and possibly other -I flags)";
const char *compile_link="gcc ";
