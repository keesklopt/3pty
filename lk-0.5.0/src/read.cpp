#define matchword(STR) (0==strncmp(keyword,STR,strlen(STR) ) )  \

#define MIN(X,Y) ((X) > (Y) ?(Y) :(X) ) 
#define MAX(X,Y) ((X) <(Y) ?(Y) :(X) ) 
#define check_num_read(R,K)  \
errorif((R) !=(K) ,"Error: expected %d arguments, got %d",K,R)  \

#define truncate_to_zero(x) ((double) (int) (x) ) 
#define PI (3.14159265358979323846)  \

/*1:*/


#include <config.h> 
#include "lkconfig.h"
#include <stdio.h> 
#include <stddef.h> 
#include <stdlib.h> 
#include <math.h> 
#include "fixincludes.h"
/*7:*/


#include "memory.h"
#include "error.h"

/*:7*//*12:*/


#include <string.h> 

/*:12*//*14:*/


#include <ctype.h> 

/*:14*//*20:*/


#include "gb_flip.h"




/*:20*//*27:*/


#include "length.h"

/*:27*//*34:*/


#include <limits.h> 

/*:34*/



#include "read.h"

/*3:*/


static tsp_instance_t*p;

/*:3*//*43:*/


length_t(*cost)(const int,const int);
length_t(*pseudo_cost)(const int,const int);

/*:43*/


/*48:*/


#if defined(COST_USE_HYPOT)
#define my_hypot(A,B) (hypot((A),(B)))
#else
#define my_hypot(A,B) (sqrt((A)*(A)+(B)*(B)))
#endif

/*:48*//*57:*/


#if SIZEOF_INT==4
typedef int int32;
#elif SIZEOF_SHORT==4
typedef short int int32;
#elif SIZEOF_LONG==4
typedef long int32;
#else
#error "I need a 32 bit integer for consistent results with DSJ_RANDOM"
#endif

/*:57*/


/*17:*/


static const char*edge_weight_type_name[]= 
{"NO_EDGE_TYPE","EUC_2D","CEIL_2D","GEO","ATT","DSJ_RANDOM","EXPLICIT","RANDOM_EDGES"};

/*:17*//*23:*/


static const char*edge_weight_format_name[]= 
{"NO_EDGE_FORMAT","LOWER_DIAG_ROW","FULL_MATRIX","UPPER_ROW"};

/*:23*//*39:*/


static double dsj_random_factor= 1;
static int32 dsj_random_param= 1;

/*:39*/


/*45:*/


length_t cost_from_matrix(const int i,const int j);

length_t
cost_from_matrix(const int i,const int j)
{
return(length_t)p->edge_weights[i][j];
}

length_t cost_from_short_matrix(const int i,const int j);

length_t
cost_from_short_matrix(const int i,const int j)
{
return(length_t)p->short_edge_weights[i][j];
}


/*:45*//*46:*/


length_t cost_from_euc2d(const int i,const int j);
length_t
cost_from_euc2d(const int i,const int j)
{
coord_2d*coord_array= p->coord;
double xd= coord_array[i].x[0]-coord_array[j].x[0];
double yd= coord_array[i].x[1]-coord_array[j].x[1];
return(length_t)floor(0.5+my_hypot(xd,yd));
}

/*:46*//*47:*/


length_t cost_from_ceil2d(const int i,const int j);
length_t
cost_from_ceil2d(const int i,const int j)
{
coord_2d*coord_array= p->coord;
double xd= coord_array[i].x[0]-coord_array[j].x[0];
double yd= coord_array[i].x[1]-coord_array[j].x[1];
return(length_t)ceil(my_hypot(xd,yd));
}

/*:47*//*49:*/


length_t cost_from_euc2d_not_rounded(const int i,const int j);
length_t
cost_from_euc2d_not_rounded(const int i,const int j)
{
coord_2d*coord_array= p->coord;
double xd= coord_array[i].x[0]-coord_array[j].x[0];
double yd= coord_array[i].x[1]-coord_array[j].x[1];
return(length_t)my_hypot(xd,yd);
}

/*:49*//*52:*/


length_t pseudo_cost_from_euc2d(const int i,const int j);
length_t
pseudo_cost_from_euc2d(const int i,const int j)
{
coord_2d*coord_array= p->coord;
double xd= coord_array[i].x[0]-coord_array[j].x[0];
double yd= coord_array[i].x[1]-coord_array[j].x[1];
return(length_t)(xd*xd+yd*yd);
}

/*:52*//*53:*/


length_t cost_from_geo(const int i,const int j);
length_t
cost_from_geo(const int i,const int j)
{
coord_2d*coord_array= p->coord;
const double x1= coord_array[i].x[0],x2= coord_array[j].x[0];
const double y1= coord_array[i].x[1],y2= coord_array[j].x[1];
double degrees,minutes;
double latitude1,latitude2,longitude1,longitude2;
double q1,q2,q3;
const double radius= 6378.388;

degrees= truncate_to_zero(x1);
minutes= x1-degrees;
latitude1= (PI/180)*(degrees+minutes*(5.0/3.0));

degrees= truncate_to_zero(x2);
minutes= x2-degrees;
latitude2= (PI/180)*(degrees+minutes*(5.0/3.0));

degrees= truncate_to_zero(y1);
minutes= y1-degrees;
longitude1= (PI/180)*(degrees+minutes*(5.0/3.0));

degrees= truncate_to_zero(y2);
minutes= y2-degrees;
longitude2= (PI/180)*(degrees+minutes*(5.0/3.0));

q1= cos(longitude1-longitude2);
q2= cos(latitude1-latitude2);
q3= cos(latitude1+latitude2);
return(length_t)
truncate_to_zero(radius*acos(0.5*((1+q1)*q2-(1-q1)*q3))+1);
}


/*:53*//*54:*/


length_t cost_from_att(const int i,const int j);
length_t
cost_from_att(const int i,const int j)
{
coord_2d*coord_array= p->coord;
double xd= coord_array[i].x[0]-coord_array[j].x[0];
double yd= coord_array[i].x[1]-coord_array[j].x[1];
return(length_t)ceil(my_hypot(xd,yd)*0.31622776601683793319988935);
}

/*:54*//*55:*/



length_t cost_from_dsj_random(const int i,const int j);
length_t
cost_from_dsj_random(const int ii,const int jj)
{
const int32 i= ii,j= jj;
const int32 salt1= 0x12345672*(i+1)+1;
const int32 salt2= 0x12345672*(j+1)+1;
int32 x,y,z;

x= salt1&salt2;
y= salt1|salt2;
z= dsj_random_param;

x*= z;
y*= x;
z*= y;

z^= dsj_random_param;

x*= z;
y*= x;
z*= y;

x= ((salt1+salt2)^z)&0x7fffffff;
return(length_t)(x*dsj_random_factor);
}

/*:55*/


/*4:*/


tsp_instance_t*
switch_to(tsp_instance_t*new_problem)
{
tsp_instance_t*old_problem;
old_problem= p;
p= new_problem;
/*56:*/


dsj_random_factor= p->dsj_random_factor;
dsj_random_param= (int)p->dsj_random_param;

/*:56*//*58:*/


switch(p->edge_weight_type){
case EXPLICIT:
cost= cost_from_matrix;
pseudo_cost= cost_from_matrix;
break;
case EUC_2D:
{
extern int noround;
cost= noround?cost_from_euc2d_not_rounded:cost_from_euc2d;
}
pseudo_cost= pseudo_cost_from_euc2d;
break;
case CEIL_2D:
{
extern int noround;
cost= noround?cost_from_euc2d_not_rounded:cost_from_ceil2d;
}
pseudo_cost= pseudo_cost_from_euc2d;
break;
case GEO:
cost= cost_from_geo;
pseudo_cost= cost_from_geo;
break;
case ATT:
cost= cost_from_att;
pseudo_cost= cost_from_att;
break;
case DSJ_RANDOM:
cost= cost_from_dsj_random;
pseudo_cost= cost_from_dsj_random;
break;
case RANDOM_EDGES:
errorif(p->short_edge_weights==NULL,"RANDOM_EDGES specified but no SEED given");
cost= cost_from_short_matrix;
pseudo_cost= cost_from_short_matrix;
break;
case NO_EDGE_TYPE:
default:
errorif(1,"Switching to an instance with unknown edge type %d",
p->edge_weight_type);
}

/*:58*/


return old_problem;
}

/*:4*//*6:*/


tsp_instance_t*
read_tsp_file(FILE*in,FILE*debug,const int force_even_num)
{
p= new_of(tsp_instance_t);
/*10:*/


p->name= NULL;
p->comment= NULL;
p->n= 0;
p->scale= 1e6;

/*:10*//*19:*/


p->edge_weight_type= NO_EDGE_TYPE;

/*:19*//*25:*/


p->edge_weight_format= NO_EDGE_FORMAT;


/*:25*//*29:*/


p->edge_weights= NULL;


/*:29*//*33:*/


p->coord= NULL;
p->xmin= p->ymin= INT_MAX;
p->xmax= p->ymax= INT_MIN;

/*:33*//*38:*/


p->seed= 1;
p->dsj_random_factor= 1.0;
p->dsj_random_param= 1;

/*:38*//*42:*/


p->short_edge_weights= NULL;

/*:42*/


/*11:*/


{
#define MAX_KEYWORD_LEN (25)
#define MAX_LINE_LEN (200)
char keyword[MAX_KEYWORD_LEN],rest_of_line[MAX_LINE_LEN];
int more_input= 1,lineno= 0;
while(more_input){
char*colon;int r;
keyword[0]= 0;
r= fscanf(in," %s ",keyword);
if(r==EOF){more_input= 0;break;}
errorif(r!=1,"%d: Couldn't read the word following \"%s\". (r==%d)",
lineno+1,keyword,r);
if(NULL!=(colon= strchr(keyword,':')))*colon= '\0';
if(matchword("EOF")){
more_input= 0;
}else if(matchword("NAME")){
/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


p->name= dup_string(rest_of_line);
}else if(matchword("COMMENT")){
/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


p->comment= dup_string(rest_of_line);
}else if(matchword("TYPE")){
/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


errorif(strcmp(rest_of_line,"TSP"),"Can't read TSPLIB files of type %s.",rest_of_line);
}else if(matchword("DIMENSION")){
/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


p->input_n= p->n= atoi(rest_of_line);
}else if(matchword("DISPLAY_DATA_TYPE")){
/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/



}else if(matchword("EDGE_WEIGHT_TYPE")){
/*15:*/


/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


if(0==strcmp(rest_of_line,"EUC_2D")){
p->edge_weight_type= EUC_2D;
}else if(0==strcmp(rest_of_line,"CEIL_2D")){
p->edge_weight_type= CEIL_2D;
}else if(0==strcmp(rest_of_line,"GEO")){
p->edge_weight_type= GEO;
}else if(0==strcmp(rest_of_line,"ATT")){
p->edge_weight_type= ATT;
}else if(0==strcmp(rest_of_line,"DSJ_RANDOM")){
p->edge_weight_type= DSJ_RANDOM;
}else if(0==strcmp(rest_of_line,"EXPLICIT")){
p->edge_weight_type= EXPLICIT;
}else if(0==strcmp(rest_of_line,"RANDOM_EDGES")){
p->edge_weight_type= RANDOM_EDGES;
}else{
errorif(1,"%d: Apology: Unknown edge weight type \"%s\".",lineno,rest_of_line);
}

/*:15*/


}else if(matchword("EDGE_WEIGHT_FORMAT")){
/*21:*/


/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


if(0==strcmp(rest_of_line,"LOWER_DIAG_ROW")){
p->edge_weight_format= LOWER_DIAG_ROW;
}else if(0==strcmp(rest_of_line,"FULL_MATRIX")){
p->edge_weight_format= FULL_MATRIX;
}else if(0==strcmp(rest_of_line,"UPPER_ROW")){
p->edge_weight_format= UPPER_ROW;
}else{
errorif(1,"%d: Unknown edge weight format \"%s\".",lineno,rest_of_line);
}

/*:21*/


}else if(matchword("EDGE_WEIGHT_SECTION")){
/*26:*/


switch(p->edge_weight_format){
long int long_dummy;
case LOWER_DIAG_ROW:
{
int row,col;
p->edge_weights= new_arr_of(length_t*,p->input_n);

for(row= 0;row<p->input_n;row++){
p->edge_weights[row]= new_arr_of(length_t,p->input_n);
}
for(row= 0;row<p->input_n;row++){
for(col= 0;col<=row;col++){
int r= fscanf(in," %ld ",&long_dummy);
errorif(1!=r,
"Couldn't convert an edge weight: %d to %d.",row+1,col+1);
p->edge_weights[col][row]= p->edge_weights[row][col]= long_dummy;
}
}
}
break;
case FULL_MATRIX:
{
int row,col;
p->edge_weights= new_arr_of(length_t*,p->input_n);

for(row= 0;row<p->input_n;row++){
p->edge_weights[row]= new_arr_of(length_t,p->input_n);
}
for(row= 0;row<p->input_n;row++){
for(col= 0;col<p->input_n;col++){
int r= fscanf(in," %ld ",&long_dummy);
errorif(1!=r,
"Couldn't convert an edge weight: %d to %d.",row+1,col+1);
p->edge_weights[row][col]= long_dummy;
}
}
for(row= 0;row<p->input_n;row++){
for(col= 0;col<row;col++){
errorif(p->edge_weights[row][col]!=p->edge_weights[col][row],
"Asymmetric FULL_MATRIX:  (%d,%d) does not match (%d,%d)",
row,col,col,row);
}
}
}
break;
case UPPER_ROW:
{
int row,col;
p->edge_weights= new_arr_of(length_t*,p->input_n);

for(row= 0;row<p->input_n;row++){
p->edge_weights[row]= new_arr_of(length_t,p->input_n);
p->edge_weights[row][row]= 0;
}
for(row= 0;row<p->input_n;row++){

for(col= row+1;col<p->input_n;col++){
int r= fscanf(in," %ld ",&long_dummy);
errorif(1!=r,
"Couldn't convert an edge weight: %d to %d.",row+1,col+1);
p->edge_weights[col][row]= p->edge_weights[row][col]= long_dummy;
}
}
}
break;
default:break;
}

/*:26*/


}else if(matchword("NODE_COORD_SECTION")){
/*30:*/


{
int i,j,r;
p->coord= new_arr_of(coord_2d,p->input_n);
for(i= 0;i<p->input_n;i++){
r= fscanf(in," %d ",&j);
check_num_read(r,1);
r= fscanf(in," %lf %lf ",&p->coord[j-1].x[0],&p->coord[j-1].x[1]);
check_num_read(r,2);
p->xmin= MIN(p->xmin,p->coord[j-1].x[0]);
p->ymin= MIN(p->ymin,p->coord[j-1].x[1]);
p->xmax= MAX(p->xmax,p->coord[j-1].x[0]);
p->ymax= MAX(p->ymax,p->coord[j-1].x[1]);
lineno++;
}
}

/*:30*/


}else if(matchword("DISPLAY_DATA_SECTION")){
/*35:*/


{
int i;
double dummy;
for(i= 0;i<p->input_n;i++){
fscanf(in," %lf %lf %lf ",&dummy,&dummy,&dummy);
}
}

/*:35*/


}else if(matchword("SEED")){
/*36:*/


/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


p->seed= atol(rest_of_line);
switch(p->edge_weight_type){
case DSJ_RANDOM:
p->dsj_random_param= 1+104*p->seed;
break;
case RANDOM_EDGES:
/*40:*/


gb_init_rand(p->seed);
{int i,j;
p->short_edge_weights= new_arr_of(short*,p->input_n);
for(i= 0;i<p->input_n;i++){
p->short_edge_weights[i]= new_arr_of(short,p->input_n);
p->short_edge_weights[i][i]= 0;
}
for(i= 0;i<p->input_n;i++)
for(j= 0;j<i;j++)
p->short_edge_weights[i][j]= p->short_edge_weights[j][i]= 
(short)(1+gb_unif_rand(1000L));
if(p->input_n<=10){
printf("Cost matrix:\n");
for(i= 0;i<p->input_n;i++){
for(j= 0;j<p->input_n;j++)printf(" %4d",p->short_edge_weights[i][j]);
printf("\n");
}
}
}

/*:40*/


break;
default:
errorif(1,"SEED directive used for edge type %s",
edge_weight_type_name[p->edge_weight_type]);
}

/*:36*/


}else if(matchword("SCALE")){
/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


p->scale= atof(rest_of_line);
p->dsj_random_factor= p->scale/2147483648.0;
}else{
/*13:*/


{
int l;char*rcp;
if(colon==NULL){
int r= fscanf(in," : ");
errorif(r!=0,"%d: Missed the colon.",lineno);
}
rcp= fgets(rest_of_line,MAX_LINE_LEN,in);
errorif(NULL==rcp,
"%d: Couldn't read after the colon; truncated file?",lineno);
l= strlen(rest_of_line)-1;
if(l>=0&&rest_of_line[l]=='\n'){rest_of_line[l--]= '\0';lineno++;}
while(l>=0&&isspace(rest_of_line[l]))rest_of_line[l--]= '\0';
if(feof(in))more_input= 0;
}

/*:13*/


errorif(1,"%d: Don't know what the keyword %s is!",lineno,keyword);
}
}
/*56:*/


dsj_random_factor= p->dsj_random_factor;
dsj_random_param= (int)p->dsj_random_param;

/*:56*//*58:*/


switch(p->edge_weight_type){
case EXPLICIT:
cost= cost_from_matrix;
pseudo_cost= cost_from_matrix;
break;
case EUC_2D:
{
extern int noround;
cost= noround?cost_from_euc2d_not_rounded:cost_from_euc2d;
}
pseudo_cost= pseudo_cost_from_euc2d;
break;
case CEIL_2D:
{
extern int noround;
cost= noround?cost_from_euc2d_not_rounded:cost_from_ceil2d;
}
pseudo_cost= pseudo_cost_from_euc2d;
break;
case GEO:
cost= cost_from_geo;
pseudo_cost= cost_from_geo;
break;
case ATT:
cost= cost_from_att;
pseudo_cost= cost_from_att;
break;
case DSJ_RANDOM:
cost= cost_from_dsj_random;
pseudo_cost= cost_from_dsj_random;
break;
case RANDOM_EDGES:
errorif(p->short_edge_weights==NULL,"RANDOM_EDGES specified but no SEED given");
cost= cost_from_short_matrix;
pseudo_cost= cost_from_short_matrix;
break;
case NO_EDGE_TYPE:
default:
errorif(1,"Switching to an instance with unknown edge type %d",
p->edge_weight_type);
}

/*:58*/


}

/*:11*/


/*61:*/


if(p->n){
switch(p->edge_weight_type){
case EUC_2D:
case CEIL_2D:
case GEO:
errorif(p->edge_weight_format!=NO_EDGE_FORMAT,
"Edge weight format should be NO_EDGE_FORMAT, but is %s",
edge_weight_format_name[p->edge_weight_format]);
errorif(p->coord==NULL,"No coordinates were read");
break;
case EXPLICIT:
errorif(p->edge_weight_format==NO_EDGE_FORMAT,
"Edge weight shouldn't be NO_EDGE_FORMAT, but it is");
errorif(p->edge_weights==NULL,"No edge weights were read");
break;
case DSJ_RANDOM:
default:
break;
}
}

/*:61*/


/*59:*/


if(force_even_num&&(p->input_n%2)){
/*60:*/


p->n--;
switch(p->edge_weight_type){
case EUC_2D:
case CEIL_2D:
case GEO:
case ATT:
{int i,lex_last= 0;
for(i= 1;i<p->input_n;i++){
if(p->coord[i].x[0]> p->coord[lex_last].x[0]
||(p->coord[i].x[0]==p->coord[lex_last].x[0]
&&p->coord[i].x[1]>=p->coord[lex_last].x[1]))
lex_last= i;
}
{
coord_2d t= p->coord[p->n];
p->coord[p->n]= p->coord[lex_last];
p->coord[lex_last]= t;
}
}
default:break;
}

/*:60*/


}

/*:59*/


/*63:*/


if(debug){
fprintf(debug,"/N {%d} def        %% number of nodes\n",p->input_n);
if(p->input_n<8191)
fprintf(debug,"/xs N 1 add array def\n/ys N 1 add array def\n");
}


/*:63*//*64:*/


if(debug){
const double xdiff= ((double)p->xmax)-((double)p->xmin);
const double ydiff= ((double)p->ymax)-((double)p->ymin);
const double maxrange= MAX(xdiff,ydiff);
const double xoffset= xdiff<ydiff?(maxrange-xdiff)/2:0.0;
const double yoffset= ydiff<xdiff?(maxrange-ydiff)/2:0.0;
fprintf(debug,"/xmin {%f} def \n",p->xmin);
fprintf(debug,"/xmax {%f} def \n",p->xmax);
fprintf(debug,"/ymin {%f} def \n",p->ymin);
fprintf(debug,"/ymax {%f} def \n",p->ymax);
fprintf(debug,"/maxrange {%f} def \n",maxrange);
fprintf(debug,"/xoffset  {%f} def \n",xoffset);
fprintf(debug,"/yoffset  {%f} def \n",yoffset);
}

/*:64*//*65:*/


if(debug){int i;
for(i= 0;i<p->input_n;i++){
fprintf(debug,"%f %f ts\n",p->coord[i].x[0],p->coord[i].x[1]);
}
fflush(debug);
}

/*:65*/


return p;
}

/*:6*//*50:*/


double
cost_from_euc2d_raw(const int i,const int j)
{
coord_2d*coord_array= p->coord;
double xd= coord_array[i].x[0]-coord_array[j].x[0];
double yd= coord_array[i].x[1]-coord_array[j].x[1];
return my_hypot(xd,yd);
}

/*:50*//*66:*/


void
write_tsp_file(tsp_instance_t*tsp,FILE*out)
{
write_tsp_file_clip(tsp,out,0);
}

void write_tsp_file_clip(tsp_instance_t*tsp,FILE*out,int force_even_num)
{
write_tsp_file_clip_matrix(tsp,out,force_even_num,0);
}

void write_tsp_file_clip_matrix(
tsp_instance_t*tsp,FILE*out,int force_even_num,int force_matrix)
{
int n= tsp->input_n;
if((n%2)&&force_even_num)n--;

if(out){
fprintf(out,"NAME: %s\n",tsp->name);
fprintf(out,"TYPE: TSP\n");
fprintf(out,"COMMENT: %s%s\n",
(tsp->comment?tsp->comment:""),
(n<tsp->input_n?"| clip":""));
fprintf(out,"DIMENSION: %d\n",n);
if(force_matrix){
int format= UPPER_ROW;
fprintf(out,"EDGE_WEIGHT_TYPE: EXPLICIT\n");
/*69:*/


{int row,col;
tsp_instance_t*old_p= switch_to(tsp);
switch(format){
case LOWER_DIAG_ROW:
fprintf(out,"EDGE_WEIGHT_FORMAT: LOWER_DIAG_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION\n");
for(row= 0;row<n;row++){
for(col= 0;col<=row;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
case FULL_MATRIX:
fprintf(out,"EDGE_WEIGHT_FORMAT: FULL_MATRIX\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for(row= 0;row<n;row++){
for(col= 0;col<n;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
case UPPER_ROW:
fprintf(out,"EDGE_WEIGHT_FORMAT: UPPER_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for(row= 0;row<n;row++){
for(col= row+1;col<n;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
default:
errorif(1,"Unknown explicit format %d\n",format);
}
switch_to(old_p);
}


/*:69*/


}else{
switch(tsp->edge_weight_type){
case CEIL_2D:fprintf(out,"EDGE_WEIGHT_TYPE: CEIL_2D\n");
/*68:*/


fprintf(out,"NODE_COORD_SECTION\n");
{int i;
for(i= 0;i<n;i++){
fprintf(out,"%d %g %g\n",i+1,tsp->coord[i].x[0],tsp->coord[i].x[1]);
}
}


/*:68*/


break;
case EUC_2D:fprintf(out,"EDGE_WEIGHT_TYPE: EUC_2D\n");
/*68:*/


fprintf(out,"NODE_COORD_SECTION\n");
{int i;
for(i= 0;i<n;i++){
fprintf(out,"%d %g %g\n",i+1,tsp->coord[i].x[0],tsp->coord[i].x[1]);
}
}


/*:68*/


break;
case GEO:fprintf(out,"EDGE_WEIGHT_TYPE: GEO\n");
/*68:*/


fprintf(out,"NODE_COORD_SECTION\n");
{int i;
for(i= 0;i<n;i++){
fprintf(out,"%d %g %g\n",i+1,tsp->coord[i].x[0],tsp->coord[i].x[1]);
}
}


/*:68*/


break;
case ATT:fprintf(out,"EDGE_WEIGHT_TYPE: ATT\n");
/*68:*/


fprintf(out,"NODE_COORD_SECTION\n");
{int i;
for(i= 0;i<n;i++){
fprintf(out,"%d %g %g\n",i+1,tsp->coord[i].x[0],tsp->coord[i].x[1]);
}
}


/*:68*/


break;
case DSJ_RANDOM:fprintf(out,"EDGE_WEIGHT_TYPE: DSJ_RANDOM\n");
fprintf(out,"SCALE: %f\nSEED: %ld",tsp->scale,tsp->seed);
break;
case EXPLICIT:fprintf(out,"EDGE_WEIGHT_TYPE: EXPLICIT\n");
{int format= tsp->edge_weight_format;
/*69:*/


{int row,col;
tsp_instance_t*old_p= switch_to(tsp);
switch(format){
case LOWER_DIAG_ROW:
fprintf(out,"EDGE_WEIGHT_FORMAT: LOWER_DIAG_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION\n");
for(row= 0;row<n;row++){
for(col= 0;col<=row;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
case FULL_MATRIX:
fprintf(out,"EDGE_WEIGHT_FORMAT: FULL_MATRIX\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for(row= 0;row<n;row++){
for(col= 0;col<n;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
case UPPER_ROW:
fprintf(out,"EDGE_WEIGHT_FORMAT: UPPER_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for(row= 0;row<n;row++){
for(col= row+1;col<n;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
default:
errorif(1,"Unknown explicit format %d\n",format);
}
switch_to(old_p);
}


/*:69*/


}
break;
case RANDOM_EDGES:fprintf(out,"EDGE_WEIGHT_TYPE: EXPLICIT\n");
{int format= UPPER_ROW;
/*69:*/


{int row,col;
tsp_instance_t*old_p= switch_to(tsp);
switch(format){
case LOWER_DIAG_ROW:
fprintf(out,"EDGE_WEIGHT_FORMAT: LOWER_DIAG_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION\n");
for(row= 0;row<n;row++){
for(col= 0;col<=row;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
case FULL_MATRIX:
fprintf(out,"EDGE_WEIGHT_FORMAT: FULL_MATRIX\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for(row= 0;row<n;row++){
for(col= 0;col<n;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
case UPPER_ROW:
fprintf(out,"EDGE_WEIGHT_FORMAT: UPPER_ROW\n");
fprintf(out,"EDGE_WEIGHT_SECTION:\n");
for(row= 0;row<n;row++){
for(col= row+1;col<n;col++){
fprintf(out," %ld",(long)cost(row,col));
}
fprintf(out,"\n");
}
break;
default:
errorif(1,"Unknown explicit format %d\n",format);
}
switch_to(old_p);
}


/*:69*/


}
break;
case NO_EDGE_TYPE:
default:
errorif(1,"No edge type specified by instance: have %d instead",
tsp->edge_weight_type);
}
}
fprintf(out,"EOF\n");
}
}

/*:66*/


const char*read_rcs_id= "$Id: read.w,v 1.143 1998/12/05 22:37:29 neto Exp neto $";


/*:1*/
