#define new_box() ((E2_box_t*) pool_alloc(box_pool) ) 
#define free_box(P) pool_free(box_pool,(P) )  \
 \

#define new_node() ((E2_node_t*) (pool_alloc(node_pool) ) ) 
#define free_node(P) pool_free(node_pool,(P) )  \

#define val(a) (coord[perm[(a) ]].x[cutdimen]) 
#define valx(a) (coord[perm[(a) ]].x[0]) 
#define valy(a) (coord[perm[(a) ]].x[1]) 
#define med3(a,b,c) (val(a) <val(b) ?(val(b) <val(c) ?b:val(a) <val(c) ?(c) :(a) )  \
:(val(b) > val(c) ?b:val(a) > val(c) ?(c) :(a) ) )  \

#define swapint(J,K) {int t= J;J= K;K= t;} \
 \

#define MIN(x,y) ((x) <(y) ?(x) :(y) ) 
#define MAX(x,y) ((x) > (y) ?(x) :(y) ) 
#define verb(a) if(verbose>=(a) ) 
/*1:*/


#include <config.h> 
#include "lkconfig.h"
/*30:*/


#include <stddef.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include "fixincludes.h"


/*:30*//*59:*/


#include <stdio.h> 

/*:59*//*82:*/


#include <math.h> 

/*:82*/


/*7:*/


#include "length.h"
#include "read.h"

/*:7*/


/*3:*/


#include "kdtree.h"

/*:3*//*17:*/


#include "pool.h"


/*:17*//*28:*/


#include "error.h"
#include "memory.h"

/*:28*//*72:*/


#include "pq.h"

/*:72*//*104:*/


#include "error.h"
#include "memory.h"

/*:104*/



/*15:*/


typedef struct{
double xmin,xmax,ymin,ymax;
}E2_box_t;

/*:15*/


/*8:*/


typedef struct E2_node_s{
/*12:*/


struct E2_node_s*parent;


/*:12*//*14:*/


E2_box_t*bbox;

/*:14*/


union{
struct{
int cutdimen;
double cutvalue;
struct E2_node_s*lo_child,*hi_child;
/*21:*/


struct E2_node_s*eq_child;

/*:21*/


}i;
struct{
int lo,hi;
/*54:*/


int hi_all;

/*:54*/


}e;
}f;
char is_bucket;
/*13:*/


#if !defined(KD_NO_HIDDEN_BIT)
char hidden;
#endif


/*:13*/


}E2_node_t;


/*:8*/


/*25:*/


static pool_t*node_pool,*box_pool;

/*:25*//*27:*/


static int n;

/*:27*//*32:*/


static E2_node_t*E2_root;
static coord_2d*coord;

/*:32*//*36:*/


static E2_node_t**E2_point_to_bucket;

/*:36*//*70:*/


static int quadrant_mask;


/*:70*//*71:*/


static int E2_nn_seed,E2_nn_incumbent,E2_nn_bin_want_size;
static double E2_nn_dist,E2_nn_dist_sq,E2_nn_seed_x,E2_nn_seed_y;
static pq_t*E2_nn_bin;
static kd_bin_t*E2_nn_bin_work= NULL;

/*:71*//*84:*/


static int E2_nn_fill_bin= 0;

/*:84*//*96:*/


static double E2_strict_upper_bound;

/*:96*//*109:*/


extern int verbose;

/*:109*/


/*9:*/


int*perm;


/*:9*//*10:*/


int kd_bucket_cutoff= 10;

/*:10*//*18:*/


int kd_bbox_skip= 3;

/*:18*/


/*33:*/


static E2_node_t*
E2_build_helper(E2_node_t*parent,int flat_dimens,int level,
int lo,int hi,
double xmin,double xmax,double ymin,double ymax){
E2_node_t*node= new_node();
node->parent= parent;
#if !defined(KD_NO_HIDDEN_BIT)
node->hidden= lo>=hi;
#endif
if((++level%kd_bbox_skip)==0){
node->bbox= new_box();
node->bbox->xmin= xmin;
node->bbox->xmax= xmax;
node->bbox->ymin= ymin;
node->bbox->ymax= ymax;
/*108:*/


#ifdef KD_CHECK_BBOX
verb(1000)printf("lo %d hi %d\n",lo,hi);
if(lo<hi)
{double xl,xh,yl,yh;int i;
xl= xmax;xh= xmin;yl= ymax;yh= ymin;
for(i= lo;i<hi;i++){
verb(1000)printf("xl %f i %d perm[i] %d %f %f xl\n",xl,i,perm[i],
coord[perm[i]].x[0],coord[perm[i]].x[1]);
xl= MIN(xl,valx(i));
yl= MIN(yl,valy(i));
xh= MAX(xh,valx(i));
yh= MAX(yh,valy(i));
}
errorif(xl!=xmin,"xl %f!= xmin %f",xl,xmin);
errorif(yl!=ymin,"yl %f!= ymin %f",yl,ymin);
errorif(xh!=xmax,"xh %f!= xmax %f",xh,xmax);
errorif(yh!=ymax,"yh %f!= ymax %f",yh,ymax);
}
#endif

/*:108*/


}else{
node->bbox= NULL;
}
if(hi-lo<=kd_bucket_cutoff||flat_dimens==0x03){
node->is_bucket= 1;
node->f.e.lo= lo;
node->f.e.hi= hi;
/*55:*/


node->f.e.hi_all= hi;

/*:55*/


/*35:*/


{int i;
for(i= lo;i<hi;i++)E2_point_to_bucket[perm[i]]= node;
}

/*:35*/


}else{
node->is_bucket= 0;
/*39:*/


{int cutdimen= 0;
switch(flat_dimens){
case 0:
if(xmax-xmin> ymax-ymin)cutdimen= 0;
else cutdimen= 1;
break;
case 1:
cutdimen= 1;
break;
case 2:
cutdimen= 0;
break;
case 3:
default:
errorif(1,"Invalid flat_dimens: %d",flat_dimens);
}
/*40:*/


{int p;
int a,b,c,d;
double exl,exh,lxl,lxh,gxl,gxh;
double eyl,eyh,lyl,lyh,gyl,gyh;
/*41:*/


p= (lo+hi)/2;
if(hi-lo> 7){
int p1= lo,pn= hi-1;
if(hi-lo> 40){
int s= (hi-lo)/8;
p1= med3(p1,p1+s,p1+s+s);
p= med3(p-s,p,p+s);
pn= med3(pn-s-s,pn-s,pn);
}
p= med3(p1,p,pn);
}

/*:41*/


/*42:*/


/*43:*/


exl= lxl= gxl= xmax;
exh= lxh= gxh= xmin;
eyl= lyl= gyl= ymax;
eyh= lyh= gyh= ymin;

/*:43*/


a= b= lo;c= d= hi-1;
{double v= val(p),diff;
node->f.i.cutdimen= cutdimen;
node->f.i.cutvalue= v;
for(;;){
while(b<=c&&(diff= val(b)-v)<=0.0){
if(diff==0.0){
/*44:*/


exl= MIN(exl,valx(b));
exh= MAX(exh,valx(b));
eyl= MIN(eyl,valy(b));
eyh= MAX(eyh,valy(b));

/*:44*/


swapint(perm[a],perm[b]);
a++;
}else{
/*45:*/


lxl= MIN(lxl,valx(b));
lxh= MAX(lxh,valx(b));
lyl= MIN(lyl,valy(b));
lyh= MAX(lyh,valy(b));

/*:45*/


}
b++;
}
while(c>=b&&(diff= val(c)-v)>=0.0){
if(diff==0.0){
/*46:*/


exl= MIN(exl,valx(c));
exh= MAX(exh,valx(c));
eyl= MIN(eyl,valy(c));
eyh= MAX(eyh,valy(c));

/*:46*/


swapint(perm[d],perm[c]);
d--;
}else{
/*47:*/


gxl= MIN(gxl,valx(c));
gxh= MAX(gxh,valx(c));
gyl= MIN(gyl,valy(c));
gyh= MAX(gyh,valy(c));

/*:47*/


}
c--;
}
if(b> c)break;
swapint(perm[b],perm[c]);
/*45:*/


lxl= MIN(lxl,valx(b));
lxh= MAX(lxh,valx(b));
lyl= MIN(lyl,valy(b));
lyh= MAX(lyh,valy(b));

/*:45*/


/*47:*/


gxl= MIN(gxl,valx(c));
gxh= MAX(gxh,valx(c));
gyl= MIN(gyl,valy(c));
gyh= MAX(gyh,valy(c));

/*:47*/


b++;c--;
}
/*48:*/


{
int s,l,h;
s= MIN(a-lo,b-a);
for(l= lo,h= b-s;s;s--){swapint(perm[l],perm[h]);l++;h++;}
s= MIN(d-c,hi-1-d);
for(l= b,h= hi-s;s;s--){swapint(perm[l],perm[h]);l++;h++;}
}

/*:48*/


}
/*103:*/


#ifdef KD_CHECK_PARTITIONING

{int i;double v= node->f.i.cutvalue;
verb(1000)printf("\nDimension %d\n",cutdimen);
for(i= lo;i<hi;i++){
if(i==lo)verb(1000)printf("Checking lesser: %d %d\n",lo,lo+b-a);
if(i==lo+b-a)verb(1000)printf("Checking equal: %d %d\n",lo+b-a,hi-(d-c));
if(i==hi-(d-c))verb(1000)printf("Checking greater: %d %d\n",hi-(d-c),hi);
verb(1000)printf("%d (%.0f,%.0f)\t %.0f %.0f\n",i,valx(i),valy(i),val(i),v);fflush(stdout);
}
for(i= lo;i<lo+b-a;i++){
errorif(val(i)>=v,"Not lesser at %d",i);
}
for(i= lo+b-a;i<hi-(d-c);i++){
errorif(val(i)!=v,"Not equal at %d",i);
}
for(i= hi-(d-c);i<hi;i++){
errorif(val(i)<=v,"Not greater at %d",i);
}
}
#endif

/*:103*/



/*:42*/


/*49:*/


#if defined(KD_BUILD_SMALLEST_SEGMENT_FIRST)
{int l= b-a,m= hi-(d-c)-lo+b-a,h= d-c;
if(l<=m){
if(m<=h){/*50:*/


node->f.i.lo_child= 
E2_build_helper(node,flat_dimens,level,lo,lo+b-a,lxl,lxh,lyl,lyh);

/*:50*/

/*52:*/


node->f.i.eq_child= 
E2_build_helper(node,flat_dimens|(cutdimen+1),level,lo+b-a,hi-(d-c),exl,exh,eyl,eyh);


/*:52*/


/*51:*/


node->f.i.hi_child= 
E2_build_helper(node,flat_dimens,level,hi-(d-c),hi,gxl,gxh,gyl,gyh);

/*:51*/

}
else if(l<=h){/*50:*/


node->f.i.lo_child= 
E2_build_helper(node,flat_dimens,level,lo,lo+b-a,lxl,lxh,lyl,lyh);

/*:50*/

/*51:*/


node->f.i.hi_child= 
E2_build_helper(node,flat_dimens,level,hi-(d-c),hi,gxl,gxh,gyl,gyh);

/*:51*/

/*52:*/


node->f.i.eq_child= 
E2_build_helper(node,flat_dimens|(cutdimen+1),level,lo+b-a,hi-(d-c),exl,exh,eyl,eyh);


/*:52*/

}
else{/*51:*/


node->f.i.hi_child= 
E2_build_helper(node,flat_dimens,level,hi-(d-c),hi,gxl,gxh,gyl,gyh);

/*:51*/

/*50:*/


node->f.i.lo_child= 
E2_build_helper(node,flat_dimens,level,lo,lo+b-a,lxl,lxh,lyl,lyh);

/*:50*/

/*52:*/


node->f.i.eq_child= 
E2_build_helper(node,flat_dimens|(cutdimen+1),level,lo+b-a,hi-(d-c),exl,exh,eyl,eyh);


/*:52*/

}
}else{
if(l<=h){/*52:*/


node->f.i.eq_child= 
E2_build_helper(node,flat_dimens|(cutdimen+1),level,lo+b-a,hi-(d-c),exl,exh,eyl,eyh);


/*:52*/

/*50:*/


node->f.i.lo_child= 
E2_build_helper(node,flat_dimens,level,lo,lo+b-a,lxl,lxh,lyl,lyh);

/*:50*/

/*51:*/


node->f.i.hi_child= 
E2_build_helper(node,flat_dimens,level,hi-(d-c),hi,gxl,gxh,gyl,gyh);

/*:51*/

}
else if(m<=h){/*52:*/


node->f.i.eq_child= 
E2_build_helper(node,flat_dimens|(cutdimen+1),level,lo+b-a,hi-(d-c),exl,exh,eyl,eyh);


/*:52*/

/*51:*/


node->f.i.hi_child= 
E2_build_helper(node,flat_dimens,level,hi-(d-c),hi,gxl,gxh,gyl,gyh);

/*:51*/

/*50:*/


node->f.i.lo_child= 
E2_build_helper(node,flat_dimens,level,lo,lo+b-a,lxl,lxh,lyl,lyh);

/*:50*/

}
else{/*51:*/


node->f.i.hi_child= 
E2_build_helper(node,flat_dimens,level,hi-(d-c),hi,gxl,gxh,gyl,gyh);

/*:51*/

/*52:*/


node->f.i.eq_child= 
E2_build_helper(node,flat_dimens|(cutdimen+1),level,lo+b-a,hi-(d-c),exl,exh,eyl,eyh);


/*:52*/

/*50:*/


node->f.i.lo_child= 
E2_build_helper(node,flat_dimens,level,lo,lo+b-a,lxl,lxh,lyl,lyh);

/*:50*/

}
}
}
#else
/*50:*/


node->f.i.lo_child= 
E2_build_helper(node,flat_dimens,level,lo,lo+b-a,lxl,lxh,lyl,lyh);

/*:50*/


/*52:*/


node->f.i.eq_child= 
E2_build_helper(node,flat_dimens|(cutdimen+1),level,lo+b-a,hi-(d-c),exl,exh,eyl,eyh);


/*:52*/


/*51:*/


node->f.i.hi_child= 
E2_build_helper(node,flat_dimens,level,hi-(d-c),hi,gxl,gxh,gyl,gyh);

/*:51*/


#endif

/*:49*/


}

/*:40*/


}

/*:39*/


}
return node;
}

/*:33*//*66:*/


static void
E2_hide_all_helper(E2_node_t*p){
#if !defined(KD_NO_HIDDEN_BIT)
p->hidden= 1;
#endif
if(p->is_bucket)p->f.e.hi= p->f.e.lo;
else{
#if !defined(KD_NO_HIDDEN_BIT)
if(!p->f.i.lo_child->hidden)E2_hide_all_helper(p->f.i.lo_child);
if(!p->f.i.eq_child->hidden)E2_hide_all_helper(p->f.i.eq_child);
if(!p->f.i.hi_child->hidden)E2_hide_all_helper(p->f.i.hi_child);
#else
E2_hide_all_helper(p->f.i.lo_child);
E2_hide_all_helper(p->f.i.eq_child);
E2_hide_all_helper(p->f.i.hi_child);
#endif
}
}

/*:66*//*68:*/


static void
E2_unhide_all_helper(E2_node_t*p){
if(p->is_bucket){
p->f.e.hi= p->f.e.hi_all;
#if !defined(KD_NO_HIDDEN_BIT)
p->hidden= p->f.e.lo>=p->f.e.hi;
#endif
}else{
#if !defined(KD_NO_HIDDEN_BIT)
p->hidden= 0;
#endif
E2_unhide_all_helper(p->f.i.lo_child);
E2_unhide_all_helper(p->f.i.eq_child);
E2_unhide_all_helper(p->f.i.hi_child);
}
}

/*:68*//*78:*/


static void
E2_rnn(E2_node_t*p)
{
if(p->is_bucket){
/*79:*/


{int i,hi= p->f.e.hi;
for(i= p->f.e.lo;i<hi;i++){
int pi= perm[i];
/*80:*/


{
const double
diff_x= E2_nn_seed_x-coord[pi].x[0],
diff_y= E2_nn_seed_y-coord[pi].x[1];
/*113:*/


#if KD_ALLOW_VERBOSE
if(verbose>=1500){
printf(" pi=%d qmask=%d x0=%.0f y0=%.0f x1=%.0f y1=%.0f dx=%.0f dy=%.0f q=%d mask=%d\n",
pi,quadrant_mask,E2_nn_seed_x,E2_nn_seed_y,coord[pi].x[0],coord[pi].x[1],
diff_x,diff_y,E2_quadrant(diff_x,diff_y),E2_quadrant_mask(diff_x,diff_y));
}
#endif

/*:113*/


if(quadrant_mask&E2_quadrant_mask(diff_x,diff_y)){

const double dist_sq= diff_x*diff_x+diff_y*diff_y;
/*110:*/


#if 1 
if(verbose>=2000){
printf("  city %5d (%7.0f,%7.0f) is dist %10.3f\n",
pi,coord[pi].x[0],coord[pi].x[1],sqrt(dist_sq));
}
#endif


/*:110*/


if(dist_sq<E2_nn_dist_sq){
/*111:*/


#if KD_ALLOW_VERBOSE
if(verbose>=1000){
printf("    new champion\n");
}
#endif

/*:111*/


/*83:*/


if(E2_nn_fill_bin){
kd_bin_t*pi_particulars;
if(pq_size(E2_nn_bin)==E2_nn_bin_want_size){
pi_particulars= (kd_bin_t *)pq_delete_min(E2_nn_bin);
}else{
pi_particulars= E2_nn_bin_work+pq_size(E2_nn_bin);
}
pi_particulars->cost_squared= dist_sq;
pi_particulars->city= pi;
pq_insert(E2_nn_bin,pi_particulars);
if(pq_size(E2_nn_bin)==E2_nn_bin_want_size){
E2_nn_dist_sq= ((kd_bin_t*)pq_min(E2_nn_bin))->cost_squared;
E2_nn_dist= sqrt(E2_nn_dist_sq);
}
}else{
E2_nn_dist= sqrt(dist_sq);
E2_nn_dist_sq= dist_sq;
E2_nn_incumbent= pi;
}

/*:83*/


}
}
}

/*:80*/


}
}


/*:79*/


}else{
/*88:*/


#if !defined(KD_NO_HIDDEN_BIT) && !defined(KD_NO_HIDDEN_RNN_TEST)
#define recurse_if_not_hidden(P) ((P)->hidden || (E2_rnn(P),42))
#else
#define recurse_if_not_hidden(P) (E2_rnn(P),42)
#endif
{
const int cutdimen= p->f.i.cutdimen;
const double seed_coord= cutdimen==0?E2_nn_seed_x:E2_nn_seed_y,
diff= seed_coord-p->f.i.cutvalue;
if(diff<0){
recurse_if_not_hidden(p->f.i.lo_child);
if(E2_nn_dist>=-diff&&((cutdimen?0x12:0x06)&quadrant_mask)){
recurse_if_not_hidden(p->f.i.eq_child);
recurse_if_not_hidden(p->f.i.hi_child);
}
}else if(diff> 0){
recurse_if_not_hidden(p->f.i.hi_child);
if(E2_nn_dist>=diff&&((cutdimen?0x0c:0x18)&quadrant_mask)){
recurse_if_not_hidden(p->f.i.eq_child);
recurse_if_not_hidden(p->f.i.lo_child);
}
}else{
recurse_if_not_hidden(p->f.i.eq_child);
recurse_if_not_hidden(p->f.i.lo_child);
recurse_if_not_hidden(p->f.i.hi_child);
}
}

/*:88*/


}
}


/*:78*/


/*6:*/


int
E2_supports(tsp_instance_t*tsp)
{
switch(tsp->edge_weight_type){
case EUC_2D:
case CEIL_2D:
case ATT:
return 1;
default:
return 0;
}
}


/*:6*//*23:*/


void
E2_create(tsp_instance_t*tsp)
{
errorif(!E2_supports(tsp),"2-d trees may not be used for this instance");
/*24:*/


node_pool= pool_create(sizeof(E2_node_t),500);
box_pool= pool_create(sizeof(E2_box_t),500);

/*:24*//*26:*/


n= tsp->n;
perm= new_arr_of(int,n);
{int i;for(i= 0;i<n;i++)perm[i]= i;}

/*:26*//*37:*/


E2_point_to_bucket= new_arr_of(E2_node_t*,n);

/*:37*//*75:*/


E2_nn_bin= pq_create(kd_bin_cmp_decreasing);

/*:75*/


/*31:*/


coord= tsp->coord;
errorif(n<=0,"Need at least one point; instance has %d points",n);
E2_root= E2_build_helper(NULL,0,0,0,n,
tsp->xmin,tsp->xmax,tsp->ymin,tsp->ymax);

/*:31*//*95:*/


{double xrange= tsp->xmax-tsp->xmin,yrange= tsp->ymax-tsp->ymin;
E2_strict_upper_bound= 2.0*(xrange*xrange+yrange*yrange)+1.0;
}

/*:95*/


}

/*:23*//*29:*/


void
E2_destroy(void){
pool_destroy(node_pool);
pool_destroy(box_pool);
box_pool= node_pool= NULL;
free_mem(perm);
/*38:*/


free_mem(E2_point_to_bucket);

/*:38*//*77:*/


pq_destroy(E2_nn_bin);

/*:77*/


}

/*:29*//*56:*/


void
E2_hide(int c){
int ci;
E2_node_t*node;
errorif(c<0||c>=n,"Invalid city %d to hide",c);
node= E2_point_to_bucket[c];
/*57:*/


{int hi_all= node->f.e.hi_all;
for(ci= node->f.e.lo;ci<hi_all;ci++){
if(perm[ci]==c)break;
}
errorif(ci==hi_all,"Point %d not found in its bucket",c);
}

/*:57*/


/*58:*/


{int t,hi= node->f.e.hi;
if(ci<hi){
hi= --node->f.e.hi;
t= perm[ci];perm[ci]= perm[hi];perm[hi]= t;
}else{
fprintf(stderr,"Hiding hidden city %d at perm[%d]\n",c,ci);
}
}

/*:58*/


/*60:*/


#if !defined(KD_NO_HIDDEN_BIT)
if(node->f.e.lo==node->f.e.hi&&!node->hidden){
do{
node->hidden= 1;

node= node->parent;
}while(node
&&!node->hidden
&&node->f.i.lo_child->hidden
&&node->f.i.eq_child->hidden
&&node->f.i.hi_child->hidden);
}
#endif

/*:60*/


}


/*:56*//*61:*/


void
E2_unhide(int c){
int ci;
E2_node_t*node= E2_point_to_bucket[c];
/*57:*/


{int hi_all= node->f.e.hi_all;
for(ci= node->f.e.lo;ci<hi_all;ci++){
if(perm[ci]==c)break;
}
errorif(ci==hi_all,"Point %d not found in its bucket",c);
}

/*:57*/


/*62:*/


{int t,hi= node->f.e.hi;
if(ci>=hi){
t= perm[ci];perm[ci]= perm[hi];perm[hi]= t;
node->f.e.hi++;
}else{
fprintf(stderr,"Unhiding unhidden city %d at perm[%d]\n",c,ci);
}
}




/*:62*/


/*63:*/


#if !defined(KD_NO_HIDDEN_BIT)
while(node&&node->hidden){
node->hidden= 0;
node= node->parent;
}
#endif

/*:63*/


}


/*:61*//*65:*/


void
E2_hide_all(void){
#if !defined(KD_NO_HIDDEN_BIT)
if(E2_root&&!E2_root->hidden)E2_hide_all_helper(E2_root);
#else
if(E2_root)E2_hide_all_helper(E2_root);
#endif
}

/*:65*//*67:*/


void
E2_unhide_all(void){
if(E2_root)E2_unhide_all_helper(E2_root);
}

/*:67*//*76:*/


int
kd_bin_cmp_increasing(const void*a,const void*b)
{
double da= ((const kd_bin_t*)a)->cost_squared;
double db= ((const kd_bin_t*)b)->cost_squared;
if(da> db)return 1;
if(da<db)return-1;
return((const kd_bin_t*)a)->city
-((const kd_bin_t*)b)->city;
}

int
kd_bin_cmp_decreasing(const void*a,const void*b)
{
double da= ((const kd_bin_t*)a)->cost_squared;
double db= ((const kd_bin_t*)b)->cost_squared;
if(da<db)return 1;
if(da> db)return-1;
return((const kd_bin_t*)a)->city
-((const kd_bin_t*)b)->city;
}

/*:76*//*89:*/


int E2_nn_func(int c){return E2_nn_quadrant(c,0x1f);}

int
E2_nn_quadrant(int c,const int mask)
{
E2_node_t*node= E2_point_to_bucket[c];
/*91:*/


quadrant_mask= mask;
E2_nn_seed= c;
E2_nn_seed_x= coord[c].x[0];
E2_nn_seed_y= coord[c].x[1];
E2_nn_incumbent= -1;
if(mask&(~1)){
E2_nn_dist= E2_nn_dist_sq= E2_strict_upper_bound;
}else{
E2_nn_dist= E2_nn_dist_sq= 1e-5;
}

/*:91*/


E2_nn_fill_bin= 0;
/*92:*/


/*97:*/


{
int i,hi= node->f.e.hi;
for(i= node->f.e.lo;i<hi;i++){
int pi= perm[i];
if(pi!=E2_nn_seed){
/*80:*/


{
const double
diff_x= E2_nn_seed_x-coord[pi].x[0],
diff_y= E2_nn_seed_y-coord[pi].x[1];
/*113:*/


#if KD_ALLOW_VERBOSE
if(verbose>=1500){
printf(" pi=%d qmask=%d x0=%.0f y0=%.0f x1=%.0f y1=%.0f dx=%.0f dy=%.0f q=%d mask=%d\n",
pi,quadrant_mask,E2_nn_seed_x,E2_nn_seed_y,coord[pi].x[0],coord[pi].x[1],
diff_x,diff_y,E2_quadrant(diff_x,diff_y),E2_quadrant_mask(diff_x,diff_y));
}
#endif

/*:113*/


if(quadrant_mask&E2_quadrant_mask(diff_x,diff_y)){

const double dist_sq= diff_x*diff_x+diff_y*diff_y;
/*110:*/


#if 1 
if(verbose>=2000){
printf("  city %5d (%7.0f,%7.0f) is dist %10.3f\n",
pi,coord[pi].x[0],coord[pi].x[1],sqrt(dist_sq));
}
#endif


/*:110*/


if(dist_sq<E2_nn_dist_sq){
/*111:*/


#if KD_ALLOW_VERBOSE
if(verbose>=1000){
printf("    new champion\n");
}
#endif

/*:111*/


/*83:*/


if(E2_nn_fill_bin){
kd_bin_t*pi_particulars;
if(pq_size(E2_nn_bin)==E2_nn_bin_want_size){
pi_particulars= (kd_bin_t *)pq_delete_min(E2_nn_bin);
}else{
pi_particulars= E2_nn_bin_work+pq_size(E2_nn_bin);
}
pi_particulars->cost_squared= dist_sq;
pi_particulars->city= pi;
pq_insert(E2_nn_bin,pi_particulars);
if(pq_size(E2_nn_bin)==E2_nn_bin_want_size){
E2_nn_dist_sq= ((kd_bin_t*)pq_min(E2_nn_bin))->cost_squared;
E2_nn_dist= sqrt(E2_nn_dist_sq);
}
}else{
E2_nn_dist= sqrt(dist_sq);
E2_nn_dist_sq= dist_sq;
E2_nn_incumbent= pi;
}

/*:83*/


}
}
}

/*:80*/


}
}
}


/*:97*/


do{
/*98:*/


{E2_box_t*b= node->bbox;
if(b
&&(b->xmin<=E2_nn_seed_x-E2_nn_dist)
&&(b->xmax>=E2_nn_seed_x+E2_nn_dist)
&&(b->ymin<=E2_nn_seed_y-E2_nn_dist)
&&(b->ymax>=E2_nn_seed_y+E2_nn_dist)){
/*112:*/


#if KD_ALLOW_VERBOSE
if(verbose>=500){
printf("     rnn break: seed=(%0.0f,%0.0f) dist=%f\n",
E2_nn_seed_x,E2_nn_seed_y,E2_nn_dist);
printf("     seedbb=(%0.0f,%0.0f,%0.0f,%0.0f) bb=(%0.0f,%0.0f,%0.0f,%0.0f)\n",
E2_nn_seed_x-E2_nn_dist,
E2_nn_seed_y-E2_nn_dist,
E2_nn_seed_x+E2_nn_dist,
E2_nn_seed_y+E2_nn_dist,
b->xmin,
b->ymin,
b->xmax,
b->ymax);
}
#endif


/*:112*/


break;
}
}

/*:98*/


/*99:*/



#if !defined(KD_NO_HIDDEN_BIT)
#define recurse_if_not_last_or_hidden(P) ((P)==last||(E2_rnn(P),42))
#else
#define recurse_if_not_last_or_hidden(P) ((P)==last||(P)->hidden||(E2_rnn(P),42))
#endif

{E2_node_t*last;
for(last= node,node= node->parent;node;last= node,node= node->parent){
E2_node_t*l= node->f.i.lo_child,*e= node->f.i.eq_child,*h= node->f.i.hi_child;
if((node->f.i.cutdimen==0&&E2_nn_seed_x<node->f.i.cutvalue)
||(node->f.i.cutdimen==1&&E2_nn_seed_y<node->f.i.cutvalue)){
recurse_if_not_last_or_hidden(l);
recurse_if_not_last_or_hidden(e);
recurse_if_not_last_or_hidden(h);
}else{
recurse_if_not_last_or_hidden(h);
recurse_if_not_last_or_hidden(e);
recurse_if_not_last_or_hidden(l);
}
/*98:*/


{E2_box_t*b= node->bbox;
if(b
&&(b->xmin<=E2_nn_seed_x-E2_nn_dist)
&&(b->xmax>=E2_nn_seed_x+E2_nn_dist)
&&(b->ymin<=E2_nn_seed_y-E2_nn_dist)
&&(b->ymax>=E2_nn_seed_y+E2_nn_dist)){
/*112:*/


#if KD_ALLOW_VERBOSE
if(verbose>=500){
printf("     rnn break: seed=(%0.0f,%0.0f) dist=%f\n",
E2_nn_seed_x,E2_nn_seed_y,E2_nn_dist);
printf("     seedbb=(%0.0f,%0.0f,%0.0f,%0.0f) bb=(%0.0f,%0.0f,%0.0f,%0.0f)\n",
E2_nn_seed_x-E2_nn_dist,
E2_nn_seed_y-E2_nn_dist,
E2_nn_seed_x+E2_nn_dist,
E2_nn_seed_y+E2_nn_dist,
b->xmin,
b->ymin,
b->xmax,
b->ymax);
}
#endif


/*:112*/


break;
}
}

/*:98*/


}
}


/*:99*/


}while(0);

/*:92*/


return E2_nn_incumbent;
}

/*:89*//*93:*/


int
E2_nn_bulk_func(int c,int num_wanted,kd_bin_t*buffer)
{return E2_nn_quadrant_bulk(c,num_wanted,buffer,0x1f);
}

int
E2_nn_quadrant_bulk(int c,int num_wanted,kd_bin_t*buffer,const int mask)
{
int num_found;
E2_node_t*node= E2_point_to_bucket[c];
/*91:*/


quadrant_mask= mask;
E2_nn_seed= c;
E2_nn_seed_x= coord[c].x[0];
E2_nn_seed_y= coord[c].x[1];
E2_nn_incumbent= -1;
if(mask&(~1)){
E2_nn_dist= E2_nn_dist_sq= E2_strict_upper_bound;
}else{
E2_nn_dist= E2_nn_dist_sq= 1e-5;
}

/*:91*/


E2_nn_fill_bin= 1;
E2_nn_bin_want_size= num_wanted;
E2_nn_bin_work= buffer;
/*92:*/


/*97:*/


{
int i,hi= node->f.e.hi;
for(i= node->f.e.lo;i<hi;i++){
int pi= perm[i];
if(pi!=E2_nn_seed){
/*80:*/


{
const double
diff_x= E2_nn_seed_x-coord[pi].x[0],
diff_y= E2_nn_seed_y-coord[pi].x[1];
/*113:*/


#if KD_ALLOW_VERBOSE
if(verbose>=1500){
printf(" pi=%d qmask=%d x0=%.0f y0=%.0f x1=%.0f y1=%.0f dx=%.0f dy=%.0f q=%d mask=%d\n",
pi,quadrant_mask,E2_nn_seed_x,E2_nn_seed_y,coord[pi].x[0],coord[pi].x[1],
diff_x,diff_y,E2_quadrant(diff_x,diff_y),E2_quadrant_mask(diff_x,diff_y));
}
#endif

/*:113*/


if(quadrant_mask&E2_quadrant_mask(diff_x,diff_y)){

const double dist_sq= diff_x*diff_x+diff_y*diff_y;
/*110:*/


#if 1 
if(verbose>=2000){
printf("  city %5d (%7.0f,%7.0f) is dist %10.3f\n",
pi,coord[pi].x[0],coord[pi].x[1],sqrt(dist_sq));
}
#endif


/*:110*/


if(dist_sq<E2_nn_dist_sq){
/*111:*/


#if KD_ALLOW_VERBOSE
if(verbose>=1000){
printf("    new champion\n");
}
#endif

/*:111*/


/*83:*/


if(E2_nn_fill_bin){
kd_bin_t*pi_particulars;
if(pq_size(E2_nn_bin)==E2_nn_bin_want_size){
pi_particulars= (kd_bin_t *)pq_delete_min(E2_nn_bin);
}else{
pi_particulars= E2_nn_bin_work+pq_size(E2_nn_bin);
}
pi_particulars->cost_squared= dist_sq;
pi_particulars->city= pi;
pq_insert(E2_nn_bin,pi_particulars);
if(pq_size(E2_nn_bin)==E2_nn_bin_want_size){
E2_nn_dist_sq= ((kd_bin_t*)pq_min(E2_nn_bin))->cost_squared;
E2_nn_dist= sqrt(E2_nn_dist_sq);
}
}else{
E2_nn_dist= sqrt(dist_sq);
E2_nn_dist_sq= dist_sq;
E2_nn_incumbent= pi;
}

/*:83*/


}
}
}

/*:80*/


}
}
}


/*:97*/


do{
/*98:*/


{E2_box_t*b= node->bbox;
if(b
&&(b->xmin<=E2_nn_seed_x-E2_nn_dist)
&&(b->xmax>=E2_nn_seed_x+E2_nn_dist)
&&(b->ymin<=E2_nn_seed_y-E2_nn_dist)
&&(b->ymax>=E2_nn_seed_y+E2_nn_dist)){
/*112:*/


#if KD_ALLOW_VERBOSE
if(verbose>=500){
printf("     rnn break: seed=(%0.0f,%0.0f) dist=%f\n",
E2_nn_seed_x,E2_nn_seed_y,E2_nn_dist);
printf("     seedbb=(%0.0f,%0.0f,%0.0f,%0.0f) bb=(%0.0f,%0.0f,%0.0f,%0.0f)\n",
E2_nn_seed_x-E2_nn_dist,
E2_nn_seed_y-E2_nn_dist,
E2_nn_seed_x+E2_nn_dist,
E2_nn_seed_y+E2_nn_dist,
b->xmin,
b->ymin,
b->xmax,
b->ymax);
}
#endif


/*:112*/


break;
}
}

/*:98*/


/*99:*/



#if !defined(KD_NO_HIDDEN_BIT)
#define recurse_if_not_last_or_hidden(P) ((P)==last||(E2_rnn(P),42))
#else
#define recurse_if_not_last_or_hidden(P) ((P)==last||(P)->hidden||(E2_rnn(P),42))
#endif

{E2_node_t*last;
for(last= node,node= node->parent;node;last= node,node= node->parent){
E2_node_t*l= node->f.i.lo_child,*e= node->f.i.eq_child,*h= node->f.i.hi_child;
if((node->f.i.cutdimen==0&&E2_nn_seed_x<node->f.i.cutvalue)
||(node->f.i.cutdimen==1&&E2_nn_seed_y<node->f.i.cutvalue)){
recurse_if_not_last_or_hidden(l);
recurse_if_not_last_or_hidden(e);
recurse_if_not_last_or_hidden(h);
}else{
recurse_if_not_last_or_hidden(h);
recurse_if_not_last_or_hidden(e);
recurse_if_not_last_or_hidden(l);
}
/*98:*/


{E2_box_t*b= node->bbox;
if(b
&&(b->xmin<=E2_nn_seed_x-E2_nn_dist)
&&(b->xmax>=E2_nn_seed_x+E2_nn_dist)
&&(b->ymin<=E2_nn_seed_y-E2_nn_dist)
&&(b->ymax>=E2_nn_seed_y+E2_nn_dist)){
/*112:*/


#if KD_ALLOW_VERBOSE
if(verbose>=500){
printf("     rnn break: seed=(%0.0f,%0.0f) dist=%f\n",
E2_nn_seed_x,E2_nn_seed_y,E2_nn_dist);
printf("     seedbb=(%0.0f,%0.0f,%0.0f,%0.0f) bb=(%0.0f,%0.0f,%0.0f,%0.0f)\n",
E2_nn_seed_x-E2_nn_dist,
E2_nn_seed_y-E2_nn_dist,
E2_nn_seed_x+E2_nn_dist,
E2_nn_seed_y+E2_nn_dist,
b->xmin,
b->ymin,
b->xmax,
b->ymax);
}
#endif


/*:112*/


break;
}
}

/*:98*/


}
}


/*:99*/


}while(0);

/*:92*/


num_found= pq_size(E2_nn_bin);
pq_make_empty(E2_nn_bin);
return num_found;
}

/*:93*//*102:*/


void
E2_frnn(int c,double rad,void(*proc)(int j)){
errorif(1,"Fixed radius nearest neighbour not implemented");
}

void E2_set_radius(int i,double r){
errorif(1,"Ball searching not implemented");
}

void E2_ball_search(int i,void(*proc)(int j)){
errorif(1,"Ball searching not implemented");
}


/*:102*//*105:*/


extern tsp_instance_t*tsp_instance;

static void
E2_postscript_show_helper(FILE*ps_out,int level,E2_node_t*node,
double xmin,double xmax,double ymin,double ymax){
if(!node->is_bucket){
double cv= node->f.i.cutvalue;
fprintf(ps_out,"%%cutdimen == %d\n",node->f.i.cutdimen);
switch(node->f.i.cutdimen){
case 0:
fprintf(ps_out,"newpath %f x %f y moveto %f x %f y lineto stroke\n",
cv,ymin,cv,ymax);
/*107:*/


verb(1000){int i;for(i= 0;i<level;i++)printf(" ");}

/*:107*/

verb(1000)printf("= in x = %f\n",cv);
E2_postscript_show_helper(ps_out,level+1,node->f.i.eq_child,cv,cv,ymin,ymax);
/*107:*/


verb(1000){int i;for(i= 0;i<level;i++)printf(" ");}

/*:107*/

verb(1000)printf("< in x < %f\n",cv);
E2_postscript_show_helper(ps_out,level+1,node->f.i.lo_child,xmin,cv,ymin,ymax);
/*107:*/


verb(1000){int i;for(i= 0;i<level;i++)printf(" ");}

/*:107*/

verb(1000)printf("> in x > %f\n",cv);
E2_postscript_show_helper(ps_out,level+1,node->f.i.hi_child,cv,xmax,ymin,ymax);
break;
case 1:
fprintf(ps_out,"newpath %f x %f y moveto %f x %f y lineto stroke\n",
xmin,cv,xmax,cv);
/*107:*/


verb(1000){int i;for(i= 0;i<level;i++)printf(" ");}

/*:107*/

verb(1000)printf("= in y = %f\n",cv);
E2_postscript_show_helper(ps_out,level+1,node->f.i.eq_child,xmin,xmax,cv,cv);
/*107:*/


verb(1000){int i;for(i= 0;i<level;i++)printf(" ");}

/*:107*/

verb(1000)printf("< in y < %f\n",cv);
E2_postscript_show_helper(ps_out,level+1,node->f.i.lo_child,xmin,xmax,ymin,cv);
/*107:*/


verb(1000){int i;for(i= 0;i<level;i++)printf(" ");}

/*:107*/

verb(1000)printf("> in y > %f\n",cv);
E2_postscript_show_helper(ps_out,level+1,node->f.i.hi_child,xmin,xmax,cv,ymax);
break;
}
}else{
int i;
/*107:*/


verb(1000){int i;for(i= 0;i<level;i++)printf(" ");}

/*:107*/


for(i= node->f.e.lo;i<node->f.e.hi_all;i++){
verb(1000)printf("%d ",perm[i]);
}
verb(1000)printf("\n");
}
}

void
E2_postscript_show(FILE*ps_out){
#if defined(KD_SHOW_KDTREE)
fprintf(ps_out,"gsave 0 setlinewidth\n");
E2_postscript_show_helper(ps_out,0,E2_root,
tsp_instance->xmin,tsp_instance->xmax,tsp_instance->ymin,tsp_instance->ymax);
fprintf(ps_out,"grestore\n");
fflush(ps_out);
fprintf(stderr,"Printed stuff to ps_out\n");
#else
return;
#endif
}

/*:105*/


const char*kdtree_rcs_id= "$Id: kdtree.w,v 1.165 1998/10/10 19:25:16 neto Exp neto $";

/*:1*/
