#! /usr/bin/perl
# Generated automatically from genhk.pl.in by configure.

use strict;

my $mode = 1;

sub doit {
	$_ = shift;
	if ( $mode ) { system $_;}
	else { print $_,"\n"};
}

my %parms = (
41,1000,42,1000,43,1000,44,1000,45,1000,
51,3162,52,3162,53,3162,55,3162,55,3162
);

my $seed;
foreach $seed (keys %parms) {
		my $n = $parms{$seed};
		my $infile = "data/dsjr.$seed.$n.tsp";
		my $outfile = $infile.".hk";
		if ( ! -r $outfile ) {
			doit "src/hk -v 80 $infile --held-karp >$outfile";
		}
		#sleep 2;
}

exit 0;
my $d;
my $s;

# The following are for getting HK values for Bentley instances.
foreach $d (0..10) {
	foreach $s (41..45) {
		my $seed=$s*20 + $d;
		my $n=1000;
		my $outfile = "data/ben.$d.$seed.$n.hk";
		if ( ! -r $outfile ) {
			doit "src/hk data/ben.$d.$seed.$n.tsp --held-karp >$outfile";
		}
		sleep 5;
	}
}
