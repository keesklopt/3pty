#! /usr/bin/perl -w
# Generated automatically from results.pl.in by configure.
#
# results.pl
# Summarize statistics from many experiments.

# usage: 
#   cat (list of filenames) | results.pl minfile 
# Content of minfile is a collection of pairs of instance names followed by
# the lowest observed length. (or some other bound, e.g. Held-Karp lower bound)
#
# Generate TeX lines with data:
#    Instance & 
#    LK time & 
#    LK+dec time & 
#    LK/LK+dec & 
#    LK tour (length/lower bound) & 
#    LK+dec (length/lower bound) & 
#    LK/LK+dec \\
#

# Key is instance name.
# Value is an array of: 
#	number of LK samples, 
#	sum LK time,
#	sum LK tour length
#	number of LK+dec samples, 
#	sum LK+dec time,
#	sum LK+dec tour length
%instance = ();


# Find the minimum value observed for all the instances, drawn from the
# file named as the first argument.
$minfile = shift(@ARGV);
open(LEAST,"<$minfile") || die "Can't open $minfile for reading.";
%least = ();
while (<LEAST>) {
	m/^([^\s]+)\s+([^\s]+)/;
	$least{$1}=$2+0;
}

@file_names=();
while(<>) {
	chop;
	push(@file_names,$_);
}

# Now parse the values found in individual experiments listed on stdin.
my $this_instance = "";
my $is_decluster;
my $seconds;

my $file;
foreach $file (@file_names) {
	print "\%\%\% file is $file\n";
	open(IN,"zcat $file |") || die "Can't uncompress input files";

	while(<IN>) {
		chop;
		if (m/^LK [0-9]+\.[0-9]+\.[0-9]+([^\s]*)/) {
			$is_decluster = $1 eq "deg";
		} elsif (m/^\s+(\d+\.\d+) user seconds and/ ) {
			$seconds = $1+0;  # Last one is the total run time.
		} elsif (m/^Instance name:\s+([^\s]+)/) {
			$this_instance=$1;
	#print "instance $1\n";
		} elsif (m/^Length:\s+([^\s]+)/) {
			my $aref = $instance{$this_instance};
			my $this_length=$1+0;
			if ( !defined($aref) ) {
				$aref = $instance{$this_instance} = [ 0, 0, 0, 0, 0, 0 ];
			}
			if ( $is_decluster ) {
				$$aref[3]++;
				$$aref[4]+=$seconds;
				$$aref[5]+=$this_length;
			} else {
				$$aref[0]++;
				$$aref[1]+=$seconds;
				$$aref[2]+=$this_length;
			}
	print "\t\% $this_instance ".join(' ',@{$instance{$this_instance}})."\n";
		}
	}
}

print STDERR "\%\%\%\%\% Now ready to output results.\n";
print "\%\%\%\%\% Now ready to output results.\n";

my $i;
LOOP: for $i ( sort { (nv($a)+0)<=> (nv($b)+0) } keys %instance ) {
print " \%". join(' ',@{$instance{$i}})."\n";
    my ($nsamp, $time, $len, $dnsamp, $dtime, $dlen ) = @{$instance{$i}};
    if ( $nsamp != $dnsamp ) { 
        print  "\% following mismatch: nsamp $nsamp != dnsamp $dnsamp\n";
        next LOOP; 
    }
    if ( !defined($least{$i}) ) { 
        print  "\% No lower bound for $i\n";
        next LOOP; 
    }
    if ( $nsamp == 0 ||  $dnsamp == 0 ) {
        print "\% nsamp $nsamp  dnsamp $dnsamp\n";
        next LOOP;
    }

printf "%s \&\t%0.1f \&\t%0.1f \&\t%0.2f \&\t%0.2f\\%% \&\t%0.2f\\%% \&\t%0.2f\\ \\\\\n",
        $i, $time/$nsamp, $dtime/$dnsamp, ($time/$nsamp)/($dtime/$dnsamp),
            pct($len/$nsamp/$least{$i}), pct($dlen/$dnsamp/$least{$i}),
            -pct($len/$nsamp/$least{$i})+ pct($dlen/$dnsamp/$least{$i});
}

exit 0;

sub pct {
	my($a)=shift;
	return ($a-1)*100;
}

sub nv { 
	my ($a)=shift ;
	$a =~ m/.*[^\d]+(\d+)/; 
#print "a $a $1\n";
	return $1+0;
}
