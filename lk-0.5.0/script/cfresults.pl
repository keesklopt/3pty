#! /usr/bin/perl
# Generated automatically from cfresults.pl.in by configure.

# Extract final length and time for preprocessing+LK.
# All the input files are presented at once, 
# e.g. out.grid1024.0.deg, out.grid1024.0.no_d, out.grid1024.1.deg...
# We tell from the version number line whether cluster compensation was used.

# $Log: cfresults.pl.in,v $
# Revision 1.2  1998/07/31 17:03:49  neto
# Compare results from multiple LK runs on a single instance.
#

$is_deg = 0;
$float_expr= "(-?\\d+\\.?\[0-9\]*|\\.\[0-9\]+)"; # See Programming Perl

$file_num = 0;

while(<>) {
	if ( m/^LK \d+\.\d+\.\d+([^\s]*)/ ) {
		# Determine whether cluster compensation was used.
		print STDERR;
		$is_deg = ($1 eq 'deg');
		$have_milestones = 0;
	} elsif ( m/^LK phase ended with incumbent_len == \(0+\*2\^32\+$float_expr\) after $float_expr sec for LK and $float_expr sec for ds\+LK/o ) {
		($len,$lk_sec,$ds_sec)=($1,$2,$3);
		print STDERR;
		print STDERR "$len $lk_sec $ds_sec\n";
		if ($is_deg) {
			($deg_len,$deg_lk_sec,$deg_ds_sec) = ($len,$lk_sec,$ds_sec);
		};
	} elsif ( m/^Final milestone: $float_expr\%/o ) {
		print STDERR;
		$have_milestones = 1;
		my $pct = $1;  # percentage from the float_expr.
		&output($pct);
	} elsif ( m/^Length: $float_expr/o && !$have_milestones ) {
		print STDERR;
		my $len = $1;	# length is picked up inside float_expr.
		&output($len);
	}
}

exit 0;

sub output {
	$value = shift;
	if ($is_deg) {
		$deg_value = $value;   # Buffer it up for next time.
	} else {
		$filename = "cf.$file_num";
		open(OUT,">$filename.deg") || die "Can't open $file_num.deg for writing";
		open(OUTNIB,">$filename.no_d") || die "Can't open $file_num.no_d for writing";
		print OUT "$deg_ds_sec $deg_value\n";
		print OUTNIB "$deg_ds_sec $deg_value\n$ds_sec $value\n";
		close(OUT);
		close(OUTNIB);

		$file_num++;
	}
}

