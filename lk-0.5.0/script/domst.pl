#! /usr/bin/perl -w
# Generated automatically from domst.pl.in by configure.
# vi: set ts=2 sw=2:
# domst.pl
# Run tspgen multiple times on the MSTEL of a given instance.
#
# This file is in the public domain, and comes with no warranty.
# David Neto, November 20, 1997.
#
# -j 0.5 .. 1 is interesting when -p is high (4 and above).
# For some reason, tspgen doesn't pay attention to its seed!


$instance = "one500";
$lkprog = "../src/lk";
$tspgen = "../src/tspgen";

@pl = ("000.0", "000.5", "001.0", "004.0", "100.0");# packing factors. 100 is "tightest"
@jl = ("0.0", "0.5", "0.7", "0.8", "1.0", "-1");  # join biases -1 is random, 0 is shortest, 1 is longest
@sl = ("42","50","98");		# seeds
# For some reason, tspgen doesn't pay attention to its seed!
@sl = ("142");

system("$lkprog -M -S dsort ../data/$instance.tsp >mst");
for $s (@sl) {
	for $j (@jl) {
		for $p (@pl) {
			$sjp = "$s" . "_" . "$j" . "_" . "$p";
			system("$tspgen -s $s -j $j -p $p <mst >mst.$instance.$sjp.tsp");
			system("$lkprog -M --no-round -S dsort <mst.$instance.$sjp.tsp -p mst.$instance.$sjp.ps >/dev/null");
		}
	}
}
