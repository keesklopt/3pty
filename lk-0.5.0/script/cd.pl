#! /usr/bin/perl
# Generated automatically from cd.pl.in by configure.

# Extract t1,t2 cost and decluster_d from LK output.
$float_expr= "(\\d+\\.?\\d*|\\.\\d+)"; # See Programming Perl
open(IN, "zcat @ARGV |");  # Uncompress input files.
open(COST,">cost.gpl") || die "Can't open cost.gpl";
open(DECD,">decd.gpl") || die "Can't open decd.gpl";
open(PROBE,">prd.gpl") || die "Can't open prd.gpl";

$i=0;
while(<IN>) {
	if ( m/\(t1,t2\)= \(\d+,\d+\) cost=$float_expr, d=$float_expr p=(\d+)/o ) {
		print COST "$i $1\n";
		print DECD "$i $2\n";
		print PROBE "$i $3\n";
		$i++;
	}
}
