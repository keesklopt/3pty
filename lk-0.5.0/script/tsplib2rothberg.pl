#! /usr/bin/perl
# Generated automatically from tsplib2rothberg.pl.in by configure.
# tsplib2rothberg.pl
# Convert a TSPLIB format file into one digestible by Rothberg's weighted
# matching code.
# See ftp://dimacs.rutgers.edu/pub/netflow/benchmarks/c

use TSP;

$tsp = new TSP;
$tsp->read(\*STDIN);
$tsp->as_Rothberg_on(\*STDOUT);
