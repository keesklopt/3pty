#! /usr/bin/perl -w
# Generated automatically from least.pl.in by configure.
#
# leastlength.pl
# Find the least length for a given instance.

# usage: 
# 	leastlength.pl file1 file2 ...
#
# output is name of instance followed by the least length value seen on the
# inputs for that instance.
#

# Key is instance name.  Value is the least length.

my %instance = ();

my $files = join(' ',@ARGV);
my $this_instance = "";

open(IN,"zcat $files|");

while(<IN>) {
	chop;
	if (m/^Instance name:\s+([^\s]+)/) {
		$this_instance=$1;
#print "instance $1\n";
	} elsif (m/^Length:\s+([^\s]+)/) {
		my $least = $instance{$this_instance};
		$this_length=$1+0;
#print "length $1 least $least\n";
		if ( !defined($least) || $least >$this_length ) {
			$instance{$this_instance} =  $this_length;
		}
	}
}

my $i;
for $i ( keys(%instance) ) {
	print "$i $instance{$i}\n";
}
