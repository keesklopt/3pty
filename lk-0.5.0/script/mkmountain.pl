#!/usr/bin/perl
# Generated automatically from mkmountain.pl.in by configure.
# mkmountain.pl
#
# Make mountain graphs from log files.
# Usage:
#	mkmountain.pl <out.tsp......deg.gz> <out.tsp......no_d.gz>
# I recommend using one at a time.

my $i;
foreach $file ( @ARGV ) {
	open IN, "gzip -d -c $file|" || die "Couldn't open $file for reading";
	open MOUT, ">$file.move" || die "Couldn't open $file.move for writing";
	open POUT, ">$file.probe" || die "Couldn't open $file.probe for writing";
	while (<IN>) {
		if ( m/^m (\d+) citydeep (\d+)/ && $1 > 0 ) {
			print MOUT "$1 $2\n";
		} elsif ( m/^p (\d+) citydeep (\d+)/ ) {
			print POUT "$1 $2\n";
		}
	}
	close IN;
}
