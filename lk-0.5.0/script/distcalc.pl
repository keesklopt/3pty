#! /usr/bin/perl
# Generated automatically from distcalc.pl.in by configure.
# distcalc.pl
# Read a TSPLIB instance on the input, followed by pairs of vertices u,v.
# For each pair $u,v$, output the distance between u and v in that
# instance.
# The TSPLIB instance must end with EOF, so we know where to start looking
# at pairs of vertices.
# The vertices are 1-based, just like in TSPLIB instances.


use TSP;

$|=1;
$tsp = new TSP;
$tsp->read(\*STDIN);
#$tsp->write(\*STDOUT);
while (<>) {
	if (m/(\d+)\s+(\d+)/) {
		$u = $1;
		$v = $2;
		print "u is $u v is $v\n";
		print "Distance between $u and $v is ",$tsp->cost($u,$v),"\n";
	}
}

