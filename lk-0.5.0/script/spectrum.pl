#! /usr/bin/perl -w
# Generated automatically from spectrum.pl.in by configure.
# Generate a spectrum of MSTEL genererated instances.

$n = 10;  # number of cities

#@packing = ("000.0", "001.0", "100.0");
@packing = (
"000.00", "000.10", "000.20", "000.30", 
"000.05", "000.15", "000.25", "000.35", 
"000.40", "000.50", "000.60", "000.70", 
"000.45", "000.55", "000.65", "000.75", 
"000.40", "000.50", "000.60", "000.70", 
"000.80", "000.90", 
"000.85", "000.95", 
"001.00");
#"001.0", "002.0", "004.0", "008.0", "016.0", "200.0");


open(OUT,">foo.mst");
print OUT "$n\n";
for $j (1..$n) {$l=10*$j;print OUT "$l\n";}
close(OUT);

for $i (@packing) {
	open(IN,"<foo.mst");
	open(OUT,"| ./tspgen -p $i | ./lk.deg --no-round -M -c nn 1 -v 50 -p mst.$i.ps >/dev/null")
		|| die "Couldn't do $i";
	while(<IN>) {print OUT};
	close(IN);
	close(OUT);
}
