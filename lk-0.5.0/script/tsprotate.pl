#! /usr/bin/perl
# Generated automatically from tsprotate.pl.in by configure.
# Rotate the cities in a Euclidean TSPLIB instance.
# Doesn't handle plotting coordinates, only definition coordinates.

$progname="tsprotate.pl";
$pkgname = "lk";
$pkgversion = "0.1.10";
$version_banner="$progname ($pkgname) $pkgversion";

$pi = 3.1415926535897932384626433832795028841972; # See Knuth's TAOCP, volume 1.
$radians_default = 42*$pi/180;
$args = "-d 42";

$usage = <<EOT;
$version_banner
Rotate the cities in a Euclidean TSPLIB instance.
$progname [options] [file file ...]
  where options can be:
    -h     --help          : Print this message
    -d n   --degrees n     : Number of degrees to rotate clockwise
                             0 degrees forces no change
    -r n   --radians n     : Number of radians to rotate clockwise
                             0 radians forces no change
           --version       : Print a version info, then exit
  Options are processed left to right; the last of -d or -r takes effect.
  If no input files are given, input is taken from standard input.
EOT

# Set the random number radians
$radians=$radians_default;

# Parse the command line options
while ($#ARGV >= 0 && $ARGV[0] =~ m/^-/) {
    $option = $_ = shift(@ARGV);
    if (m/^-h$/ || m/^--help$/)    { print $usage; exit; }
    if (m/^-d$/ || m/^--degrees$/) { # mandatory degrees parameter
		if ( $#ARGV >= 0 ) {
			$a = shift(@ARGV); $args = "-d $a";
			$radians=$a*$pi/180; next;
		}
		else { die "$progname: option $option requires a degrees argument\n";}
	}
    if (m/^-r$/ || m/^--raidans$/) { # mandatory radians parameter
		if ( $#ARGV >= 0 ) {
			$a = shift(@ARGV); $args = "-r $a";
			$radians=$a; next;
		}
		else { die "$progname: option $option requires a radians argument\n";}
	}
    if (m/^--version$/) { print "$version_banner\n"; exit; }
    die "$progname: Unknown option $option\n$usage";
}


############################################

# print STDERR "radians is $radians\n";

if (($radians + 0) == 0) { while(<>) {print;} exit 0;} # Identity transform.

$ct = cos($radians);
$st = sin($radians);
$float_expr= "(-?\\d+\\.?\\d*|\\.\\d+)"; # See Programming Perl


$in_coords=0;
while ($line=<>) {
	$_ = $line;
	if (m/^COMMENT\s*:(.*)/) {
		local($comment)=$1;
		print "COMMENT: $comment | tsprotate.pl $args\n";
	} elsif (m/NODE_COORD_SECTION/) {
		print;
		$in_coords=1;
	} elsif (m/EOF/) {
		print;
	} elsif ( $in_coords && $line =~ m/^(\s*\d+\s+)$float_expr\s+$float_expr/) {
		local($i,$x,$y,$tx,$ty) = ($1,$2,$3);
		$tx = $ct*$x - $st*$y;
		$ty = $st*$x + $ct*$y;
		printf "%s%.30f %.30f\n", $i,$tx, $ty;
	} else {
		# All other output piped straight through
		print;
	}
}
