#! /usr/bin/perl -w
# Generated automatically from doshake.pl.in by configure.
# vi: set ts=2 sw=2:
# doshake.pl
# Run shake multiple times on a given instance
#
# This file is in the public domain, and comes with no warranty.
# David Neto, December 17, 1997.
#
# Example parameters: dsj1000 lin105
# For argument dsj1000, dsj1000.tsp must exist in the current directory.
# Also, for postscript output, prolog.ps must exist in the current directory.

my($shakeprog) = "../src/shake";
my($psprog) = "../src/tspps";
my($lkprog) = "../src/lk";

my(@bl) = (1,2,4,10,20);  # Number of branches to rotate.
my(@dl) = (0.5,1,5);  # Number of branches to rotate.
my(@sl) = ("48","50","98");		# seeds

# For testing, search a small space.
#my(@bl) = (4);  # Number of branches to rotate.
#my(@dl) = (5);  # Number of branches to rotate.
#my(@sl) = ("50","98");		# seeds

for $instance (@ARGV) {

my($s,$b,$d,$sbd);

for $s (@sl) {
	for $b (@bl) {
		for $d (@dl) {
			$sbd = "$s"."_"."$b"."_"."$d";
system("$shakeprog -s $s -b $b -d $d <$instance.tsp >$instance.shake.$sbd.tsp");
system("$lkprog -M --no-round $instance.shake.$sbd.tsp -p $instance.shake.$sbd.mst.ps >/dev/null");
		}
	}
}
}
