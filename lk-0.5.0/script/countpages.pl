#! /usr/bin/perl
#Generated automatically from countpages.pl.in by configure.
# countpages.pl
# Count the number of TeX pages in a set of documents.
# Usage: countpages.pl *.log
#
$total=0;
while($line=<>) {
	$_ = $line;
	if (m/\((\d+) pages/) {
		$total += $1;
		print $line;
	}
}
print "$total total pages\n";
