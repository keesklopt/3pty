
use strict;
my $blossom = "/home/neto/concorde/970827/MATCH/blossom4";

my $mode = 1; # 0 is write files; 1 is generate optimals.

sub gen_file {
	my ($n, @seeds)=@_;
	print "n is $n; seeds are ", join(' ',@seeds),"\n";
	if ( $mode == 0 ) {
		foreach (@seeds) {
			my $seed = $_;
			my $file = "dsjr.$seed.$n.tsp";
			system "echo Generating $file >>gendsjr.log";
			my $text=<<END_OF_TEMPLATE;
NAME: dsjr.$seed.$n
TYPE: TSP
COMMENT: Random edge lengths ($n nodes)
DIMENSION: $n
EDGE_WEIGHT_TYPE: DSJ_RANDOM
SEED: $seed
SCALE: 1000000
EOF
END_OF_TEMPLATE
			open OUT, ">$file" || die "Can't open $file for writing";
			print OUT $text;
			close OUT;
		}
	} else {
		foreach (@seeds) {
			my $seed = $_;
			my $file = "dsjr.$seed.$n.tsp.wpm.gz";
			if ( ! -e $file ) {
				system "echo Running blossom code for $file >>gendsjr.log";
				system "$blossom -8 -k $n -s $seed 2>&1 | gzip -c >$file";
			}
		}
	}
}

foreach (0..1) {
	$mode=$_;
	gen_file(1000,41..45);
	gen_file(3162,51..55);
#	gen_file(10000,61..65);
#	gen_file(31622,71..75);
#	gen_file(100000,81..85);
}

exit 0;
