#! /usr/bin/perl
# Generated automatically from gen.pl.in by configure.

use strict;

my $mode = 1;

sub doit {
	$_ = shift;
	if ( $mode ) { system $_;}
	else { print $_,"\n"};
}

sub make_hk {
print "About to create hkconfig.h\n";
	open IN, "<src/lkconfig.h";
	open OUT, ">src/hkconfig.h";
	while (<IN>) {
		if (m/^#undef LENGTH_DOUBLE/) {
			print OUT "#define LENGTH_DOUBLE\n";
		} elsif (m/^#define LENGTH_INT/) {
			print OUT "#undef LENGTH_INT\n";
		} else {
			print OUT $_;
#			print $_;
		}
	}
	close IN;
	close OUT;
print "Done modifying hkconfig.h\n";
	doit "(cd src; mv lkconfig.h lkconfig.h.orig; cp hkconfig.h lkconfig.h; make clean; make lk; mv lk hk;"
			."mv lkconfig.h.orig lkconfig.h)";
print "Done making hk\n";
}

doit "make";
doit "(cd script; make myall)";
doit "(cd src; ../script/lkdoitcmp)";

make_hk;
exit 0;

my $d;
my $s;
my $n=1000;
foreach $d (0..10) {
	foreach $s (41..45) {
		my $seed=$s*20 + $d;
		doit "/usr/bin/perl -I script script/tspbgen.pl -d $d -n $n -s $seed >data/ben.$d.$seed.$n.tsp";
	}
}
exit 0;
