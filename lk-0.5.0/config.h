/* config.h.  Generated automatically by configure.  */
/* config.h.in.  Generated automatically from configure.in by autoheader.  */

/* Define to empty if the keyword does not work.  */
/* #undef const */

/* Define if you don't have vprintf but do have _doprnt.  */
/* #undef HAVE_DOPRNT */

//#include <stdafx.h>

#ifdef _WIN32
#define strdup _strdup
#endif

/* Define if the `long double' type works.  */
#define HAVE_LONG_DOUBLE 1

/* Define if you have the vprintf function.  */
#define HAVE_VPRINTF 1

/* Define as __inline if that's what the C compiler calls it.  */
#define inline __inline__

/* Define to `unsigned' if <sys/types.h> doesn't define.  */
/* #undef size_t */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if your processor stores words with the most significant
   byte first (like Motorola and SPARC, unlike Intel and VAX).  */
/* #undef WORDS_BIGENDIAN */

/* We want to know our own version number */
#define VERSION_STRING "0.5.0"
#define PACKAGE "lk"

/* We may need to supply function prototypes and structures. */
#define OS_HAS_BROKEN_HEADERS 0

/* The following are used only on SunOS/Solaris. */
#define OS_IS_SUNOS 0
#define OS_VERSION_MAJOR 3
#define OS_VERSION_MINOR 4

/* Sometimes libraries define optarg, etc. but their headers don't declare them.  Solaris can be like that.  */
#define LIBRARY_DEFINES_OPTARG_ETC 1
#define HEADERS_DECLARE_OPTARG_ETC 1
#define COMPILER_SUPPORTS_INLINE 1

/* The number of bytes in a int.  */
#define SIZEOF_INT 4

/* The number of bytes in a long.  */
#define SIZEOF_LONG 8

/* The number of bytes in a long long.  */
#define SIZEOF_LONG_LONG 8

/* The number of bytes in a short.  */
#define SIZEOF_SHORT 2

/* The number of bytes in a unsigned int.  */
#define SIZEOF_UNSIGNED_INT 4

/* Define if you have the ctime function.  */
#ifdef __linux
#define HAVE_CTIME 1
#endif

/* Define if you have the gethostname function.  */
#ifdef __linux__
#define HAVE_GETHOSTNAME 1
#endif

/* Define if you have the getopt function.  */
#define HAVE_GETOPT 1

/* Define if you have the getpagesize function.  */
#define HAVE_GETPAGESIZE 1

/* Define if you have the getrusage function.  */
#ifdef __linux__
#define HAVE_GETRUSAGE 1
#endif

/* Define if you have the nrand48 function.  */
#ifdef __linux__
#define HAVE_NRAND48 1
#endif

/* Define if you have the strdup function.  */
#define HAVE_STRDUP 1

/* Define if you have the time function.  */
#ifdef __linux__
#define HAVE_TIME 1
#endif

/* Define if you have the <limits.h> header file.  */
#define HAVE_LIMITS_H 1

/* Define if you have the <sys/time.h> header file.  */
#define HAVE_SYS_TIME_H 1

/* Define if you have the <time.h> header file.  */
#define HAVE_TIME_H 1

#ifndef _WIN32
#define HAVE_UNISTD_H 1
#endif

/* Define if you have the m library (-lm).  */
#define HAVE_LIBM 1

/* Define if you have the ucb library (-lucb).  */
/* #undef HAVE_LIBUCB */

/* Name of package */
#define PACKAGE "lk"

/* Version number of package */
#define VERSION "0.5.0"

